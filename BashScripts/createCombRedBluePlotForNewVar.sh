#! /bin/bash

# SampleList=("ttbar" "SingleTop" "diboson" "ttV" "wejets" "wmujets" "wtaujets" "zjets")
SampleList=("ttbar")

variable="leptop_closestAddjet_dR"

for Sample in ${SampleList[@]}; do

    echo ''
    echo Combining ${Sample} plots
    echo ''

    rm -r ./nominalFit/mWFit/Systematics/AppendedPDF_${variable}_${Sample}/Combined
    rm -r ./nominalFit/mWFit/Systematics/AppendedPDF_${variable}_${Sample}
    mkdir -p ./nominalFit/mWFit/Systematics/AppendedPDF_${variable}_${Sample}
    mkdir -p ./nominalFit/mWFit/Systematics/AppendedPDF_${variable}_${Sample}/Combined

    sysdir="/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/nominalFit/mWFit/Systematics/"

    for i in $(ls ${sysdir}); do
        if [[ $i == "Diboson_normalisation_uncertainty" ]]; then
            continue
        fi
        if [[ $i == "ttV_normalisation_uncertainty" ]]; then
            continue
        fi
        if [[ $i == "SingleTop_weight_normalisation_uncertainty" ]]; then
            continue
        fi
        if [[ $i == "Zjets_normalisation_uncertainty" ]]; then
            continue
        fi
        if [[ $i == "SingleTopH7" ]]; then
            continue
        fi
        if [[ $i == "SingleTopDSDR" ]]; then
            continue
        fi
        if [[ $i == "NNLORW" ]]; then
            continue
        fi
        if [[ $i != "AppendedPDF"* ]]; then
            cp $sysdir/$i/${variable}_${Sample}_$i.pdf $sysdir/AppendedPDF_${variable}_${Sample}/${variable}_${Sample}_$i.pdf
        fi
    done

    cd /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/nominalFit/mWFit/Systematics/AppendedPDF_${variable}_${Sample}
    convert_command="pdfunite "
    for j in $(ls); do
        if [[  -f $j ]]; then
            convert_command+=$j" "
        fi
    done
    convert_command+=" Combined/Combined.pdf"
    echo $convert_command
    eval $convert_command
    
    cd /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/

done

cd /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/nominalFit/mWFit/Systematics
convert_command="pdfunite "
for i in $(ls ${sysdir}); do
    if [[ $i == "AppendedPDF_${variable}_"* ]]; then
            convert_command+=$sysdir"/"$i"/Combined/Combined.pdf "
    fi
done
convert_command+=" AllPlots_${variable}.pdf"
cd /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/nominalFit/mWFit/
echo $convert_command
eval $convert_command

echo ''
echo 'Done'
echo ''
