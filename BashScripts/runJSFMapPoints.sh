#!/bin/bash

AFII_filelistM="filelist_V34_PFlow_ttallpp8_"

AFII_filelists=("filelist_V34_PFlow_ttallpp8_1516" "filelist_V34_PFlow_ttallpp8_17" "filelist_V34_PFlow_ttallpp8_18") 

#JSFs=("0_97" "0_98" "0_99" "1_00" "1_01" "1_02" "1_03")
JSFs=("1_00") 

declare -A dummy_JSFs

#dummy_JSFs=( ["0_97"]="JSFmap_Dummy_Mass0-97.txt" ["0_98"]="JSFmap_Dummy_Mass0-98.txt" ["0_99"]="JSFmap_Dummy_Mass0-99.txt" ["1_00"]="JSFmap_Dummy.txt" ["1_01"]="JSFmap_Dummy_Mass1-01.txt" ["1_02"]="JSFmap_Dummy_Mass1-02.txt" ["1_03"]="JSFmap_Dummy_Mass1-03.txt" )
dummy_JSFs=( ["1_00"]="JSFmap_Dummy.txt" )

configfile="../GlasgowAnaCore/share/JJBoostedttbar_ExtraJets.txt"
tag1="JSF_"
tag2="_FullSim"
#tag2="_FastSim"

output="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_24_ttbar_SystematicsWithoutNewFlavourUncertainty_FullSim/" 

echo "Welcome to the autorunner pre-flight Checklist:"
echo "Are you on the glasgow HEX machine?"
read -p 'Y/N: ' YNhex
if [[ ${YNhex} == "Y" ]] || [[ ${YNhex} == "y" ]]; then
    echo "Good,"
else
    echo "You should go do that"
    exit -1
fi

echo "Have you setup glasgow-ana and emi correctly to run gana on condor (setuphex)?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
else
    echo "You should go do that"
    exit -1
fi

echo "Have you correctly tarred the current environment (python ../GlasgowAnaScripts/scripts/buildTar.py)?"
read -p 'Y/N: ' YNtar
if [[ ${YNtar} == "Y" ]] || [[ ${YNtar} == "y" ]]; then
    echo "Good"
else
    echo "You should go do that"
    exit -1
fi

echo "Using config file: ${configfile}"
echo "Is this right?"
read -p 'Y/N: ' YNconfig
if [[ ${YNconfig} == "Y" ]] || [[ ${YNconfig} == "y" ]]; then
    echo "Good"
else
    echo "You should go change that"
    exit -1
fi

#echo "Using files from: ${filelists[@]}"
echo "And AFII files from: ${AFII_filelists[@]}"
echo "Are these right?"
read -p 'Y/N: ' YNfiles
if [[ ${YNfiles} == "Y" ]] || [[ ${YNfiles} == "y" ]]; then
    echo "Good"
else
    echo "You should go change that"
    exit -1
fi

echo "Using JSF values: ${JSFs[@]}"
echo "Are these right?"
read -p 'Y/N: ' YNJSF
if [[ ${YNJSF} == "Y" ]] || [[ ${YNJSF} == "y" ]]; then
    echo "Good"
else
    echo "You should go change them"
    exit -1
fi

echo "Using tag string: ${tag1}<JSF>${tag2} to name output and scripts" 
echo "Saving output to: ${output}<tag>" 
echo "Do these seem sensible"
read -p 'Y/N: ' YNtag
if [[ ${YNtag} == "Y" ]] || [[ ${YNtag} == "y" ]]; then
    echo "Good"
else
    echo "You should go change that"
    exit -1
fi

echo "Automating..."
for JSF in ${JSFs[@]}; do
    tag=${tag1}${JSF}${tag2}
    dummy=${dummy_JSFs[${JSF}]}
    echo "Setting JSF value from: ${dummy}"
    sed -i "s|share/JSFmap.*\.txt ./JSFmap.txt|share/${dummy} ./JSFmap.txt|g" ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py
    
    # JONATHAN'S ORIGINAL
    #echo "Generating GEANT4 scripts:"
    #python ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py ${filelistM} ${configfile} ${output}${tag} ttbar 0 0 condor 'group_disk' ${tag}
    #echo "Generating AFII scripts:"
    #python ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py ${AFII_filelistM} ${configfile} $#{output}${tag} ttbar 0 1 condor 'group_disk' ${tag}
    #echo "submitting scripts:"
    #for flist in ${filelists[@]}; do
	#. submitstd_${flist}_${tag}.sh
    #done
    #for AFII_flist in ${AFII_filelists[@]}; do
    #	. submitsyst_${AFII_flist}_${tag}.sh
    #done

    # Fast sim ttpp8AF2 JSF varied samples - need to make sure the JSF map files say sample name instead of nominal OR comment out line 54 in config.
    # echo "Generating submission scripts for JSF varied jobs:"
    # python ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py ${AFII_filelistM} ${configfile} ${output}${tag} ttbar 0 1 condor 'group_disk' ${tag}
    # echo "submitting scripts for JSF varied jobs:"
    # for AFII_flist in ${AFII_filelists[@]}; do
    # 	. submitsyst_${AFII_flist}_${tag}.sh
    # done

    # Systematics (within ttallpp8), changed the first 0 from -1 for all systematics (here we just get nominal with 0)
    echo "Generating systematics submission scripts (systematics in ttallpp8):"
    python ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py ${AFII_filelistM} ${configfile} ${output}${tag} ttbar -1 0 condor 'group_disk' ${tag}
    echo "submitting scripts:"
    for AFII_flist in ${AFII_filelists[@]}; do
    	. submitstd_${AFII_flist}_${tag}.sh
    done

    # Modelling/generator uncertainties from alternative samples
    # echo "Generating submission scripts for alternative samples:"
    # python ../GlasgowAnaScripts/scripts/GenerateSubmissionScripts.py ${AFII_filelistM} ${configfile} ${output}${tag} ttbar 0 1 condor 'group_disk' ${tag}
    # echo "submitting scripts:"
    # for AFII_flist in ${AFII_filelists[@]}; do
    # 	. submitsyst_${AFII_flist}_${tag}.sh
    # done

done
echo "All done"
exit 0

