#! /bin/bash

rm -r ./mWFit/Systematics/AppendedPDF/Combined
mkdir -p ./mWFit/Systematics/AppendedPDF/Combined

sysdir="/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/mWFit/Systematics/"

convert_command="pdfunite "
for i in $(ls ${sysdir}); do
    convert $sysdir/$i/mW_ttbar_$i.png mWFit/Systematics/AppendedPDF/mW_ttbar_$i.pdf
done

cd mWFit/Systematics/AppendedPDF

for j in $(ls); do
    if [[  -f $j ]]; then
        convert_command+=$j" "
    fi
done

convert_command+=" Combined/Combined.pdf"
echo $convert_command
eval $convert_command

echo AHHH

cd /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/
