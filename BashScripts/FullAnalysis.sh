cd TRExConfigs
rm -r nominalFit
# cp -r /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/RecoilOptimisation/DeltaR/nominalFit_mass_0-8 .
# mv nominalFit_mass_0-8 nominalFit
cp -r /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/nominalFit .
# mv nominalFit_FSRsamples_dRRecoilVar nominalFit
cd ..
python rdfMakeMtopSystHists.py
python createMtopMwWorkspaceWithBackgrounds.py
cd RooFitUtils/build/
source setup.sh
cd ../../RooFitUtilsScripts
bash RankingPostFit.sh
# bash RankingPostFitNoR.sh
# bash RankingPostFitmJOnly.sh
cd ..
echo 'Done'