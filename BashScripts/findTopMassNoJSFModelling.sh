#!/bin/bash
echo ""
echo "Welcome to the script that will find the top mass for the nominal and MC modelling samples!"

echo ""
echo "Are you on the glasgow machine?"
read -p 'Y/N: ' YNglas
if [[ ${YNglas} == "Y" ]] || [[ ${YNglas} == "y" ]]; then
    echo "Good"
else
    echo "You should go on a Glasgow machine"
    exit -1
fi
echo ""
echo "Have you setup the environment correclty (setupAnalysisScriptEnv)?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
else
    echo "You should go do that"
    
    exit -1
fi

echo ""
echo "Starting..."
echo ""

#################################
################################# ttpp8AF2, JSF varied
#################################

# JSFs=("0_97" "0_98" "0_99" "1_00" "1_01" "1_02" "1_03")
JSFs=("1_00")
for JSF in ${JSFs[@]}; do
    echo ""
    echo "Sample: ttpp8AF2, JSF = ${JSF}"
    python ../PythonScripts/findTopMassNoJSF.py -i ../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_${JSF}_ttpp8AF2_ALL.root -A 168.832795 -B 0.476824
    echo "Finished with Sample: ttpp8AF2, JSF = ${JSF}"
done

#################################
################################# ttallpp8, nominal
#################################

echo ""
echo "Sample: ttallpp8, JSF = 1_00, nominal"
python ../PythonScripts/findTopMassNoJSF.py -i ../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root -A 168.832795 -B 0.476824
echo "Finished with Sample: ttallpp8, JSF = 1_00, nominal"

#################################
################################# ttallpp8, ISR and FSR
#################################

# ISRandFSR=("isr_up" "isr_down" "fsr_up" "fsr_down")
# for template in ${ISRandFSR[@]}; do
#     echo ""
#     echo "Sample: ttallpp8, JSF = 1_00, ${template}"
#     python ../PythonScripts/findTopMassNoJSF.py -i ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_test_${template}.root -A 168.844494 -B 0.471483
#     echo "Finished with Sample: ttallpp8, JSF = 1_00, ${template}"
# done

#################################
################################# Alternative samples - full sim
#################################

# Samples_FullSim=("ttph721" "ttpp8Var")
# for sample in ${Samples_FullSim[@]}; do
#     echo ""
#     echo "Sample: ${sample}, JSF = 1_00"
#     python ../PythonScripts/findTopMassNoJSF.py -i ../output_root_files/2022-10-20-NewJESFlavourUncertainties/${sample}/All/JSF_1_00_${sample}_test.root -A 168.844494 -B 0.471483
#     echo "Finished with Sample: ${sample}, JSF = 1_00"
# done

#################################
################################# Alternative samples - fast sim
#################################

# Samples_FastSim=("ttph713" "ttnfaMCatNLO" "ttMCatNLOH713" "ttpp8CR" "ttpp8m169" "ttpp8m171" "ttpp8m172" "ttpp8m173" "ttpp8m174" "ttpp8m176" "ttpp8MECOff" "ttpp8RTT")
Samples_FastSim=("ttph713")
for sample in ${Samples_FastSim[@]}; do
    echo ""
    echo "Sample: ${sample}, JSF = 1_00"
    python ../PythonScripts/findTopMassNoJSF.py -i ../output_root_files/2023-01-13-NewJESFlavourUncertainties/${sample}/All/JSF_1_00_${sample}_ALL.root -A 168.832795 -B 0.476824
    echo "Finished with Sample: ${sample}, JSF = 1_00"
done

echo ""
echo "...Finished!"
exit 0