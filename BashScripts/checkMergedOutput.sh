#!/bin/bash
echo ""
echo "Welcome to the script that will make ALL of the analysis histograms!"
echo ""

echo "Are you on the glasgow machine?"
read -p 'Y/N: ' YNglas
if [[ ${YNglas} == "Y" ]] || [[ ${YNglas} == "y" ]]; then
    echo "Good"
else
    echo "You should go on a Glasgow machine"
    exit -1
fi

# folderName="2022_11_15-NewJESUncertaintiesStuff-do-not-use/2022_10_24_ttbar_SystematicsWithoutNewFlavourUncertainty_FullSim_COPYtoTEST"
folderName="2022_11_15_ttbar_OldFlavourScheme_FullSim"
JSF="1_00"
FullorFastsim="FullSim"
# subfolderNumber=("0") 
subfolderNumber=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31" "32" "33" "34" "35" "36" "37")
# subfolderNumber=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31" "32" "33" "34" "35" "36" "37" "38" "39" "40" "41" "42" "43" "44" "45" "46") 

echo ""
echo "Starting..."
echo ""

for folderNumber in ${subfolderNumber[@]}; do
    cd /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/1516/${folderNumber}/logs/
    NumberOfFilesToCheck=$(ls | wc -l)
    NumberOfFilesToCheckNew=$(expr ${NumberOfFilesToCheck} - 4)
    python /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/PythonScripts/checkMergedOutput.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/1516/${folderNumber}/logs/AutoStdErrCheck_ttallpp8_${NumberOfFilesToCheckNew}.txt
done

for folderNumber in ${subfolderNumber[@]}; do
    cd /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/17/${folderNumber}/logs/
    NumberOfFilesToCheck=$(ls | wc -l)
    NumberOfFilesToCheckNew=$(expr ${NumberOfFilesToCheck} - 4)
    python /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/PythonScripts/checkMergedOutput.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/17/${folderNumber}/logs/AutoStdErrCheck_ttallpp8_${NumberOfFilesToCheckNew}.txt
done

for folderNumber in ${subfolderNumber[@]}; do
    cd /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/18/${folderNumber}/logs/
    NumberOfFilesToCheck=$(ls | wc -l)
    NumberOfFilesToCheckNew=$(expr ${NumberOfFilesToCheck} - 4)
    python /home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/PythonScripts/checkMergedOutput.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/${folderName}/JSF_${JSF}_${FullorFastsim}/18/${folderNumber}/logs/AutoStdErrCheck_ttallpp8_${NumberOfFilesToCheckNew}.txt
done

echo ""
echo "...Finished!"
exit 0