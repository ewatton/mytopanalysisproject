rm -r impactsmJ
rm impactsmJ.pdf
python ../RooFitUtils/scripts/fit.py --input ../mJ_workspace.root --fit --workspace mJcomb --data test_m172p5 --impacts "alpha_*" --writeSubmit impactsmJ
python runParallelScript.py impactsmJ/jobs.txt
../RooFitUtils/scripts/plotpulls.py -i red "impactsmJ/*nominal*.json" --impacts mtop "impactsmJ/*.json" --range -2 2 --scaleimpacts 2 --numbers --atlas True --output impactsmJ.tex
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2020/bin/x86_64-linux:$PATH
pdflatex impactsmJ
