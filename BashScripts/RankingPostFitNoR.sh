rm -r impactsNoR
rm impactsNoR.pdf
python ../RooFitUtils/scripts/combine.py -i combineConfigNoR.cfg -o combWSNoR.root --autocorr --poi mtop_mJ,SigXsecOverSM_mW >& combNoR.log
python ../RooFitUtils/scripts/fit.py --input combWSNoR.root --fit --workspace combined --data combData --poi mtop_mJ SigXsecOverSM_mW --write-workspace postfitNoR.root -o nominalNoR.json >& fit.NoR
python ../RooFitUtils/scripts/fit.py --input postfitNoR.root --fit --workspace combined --data combData --impacts "alpha_*" --writeSubmit impactsNoR
python runParallelScript.py impactsNoR/jobs.txt
../RooFitUtils/scripts/plotpulls.py -i red "impactsNoR/*nominal*.json" --impacts mtop_mJ "impactsNoR/*.json" --range -2 2 --scaleimpacts 2 --numbers --atlas True --output impactsNoR.tex >& plotpulls.NoR
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2020/bin/x86_64-linux:$PATH
pdflatex impactsNoR