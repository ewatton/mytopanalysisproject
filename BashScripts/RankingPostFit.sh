rm -r impacts
rm impacts.pdf
python ../RooFitUtils/scripts/combine.py -i combineConfig.cfg -o combWS.root --autocorr --poi mtop_mJ,SigXsecOverSM_mW >& comb.log
python ../RooFitUtils/scripts/fit.py --input combWS.root --fit --workspace combined --data combData --poi mtop_mJ SigXsecOverSM_mW --write-workspace postfit.root -o nominal.json >& fit.nom
python ../RooFitUtils/scripts/fit.py --input postfit.root --fit --workspace combined --data combData --impacts "alpha_*" --writeSubmit impacts
python runParallelScript.py impacts/jobs.txt
../RooFitUtils/scripts/plotpulls.py -i red "impacts/*nominal*.json" --impacts mtop_mJ "impacts/*.json" --range -2 2 --scaleimpacts 2 --numbers --atlas True --output impacts.tex
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2020/bin/x86_64-linux:$PATH
pdflatex impacts
