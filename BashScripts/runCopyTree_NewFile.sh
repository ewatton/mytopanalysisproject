#!/bin/bash
echo ""
echo "Welcome to the script that run CopyTree.py and make TTrees with correct DSIDs"
echo ""
echo "Are you on the glasgow machine?"
read -p 'Y/N: ' YNglas
if [[ ${YNglas} == "Y" ]] || [[ ${YNglas} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go on a Glasgow machine"
    exit -1
fi
echo "Have you setup the environment correclty (setupAnalysisScriptEnv)?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go do that"
    exit -1
fi

inputFile=/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_18_TTDIFFXSMASSv03_RTT_pThard1_FSR_TEST_OriginalDataset_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttpp8FSRup.root
outputFile=/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_18_TTDIFFXSMASSv03_RTT_pThard1_FSR_TEST_OriginalDataset_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttpp8FSRup_NEW_TREE.root
isRTT=False
ispThard=False
isFSRup=True
isFSRdown=False

choice=""
if [[ ${isRTT} == "True" ]]; then
    choice=RTT
fi
if [[ ${ispThard} == "True" ]]; then
    choice=pThard
fi
if [[ ${isFSRup} == "True" ]]; then
    choice=FSRup
fi
if [[ ${isFSRdown} == "True" ]]; then
    choice=FSRdown
fi

echo "Is this your input file?"
echo $inputFile
read -p 'Y/N: ' YNinput
if [[ ${YNinput} == "Y" ]] || [[ ${YNinput} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go change that"
    exit -1
fi
echo "Is this your output file?"
echo $outputFile
read -p 'Y/N: ' YNoutput
if [[ ${YNoutput} == "Y" ]] || [[ ${YNoutput} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go change that"
    exit -1
fi
echo "Is this your sample choice?"
echo $choice
read -p 'Y/N: ' YNchoice
if [[ ${YNchoice} == "Y" ]] || [[ ${YNchoice} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go change that"
    exit -1
fi
echo ""
echo ================================================================================
echo ""
echo "Running CopyTree.py..."
echo ""
if [[ ${choice} == "RTT" ]]; then
    python CopyTree.py -i $inputFile -o $outputFile -r
fi
if [[ ${choice} == "pThard" ]]; then
    python CopyTree.py -i $inputFile -o $outputFile -p
fi
if [[ ${choice} == "FSRup" ]]; then
    python CopyTree.py -i $inputFile -o $outputFile -u
fi
if [[ ${choice} == "FSRdown" ]]; then
    python CopyTree.py -i $inputFile -o $outputFile -d
fi

echo ""
echo "Finished"
echo ""

exit 0