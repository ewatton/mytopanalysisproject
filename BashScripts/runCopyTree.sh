#!/bin/bash
echo ""
echo "Welcome to the script that run CopyTree.py and make TTrees with correct DSIDs"
echo ""
echo "Are you on the glasgow machine?"
read -p 'Y/N: ' YNglas
if [[ ${YNglas} == "Y" ]] || [[ ${YNglas} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go on a Glasgow machine"
    exit -1
fi
echo "Have you setup the environment correclty (setupAnalysisScriptEnv)?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go do that"
    exit -1
fi

# choice="FSRup"
choice="FSRdown"

if [[ ${choice} == "FSRup" ]]; then
    inputdatasetdir="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_25_OriginalFSRup/user.ewatton.601670.PhPy8EG.DAOD_TOPQ1.e8435_a875_r10724_p4514.TTDIFFXSMASSv03_AntiKt4EMDifferential_output_root/"
    outputdatasetdir="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_25_ModifiedFSRup/user.ewatton.601670.PhPy8EG.DAOD_TOPQ1.e8435_a875_r10724_p4514.TTDIFFXSMASSv03_AntiKt4EMDifferential_output_root/"
    fileNumbers=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17")
    taskID="33299081"
fi
if [[ ${choice} == "FSRdown" ]]; then
    inputdatasetdir="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_25_OriginalFSRdown/user.ewatton.601669.PhPy8EG.DAOD_TOPQ1.e8435_a875_r10724_p4514.TTDIFFXSMASSv03_v1_AntiKt4EMDifferential_output_root/"
    outputdatasetdir="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_05_25_ModifiedFSRdown/user.ewatton.601669.PhPy8EG.DAOD_TOPQ1.e8435_a875_r10724_p4514.TTDIFFXSMASSv03_v1_AntiKt4EMDifferential_output_root/"
    fileNumbers=("01" "02" "03" "04" "05" "06" "07" "08" "09")
    taskID="33382280"
fi

echo "Is this your choice?"
echo ${choice}
read -p 'Y/N: ' YNinput
if [[ ${YNinput} == "Y" ]] || [[ ${YNinput} == "y" ]]; then
    echo "Good"
    echo ""
else
    echo "You should go change that"
    exit -1
fi

filePrefix="user.ewatton."
fileMiddle="._0000"
fileSuffix=".AntiKt4EMDifferential_output.root"

echo ""
echo ================================================================================
echo ""
echo "Running CopyTree.py..."

for fileNumber in ${fileNumbers[@]}; do

    echo ""
    echo "Processing file number "${fileNumber}"..."
    echo ""

    fullFilePath=${filePrefix}${taskID}${fileMiddle}${fileNumber}${fileSuffix}
    inputFilePath=${inputdatasetdir}${fullFilePath}
    outputFilePath=${outputdatasetdir}${fullFilePath}

    if [[ ${choice} == "FSRup" ]]; then
        python CopyTree.py -i ${inputFilePath} -o ${outputFilePath} -u
    fi
    if [[ ${choice} == "FSRdown" ]]; then
        python CopyTree.py -i ${inputFilePath} -o ${outputFilePath} -d
    fi
done

echo ""
echo "Finished processing all files"
echo ""

exit 0