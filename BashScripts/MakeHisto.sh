#!/bin/bash
echo ""
echo "Welcome to the script that will make ALL of the analysis histograms!"

echo ""
echo "Are you on the glasgow machine?"
read -p 'Y/N: ' YNglas
if [[ ${YNglas} == "Y" ]] || [[ ${YNglas} == "y" ]]; then
    echo "Good"
else
    echo "You should go on a Glasgow machine"
    exit -1
fi
echo ""
echo "Have you setup the environment correclty (setupAnalysisScriptEnv)?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
else
    echo "You should go do that"
    
    exit -1
fi

echo ""
echo "Starting..."
echo ""

#################################
################################# ttpp8AF2, JSF varied
#################################

# JSFs=("0_97" "0_98" "0_99" "1_00" "1_01" "1_02" "1_03")
# for JSF in ${JSFs[@]}; do
#     echo ""
#     echo "Sample: ttpp8AF2, JSF = ${JSF}"
#     python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_20_ttbar_ttpp8AF2_JSFvaried_FastSim/JSF_${JSF}_FastSim/AllYears/ttbarBoosted/ttpp8AF2.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_${JSF}_ttpp8AF2_test.root 
#     echo "Finished with Sample: ttpp8AF2, JSF = ${JSF}"
# done

#################################
################################# ttallpp8, nominal
#################################

# echo ""
# echo "Sample: ttallpp8, JSF = 1_00, nominal"
# python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_21_ttbar_SystematicsWithNewFlavourUncertainty_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttallpp8.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_test.root 
# echo "Finished with Sample: ttallpp8, JSF = 1_00, nominal"

#################################
################################# ttallpp8, systematics
#################################

# echo ""
# echo "Sample: ttallpp8, JSF = 1_00, systematics"
# python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_21_ttbar_SystematicsWithNewFlavourUncertainty_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttallpp8.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_systematics_test.root -s 
# echo "Finished with Sample: ttallpp8, JSF = 1_00, systematics"

#################################
################################# ttallpp8, JER systematics
#################################

# echo ""
# echo "Sample: ttallpp8, JSF = 1_00, JER systematics"
# python ../PythonScripts/MakeHisto_JERSystematics.py -i ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_systematics_test.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_JERsystematics_test.root -n ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_test.root  
# echo "Finished with Sample: ttallpp8, JSF = 1_00, JER systematics"

#################################
################################# ttallpp8, ISR and FSR
#################################

ISRandFSR=("isr_up" "isr_down" "fsr_up" "fsr_down")
for template in ${ISRandFSR[@]}; do
    echo ""
    echo "Sample: ttallpp8, JSF = 1_00, ${template}"
    python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_11_15_ttbar_NewFlavourScheme_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttallpp8.root -o ../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_test.root -r 
    echo "Finished with Sample: ttallpp8, JSF = 1_00, ${template}"
done

#################################
################################# ttallpp8, other systematic weights
#################################

# echo ""
# echo "Sample: ttallpp8, JSF = 1_00, other systematic weights"
# python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_21_ttbar_SystematicsWithNewFlavourUncertainty_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/ttallpp8.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_systamticWeights_test.root -w 
# echo "Finished with Sample: ttallpp8, JSF = 1_00, other systematic weights"

#################################
################################# Alternative samples - full sim
#################################

# Samples_FullSim=("ttph721" "ttpp8Var")
# for sample in ${Samples_FullSim[@]}; do
#     echo ""
#     echo "Sample: ${sample}, JSF = 1_00"
#     python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_21_ttbar_FullSimAlternative_FullSim/JSF_1_00_FullSim/AllYears/ttbarBoosted/${sample}.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/${sample}/All/JSF_1_00_${sample}_test.root
#     echo "Finished with Sample: ${sample}, JSF = 1_00"
# done

#################################
################################# Alternative samples - fast sim
#################################

# Samples_FastSim=("ttph713" "ttnfaMCatNLO" "ttMCatNLOH713" "ttpp8CR" "ttpp8m169" "ttpp8m171" "ttpp8m172" "ttpp8m173" "ttpp8m174" "ttpp8m176" "ttpp8MECOff" "ttpp8RTT")
# for sample in ${Samples_FastSim[@]}; do
#     echo ""
#     echo "Sample: ${sample}, JSF = 1_00"
#     python ../PythonScripts/MakeHisto.py -i /nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_21_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim/AllYears/ttbarBoosted/${sample}.root -o ../output_root_files/2022-10-20-NewJESFlavourUncertainties/${sample}/All/JSF_1_00_${sample}_test.root
#     echo "Finished with Sample: ${sample}, JSF = 1_00"
# done

echo ""
echo "...Finished!"
exit 0