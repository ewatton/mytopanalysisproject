#!/bin/bash
#filelists=("filelist_V24MM_PFlow_MASTER1516" "filelist_V24MM_PFlow_MASTER17" "filelist_V24MM_PFlow_MASTER18")
#filelists=("filelist_V24MM_PFlow_ttallpp1516" "filelist_V24MM_PFlow_ttallpp17" "filelist_V24MM_PFlow_ttallpp18")
#filelists=("filelist_V24MM_PFlow_AFII1516" "filelist_V24MM_PFlow_AFII17" "filelist_V24MM_PFlow_AFII18")
#filelists=("filelist_V24MM_PFlow_Generator1516" "filelist_V24MM_PFlow_Generator17" "filelist_V24MM_PFlow_Generator18")
#filelists=("filelist_V24MM_PFlow_PP8MECOff1516" "filelist_V24MM_PFlow_PP8MECOff17" "filelist_V24MM_PFlow_PP8MECOff18")
#filelists=("filelist_V24MM_PFlow_aMCatNLOP8FastSim1516" "filelist_V24MM_PFlow_aMCatNLOP8FastSim17" "filelist_V24MM_PFlow_aMCatNLOP8FastSim18")
filelists=("filelist_V34_PFlow_ttallpp8_1516" "filelist_V34_PFlow_ttallpp8_17" "filelist_V34_PFlow_ttallpp8_18")
std_or_syst="std" # Changed from syst to std for systematics - Elliot
#JSFs=("0_97" "0_98" "0_99" "1_00" "1_01" "1_02" "1_03")
JSFs=("1_00")
tag1="JSF_"
tag2="_FullSim"
#tag1="V24MM_PFlow_Nominal_MECoffAlts_JSF"
#tag2="_ForMap_MECoff_v1_02_10_2021"
output="/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_10_24_ttbar_SystematicsWithoutNewFlavourUncertainty_FullSim/"

echo "Welcome to the automerger pre-flight Checklist:"
echo "Have you setup ATLAS and ROOT?"
read -p 'Y/N: ' YNsetup
if [[ ${YNsetup} == "Y" ]] || [[ ${YNsetup} == "y" ]]; then
    echo "Good"
else
    echo "You should go do that"
    exit -1
fi

echo "Merging files for JSF values: ${JSFs[@]}"
echo "Are these right?"
read -p 'Y/N: ' YNJSF
if [[ ${YNJSF} == "Y" ]] || [[ ${YNJSF} == "y" ]]; then
    echo "Good"
else
    echo "You should go change them"
    exit -1
fi

echo "Merging files in: ${output}${tag1}<JSF>${tag2}" 
echo "Do these seem sensible"
read -p 'Y/N: ' YNtag
if [[ ${YNtag} == "Y" ]] || [[ ${YNtag} == "y" ]]; then
    echo "Good"
else
    echo "You should go change that"
    exit -1
fi

#echo "Automating..."
#echo "merging files:"
#for JSF in ${JSFs[@]}; do
#    tag=${tag1}${JSF}${tag2}
#    python ../GlasgowAnaScripts/scripts/mergeT2Output.py condor ${output}${tag}/1516/ ${output}${tag}/17/ ${output}${tag}/18/ ${output}${tag}/AllYears/
#    for flist in ${filelists[@]}; do
#        python ../GlasgowAnaScripts/scripts/mergeT2Output.py condor ${output}${tag}/1516/,submit${std_or_syst}_${flist}_${tag}.sh ${output}${tag}/17/,submit${std_or_syst}_${flist}_${tag}.sh ${output}${tag}/18/,submit${std_or_syst}_${flist}_${tag}.sh ${output}${tag}/AllYears/
#    done
#done
#echo "All done"
#exit 0

######
###### My version - 10/12/2021
######

name="filelist_V34_PFlow_ttallpp8_"
echo "Automating..."
echo "merging files:"
for JSF in ${JSFs[@]}; do
    tag=${tag1}${JSF}${tag2}
    #python ../GlasgowAnaScripts/scripts/mergeT2Output.py condor ${output}${tag}/1516/ ${output}${tag}/17/ ${output}${tag}/18/ ${output}${tag}/AllYears/
    python ../GlasgowAnaScripts/scripts/mergeT2Output.py condor ${output}${tag}/1516/,submit${std_or_syst}_${name}1516_${tag}.sh ${output}${tag}/17/,submit${std_or_syst}_${name}17_${tag}.sh ${output}${tag}/18/,submit${std_or_syst}_${name}18_${tag}.sh ${output}${tag}/AllYears/
done
echo "All done"
exit 0
