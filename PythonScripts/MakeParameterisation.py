# A macro that will generate a scatter plot, and fit to the distribution (must be editted for data, desired fit, etc)
# First created by Elliot Watton 13/10/2021
# Last editted by Elliot Watton 13/10/2021

import ROOT as r
import math
from array import array
r.TH1.SetDefaultSumw2(True)

def MakeParameterisationPlot(n,x,y,title,save_name):

    print('x = ', x)
    print('y = ', y)
    print('xerr = ', xerr)
    print('yerr = ', yerr)
    canvas_scatter = r.TCanvas("canvas_overlayed")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetTicks()
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.35)
    pad2.SetBorderMode(0)
    pad2.SetTicks()
    # pad1.SetGrid()
    # pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_mt = r.TF1("fit_1D_mt","[0] + ((x-172.5)*[1])",171.0,174.0)
    graph_1D_mt = r.TGraphErrors(5)
    for i in range(0,5):
        # print(i)
        # print(x[i],xerr[i],y[i],yerr[i])
        graph_1D_mt.SetPoint(i,x[i],y[i])
        graph_1D_mt.SetPointError(i,xerr[i],yerr[i])
    r.gStyle.SetErrorX(0.)
    graph_1D_mt.SetMarkerStyle(5)
    graph_1D_mt.SetMarkerSize(0.5)
    graph_1D_mt.Fit(fit_1D_mt, "RB")
    graph_1D_mt.Draw("AP")
    graph_1D_mt.SetTitle("")
    graph_1D_mt.GetYaxis().SetTitle("#bar{#it{m}_{top-jet}} [GeV]")
    graph_1D_mt.GetXaxis().SetTitle("#it{m}_{t} [GeV]")
    # graph_1D_mt.GetYaxis().SetTitleOffset(1.4)

    graph_1D_mt.GetYaxis().SetTitleSize(0.05)
    graph_1D_mt.GetYaxis().SetTitleOffset(1.05)
    graph_1D_mt.GetYaxis().SetLabelSize(0.05)

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    latex.DrawLatex(0.15,0.80,"#it{ATLAS} #bf{Simlation Internal}")
    latex.DrawLatex(0.15,0.74,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")

    chi2_1D_mt = fit_1D_mt.GetChisquare()
    ndof_1D_mt = fit_1D_mt.GetNDF()
    par_1D_mt = fit_1D_mt.GetParameters()
    par_errors_1D_mt = fit_1D_mt.GetParErrors()
    # latex.DrawText(0.55,0.20,"Fit function: mJ = A + B(mt - 172.5)")
    # latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_mt[0],par_errors_1D_mt[0]))
    # latex.DrawText(0.55,0.10,"B = %.2f +- %.2f" %(par_1D_mt[1],par_errors_1D_mt[1]))
    # latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_mt,ndof_1D_mt))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    edges = array('f',[170.25, 171.75, 172.25, 172.75, 173.25, 174.75])
    subplot2 = r.TH1D("graph","graph title",6,edges)
    for i in range(1,6):
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
    subplot2.Divide(fit_1D_mt)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,6):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    print('')
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    for i in range(1,6):
        subplot_x_values.append(x[i-1])
        subplot_x_errors.append(0)
    subplot = r.TGraphErrors(5,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    print("")
    print("subplot_x_values:")
    print(subplot_x_values)
    print("subplot_x_errors:")
    print(subplot_x_errors)
    print("subplot_y_values:")
    print(subplot_y_values)
    print("subplot_y_errors:")
    print(subplot_y_errors)
    print("")
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")

    subplot.GetXaxis().SetLabelSize(0.14)
    subplot.GetXaxis().SetTitleSize(0.15)
    subplot.GetXaxis().SetTitleOffset(1.0)
    subplot.GetYaxis().SetLabelSize(0.12)
    subplot.GetYaxis().SetTitleOffset(0.45)
    subplot.GetYaxis().SetTitleSize(0.12)

    # subplot.GetXaxis().SetLabelFont(63)
    # subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("#it{m}_{t} [GeV]")
    # subplot.GetXaxis().SetTitleSize(0.11)
    # subplot.GetXaxis().SetTitleOffset(0.8)
    # subplot.GetYaxis().SetLabelFont(63)
    # subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    # subplot.GetYaxis().SetTitleOffset(0.38)
    # subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9995,1.0005)
    line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-09-27/Parameterisation.pdf")

    return par_1D_mt[0],par_errors_1D_mt[0],par_1D_mt[1],par_errors_1D_mt[1]

def MakeParameterisationPlotFixedA(n,x,y,title,save_name,nomMean):

    print('x = ', x)
    print('y = ', y)
    print('xerr = ', xerr)
    print('yerr = ', yerr)
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetTicks()
    pad2.SetTicks()
    # pad1.SetGrid()
    # pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_mt = r.TF1("fit_1D_mt","[0] + ((x-172.5)*[1])",171.0,174.0)
    fit_1D_mt.FixParameter(0,nomMean)
    graph_1D_mt = r.TGraphErrors(5)
    for i in range(0,5):
        # print(i)
        # print(x[i],xerr[i],y[i],yerr[i])
        graph_1D_mt.SetPoint(i,x[i],y[i])
        graph_1D_mt.SetPointError(i,xerr[i],yerr[i])
    r.gStyle.SetErrorX(0.)
    graph_1D_mt.SetMarkerStyle(5)
    graph_1D_mt.SetMarkerSize(0.5)
    graph_1D_mt.Fit(fit_1D_mt, "RB")
    graph_1D_mt.Draw("AP")
    graph_1D_mt.SetTitle("")
    graph_1D_mt.GetYaxis().SetTitle("#bar{#it{m}_{top-jet}} [GeV]")
    graph_1D_mt.GetXaxis().SetTitle("#it{m}_{t} [GeV]")
    graph_1D_mt.GetYaxis().SetTitleOffset(1.4)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_mt = fit_1D_mt.GetChisquare()
    ndof_1D_mt = fit_1D_mt.GetNDF()
    par_1D_mt = fit_1D_mt.GetParameters()
    par_errors_1D_mt = fit_1D_mt.GetParErrors()
    # latex.DrawText(0.55,0.20,"Fit function: mJ = A + B(mt - 172.5)")
    # latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_mt[0],par_errors_1D_mt[0]))
    # latex.DrawText(0.55,0.10,"B = %.2f +- %.2f" %(par_1D_mt[1],par_errors_1D_mt[1]))
    # latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_mt,ndof_1D_mt))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    edges = array('f',[170.25, 171.75, 172.25, 172.75, 173.25, 174.75])
    subplot2 = r.TH1D("graph","graph title",6,edges)
    for i in range(1,6):
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
    subplot2.Divide(fit_1D_mt)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,6):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    print('')
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    for i in range(1,6):
        subplot_x_values.append(x[i-1])
        subplot_x_errors.append(0)
    subplot = r.TGraphErrors(5,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    print("")
    print("subplot_x_values:")
    print(subplot_x_values)
    print("subplot_x_errors:")
    print(subplot_x_errors)
    print("subplot_y_values:")
    print(subplot_y_values)
    print("subplot_y_errors:")
    print(subplot_y_errors)
    print("")
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")

    subplot.GetXaxis().SetLabelSize(0.14)
    subplot.GetXaxis().SetTitleSize(0.15)
    subplot.GetXaxis().SetTitleOffset(1.0)
    subplot.GetYaxis().SetLabelSize(0.12)
    subplot.GetYaxis().SetTitle("Ratio")
    subplot.GetYaxis().SetTitleOffset(0.25)
    subplot.GetYaxis().SetTitleSize(0.16)

    # subplot.GetXaxis().SetLabelFont(63)
    # subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("#it{m}_{t} [GeV]")
    # subplot.GetXaxis().SetTitleSize(0.11)
    # subplot.GetXaxis().SetTitleOffset(0.8)
    # subplot.GetYaxis().SetLabelFont(63)
    # subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    # subplot.GetYaxis().SetTitleOffset(0.38)
    # subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-09-27/ParameterisationFixedA.pdf")

    return par_1D_mt[1],par_errors_1D_mt[1]

print('')
print('Making parameterisation plot...')
print('')

##############################
### Parameterisation setup ###
##############################

n = 5
x = array( 'f', [171.0,172.0,172.5,173.0,174.0])
y = array( 'f', [])
xerr = array( 'f', [0,0,0,0,0])
yerr = array( 'f', [])
title = "mJ vs m(top)"
save_name = "mJ_vs_mt"
useDataLikeErrors = True

#########################################
### Nominal signal + background setup ###
#########################################

histPathPrefix = "../output_root_files/2023-07-21-NewVariables/"
histPathSuffix = "/Histograms.root"
backgrounds = ["SingleTop","zjets","wejets","wmujets","wtaujets","diboson","ttV","Fakes"]

samples = ["ttpp8m171","ttpp8m172","ttallpp8","ttpp8m173","ttpp8m174"]
for sample in samples:
    path = histPathPrefix + sample + histPathSuffix    
    massPointFile = r.TFile.Open(path)
    massPointHist = massPointFile.Get("nominal_topjet_m")
    
    for background in backgrounds:
        path = histPathPrefix + background + histPathSuffix
        backgroundFile = r.TFile.Open(path)
        if background != "Fakes":
            backgroundHist = backgroundFile.Get("nominal_topjet_m")
        else:
            backgroundHist = backgroundFile.Get("nominal_Loose_topjet_m")
        backgroundHist.SetDirectory(0)
        massPointHist.Add(backgroundHist)
        backgroundFile.Close()
    
    y.append(massPointHist.GetMean())
    if (useDataLikeErrors == True):
        yerr.append(massPointHist.GetStdDev()/math.sqrt(massPointHist.Integral()))
    else:
        yerr.append(massPointHist.GetMeanError())

    if sample == "ttallpp8":
        nominalMean = massPointHist.GetMean()
        if (useDataLikeErrors == True):
            nominalMeanError = massPointHist.GetStdDev()/math.sqrt(massPointHist.Integral())
        else:
            nominalMeanError = massPointHist.GetMeanError()

    massPointFile.Close()

#############################
### Make parameterisation ###
#############################

A, Aerr, B, Berr = MakeParameterisationPlot(n,x,y,xerr,yerr)

print('')
print('Parameterisation plot constructed and saved!')
print('\n\tmJ = A + B(mt - 172.5)')
print("\n\tA = %f +- %f" %(A,Aerr))
print("\n\tB = %f +- %f" %(B,Berr))
print('')

B, Berr = MakeParameterisationPlotFixedA(n,x,y,xerr,yerr,nominalMean)

print('')
print('Parameterisation plot constructed and saved!')
print('\n\tmJ = A + B(mt - 172.5)')
print("\n\tA = %f +- %f" %(nominalMean,nominalMeanError))
print("\n\tB = %f +- %f" %(B,Berr))
print('')