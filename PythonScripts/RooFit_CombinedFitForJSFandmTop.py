# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np
from array import array
import math

def RooFit_Minimisation(JSF_value,filename,doFSRorISR,FSRorISR,useDataLikeErrors,useNLL):

    print("\n-----------------------------------------------------------------------------------------\n")
    print("\nFile = %s, JSF = %s\n" %(filename,JSF_value))
    print("\n-----------------------------------------------------------------------------------------\n")

    # Get mean top jet mass with error from histogram
    # ----------------------------------------------------------------------------------------------------------------

    # This is one of our observables, mean(top jet mass)
    meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)
    # meanTopJetMass.setConstant()

    if (filename == "ttallpp8") or (filename == "ttpp8AF2"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/mJ/JSF_" + JSF_value + "_" + filename + ".root"
    elif (filename == "radiation_up") or (filename == "radiation_down") or (filename == "fsr_up") or (filename == "fsr_down"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_" + filename + ".root"
    else:
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/All/JSF_" + JSF_value + "_" + filename + "_ALL.root"
    histfile = ROOT.TFile.Open(histogram_file_path)
    hist = histfile.Get("nominal_topjet_m")
    hist.SetDirectory(0)
    histfile.Close()
    nominalMean = hist.GetMean()
    if (useDataLikeErrors == True):
        errorOnMean = hist.GetStdDev()/math.sqrt(hist.Integral())
    else:
        errorOnMean = hist.GetMeanError()

    # Import TH1D for mW
    # ----------------------------------------------------------------------------------------------------------------

    histogram_file_path_nominal = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/mW/JSF_1_00_ttpp8AF2.root"
    histfile_nominal = ROOT.TFile.Open(histogram_file_path_nominal)
    hist_nominal = histfile_nominal.Get("nominal_topjet_W_invMass")
    hist_nominal.SetDirectory(0)
    histfile_nominal.Close()
    nominalMean_mW = hist_nominal.GetMean()

    if (filename == "ttallpp8") or (filename == "ttpp8AF2"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/mW/JSF_" + JSF_value + "_" + filename + ".root"
    elif (filename == "radiation_up") or (filename == "radiation_down") or (filename == "fsr_up") or (filename == "fsr_down"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_" + filename + ".root"
    else:
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/All/JSF_" + JSF_value + "_" + filename + "_ALL.root"

    histfile = ROOT.TFile.Open(histogram_file_path)
    hist_mW = histfile.Get("nominal_topjet_W_invMass")
    hist_mW.SetDirectory(0)
    histfile.Close()
    mean_mW = hist_mW.GetMean()

    if (filename == "ttpp8m169") or (JSF_value == "0_97"):
        ratioOfMeanmW = mean_mW/nominalMean_mW - 0.01
    elif filename == "ttpp8m176":
        ratioOfMeanmW = mean_mW/nominalMean_mW + 0.01
    else:
        ratioOfMeanmW = mean_mW/nominalMean_mW

    if (useDataLikeErrors == True): # If using data-like errors
        for bin_number in range(hist_mW.FindBin(55)-1,hist_mW.FindBin(110)+1):
            hist_mW.SetBinError(bin_number,np.sqrt(hist_mW.GetBinContent(bin_number)))


    # Set up model for mean top jet mass, mJ
    # ---------------------------------------------------------------------------------------------------------------

    # JSF
    #JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)
    JSF = ROOT.RooRealVar("JSF","JSF",ratioOfMeanmW,0.5,1.5)
    # JSF.setConstant()

    # Top quark mass
    mtop = ROOT.RooRealVar("mtop","mtop",172.5,150,200)

    # Fixed parameter in the equation for mean(top jet mass)
    amJ = ROOT.RooRealVar("amJ","amJ",168.832795) # FAST SIM
    # amJ = ROOT.RooRealVar("amJ","amJ",169.246951) # FULL SIM
    amJ.setConstant(True)

    # Fixed parameter relating JSF to mean(top jet mass)
    cJSF = ROOT.RooRealVar("cJSF","cJSF",78.189705) # FAST SIM
    # cJSF = ROOT.RooRealVar("cJSF","cJSF",76.954998) # FULL SIM
    cJSF.setConstant(True)

    # Fixed parameter relating mtop to mean(top jet mass)
    bmtop = ROOT.RooRealVar("bmtop","bmtop",0.476824) # FAST SIM
    # bmtop = ROOT.RooRealVar("bmtop","bmtop",0.457656) # FULL SIM

    bmtop.setConstant(True)

    # code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + C*(JSF-1) + B*(mtop - 172.5)
    meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2 - 1.0) + @3*(@4-172.5)",ROOT.RooArgList(amJ,cJSF,JSF,bmtop,mtop))

    # Assume are measurements of the mean top-jet mass (we will have only 1) are distributed according to a gaussian with mean related to JSF.
    # The sigma of the gaussian is fixed to the error on the mean
    pdf_topJetMass = ROOT.RooGaussian("pdf_topJetMass","pdf_topJetMass",meanTopJetMass, meanFormula, ROOT.RooRealConstant.value(errorOnMean))

    # Create data for mJ
    # ---------------------------------------------------------------------------------------------------------------

    # Create data - just one measurement of mean(top jet mass)
    toyData = ROOT.RooDataSet("d","d",ROOT.RooArgSet(meanTopJetMass))
    meanTopJetMass.setVal(nominalMean)
    toyData.add(ROOT.RooArgSet(meanTopJetMass))
    toyData.get(0).Print("V")

    # Set up model for reconstructed W boson mass, mW
    # ---------------------------------------------------------------------------------------------------------------

    # Declare observable mW
    mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)

    # Delcare constants in relation mean1 = A_2 + B_2*JSF
    # FAST SIM
    A_2 = ROOT.RooRealVar("A_2","A_2",11.6346)
    B_2 = ROOT.RooRealVar("B_2","B_2",71.6033)
    # FULL SIM
    # A_2 = ROOT.RooRealVar("A_2","A_2",13.5614)
    # B_2 = ROOT.RooRealVar("B_2","B_2",70.2612)

    # Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and their parameters
    mean1 = ROOT.RooFormulaVar("mean1","mean of first gaussian","@0 + @1*@2", ROOT.RooArgList(A_2,B_2,JSF))
    mean2 = ROOT.RooRealVar("mean2", "mean of second gaussian", 80.7167120345, 70,90)
    sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.7785340145,0,50)
    sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.6507628753,0,50)
    
    gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
    gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
    
    # Sum the signal components into a composite p.d.f.
    gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0, 1.0)
    combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))

    mWDataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist_mW, 1.0)

    # Perform negative log likelihood on both PDFs 
    # ---------------------------------------------------------------------------------------------------------------

    if useNLL == True:
        mW_NLL = combinedGauss.createNLL(mWDataHist)
        # mJ_NLL = pdf_topJetMass.createNLL(toyData)
    else:
        # print("test")
        # dummyset = ROOT.RooLinkedList()
        mW_NLL = combinedGauss.createChi2(mWDataHist)
        # mJ_NLL = pdf_topJetMass.createChi2(toyData,dummyset)
    mJ_NLL = pdf_topJetMass.createNLL(toyData)
    Added_NLL = ROOT.RooAddition("Added_NLL","Combined NLLs", ROOT.RooArgList(mW_NLL,mJ_NLL))
    Minimise_NLL = ROOT.RooMinimizer(Added_NLL)
    Minimise_NLL.minimize("Minuit")
    fitResult = Minimise_NLL.save()
    fitResult.Print("v")
    print("\nInitial value of floating parameters")
    fitResult.floatParsInit().Print("s")
    print("\nFinal value of floating parameters")
    fitResult.floatParsFinal().Print("s")
    print("\nValue of constant parameters")
    fitResult.constPars().Print("s")

    # outputfile = '../OutputTextFiles/MinimisationResults.txt'
    # lines = [JSF.getVal(),JSF.getError(),gauss1frac.getVal(),gauss1frac.getError(),sigma1.getVal(),sigma1.getError(),mean2.getVal(),mean2.getError(),sigma2.getVal(),sigma2.getError(),mtop.getVal(),mtop.getError()]
    # f = open(outputfile, 'a')
    # for line in lines:
    #     f.write('%.4f' %line)
    #     f.write('\t')
    # f.write('\n')
    # f.close()

    # Save the fit result
    # ---------------------------------------------------------------------------------------------------------------

    # mWframe = mW.frame()
    # mWDataHist.plotOn(mWframe,ROOT.RooFit.MarkerSize(0.05))
    # combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("combinedGauss"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(1))
    # combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss1"),ROOT.RooFit.LineColor(4),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(2),ROOT.RooFit.Components("gauss1"))
    # combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss2"),ROOT.RooFit.LineColor(8),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(3),ROOT.RooFit.Components("gauss2"))
    # mWframe.GetYaxis().SetTitle("Entries")
    # mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
    # canvas = ROOT.TCanvas("canvas")
    # mWframe.Draw()

    # legend = ROOT.TLegend(0.12,0.72,0.4,0.89)
    # legend.AddEntry("gauss1", "Gaussian 1", "lp")
    # legend.AddEntry("gauss2", "Gaussian 2", "lp")
    # legend.AddEntry("combinedGauss", "Final fit", "lp")
    # legend.SetLineWidth(0)
    # legend.SetFillStyle(0)
    # legend.Draw("same")

    # canvas.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/mW_twoGaussFit_SimultaneousFittingJSFmtop_" + filename + "_MC2MC.pdf")

    return JSF.getVal(),JSF.getError(),mtop.getVal(),mtop.getError()

# Main body of code
# -------------------------------------------------------------------------------------------

print('')
filenames = [
    "JSF_1_00_ttpp8AF2",
    "JSF_0_97_ttpp8AF2",
    "JSF_0_98_ttpp8AF2",
    "JSF_0_99_ttpp8AF2",
    "JSF_1_01_ttpp8AF2",
    "JSF_1_02_ttpp8AF2",
    "JSF_1_03_ttpp8AF2",
    "ttpp8m169",
    "ttpp8m171",
    "ttpp8m172",
    "ttpp8m173",
    "ttpp8m174",
    "ttpp8m176",
    "ttph713",
    "ttpp8MECOff",
    "ttpp8CR0",
    "ttpp8CR1",
    "ttpp8CR2",
    "ttpp8RTT",
    "ttnfaMCatNLO",
    "ttMCatNLOH713",
    "ttpp8m172_25",
    "ttpp8m172_75",
    "ttallpp8",
    "ttpp8Var",
    "radiation_up",
    "radiation_down",
    "fsr_up",
    "fsr_down",
    "ttph721",
]

# JSFs = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
# JSFs = ["1_00"]

doFSRorISR = [False]
FSRorISR = ["radiation_up","radiation_down","fsr_up","fsr_down"]

###################
################### MC errors
###################

JSF_results_MC = []
JSF_error_results_MC = []
mtop_results_MC = []
mtop_error_results_MC = []

for i in range(0,len(filenames)):
    if "ttpp8AF2" in filenames[i]:
        filenameString = filenames[i]
        JSFtoUse = filenameString[4:8]
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation(JSFtoUse,"ttpp8AF2",doFSRorISR[0],FSRorISR[0],False,False)
        JSF_results_MC.append(JSF_result)
        JSF_error_results_MC.append(JSF_error_result)
        mtop_results_MC.append(mtop_result)
        mtop_error_results_MC.append(mtop_error_result)
    else:
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation("1_00",filenames[i],doFSRorISR[0],FSRorISR[0],False,False)
        JSF_results_MC.append(JSF_result)
        JSF_error_results_MC.append(JSF_error_result)
        mtop_results_MC.append(mtop_result)
        mtop_error_results_MC.append(mtop_error_result)

###################
################### Data errors (NLL)
###################

JSF_results_Data_NLL = []
JSF_error_results_Data_NLL = []
mtop_results_Data_NLL = []
mtop_error_results_Data_NLL = []

for i in range(0,len(filenames)):
    if "ttpp8AF2" in filenames[i]:
        filenameString = filenames[i]
        JSFtoUse = filenameString[4:8]
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation(JSFtoUse,"ttpp8AF2",doFSRorISR[0],FSRorISR[0],True,True)
        JSF_results_Data_NLL.append(JSF_result)
        JSF_error_results_Data_NLL.append(JSF_error_result)
        mtop_results_Data_NLL.append(mtop_result)
        mtop_error_results_Data_NLL.append(mtop_error_result)
    else:
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation("1_00",filenames[i],doFSRorISR[0],FSRorISR[0],True,True)
        JSF_results_Data_NLL.append(JSF_result)
        JSF_error_results_Data_NLL.append(JSF_error_result)
        mtop_results_Data_NLL.append(mtop_result)
        mtop_error_results_Data_NLL.append(mtop_error_result)

###################
################### Data errors (Chi2)
###################

JSF_results_Data_Chi2 = []
JSF_error_results_Data_Chi2 = []
mtop_results_Data_Chi2 = []
mtop_error_results_Data_Chi2 = []

for i in range(0,len(filenames)):
    if "ttpp8AF2" in filenames[i]:
        filenameString = filenames[i]
        JSFtoUse = filenameString[4:8]
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation(JSFtoUse,"ttpp8AF2",doFSRorISR[0],FSRorISR[0],True,False)
        JSF_results_Data_Chi2.append(JSF_result)
        JSF_error_results_Data_Chi2.append(JSF_error_result)
        mtop_results_Data_Chi2.append(mtop_result)
        mtop_error_results_Data_Chi2.append(mtop_error_result)
    else:
        JSF_result, JSF_error_result, mtop_result, mtop_error_result = RooFit_Minimisation("1_00",filenames[i],doFSRorISR[0],FSRorISR[0],True,False)
        JSF_results_Data_Chi2.append(JSF_result)
        JSF_error_results_Data_Chi2.append(JSF_error_result)
        mtop_results_Data_Chi2.append(mtop_result)
        mtop_error_results_Data_Chi2.append(mtop_error_result)

strBeginning = [
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.00 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.97 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.98 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.99 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.01 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.02 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.03 & Fast & ",
    "Powheg+Pythia8, 169 GeV & Fast & ",
    "Powheg+Pythia8, 171 GeV & Fast & ",
    "Powheg+Pythia8, 172 GeV & Fast & ",
    "Powheg+Pythia8, 173 GeV & Fast & ",
    "Powheg+Pythia8, 174 GeV & Fast & ",
    "Powheg+Pythia8, 176 GeV & Fast & ",
    "Powheg+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, MEC turned off & Fast & ",
    "Powheg+Pythia8, CR0 & Fast & ",
    "Powheg+Pythia8, CR1 & Fast & ",
    "Powheg+Pythia8, CR2 & Fast & ",
    "Powheg+Pythia8, recoil-to-top & Fast & ",
    "aMC@NLO+Pythia8 & Fast & ",
    "aMC@NLO+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, 172.25 GeV & Fast & ",
    "Powheg+Pythia8, 172.75 GeV & Fast & ",
    "Powheg+Pythia8 & Full & ",
    "Powheg+Pythia8, varied $h_\text{damp}$ & Full & ",
    "Powheg+Pythia8, ISR_up & Full & ",
    "Powheg+Pythia8, ISR_down & Full & ",
    "Powheg+Pythia8, FSR_up & Full & ",
    "Powheg+Pythia8, FSR_down & Full & ",
    "Powheg+Herwig7.2.1 & Full & ",
]

# for i in range(0,len(filenames)):
#     print(strBeginning[i])
#     print("")
#     print(filenames[i])
#     print("")
#     print(JSF_results_MC[i])
#     print(JSF_error_results_MC[i])
#     print(mtop_results_MC[i])
#     print(mtop_error_results_MC[i])
#     print(JSF_results_Data_NLL[i])
#     print(JSF_error_results_Data_NLL[i])
#     print(mtop_results_Data_NLL[i])
#     print(mtop_error_results_Data_NLL[i])
#     print(JSF_results_Data_Chi2[i])
#     print(JSF_error_results_Data_Chi2[i])
#     print(mtop_results_Data_Chi2[i])
#     print(mtop_error_results_Data_Chi2[i])

outputfile = '../OutputTextFiles/MinimisationResults.txt'
f = open(outputfile, 'a')
for i in range(0,len(filenames)):
    f.write(strBeginning[i])
    f.write('%.5f $\pm$ %.5f & %.3f$\pm$%.3f & %.5f$\pm$%.5f & %.3f$\pm$%.3f & %.5f$\pm$%.5f & %.3f$\pm$%.3f\\\\' %(JSF_results_MC[i],JSF_error_results_MC[i],mtop_results_MC[i],mtop_error_results_MC[i],JSF_results_Data_NLL[i],JSF_error_results_Data_NLL[i],mtop_results_Data_NLL[i],mtop_error_results_Data_NLL[i],JSF_results_Data_Chi2[i],JSF_error_results_Data_Chi2[i],mtop_results_Data_Chi2[i],mtop_error_results_Data_Chi2[i]))
    # f.write('%s%.4f$\pm$%.4f& %.3f$\pm$%.3f%.4f$\pm$%.4f& %.3f$\pm$%.3f%.4f$\pm$%.4f& %.3f$\pm$%.3f' %(strBeginning[i],JSF_results_MC[i],JSF_error_results_MC[i],mtop_results_MC[i],mtop_error_results_MC[i],JSF_results_Data_NLL[i],JSF_error_results_Data_NLL[i],mtop_results_Data_NLL[i],mtop_error_results_Data_NLL[i],JSF_results_Data_Chi2[i],JSF_error_results_Data_Chi2[i],mtop_results_Data_Chi2[i],mtop_error_results_Data_Chi2[i]))
    f.write('\n')
f.close()

print("\nScript has finished\n")
