# Macro to calculate the mean of the histogram and print the integral
# First created by Elliot Watton - 12/10/2021
# Last editted by Elliot Watton - 18/10/2021

import ROOT as r
import ctypes
r.TH1.SetDefaultSumw2(True)

print('')

def CalcMeanIntegralStd(hist_name):
    
    # Make a variable that will track the sum in mean calculation
    total = 0
    total_num_of_entries = 0
    
    print('Calculating the mean, number of entries and integral for:')
    print(hist_name)
    
    # Go over all bins
    for i in range(1,hist_name.GetNbinsX()+1):
    
        # Get the midpoint of each bin for the mid value of that bin
        mid_value = hist_name.GetBinCenter(i)

        # Get how many entries there are in the bin
        num_of_entries_in_bin = hist_name.GetBinContent(i)

        # Add the number of entries in this bin to the total
        total_num_of_entries += num_of_entries_in_bin
    
        # Add the product of the previous variables to the total
        total += (mid_value * num_of_entries_in_bin)

    # When the for loop has finished, must divide the total by number of entries
    average = total / total_num_of_entries
    print('The average is:')
    print(average)
    print('The error on this average is:')
    print(hist_name.GetMeanError())
    print('The integral of the histogram is:')
    print(hist_name.Integral(1,hist_name.GetNbinsX()))
    print('The standard deviation is:')
    print(hist_name.GetStdDev())
    print('')

    return hist_name.Integral(1,hist_name.GetNbinsX()), average, hist_name.GetStdDev() 

########
######## The following code was made previously to find the mean and integral of the first set of histograms made
########

# Open the file containing previously made histograms
#histfile = r.TFile.Open("output.root")

# Get the histograms from the file
#hist_m1 = histfile.Get("h_topjet_m1")
#hist_m2 = histfile.Get("h_topjet_m2")
#hist_m3 = histfile.Get("h_topjet_m3")
#hist_m4 = histfile.Get("h_topjet_m4")
#hist_m5 = histfile.Get("h_topjet_m5")
#hist_m1_redu_axis = histfile.Get("h_topjet_m1_redu_axis")
#hist_m2_redu_axis = histfile.Get("h_topjet_m2_redu_axis")
#hist_m3_redu_axis = histfile.Get("h_topjet_m3_redu_axis")
#hist_m4_redu_axis = histfile.Get("h_topjet_m4_redu_axis")
#hist_m5_redu_axis = histfile.Get("h_topjet_m5_redu_axis")

#histograms = [hist_m1, hist_m2, hist_m3, hist_m4, hist_m5, hist_m1_redu_axis, hist_m2_redu_axis, hist_m3_redu_axis, hist_m4_redu_axis, hist_m5_redu_axis]

########
######## The following code is to find the mean and integral of the new histograms
########


# Get the histograms from the file
# histfile = r.TFile.Open("../output_root_files/mt_TEST/output_mt_JSF_1_00_ttpp8AF2.root")
# histograms = []
# histograms_names = []
# for h in histfile.GetListOfKeys():
#     h = h.ReadObj()
#     #substring = 'CategoryReduction_JET_'
#     #if (substring in h.GetName()):
#     hist = histfile.Get(h.GetName())
#     histograms.append(hist)
#     histograms_names.append(h.GetName())


# ########
# ######## The following must always be applied no matter what histograms are being used
# ########

# # Change the histogram directories so that they do not disappear when histogram file is closed
# for i in range(0,len(histograms)):
#     histograms[i].SetDirectory(0)

# # Close the histogram file
# histfile.Close()

# integral_list = []
# m_j_list = []
# std_list = []

# # Find and print the mean, number of entries used in mean calculation, integral
# for i in range(0,len(histograms)):
#     integral, m_j, std = 0, 0, 0
#     integral, m_j, std = CalcMeanIntegralStd(histograms[i])
#     integral_list.append(integral)
#     m_j_list.append(m_j)
#     std_list.append(std)

# file = open("Systematics_m_j_integral_std_results_ttpp8Var.txt","w")
# for i in range(0,len(integral_list)):
#     file.write(("%s\t%s\t%s\t%s\n") % (histograms_names[i], integral_list[i], m_j_list[i], std_list[i]))
# file.close()


# nominalMean = 0
# histfile = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root")
# for h in histfile.GetListOfKeys():
#     h = h.ReadObj()
#     if "nominal" in h.GetName():
#         hist = histfile.Get(h.GetName())
#         nominalMean = h.GetMean()


# histfile = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_otherSystematicWeights.root")
# histograms = []
# histograms_names = []
# histograms_means = []
# diffs = []
# for h in histfile.GetListOfKeys():
    # h = h.ReadObj()
    # hist = histfile.Get(h.GetName())
    # histograms.append(hist)
    # histograms_names.append(h.GetName())
    # histograms_means.append(h.GetMean())
    # diffs.append(h.GetMean() - nominalMean)

# file = open("../OutputTextFiles/means.txt","w")
# for i in range(0,len(histograms_means)):
#     file.write(("%s\t%f\t%f\t%f\n") % (histograms_names[i], nominalMean, histograms_means[i], diffs[i]))
# file.close()

histfile = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttallpp8/Nominal_All.root")
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal_lep_phi"):
        hist = histfile.Get(h.GetName())
        error = ctypes.c_double()
        integral = hist.IntegralAndError(1,200,error,"")
        print("Integral is " + str(integral) + " +- " + str(error))

