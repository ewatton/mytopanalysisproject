# A macro that will use TMinuit to minimise chi^2 while finding JSF and m_t as fit parameters
# This script was initially a copy of TMinuit_as_func.py
# First created by Elliot Watton - 26/11/2021
# Last editted by Elliot Watton - 26/11/2021

# Imports
from ROOT import TMinuit , Double , Long
from array import array as arr
import ROOT as r
import math
from array import array
r.TH1.SetDefaultSumw2(True)
# Initialise global variables 
mJ = 0                              # mJ
mJ_error = 0                        # sigma_{mJ}
mW = 0                              # mW
mW_err = 0                          # sigma_{mW}
A, B, C, D, E, F = 0, 0, 0, 0, 0, 0 # Constants in equations: mJ = A + B(m_t - 172.5) + C(JSF-1), mW = D + E(m_t - 172.5) + F(JSF -1)
npar = 2                            # Number of fit parameters

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    JSF = apar[1]
    mW_fit_value, mJ_fit_value = Double(0), Double(0)
    mJ_fit_value = A + (B * (m_t - 172.5)) + (C * (JSF - 1))
    mW_fit_value = D + (E * (m_t - 172.5)) + (F * (JSF - 1))
    chisq = ((mW - mW_fit_value)*(mW - mW_fit_value)) / (mW_err * mW_err) + ((mJ - mJ_fit_value)*(mJ - mJ_fit_value)) / (mJ_err * mJ_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getJSFandTopMass(m_W, sigma_m_W, m_J, m_J_err, A_val, B_val, C_val, D_val, E_val, F_val):
    
    # Define data
    global mW
    mW = m_W
    global mW_err 
    mW_err = sigma_m_W
    global mJ
    mJ = m_J
    global mJ_err
    mJ_err = m_J_err
    global A 
    A = A_val
    global B
    B = B_val
    global C
    C = C_val
    global D
    D = D_val
    global E
    E = E_val
    global F
    F = F_val
    
    # Set up fit parameters, starting points and step size
    name = ["m_t","JSF"]
    vstart = arr( 'd', (172, 1.00) )
    step = arr( 'd' , (0.001 , 0.001) )
    npar =len(name)
    
    # Set up Minuit
    myMinuit = TMinuit(2)         # Initialise TMinuit with maximum of parameters (2 for this case as we have m_t and JSF)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Define the parameters for the fit
    for i in range(0,npar):
        myMinuit.mnparm(i, name[i], vstart[i], step[i], 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    finalPar = []
    finalParErr = []
    p, pe = Double(0), Double(0)
    for i in range(0,npar):
        myMinuit.GetParameter(i, p, pe)
        finalPar.append(float(p))
        finalParErr.append(float(pe))
    
    """ # get covariance matrix
    buf = arr('d', npar*npar*[0.])
    myMinuit.mnemat(buf,npar) # retrieve error matrix
    emat = np.array(buf).reshape(npar,npar)

    # --> provide formatted output of results
    print "\n"
    print "*==* MINUIT fit completed:"
    print ' fcn@minimum = %.3g' %(amin)," error code =",ierflg," status =",icstat print " Results: \t value error corr. mat."
    for i in range(0,npar):
        print ' %s: \t%10.3e +/- %.1e '%(name[i] ,finalPar[i] ,finalParErr[i])
        for j in range (0,i):
            print '%+.3g ' %(emat[i][j]/np.sqrt(emat[i][i])/np.sqrt(emat[j][j]))
        printf ' ' """
    
    print('')
    print('*==* MINUIT fit completed:')
    print 'error code = ', ierflg
    print 'status = ', icstat
    print('')
    print('Results:')
    print('\t m_t = (%f +/- %f) GeV/c^2' %(finalPar[0], finalParErr[0]))
    print('')
    print('\t JSF = (%f +/- %f) ' %(finalPar[1], finalParErr[1]))
    print('')

    return finalPar[0], finalParErr[0], finalPar[1], finalParErr[1]

# Now need to find the specific values for quantities at these values

#########
######### For the full 3d plots (JSF and mass varied fast sim samples)
#########

""" outputfile = '../OutputTextFiles/MinimisationResults.txt'
mass = ["169","172","173","176"]
JSF_values_string = ["0_97","0_98","0_99","1_01","1_02","1_03"]

for i in range(0,2):

    # Finding mW and mW_err
    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m" + mass[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())
    mW = hist.GetMean()
    hist_integral = hist.Integral()
    mW_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Finding mJ and mJ_err
    histogram_path = "output_root_files/output_topjet_m_JSF_1_00_ttpp8m" + mass[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    mJ = hist.GetMean()
    hist_integral = hist.Integral()
    mJ_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Values of the constants in the equations for mJ and mW
    A = 168.825399
    B = 0.482998
    C = 78.169244
    D = 84.537928
    E = 0.025641 #-0.025641
    F = 50.396756


    m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW, mW_err, mJ, mJ_err, A, B, C, D, E, F)

    lines = [mJ, mJ_err, mW, mW_err, m_t, m_t_err, JSF, JSF_err]
    f = open(outputfile, 'a')
    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')

# Finding mW and mW_err
histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8AF2_more_events_SkipTruth_1.root"    
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal_no_neg"):
        hist = histogram_root_file.Get(h.GetName())
mW = hist.GetMean()
hist_integral = hist.Integral()
mW_err = hist.GetStdDev()/math.sqrt(hist_integral)

# Finding mJ and mJ_err
histogram_path = "output_root_files/output_topjet_m_JSF_1_00_ttpp8AF2_more_events_SkipTruth_1.root"    
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal"):
        hist = histogram_root_file.Get(h.GetName())
mJ = hist.GetMean()
hist_integral = hist.Integral()
mJ_err = hist.GetStdDev()/math.sqrt(hist_integral)

# Values of the constants in the equations for mJ and mW
A = 168.825399
B = 0.482998
C = 78.169244
D = 84.537928
E = 0.025641 #-0.025641
F = 50.396756

m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW, mW_err, mJ, mJ_err, A, B, C, D, E, F)

lines = [mJ, mJ_err, mW, mW_err, m_t, m_t_err, JSF, JSF_err]
f = open(outputfile, 'a')
for line in lines:
    f.write('%f' %line)
    f.write('\t')
f.write('\n')

for i in range(2,4):

    # Finding mW and mW_err
    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m" + mass[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())
    mW = hist.GetMean()
    hist_integral = hist.Integral()
    mW_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Finding mJ and mJ_err
    histogram_path = "output_root_files/output_topjet_m_JSF_1_00_ttpp8m" + mass[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    mJ = hist.GetMean()
    hist_integral = hist.Integral()
    mJ_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Values of the constants in the equations for mJ and mW
    A = 168.825399
    B = 0.482998
    C = 78.169244
    D = 84.537928
    E = 0.025641 #-0.025641
    F = 50.396756

    m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW, mW_err, mJ, mJ_err, A, B, C, D, E, F)

    lines = [mJ, mJ_err, mW, mW_err, m_t, m_t_err, JSF, JSF_err]
    f = open(outputfile, 'a')
    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')

for i in range(0,6):
    # Finding mW and mW_err
    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_" + JSF_values_string[i] + "_ttpp8AF2_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())
    mW = hist.GetMean()
    hist_integral = hist.Integral()
    mW_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Finding mJ and mJ_err
    histogram_path = "output_root_files/output_topjet_m_JSF_" + JSF_values_string[i] + "_ttpp8AF2_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    mJ = hist.GetMean()
    hist_integral = hist.Integral()
    mJ_err = hist.GetStdDev()/math.sqrt(hist_integral)

    # Values of the constants in the equations for mJ and mW
    A = 168.825399
    B = 0.482998
    C = 78.169244
    D = 84.537928
    E = 0.025641 #-0.025641
    F = 50.396756

    m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW, mW_err, mJ, mJ_err, A, B, C, D, E, F)

    lines = [mJ, mJ_err, mW, mW_err, m_t, m_t_err, JSF, JSF_err]
    f = open(outputfile, 'a')
    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')

f.close() """


##########
########## Systematics
##########

outputfile = '../OutputTextFiles/MinimisationResults.txt'
mW_array = array('f')
mW_err_array = array('f')
mJ_array = array('f')
mJ_err_array = array('f')
hist_names = []

# Finding mW and mW_err
histogram_path = "../output_root_files/output_GeneratorSamples_mW_JSF_1_00_mt_172_5_ttaMCatNLO.root"    
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    hist_names.append(h.GetName())
    hist = histogram_root_file.Get(h.GetName())
    mW_array.append(hist.GetMean())
    mW_err_array.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

# Finding mJ and mJ_err 
histogram_path = "../output_root_files/output_GeneratorSamples_mJ_JSF_1_00_mt_172_5_ttaMCatNLO.root"  
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    hist = histogram_root_file.Get(h.GetName())
    mJ_array.append(hist.GetMean())
    mJ_err_array.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

# Values of the constants in the equations for mJ and mW
A = 168.825399
B = 0.482998
C = 78.169244
D = 84.537928
E = -0.025641
F = 50.396756

for i in range(0,len(mJ_array)):
    m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW_array[i], mW_err_array[i], mJ_array[i], mJ_err_array[i], A, B, C, D, E, F)

    lines = [mJ_array[i], mJ_err_array[i], mW_array[i], mW_err_array[i], m_t, m_t_err, JSF, JSF_err]

    f = open(outputfile, 'a')

    f.write(hist_names[i])
    f.write('\t')

    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')

f.close()

