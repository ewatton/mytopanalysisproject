# A macro that will generate a scatter plot, and fit to the distribution (must be editted for data, desired fit, etc)
# First created by Elliot Watton 13/10/2021
# Last editted by Elliot Watton 13/10/2021

import ROOT as r
from array import array
r.TH1.SetDefaultSumw2(True)

def calcChi2(measuredList, expectedList, errorList):
    result = 0
    for i in range(0,len(measuredList)):
        result += ((measuredList[i] - expectedList[i])*(measuredList[i] - expectedList[i]))/(errorList[i]*errorList[i])
    return result

def Scatter(n,x,y,xerr,yerr,title,save_name):

    canvas_scatter = r.TCanvas("canvas_scatter")
    
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    # pad1.SetGrid()
    # pad2.SetGrid()
    pad1.SetTicks()
    pad2.SetTicks()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph = r.TGraphErrors(n, x, y, xerr, yerr)
    graph.SetTitle(title)
    graph.GetXaxis().SetTitle("m_{#it{t}} [GeV]")
    graph.GetYaxis().SetTitle("#bar{m_{#it{top-jet}}} [GeV]")
    # graph.GetXaxis().SetTitle("Input JSF")
    # graph.GetYaxis().SetTitle("Fitted JSF")
    graph.SetMarkerStyle(20)
    graph.SetMarkerSize(0.5)
    graph.GetYaxis().SetTitleOffset(1.0)
    graph.GetYaxis().SetTitleSize(0.04)
    graph.GetYaxis().SetLabelFont(62)

    fit = r.TF1("fit","[0]+(x-172.5)*[1]",171,174)
    # xyline = r.TLine(0.97,0.97,1.03,1.03)
    # xyline = r.TLine(169,169,176,176)
    # xyline = r.TLine(171,171,174,174)
    # xyline.SetLineColor(r.kRed)
    # xyline.SetLineWidth(2)
    graph.Draw("AP")
    xyline.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    chi2 = calcChi2(y,x,yerr)
    ndof = n-2

    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()

    par = fit.GetParameters()
    grad_error = fit.GetParError(1)
    intercept_error = fit.GetParError(0)

    print('')
    print('Gradient:')
    print(par[1])
    print('Gradient error:')
    print(grad_error)
    print('Intercept:')
    print(par[0])
    print('Intercept error:')
    print(intercept_error)
    print('')

    # legend = r.TLegend(0.60,0.12,0.88,0.32)
    # # legend.AddEntry(xyline, "Fitted top quark mass = MC top quark mass line")
    # legend.AddEntry(xyline, "Fitted JSF = MC JSF line")
    # legend.SetLineWidth(0)
    # legend.SetFillStyle(0)
    # legend.Draw("same")

    latex.DrawText(0.45,0.8,"Reduced chi^{2} = %.2f/%.0f" %(chi2,ndof))
    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    # latex.DrawText(0.45,0.76,"Gradient = %.4f +- %.4f" %(par[1],grad_error))
    # latex.DrawText(0.45,0.72,"y-intercept = %.4f +- %.4f" %(par[0],intercept_error))
    
    pad2.cd()

    r.gStyle.SetOptStat(0)

    # MC Top Mass vs Fitted Top Mass
    # edges = array('f',[167.75, 170.25, 171.75, 172.25, 172.75, 173.25, 174.75, 177.25])
    # edges = array('f',[0.965, 0.975, 0.985, 0.995, 1.005, 1.015, 1.025, 1.035])
    edges = array('f',[170.25, 171.75, 172.25, 172.75, 173.25, 174.75])
    # edges = array('f',[167.85,170.15,171.85,172.15,172.35,172.65,173.35,174.65,177.35])
    subplot2 = r.TH1D("graph","graph title",n,edges)
    subplot3 = r.TH1D("graph2","graph title",n,edges)
    subplot2.GetXaxis().SetRangeUser(170.25,174.75)
    subplot3.GetXaxis().SetRangeUser(170.25,174.75)
    # subplot2.GetXaxis().SetRangeUser(170.5,174.5)
    # subplot3.GetXaxis().SetRangeUser(170.5,174.5)
    for i in range(1,n+1):
        print(i)
        print(x[i-1])
        print(xerr[i-1])
        print(y[i-1])
        print(yerr[i-1])
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
    for i in range(1,n+1)
        print(i)
        print(x[i-1])
        print(xerr[i-1])
        print(y[i-1])
        print(yerr[i-1])
        subplot3.SetBinContent(i,x[i-1])
        subplot3.SetBinError(i,xerr[i-1])
    subplot2.Divide(subplot3)
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    # subplot_y_errors = array('f',[0.0010999999940395355, 0.0011224821209907532, 0.001212148810736835, 0.0012000193819403648, 0.0011881302343681455, 0.0011764743831008673, 0.0012621319619938731])
    for i in range(1,n+1):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
        subplot_x_values.append(x[i-1])
        subplot_x_errors.append(0)
    subplot = r.TGraphErrors(n,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("m_{#it{t}} [GeV]")
    # subplot.GetXaxis().SetTitle("Input JSF")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / Fit")

    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.993,1.007)
    # line = r.TLine(0.97,1,1.03,1)
    line = r.TLine(171,1,174,1)
    # line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-09-27/" + save_name + ".pdf") 

def Scatter_TwoLines(n,x,y,xerr,yerr,x2,y2,xerr2,yerr2,title,save_name):

    canvas_scatter = r.TCanvas("canvas_scatter")
    
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph1 = r.TGraphErrors(n, x, y, xerr, yerr)
    graph1.SetTitle(title)
    graph1.GetXaxis().SetTitle("MC top quark mass [GeV]")
    graph1.GetYaxis().SetTitle("Fitted top quark mass [GeV]")
    # graph1.GetXaxis().SetTitle("Input JSF")
    # graph1.GetYaxis().SetTitle("Fitted JSF")
    graph1.SetMarkerStyle(20)
    graph1.SetMarkerSize(0.5)
    graph1.GetYaxis().SetTitleOffset(1.0)
    graph1.GetYaxis().SetTitleSize(0.04)
    graph1.GetYaxis().SetLabelFont(62)
    graph2 = r.TGraphErrors(n, x2, y2, xerr2, yerr2)
    graph2.SetTitle(title)
    graph2.GetXaxis().SetTitle("MC top quark mass [GeV]")
    graph2.GetYaxis().SetTitle("Fitted top quark mass [GeV]")
    # graph2.GetXaxis().SetTitle("Input JSF")
    # graph2.GetYaxis().SetTitle("Fitted JSF")
    graph2.SetMarkerStyle(20)
    graph2.SetMarkerSize(0.5)
    graph2.GetYaxis().SetTitleOffset(1.0)
    graph2.GetYaxis().SetTitleSize(0.04)
    graph2.GetYaxis().SetLabelFont(62)

    # fit = r.TF1("fit","[0]+x*[1]",0.97,1.03)
    # xyline = r.TLine(0.97,0.97,1.03,1.03)
    xyline = r.TLine(169,169,176,176)
    # xyline = r.TLine(171,171,174,174)
    xyline.SetLineColor(r.kRed)
    xyline.SetLineWidth(2)
    graph1.Draw("AP")
    graph2.Draw("AP")
    xyline.Draw("same")

    graph1.SetLineColor(r.kGreen-2)
    graph2.SetLineColor(r.kBlue)

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    chi2 = calcChi2(y,x,yerr)
    chi2_2 = calcChi2(y2,x2,yerr2)
    ndof = n-2

    legend = r.TLegend(0.70,0.12,0.88,0.32)
    legend.AddEntry(graph1, "Using createNLL()")
    legend.AddEntry(graph2, "Using createChi2()")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex.DrawText(0.45,0.8,"NLL reduced chi^{2} = %.2f/%.0f" %(chi2,ndof))
    latex.DrawText(0.45,0.75,"Chi2 reduced chi^{2} = %.2f/%.0f" %(chi2_2,ndof))
    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    
    pad2.cd()

    r.gStyle.SetOptStat(0)

    # MC Top Mass vs Fitted Top Mass
    # edges = array('f',[167.75, 170.25, 171.75, 172.25, 172.75, 173.25, 174.75, 177.25])
    # edges = array('f',[0.965, 0.975, 0.985, 0.995, 1.005, 1.015, 1.025, 1.035])
    # edges = array('f',[170.5, 171.5, 173.5, 174.5])
    edges = array('f',[167.85,170.15,171.85,172.15,172.35,172.65,173.35,174.65,177.35])
    subplot2 = r.TH1D("graph","graph title",n,edges)
    subplot3 = r.TH1D("graph2","graph title",n,edges)
    subplot4 = r.TH1D("graph2","graph title",n,edges)
    subplot2.GetXaxis().SetRangeUser(167.75,177.25)
    subplot3.GetXaxis().SetRangeUser(167.75,177.25)
    subplot4.GetXaxis().SetRangeUser(167.75,177.25)
    # subplot2.GetXaxis().SetRangeUser(170.5,174.5)
    # subplot3.GetXaxis().SetRangeUser(170.5,174.5)
    for i in range(1,n+1):
        print(i)
        print(x[i-1])
        print(xerr[i-1])
        print(y[i-1])
        print(yerr[i-1])
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
        subplot4.SetBinContent(i,y2[i-1])
        subplot4.SetBinError(i,yerr2[i-1])
    for i in range(1,n+1):
        print(i)
        print(x[i-1])
        print(xerr[i-1])
        print(y[i-1])
        print(yerr[i-1])
        subplot3.SetBinContent(i,x[i-1])
        subplot3.SetBinError(i,xerr[i-1])
        subplot4.SetBinContent(i,x2[i-1])
        subplot4.SetBinError(i,xerr2[i-1])
    subplot2.Divide(subplot3)
    subplot4.Divide(subplot3)
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    subplot4_x_values = array('f')
    subplot4_x_errors = array('f')
    subplot4_y_values = array('f')
    subplot4_y_errors = array('f')
    for i in range(1,n+1):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
        subplot_x_values.append(x[i-1])
        subplot_x_errors.append(0)
        subplot4_y_values.append(subplot4.GetBinContent(i))
        subplot4_y_errors.append(subplot4.GetBinError(i))
        subplot4_x_values.append(x2[i-1])
        subplot4_x_errors.append(0)
    subplot = r.TGraphErrors(n,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    subplot_2 = r.TGraphErrors(n,subplot4_x_values,subplot4_y_values,subplot4_x_errors,subplot4_y_errors)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.SetLineColor(r.kGreen-2)
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("MC top quark mass [GeV]")
    # subplot.GetXaxis().SetTitle("Input JSF")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Fitted / MC")

    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.993,1.007)

    subplot_2.SetMarkerStyle(5)
    subplot_2.SetMarkerSize(0.5)
    subplot_2.Draw("same")
    subplot_2.SetLineColor(r.kBlue)
    subplot_2.SetTitle("")
    subplot_2.GetXaxis().SetLabelFont(63)
    subplot_2.GetXaxis().SetLabelSize(10)
    subplot_2.GetXaxis().SetTitle("MC top quark mass [GeV]")
    # subplot_2.GetXaxis().SetTitle("Input JSF")
    subplot_2.GetXaxis().SetTitleSize(0.11)
    subplot_2.GetXaxis().SetTitleOffset(0.8)
    subplot_2.GetYaxis().SetLabelFont(63)
    subplot_2.GetYaxis().SetLabelSize(10)
    subplot_2.GetYaxis().SetTitle("Fitted / MC")

    subplot_2.GetYaxis().SetTitleOffset(0.38)
    subplot_2.GetYaxis().SetTitleSize(0.11)
    subplot_2.GetYaxis().SetNdivisions(207)
    subplot_2.GetYaxis().SetRangeUser(0.993,1.007)

    # line = r.TLine(0.97,1,1.03,1)
    line = r.TLine(169,1,176,1)
    # line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-01-13/" + save_name + "_yVsx_FastSim_Dataerrors.pdf")   

def Scatter_NoSubplot(n,x,y,xerr,yerr,title,save_name):

    canvas_scatter = r.TCanvas("canvas_scatter")
    
    graph = r.TGraphErrors(n, x, y, xerr, yerr)
    graph.SetTitle(title)
    graph.GetXaxis().SetTitle("MC top mass [GeV]")
    graph.GetYaxis().SetTitle("Fitted top mass [GeV]")
    graph.GetYaxis().SetTitleOffset(1.4)
    graph.SetMarkerStyle(20)
    graph.SetMarkerSize(0.5)

    graph.Draw("AP")

    fit = r.TF1("fit","[0]+x*[1]",169,176)
    graph.Fit("fit","R")
    graph.Draw("AP")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.03)

    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()

    par = fit.GetParameters()
    grad_error = fit.GetParError(1)
    intercept_error = fit.GetParError(0)

    print('')
    print('Gradient:')
    print(par[1])
    print('Gradient error:')
    print(grad_error)
    print('Intercept:')
    print(par[0])
    print('Intercept error:')
    print(intercept_error)
    print('')

    latex.DrawText(0.6,0.8,"Reduced chi^{2} = %.2f/%.0f = %.2f" %(chi2,ndof,chi2/ndof))
    latex.DrawText(0.6,0.76,"Gradient = %.4f +- %.4f" %(par[1],grad_error))
    latex.DrawText(0.6,0.72,"y-intercept = %.2f +- %.2f" %(par[0],intercept_error))

    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2022-10-20-NewJESFlavourUncertainties/" + save_name +".pdf")

    print('')


def Scatter_no_errors(n,x,y,title,save_name):

    canvas_scatter = r.TCanvas("canvas_scatter")
    graph = r.TGraph(n, x, y)
    graph.SetTitle(title)
    graph.GetXaxis().SetTitle("Truth top mass [GeV]")
    graph.GetYaxis().SetTitle("Parameter value")
    graph.GetYaxis().SetTitleOffset(1.4)
    graph.SetMarkerStyle(20)
    graph.SetMarkerSize(0.5)
    graph.Draw("AP")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/mW_distributions_varied_mtop/Fitting/TwoGaussians/" + save_name +".pdf")
    print('')

print('')
print('Making scatter plots...')
print('')

###################
#### MC ERRORS ####
###################

# n = 9
# x = array( 'f', [169,171,172,172.25,172.5,172.75,173,174,176])
# y = array( 'f', [168.754,170.901,171.972,172.385,172.529,172.657,172.972,173.956,175.785])
# xerr = array( 'f', [0,0,0,0,0,0,0,0,0])
# yerr = array( 'f', [0.159,0.159,0.089,0.806,0.100,0.154,0.089,0.153,0.155]) 
# title = "MC m(top) vs Fitted m(top)"
# save_name = "MC_mtop_vs_Fitted_mtop"

# n = 7
# x = array( 'f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
# y = array( 'f', [0.9701,0.9801,0.9901,0.9997,1.0098,1.0199,1.0303])
# xerr = array( 'f', [0,0,0,0,0,0,0])
# yerr = array( 'f', [0.0004,0.0004,0.0004,0.0004,0.0005,0.0005,0.0004]) Your location
# title = "Input JSF vs Fitted JSF"
# save_name = "Input_JSF_vs_Fitted_JSF"

# Scatter(n,x,y,xerr,yerr,title,save_name)

###################
### DATA ERRORS ###
###################

n = 5
x = array( 'f', [171,172,172.5,173,174])
y = array( 'f', [])
xerr = array( 'f', [0,0,0,0,0,0,0,0,0])
yerr = array( 'f', []) 
title = ""
save_name = "MC_mtop_vs_mJ"

sample = ["ttpp8m171","ttpp8m172","ttallpp8","ttpp8m173","ttppm174"]
for i in range(0,len(sample)):
    histfile = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + sample[i] + "/Histograms.root")
    hist = histfile.Get("nominal_topjet_m")
    hist.SetDirectory(0)
    histfile.Close()
    for ib in range(1,hist.GetNbinsX()+1):
        hist.SetBinError(ib, sqrt(hist.GetBinContent(ib)))
    mean = hist.GetMean()
    errorOnMean = hist.GetStdDev()/sqrt(hist.Integral())
    y.append(mean)
    yerr.append(errorOnMean)

print(x)
print(xerr)
print(y)
print(yerr)

# n = 9
# x = array( 'f', [169,171,172,172.25,172.5,172.75,173,174,176])
# y = array( 'f', [168.753,170.889,171.970,172.019,172.526,172.655,172.969,173.955,175.782])
# xerr = array( 'f', [0,0,0,0,0,0,0,0,0])
# yerr = array( 'f', [0.225,0.232,0.225,0.238,0.230,0.230,0.230,0.231,0.241])
# x2 = array( 'f', [169,171,172,172.25,172.5,172.75,173,174,176])
# y2 = array( 'f', [168.751,170.895,171.970,172.150,172.528,172.652,172.966,173.953,175.783])
# xerr2 = array( 'f', [0,0,0,0,0,0,0,0,0])
# yerr2 = array( 'f', [0.256,0.264,0.259,0.266,0.264,0.263,0.263,0.265,0.276]) 
# title = "MC m(top) vs Fitted m(top)"
# save_name = "MC_mtop_vs_Fitted_mtop"

# n = 7
# x = array( 'f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
# y = array( 'f', [0.9701,0.9800,0.9900,0.9997,1.0098,1.0200,1.0303])
# xerr = array( 'f', [0,0,0,0,0,0,0])
# yerr = array( 'f', [0.0011,0.0011,0.0012,0.0012,0.0012,0.0012,0.0013]) 
# x2 = array( 'f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
# y2 = array( 'f', [0.9701,0.9801,0.9901,0.9997,1.0098,1.0199,1.0303])
# xerr2 = array( 'f', [0,0,0,0,0,0,0])
# yerr2 = array( 'f', [0.0011,0.0011,0.0012,0.0012,0.0012,0.0012,0.0013]) 
# title = "Input JSF vs Fitted JSF"
# save_name = "Input_JSF_vs_Fitted_JSF"

# Scatter_TwoLines(n,x,y,xerr,yerr,x2,y2,xerr2,yerr2,title,save_name)



print('Scatter plots completed and saved!')
print('')
    
