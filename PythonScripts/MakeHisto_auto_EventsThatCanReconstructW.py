from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)
        if (isRecoLevel == True):
            #baseWeight = tree_name.weight_normalise
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.weight_normalise
        else:
            #baseWeight = tree_name.mcEventNormWeight
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.mcEventNormWeight
    
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight

        Wmass = tree_name.topjet_W_invMass
        DoNotUseEvent = False

        if (Wmass < 0):
            DoNotUseEvent = True

        if (DoNotUseEvent == False):
            if (isNeedingDivisionByOneThousand == True):
                if (variableType == "vector"):
                    for j in range(0,len(variable)):
                        hist_name.Fill( variable[j] / 1000.0, weight)
                else:
                    hist_name.Fill(variable / 1000.0, weight)
            else:
                if (variableType == "vector"):
                    for j in range(0,len(variable)):
                        hist_name.Fill( variable[j], weight)
                else:
                    hist_name.Fill(variable, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    continue
            else:
                continue

    
    print("Finished filling histogram for variable: " + variable_name)
    print('')


def MakeHisto_auto(filePathSegment,sample,useMC2MC_SF):

    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    if (useMC2MC_SF == False):
        output_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample + "/All/JSF_1_00_" + sample + ".root"
    else:
        output_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample + "_MC2MC/All/JSF_1_00_" + sample + ".root"

    print('')
    print('Grabbing the TTree from file: %s' %(input_file_path))
    print('')
    print('Saving the histograms to file: %s' %(output_file_path))
    print('')
    
    infile = TFile.Open(input_file_path,"READ");

    # Get trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree') and (h.GetName() == 'nominal'):
            tree = h
            trees.append(tree)

    print('')
    print('The list of TTrees read in from %s:' %(input_file_path))
    print(trees)
    print('')

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable = [
        "topjet_pt",
        "topjet_eta",
        "topjet_phi",
        "topjet_m",
        "topjet_y",
        "topjet_nSubjets",
        "topjet_avgSubjet_pt",
        "topjet_avgSubjet_eta",
        "topjet_W_invMass",
        "topjet_nbtaggedSubjets",
        "topjet_Subjet_eta",
        "topjet_Subjet_phi",
        "topjet_Subjet_pT",
        "topjet_Subjet_mass",
        "topjet_Subjet_type",
        "topjet_Subjet_DeltaR",
        "topjet_Subjet_isMatched",
        "topjet_Subjet_jetresponse",
        "topjet_Subjet_bjetresponse",
        "topjet_Subjet_lightjetresponse"
    ]

    variableType = [
        "float",
        "float",
        "float",
        "float",
        "float",
        "int",
        "float",
        "float",
        "float",
        "int",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector",
        "vector"
    ]

    isNeedingDivisionByOneThousand = [
        True,
        False,
        False,
        True,
        False,
        False,
        True,
        False,
        True,
        False,
        False,
        False,
        True,
        True,
        False,
        False,
        False,
        False,
        False,
        False
    ]

    for z in range(0,len(trees)):
        histograms = []
        for i in range(0,len(variable)):
            #name = trees[z].GetName() + "_" + variable[i] + "_" + "weight_"
            name = trees[z].GetName() + "_" + variable[i] + "_EventsThatCanReconstructW"

            if (variable[i] == "topjet_pt"):
                hist = TH1D(name, "h_topjet_pt; pT [GeV]; Events", 200, 355.0, 1000.0)
            elif (variable[i] == "topjet_eta"):
                hist = TH1D(name, "h_topjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] == "topjet_phi"):
                hist = TH1D(name, "h_topjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
            elif (variable[i] == "topjet_m"):
                hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 100, 135.0, 205.0)
                #hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 50, 120.0, 220.0)
            elif (variable[i] =="topjet_y"):
                hist = TH1D(name, "h_topjet_y; y; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_nSubjets"):
                hist = TH1D(name, "h_topjet_nSubjets; Number of sub-jets; Events", 8, 0, 7)
            elif (variable[i] =="topjet_avgSubjet_pt"):
                hist = TH1D(name, "h_topjet_avgSubjet_pt; pT [GeV]; Events", 200, 0.0, 900.0)
            elif (variable[i] =="topjet_avgSubjet_eta"):
                hist = TH1D(name, "h_topjet_avgSubjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_W_invMass"):
                hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV]; Events", 200, 0.0, 200.0)
            elif (variable[i] =="topjet_nbtaggedSubjets"):
                hist = TH1D(name, "h_topjet_nbtaggedSubjets; Number of b tagged jets; Events", 6, 0.0, 5)
            elif (variable[i] =="topjet_Subjet_eta"):
                hist = TH1D(name, "h_topjet_Subjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_Subjet_phi"):
                hist = TH1D(name, "h_topjet_Subjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
            elif (variable[i] =="topjet_Subjet_pT"):
                hist = TH1D(name, "h_topjet_Subjet_pT; pT [GeV]; Events", 200, 0.0, 900.0)
            elif (variable[i] =="topjet_Subjet_mass"):
                hist = TH1D(name, "h_topjet_Subjet_mass; Mass [GeV]; Events", 200, 0.0, 180.0)
            elif (variable[i] =="topjet_Subjet_type"):
                hist = TH1D(name, "h_topjet_Subjet_type; Type scoring; Events", 5, -1, 4)
            elif (variable[i] =="topjet_Subjet_DeltaR"):
                hist = TH1D(name, "h_topjet_Subjet_DeltaR; Delta R; Events", 100, 0.0, 0.5)
            elif (variable[i] =="topjet_Subjet_isMatched"):
                hist = TH1D(name, "h_topjet_Subjet_isMatched; Matching score; Events", 5, -2, 3)
            elif (variable[i] =="topjet_Subjet_jetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_jetresponse; Response; Events", 200, 0.0, 2.0)
            elif (variable[i] =="topjet_Subjet_bjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_bjetresponse; Response; Events", 200, 0.0, 2.0)
            elif (variable[i] =="topjet_Subjet_lightjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_lightjetresponse; Response; Events", 200, 0.0, 2.0)

            histograms.append(hist)

        for j in range(0,len(histograms)):
            FillHisto_auto(trees[z],histograms[j],isRecoLevel,variable[j],variableType[j],isNeedingDivisionByOneThousand[j])
            histograms[j].Write()

    outfile.Close()


######
###### Main part of code
######

print('')
samplesToRunOn = ["ttph713","ttph713","ttMCatNLOH713","ttMCatNLOH713"]
filePathSegments = ["2022_07_06_ttbar_AFIIandModelling_FastSim/JSF_1_00_FastSim","2022_07_06_ttbar_AFIIandModellingMC2MC_FastSim/JSF_1_00_FastSim","2022_07_06_ttbar_AFIIandModelling_FastSim/JSF_1_00_FastSim","2022_07_06_ttbar_AFIIandModellingMC2MC_FastSim/JSF_1_00_FastSim"]
MC2MC_SF = [False,True,False,True]

#samplesToRunOn = ["ttpp8AF2"]
#filePathSegments = ["2022_07_06_ttbar_AFII_JSFvaried_ManchesterDisk_SkipTruth1_FastSim/JSF_1_00_FastSim_ManchesterDisk"]
#MC2MC_SF = [False]

for i in range(0,len(samplesToRunOn)):
    MakeHisto_auto(filePathSegments[i],samplesToRunOn[i],MC2MC_SF[i])

print('')
print('Histograms have been saved to the output_root_files folder')
print('')
