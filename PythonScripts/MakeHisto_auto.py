from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)
        if (isRecoLevel == True):
            #baseWeight = tree_name.weight_normalise
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.weight_normalise
        else:
            #baseWeight = tree_name.mcEventNormWeight
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.mcEventNormWeight
    
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight )
            else:
                hist_name.Fill(variable / 1000.0, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight )
            else:
                hist_name.Fill(variable, weight)
    
    print("Finished filling histogram for variable: " + variable_name)


def MakeHisto_auto(filePathSegment,sample,JSF):

    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    output_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample + "/All/JSF_" + JSF + "_" + sample + ".root"
    # output_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample + "/All/JSF_" + JSF + "_" + sample + ".root"
    
    infile = TFile.Open(input_file_path,"READ");

    # Get trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        # if (h.ClassName() == 'TTree'):
        if (h.ClassName() == 'TTree') and (h.GetName() == 'nominal'):
            tree = h
            trees.append(tree)

    print('\n==============================================================================')
    print('==============================================================================')
    print('\nSample = ' + sample + ', JSF = ' + JSF)
    print('\nTTrees from file: %s' %(input_file_path))
    print('\nOutput ROOT file: %s' %(output_file_path))
    print('\nThe list of TTrees read in is:')
    for treename in trees:
        print('')
        print(treename)
    print('\nThe total number of trees read in is: %i' %(len(trees)))
    print('\n==============================================================================')
    print('==============================================================================\n')

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable = [
        # "topjet_pt",
        # "topjet_eta",
        # "topjet_phi",
        # "topjet_m",
        # "topjet_y",
        # "topjet_nSubjets",
        # "topjet_avgSubjet_pt",
        # "topjet_avgSubjet_eta",
        # "topjet_W_invMass",
        # "topjet_nbtaggedSubjets",
        # "topjet_Subjet_eta",
        # "topjet_Subjet_phi",
        # "topjet_Subjet_pT",
        # "topjet_Subjet_mass",
        # "topjet_Subjet_type",
        # "topjet_Subjet_DeltaR",
        # "topjet_Subjet_isMatched",
        # "topjet_Subjet_jetresponse",
        # "topjet_Subjet_bjetresponse",
        # "topjet_Subjet_lightjetresponse",

        # "leadadd_top_deltaR",
        # "leadadd_top_deltaPhi",
        # "subleadadd_hadtop_deltaR",
        # "subleadadd_hadtop_deltaPhi", 
        # "leadadd_subleadadd_deltaR",
        # "leadadd_subleadadd_deltaPhi",
        # "leadadd_lepb_deltaR",
        # "leadadd_lepb_deltaPhi",
        # "tmm_leadadd_leptop_deltaR",
        # "tmm_leadadd_leptop_deltaPhi",
        # "tmm_subleadadd_leptop_deltaR",
        # "tmm_subleadadd_leptop_deltaPhi",
        # "tmm_leadadd_ttbar_deltaR",
        # "tmm_leadadd_ttbar_deltaPhi",
        # "tmm_subleadadd_ttbar_deltaR",
        # "tmm_subleadadd_ttbar_deltaPhi",
        # "tmm_leadadd_leadtop_deltaR",
        # "tmm_leadadd_leadtop_deltaPhi",
        # "tmm_subleadadd_leadtop_deltaR",
        # "tmm_subleadadd_leadtop_deltaPhi",

        "lep_phi"
    ]

    variableType = [
        # "float",
        # "float",
        # "float",
        # "float",
        # "float",
        # "int",
        # "float",
        # "float",
        # "float",
        # "int",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",

        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",
        # "vector",

        "vector"
    ]

    isNeedingDivisionByOneThousand = [
        # True,
        # False,
        # False,
        # True,
        # False,
        # False,
        # True,
        # False,
        # True,
        # False,
        # False,
        # False,
        # True,
        # True,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,

        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,
        # False,

        False
    ]

    for z in range(0,len(trees)):
        histograms = []
        for i in range(0,len(variable)):
            name = trees[z].GetName() + "_" + variable[i]
            #name = trees[z].GetName() + "_" + variable[i] + "_" + "weight_"
            # name = trees[z].GetName() + "_" + variable[i] + "_50bins"

            if (variable[i] == "topjet_pt"):
                hist = TH1D(name, "h_topjet_pt; pT [GeV]; Events", 200, 355.0, 1000.0)
            elif (variable[i] == "topjet_eta"):
                hist = TH1D(name, "h_topjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] == "topjet_phi"):
                hist = TH1D(name, "h_topjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
            elif (variable[i] == "topjet_m"):
                # hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 100, 135.0, 205.0)
                name = trees[z].GetName() + "_" + variable[i] + "_50bins"
                hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 50, 120.0, 220.0)
            elif (variable[i] =="topjet_y"):
                hist = TH1D(name, "h_topjet_y; y; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_nSubjets"):
                hist = TH1D(name, "h_topjet_nSubjets; Number of sub-jets; Events", 8, 0, 7)
            elif (variable[i] =="topjet_avgSubjet_pt"):
                hist = TH1D(name, "h_topjet_avgSubjet_pt; pT [GeV]; Events", 200, 0.0, 900.0)
            elif (variable[i] =="topjet_avgSubjet_eta"):
                hist = TH1D(name, "h_topjet_avgSubjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_W_invMass"):
                hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV]; Events", 200, 0.0, 200.0)
            elif (variable[i] =="topjet_nbtaggedSubjets"):
                hist = TH1D(name, "h_topjet_nbtaggedSubjets; Number of b tagged jets; Events", 6, 0.0, 5)
            elif (variable[i] =="topjet_Subjet_eta"):
                hist = TH1D(name, "h_topjet_Subjet_eta; eta; Events", 200, -2.5, 2.5)
            elif (variable[i] =="topjet_Subjet_phi"):
                hist = TH1D(name, "h_topjet_Subjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
            elif (variable[i] =="topjet_Subjet_pT"):
                hist = TH1D(name, "h_topjet_Subjet_pT; pT [GeV]; Events", 200, 0.0, 900.0)
            elif (variable[i] =="topjet_Subjet_mass"):
                hist = TH1D(name, "h_topjet_Subjet_mass; Mass [GeV]; Events", 200, 0.0, 180.0)
            elif (variable[i] =="topjet_Subjet_type"):
                hist = TH1D(name, "h_topjet_Subjet_type; Type scoring; Events", 5, -1, 4)
            elif (variable[i] =="topjet_Subjet_DeltaR"):
                hist = TH1D(name, "h_topjet_Subjet_DeltaR; Delta R; Events", 100, 0.0, 0.5)
            elif (variable[i] =="topjet_Subjet_isMatched"):
                hist = TH1D(name, "h_topjet_Subjet_isMatched; Matching score; Events", 5, -2, 3)
            elif (variable[i] =="topjet_Subjet_jetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_jetresponse; Response; Events", 200, 0.0, 2.0)
            elif (variable[i] =="topjet_Subjet_bjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_bjetresponse; Response; Events", 200, 0.0, 2.0)
            elif (variable[i] =="topjet_Subjet_lightjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_lightjetresponse; Response; Events", 200, 0.0, 2.0)
            elif (("deltaR" in variable[i]) and (variable[i] != "topjet_Subjet_DeltaR")):
                hist = TH1D(name, "deltaR; Delta R; Events", 200, 0.0, 6.0)
            elif ("deltaPhi" in variable[i]):
                hist = TH1D(name, "deltaPhi; phi [radians]; Events", 100, -3.15, 3.15)
            elif (variable[i] == "lep_phi"):
                hist = TH1D(name, "lep_phi; phi [radians]; Events", 100, -3.15, 3.15)

            histograms.append(hist)

        for j in range(0,len(histograms)):
            FillHisto_auto(trees[z],histograms[j],isRecoLevel,variable[j],variableType[j],isNeedingDivisionByOneThousand[j])
            print("Finished filling histogram number " + str(j+1) + "/" + str(len(histograms)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
            histograms[j].Write()
        print("Finished filling the histograms for tree number " + str(z+1) + "/" + str(len(trees)))

    outfile.Close()


# ==========================================================================
# =========================== Main part of code ============================
# ==========================================================================

print('')
samplesToRunOn = [
    
    # "ttallpp8",
    # "ttMCatNLOH713",
    # "ttnfaMCatNLO",
    # "ttph713",
    # "ttph721",
    # "ttpp8AF2",
    "ttpp8CR",
    # "ttpp8m169",
    # "ttpp8m171",
    # "ttpp8m172",
    # "ttpp8m173",
    # "ttpp8m174",
    # "ttpp8m176",
    # "ttpp8MECOff",
    # "ttpp8RTT",
    # "ttpp8Var",

    ]

JSFs = [
    
    # "0_97",
    # "0_98",
    # "0_99",
    "1_00",
    # "1_01",
    # "1_02",
    # "1_03"
    
]

for i in range(0,len(samplesToRunOn)):
    JSFtoUse = "1_00"
    if samplesToRunOn[i] == "ttallpp8":
        # filePathSegment = "2022_07_06_ttbar_ttallpp8_2ndAttempt_FullSim/JSF_1_00_FullSim"
        filePathSegment = "2022_11_15_ttbar_NewFlavourScheme_FullSim/JSF_1_00_FullSim"
        # filePathSegment = "2022_11_15_ttbar_OldFlavourScheme_FullSim/JSF_1_00_FullSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
        # JSFs = ["0_97","0_98","0_99","1_01","1_02","1_03"]
        # for k in range(0,len(JSFs)):
            # filePathSegment = "2022_11_14_ttbar_ttallpp8_JSFvaried_FullSim/JSF_" + JSFs[k] + "_FullSim"
            # MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFs[k])
    elif samplesToRunOn[i] == "ttMCatNLOH713":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttnfaMCatNLO":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttph713":
        # filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        filePathSegment = "2022_11_14_ttbar_PowhegHerwigNoMC2MC_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8CR":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m169":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m171":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        # filePathSegment = "2022_11_14_ttbar_AlternativeMassPoints_FullSim/JSF_1_00_FullSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m172":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m173":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m174":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        # filePathSegment = "2022_11_14_ttbar_AlternativeMassPoints_FullSim/JSF_1_00_FullSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8m176":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8MECOff":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8RTT":
        filePathSegment = "2022_11_15_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttph721":
        filePathSegment = "2022_11_15_ttbar_FullSimAlternative_FullSim/JSF_1_00_FullSim"
        # filePathSegment = "2022_11_14_ttbar_PowhegHerwigNoMC2MC_FastSim/JSF_1_00_FastSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8Var":
        filePathSegment = "2022_11_15_ttbar_FullSimAlternative_FullSim/JSF_1_00_FullSim"
        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse)
    elif samplesToRunOn[i] == "ttpp8AF2":
        # JSFs = ["0_97","0_98","0_99","1_01","1_02","1_03"]
        JSFs = ["0_97","1_00","1_03"]
        # JSFs = ["1_00"]
        for k in range(0,len(JSFs)):
            filePathSegment = "2022_11_24_ttbar_AF2_JSFvaried_FastSim/JSF_" + JSFs[k] + "_FastSim"
            # filePathSegment = "2022_07_06_ttbar_AFII_JSFvaried_" + JSFs[k] + "_FastSim/JSF_" + JSFs[k] + "_FastSim_" + JSFs[k]
            MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFs[k])

print('')
print('Histograms have been saved to the output_root_files folder')
print('')