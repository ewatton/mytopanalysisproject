# A macro that will use TMinuit to minimise chi^2 while finding JSF as a fit parameter
# This script was initially a copy of TMinuit_as_func.py
# First created by Elliot Watton - 26/11/2021
# Last editted by Elliot Watton - 26/11/2021

# Imports
from ROOT import TMinuit , Double , Long
import numpy as np
from array import array as arr
r.TH1.SetDefaultSumw2(True)
# Initialise global variables 
average_mW = 0      # mW
average_mW = 0      # sigma_{mW}
grad = 0            # s
intercept = 0       # c
npar = 1            # Number of fit parameters

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    JSF = apar[0]
    average_mW_fit_value = Double(0)
    average_mW_fit_value = grad * (JSF - 1) + intercept
    chisq = ((average_mW - average_mW_fit_value)*(average_mW - average_mW_fit_value)) / (average_mW_err * average_mW_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getJSF(mW, sigma_mW, s, c):
    
    # Define data
    global average_mW 
    average_mW = mW
    global average_mW_err 
    average_mW_err = sigma_mW
    global grad 
    grad = s
    global intercept
    intercept = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "JSF", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_JSF = p
    final_JSF_err = pe
    
    print('')
    print('*==* MINUIT fit completed:')
    print 'error code = ', ierflg
    print 'status = ', icstat
    print('')
    print('Results:')
    print('\t JSF = (%.3f +/- %.3f) GeV/c^2' %(final_JSF, final_JSF_err))
    print('')

    return final_JSF, final_JSF_err


average_mW = 
average_mW_err = 
grad = 
intercept = 

JSF, JSF_err = getJSF(average_mW, average_mW_err, grad, intercept)
