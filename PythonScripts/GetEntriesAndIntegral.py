import ROOT as r
from array import array
import math

r.TH1.SetDefaultSumw2(True)

#sample_name = ["ttpp8AF2","ttpp8m169","ttpp8m172","ttpp8m173","ttpp8m176","ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]
#JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]

sample_name = ["ttph7","ttaMCatNLO","ttpp8Var"]
#JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
JSF_values_string = ["1_00"]


print('Sample\tMean\tError')
for i in range(0,len(sample_name)):
    for j in range(0,len(JSF_values_string)):
        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist = histfile.Get("nominal_topjet_m")
        hist.SetDirectory(0)
        histfile.Close()
        print('%s' %(sample_name[i]))
        print('%s' %(sample_name[i]))

for i in range(0,len(sample_name)):
    for j in range(0,len(JSF_values_string)):

        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist1 = histfile.Get("nominal_topjet_m")
        hist1.SetDirectory(0)
        histfile.Close()
        print('%s' %(hist1.GetMean()))

        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist2 = histfile.Get("nominal_topjet_W_invMass")
        hist2.SetDirectory(0)
        hist2.GetXaxis().SetRangeUser(60,110) 
        histfile.Close()
        print('%s' %(hist2.GetMean()))

for i in range(0,len(sample_name)):
    for j in range(0,len(JSF_values_string)):
        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist1 = histfile.Get("nominal_topjet_m")
        hist1.SetDirectory(0)
        histfile.Close()
        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist2 = histfile.Get("nominal_topjet_W_invMass")
        hist2.SetDirectory(0)
        hist2.GetXaxis().SetRangeUser(60,110)
        histfile.Close()
        print('%s' %(hist1.GetMeanError()))
        print('%s' %(hist2.GetMeanError()))


print('\n\n\n')

for i in range(0,len(sample_name)):
    for j in range(0,len(JSF_values_string)):
        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist1 = histfile.Get("nominal_topjet_m")
        hist1.SetDirectory(0)
        histfile.Close()
        input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/All/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
        histfile = r.TFile.Open(input_histogram_path)
        hist2 = histfile.Get("nominal_topjet_W_invMass")
        hist2.SetDirectory(0)
        hist2.GetXaxis().SetRangeUser(60,110)
        histfile.Close()
        print('%s' %(hist1.GetStdDev()/math.sqrt(hist1.Integral())))
        print('%s' %(hist2.GetStdDev()/math.sqrt(hist2.Integral())))


""" print('\n\n\n\n\n\n')

input_histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample_name[i] + "/mJ/JSF_" + JSF_values_string[j] + "_" + sample_name[i] + ".root"
histfile = r.TFile.Open(input_histogram_path)
hist1 = histfile.Get("nominal")
hist1.SetDirectory(0)
histfile.Close()
print('%s' %(hist1.GetMean()))
 """
