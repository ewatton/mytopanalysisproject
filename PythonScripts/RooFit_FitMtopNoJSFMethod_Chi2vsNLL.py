# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np
from array import array
import math

def RooFit_Minimisation(filename,useDataLikeErrors,useNLL):

    print("\n-----------------------------------------------------------------------------------------\n")
    print("\nFile = %s" %(filename))
    print("\n-----------------------------------------------------------------------------------------\n")

    # Get mean top jet mass with error from histogram
    # ----------------------------------------------------------------------------------------------------------------

    # This is one of our observables, mean(top jet mass)
    meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)

    if (filename == "ttallpp8") or (filename == "ttpp8AF2"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/mJ/JSF_1_00_" + filename + ".root"
    elif (filename == "radiation_up") or (filename == "radiation_down") or (filename == "fsr_up") or (filename == "fsr_down"):
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_" + filename + ".root"
    else:
        histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + filename + "/All/JSF_1_00_" + filename + "_ALL.root"
    histfile = ROOT.TFile.Open(histogram_file_path)
    hist = histfile.Get("nominal_topjet_m")
    hist.SetDirectory(0)
    histfile.Close()
    nominalMean = hist.GetMean()
    meanTopJetMass.setVal(nominalMean)
    meanTopJetMass.setConstant(True)
    if (useDataLikeErrors == True):
        errorOnMean = hist.GetStdDev()/math.sqrt(hist.Integral())
    else:
        errorOnMean = hist.GetMeanError()

    # Set up model for mean top jet mass, mJ
    # ---------------------------------------------------------------------------------------------------------------

    # JSF
    JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)

    # Top quark mass
    mtop = ROOT.RooRealVar("mtop","mtop",172.5,150,200)

    # Fixed parameter in the equation for mean(top jet mass)
    amJ = ROOT.RooRealVar("amJ","amJ",168.832795) # FAST SIM
    # amJ = ROOT.RooRealVar("amJ","amJ",169.246951) # FULL SIM
    amJ.setConstant(True)

    # Fixed parameter relating mtop to mean(top jet mass)
    bmtop = ROOT.RooRealVar("bmtop","bmtop",0.476824) # FAST SIM
    # bmtop = ROOT.RooRealVar("bmtop","bmtop",0.457656) # FULL SIM

    bmtop.setConstant(True)

    # code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + B*(mtop - 172.5)
    meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2-172.5)",ROOT.RooArgList(amJ,bmtop,mtop))

    mJ_chi2 = ROOT.RooFormulaVar("chi2", "(@0-@1)*(@0-@1)/@2", ROOT.RooArgList(meanFormula, meanTopJetMass, ROOT.RooRealConstant.value(errorOnMean*errorOnMean)))
    Minimise_chi2 = ROOT.RooMinimizer(mJ_chi2)
    Minimise_chi2.minimize("Minuit")
    fitResult = Minimise_chi2.save()

    # Print results
    # -----------------------------------------------------------------------------------------------------------------

    fitResult.Print("v")
    print("\nInitial value of floating parameters")
    fitResult.floatParsInit().Print("s")
    print("\nFinal value of floating parameters")
    fitResult.floatParsFinal().Print("s")
    print("\nValue of constant parameters")
    fitResult.constPars().Print("s")

    return mtop.getVal(),mtop.getError()

# Main body of code
# -------------------------------------------------------------------------------------------

print('')
filenames = [
    "ttpp8AF2",
    "ttpp8m169",
    "ttpp8m171",
    "ttpp8m172",
    "ttpp8m173",
    "ttpp8m174",
    "ttpp8m176",
    "ttph713",
    "ttpp8MECOff",
    "ttpp8CR0",
    "ttpp8CR1",
    "ttpp8CR2",
    "ttpp8RTT",
    "ttnfaMCatNLO",
    "ttMCatNLOH713",
    "ttpp8m172_25",
    "ttpp8m172_75",
    "ttallpp8",
    "ttpp8Var",
    "radiation_up",
    "radiation_down",
    "fsr_up",
    "fsr_down",
    "ttph721",
]

###################
################### MC errors
###################

mtop_results_MC = []
mtop_error_results_MC = []

for i in range(0,len(filenames)):
    mtop_result, mtop_error_result = RooFit_Minimisation(filenames[i],False,False)
    mtop_results_MC.append(mtop_result)
    mtop_error_results_MC.append(mtop_error_result)

###################
################### Data errors (NLL)
###################

mtop_results_Data_NLL = []
mtop_error_results_Data_NLL = []

for i in range(0,len(filenames)):
    mtop_result, mtop_error_result = RooFit_Minimisation(filenames[i],True,True)
    mtop_results_Data_NLL.append(mtop_result)
    mtop_error_results_Data_NLL.append(mtop_error_result)

###################
################### Data errors (Chi2)
###################

mtop_results_Data_Chi2 = []
mtop_error_results_Data_Chi2 = []

for i in range(0,len(filenames)):
    mtop_result, mtop_error_result = RooFit_Minimisation(filenames[i],True,False)
    mtop_results_Data_Chi2.append(mtop_result)
    mtop_error_results_Data_Chi2.append(mtop_error_result)

strBeginning = [
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.00 & Fast & ",
    "Powheg+Pythia8, 169 GeV & Fast & ",
    "Powheg+Pythia8, 171 GeV & Fast & ",
    "Powheg+Pythia8, 172 GeV & Fast & ",
    "Powheg+Pythia8, 173 GeV & Fast & ",
    "Powheg+Pythia8, 174 GeV & Fast & ",
    "Powheg+Pythia8, 176 GeV & Fast & ",
    "Powheg+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, MEC turned off & Fast & ",
    "Powheg+Pythia8, CR0 & Fast & ",
    "Powheg+Pythia8, CR1 & Fast & ",
    "Powheg+Pythia8, CR2 & Fast & ",
    "Powheg+Pythia8, recoil-to-top & Fast & ",
    "aMC@NLO+Pythia8 & Fast & ",
    "aMC@NLO+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, 172.25 GeV & Fast & ",
    "Powheg+Pythia8, 172.75 GeV & Fast & ",
    "Powheg+Pythia8 & Full & ",
    "Powheg+Pythia8, varied $h_\text{damp}$ & Full & ",
    "Powheg+Pythia8, ISR_up & Full & ",
    "Powheg+Pythia8, ISR_down & Full & ",
    "Powheg+Pythia8, FSR_up & Full & ",
    "Powheg+Pythia8, FSR_down & Full & ",
    "Powheg+Herwig7.2.1 & Full & ",
]

outputfile = '../OutputTextFiles/MinimisationResults.txt'
f = open(outputfile, 'a')
for i in range(0,len(filenames)):
    f.write(strBeginning[i])
    f.write('%.4f$\pm$%.4f & %.4f$\pm$%.4f & %.4f$\pm$%.4f\\\\' %(mtop_results_MC[i],mtop_error_results_MC[i],mtop_results_Data_NLL[i],mtop_error_results_Data_NLL[i],mtop_results_Data_Chi2[i],mtop_error_results_Data_Chi2[i]))
    f.write('\n')
f.close()

print("\nScript has finished\n")
