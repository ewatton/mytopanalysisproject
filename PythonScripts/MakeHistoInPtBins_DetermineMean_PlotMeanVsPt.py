# Small macro to make some plots from the output of Jonathan's analysis
#
# To run you first need to setup Root (via the ATLAS software):
""" export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup 21.2.146,AnalysisBase """
#

# import ROOT classes
from ROOT import TFile, TH1, TH1D
import ROOT as r
from array import array
import copy
r.TH1.SetDefaultSumw2(True)
print('')

def FillHisto(tree_name,hist_name,lower_bound,upper_bound):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.weight_normalise # RECO LEVEL
        #weight = tree_name.mcEventNormWeight # PARTICLE LEVEL
    
        # access a variable
        #variable = tree_name.topjet_m
        variable = tree_name.topjet_Subjet_invMass
        #variable = tree_name.topjet_Subjet_invMass_lightjets
        #variable = tree_name.topjet_Subjet_invMass_ClosestToW

        # Access pT(top)
        pT_top = (tree_name.topjet_pt)/1000.0

        if (pT_top > lower_bound) and (pT_top < upper_bound):
            hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling histogram')
    print('')

def SaveHisto(hist_name,hist_save_name):

    print('Saving the following histogram as a png file:')
    print(hist_name)

    canvas = r.TCanvas("canvas")
    hist_name.Draw()
    canvas.SaveAs("../histogram_images/mW_TEST/" + hist_save_name + ".pdf")

    print('')

def MakeHisto():

    """ input_file_path_ttallpp8 = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/glasgow-ana/run/output/ttbarBoosted_PL/ttallpp8.root"
    input_file_paths = [input_file_path_ttallpp8]
    filenames = ["TEST2"]  """
    
    input_file_path_ttallpp8 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_01_24_ttbarSystematicSamplesFullSim/Systematics_JSF_1_00_mt_172_5/AllYears/ttbarBoosted/ttallpp8.root"
    input_file_path_ttph7 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_01_25_ttbarGeneratorSamplesFullSim/Generator_JSF_1_00_mt_172_5/AllYears/ttbarBoosted/ttph7.root"
    input_file_path_ttaMCatNLO = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_01_25_ttbarGeneratorSamplesFullSim/Generator_JSF_1_00_mt_172_5/AllYears/ttbarBoosted/ttaMCatNLO.root"
    input_file_path_ttpp8Var = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_01_25_ttbarGeneratorSamplesFullSim/Generator_JSF_1_00_mt_172_5/AllYears/ttbarBoosted/ttpp8Var.root"
    input_file_paths = [input_file_path_ttallpp8, input_file_path_ttph7, input_file_path_ttaMCatNLO, input_file_path_ttpp8Var]
    filenames = ["ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]

    bin_boundaries = array('f', [355.0, 381.0, 420.0, 478.0, 549.0, 633.0, 720.0, 836.0, 2000.0])
    bin_boundaries_string = ["355","381","420","478","549","633","720","836","2000"]

    
    for i in range(0,len(input_file_paths)):
        for j in range(1,len(bin_boundaries)):

            infile = TFile.Open(input_file_paths[i],"READ");
            trees = []
            for h in infile.GetListOfKeys():
                h = h.ReadObj()
                if (h.ClassName() == 'TTree' and h.GetName() == 'nominal'):
                    tree = h
                    trees.append(tree)

            print('')
            print(trees)
            print('')

            # open output file to hold histograms
            output_file_path = "../output_root_files/mW_TEST/output_mW_JSF_1_00_mt_172_5_" + filenames[i] + "_pT_" + bin_boundaries_string[j-1] + "_" + bin_boundaries_string[j] + ".root"
            
            outfile = TFile.Open(output_file_path,"RECREATE")

            # Turn on proper stat uncertainties in Root histograms
            TH1D.SetDefaultSumw2(True)

            # Make empty mW histograms
            histograms_mW = []
            for k in range(0,len(trees)):
                name = trees[k].GetName()
                hist = TH1D(name, "h_topjet_Subjets_invMass_largestPT; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0)
                #hist = TH1D(name, "h_topjet_m; Mass [GeV/c^{2}]; Events", 100, 135.0, 205.0)
                histograms_mW.append(hist)
            
            # Fill empty mW histograms
            for k in range(0,len(histograms_mW)):
                FillHisto(trees[k],histograms_mW[k],bin_boundaries[j-1],bin_boundaries[j])
                histograms_mW[k].Write()
                SaveHisto(histograms_mW[k],"mW_JSF_1_00_mt_172_5_" + filenames[i] + "_pT_" + bin_boundaries_string[j-1] + "_" + bin_boundaries_string[j])

            # SAVE HISTOGRAMS INTO OUTPUTFILE
            outfile.Close()

def MakePlot_withFits(n,y,yerr,identifier):

    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph = r.TH1D("graph","graph title",n,bin_boundaries)

    for i in range(1,n+1):
        graph.SetBinContent(i,y[i-1])
        graph.SetBinError(i,yerr[i-1])
    #r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("E1")

    # Changes the title of the scatter plot depending on what type of invariant mass calculation is being used
    if (identifier == "ttallpp8"): 
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        #graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Pythia")
        graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Pythia (reduced axis used for mW)")
    elif (identifier == "ttph7"):
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        #graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Herwig")
        graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Herwig (reduced axis used for mW)")
    elif (identifier == "ttaMCatNLO"):
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        #graph.SetTitle("Average reconstructed W boson mass vs pT(top) for aMC@NLO+Pythia")
        graph.SetTitle("Average reconstructed W boson mass vs pT(top) for aMC@NLO+Pythia (reduced axis used for mW)")
    elif (identifier == "ttpp8Var"):
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        #graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Pythia with varied hdamp parameter")
        graph.SetTitle("Average reconstructed W boson mass vs pT(top) for Powheg+Pythia with varied hdamp parameter (reduced axis used for mW)")

    else:
        graph.SetTitle("Default title")
    
    graph.GetYaxis().SetTitleOffset(1.2)
    graph.GetYaxis().SetLabelFont(62)

    # Applies a linear fit between the ranges specified
    #fit = r.TF1("fit","[0]+([1]*x)",bin_boundaries[0],bin_boundaries[n])
    fit = r.TF1("fit","[0]+([1]*x)+([2]*x*x)",bin_boundaries[0],bin_boundaries[n])
    graph.Fit("fit","R")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()
    par = fit.GetParameters()
    par_error = fit.GetParErrors()

    """ grad_error = fit.GetParError(1)
    intercept_error = fit.GetParError(0)
    latex.DrawText(0.65,0.15,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))
    latex.DrawText(0.65,0.10,"Gradient = %.3f +- %.3f" %(par[1],grad_error))
    latex.DrawText(0.65,0.05,"y-intercept = %.3f +- %.3f" %(par[0],intercept_error)) """

    latex.DrawText(0.55,0.25,"mW = A + B(pT(top)) + C(pT(top)^{2})")
    latex.DrawText(0.55,0.20,"A = %.3f +- %.3f" %(par[0],par_error[0]))
    latex.DrawText(0.55,0.15,"B = %.3f +- %.3f" %(par[1],par_error[1]))
    latex.DrawText(0.55,0.10,"C = %.6f +- %.6f" %(par[2],par_error[2]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))

    pad2.cd()
    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("pT(t^{had}) [GeV]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.987,1.013)
    line = r.TLine(bin_boundaries[0],1,bin_boundaries[n],1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")

    canvas_scatter.cd()
    canvas_scatter.Draw()

    # Sets the save file name depending on what type of invariant mass calculation is being used
    if (identifier == "ttallpp8"):
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttallpp8_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttallpp8_reduced_axis_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttallpp8_withFit_NoHigherPtBins.pdf")
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttallpp8_reduced_axis_withFit_NoHigherPtBins.pdf")
    elif (identifier == "ttph7"):
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttph7_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttph7_reduced_axis_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttph7_withFit_NoHigherPtBins.pdf")
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttph7_reduced_axis_withFit_NoHigherPtBins.pdf")
    elif (identifier == "ttaMCatNLO"):
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttaMCatNLO_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttaMCatNLO_reduced_axis_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttaMCatNLO_withFit_NoHigherPtBins.pdf")
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttaMCatNLO_reduced_axis_withFit_NoHigherPtBins.pdf")
    elif (identifier == "ttpp8Var"):
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttpp8Var_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttpp8Var_reduced_axis_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttpp8Var_withFit_NoHigherPtBins.pdf")
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_ttpp8Var_reduced_axis_withFit_NoHigherPtBins.pdf")
    else:
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_Default_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_Default_reduced_axis_withFit.pdf")
        #canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_Default_withFit_NoHigherPtBins.pdf")
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_pT_PL/mW_Vs_pT_Default_reduced_axis_withFit_NoHigherPtBins.pdf")

    print('')

def MakePlot(n,y,yerr,identifier):
    
    output_file_path = "../output_root_files/mt_Vs_pT_PL/output_mt_JSF_1_00_mt_172_5_" + identifier + ".root"
    #output_file_path = "../output_root_files/mt_Vs_pT/output_mt_JSF_1_00_mt_172_5_" + identifier + "_reduced_axis.root"
    #output_file_path = "../output_root_files/mt_Vs_pT/output_mt_JSF_1_00_mt_172_5_" + identifier + "_NoHigherPtBins.root"
    #output_file_path = "../output_root_files/mt_Vs_pT/output_mt_JSF_1_00_mt_172_5_" + identifier + "_reduced_axis_NoHigherPtBins.root"
    outfile = TFile.Open(output_file_path,"RECREATE")

    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph = r.TH1D("graph","graph title",n,bin_boundaries)

    for i in range(1,n+1):
        graph.SetBinContent(i,y[i-1])
        graph.SetBinError(i,yerr[i-1])
    #r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("E1")
    graph.GetYaxis().SetTitle("Average top jet mass [GeV]")

    # Changes the title of the scatter plot depending on what type of invariant mass calculation is being used
    if (identifier == "ttallpp8"): 
        graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Pythia")
        #graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Pythia (reduced axis used for mW)")
    elif (identifier == "ttph7"):
        graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Herwig")
        #graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Herwig (reduced axis used for mW)")
    elif (identifier == "ttaMCatNLO"):
        graph.SetTitle("Average top jet mass vs pT(top) for aMC@NLO+Pythia")
        #graph.SetTitle("Average top jet mass vs pT(top) for aMC@NLO+Pythia (reduced axis used for mW)")
    elif (identifier == "ttpp8Var"):
        graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Pythia with varied hdamp parameter")
        #graph.SetTitle("Average top jet mass vs pT(top) for Powheg+Pythia with varied hdamp parameter (reduced axis used for mW)")
    else:
        graph.SetTitle("Default title")
    
    graph.GetYaxis().SetTitleOffset(1.2)
    graph.GetYaxis().SetLabelFont(62)
    graph.GetXaxis().SetTitle("pT(t^{had}) [GeV]")
    canvas_scatter.Draw()

    print('')

    graph.Write()

def MakeOverlayedHisto():
    
    histogram_array = []

    for i in range(0,len(filenames)):

        histogram_file_path = "../output_root_files/mt_Vs_pT_PL/output_mt_JSF_1_00_mt_172_5_" + filenames[i] + ".root"
        #histogram_file_path = "../output_root_files/mt_Vs_pT/output_mW_JSF_1_00_mt_172_5_" + filenames[i] + "_reduced_axis.root"
        #histogram_file_path = "../output_root_files/mt_Vs_pT/output_mW_JSF_1_00_mt_172_5_" + filenames[i] + "_NoHigherPtBins.root"
        #histogram_file_path = "../output_root_files/mt_Vs_pT/output_mW_JSF_1_00_mt_172_5_" + filenames[i] + "_reduced_axis_NoHigherPtBins.root"
        histfile = r.TFile.Open(histogram_file_path)
        hist = histfile.Get("graph")
        hist.SetDirectory(0)
        histfile.Close()
        hist.SetStats(0)
        hist.SetLineWidth(2)
        hist.SetTitle("")
        histogram_array.append(hist)

    histogram_array[0].SetLineColor(r.kBlack)
    histogram_array[1].SetLineColor(r.kRed-3)
    histogram_array[2].SetLineColor(r.kCyan-3)
    histogram_array[3].SetLineColor(r.kGreen-3)

    canvas_overlayed = r.TCanvas("canvas_overlayed")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    histogram_array[0].Draw()
    
    histogram_array[0].SetTitle("m(top) vs pT(top) using full range for mW calculation")
    histogram_array[0].GetYaxis().SetRangeUser(166.5,174.5)
    """ histogram_array[0].SetTitle("m(top) vs pT(top) using reduced range for mW calculation")
    histogram_array[0].GetYaxis().SetRangeUser(80,84) """

    if len(histogram_array) > 1:
        for i in range(0,len(histogram_array)):
            if i == 3:
                
                histogram_array[i].SetTitle("m(top) vs pT(top) using full range for mW calculation")
                histogram_array[i].GetYaxis().SetRangeUser(166.5,174.5)
        
                """ histogram_array[i].SetTitle("m(top) vs pT(top) using reduced range for mW calculation")
                histogram_array[i].GetYaxis().SetRangeUser(80,84) """

                histogram_array[i].Draw("same")

    legend_array = ["Powheg+Pythia","Powheg+Herwig","aMC@NLO+Pythia","Powheg+Pythia, with varied hdamp parameter"]

    #legend = r.TLegend(0.5,0.65,0.89,0.89)
    #legend = r.TLegend(0.5,0.05,0.89,0.3)

    legend = r.TLegend(0.6,0.05,0.89,0.2)
    #legend = r.TLegend(0.6,0.75,0.89,0.89)
    for i in range(0,len(legend_array)):
        if (i == 3) or (i == 0):
            legend.AddEntry(histogram_array[i], legend_array[i])
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    pad2.cd()
    r.gStyle.SetOptStat(0)
    #subplot1 = copy.deepcopy(histogram_array[1])
    #subplot2 = copy.deepcopy(histogram_array[2])
    subplot1 = copy.deepcopy(histogram_array[3])
    subplot1.Divide(histogram_array[0])
    #subplot2.Divide(histogram_array[0])
    #subplot3.Divide(histogram_array[0])
    subplot1.SetTitle("")
    subplot1.GetXaxis().SetLabelFont(63)
    subplot1.GetXaxis().SetLabelSize(10)
    subplot1.GetXaxis().SetTitle("pT(t^{had}) [GeV]")
    subplot1.GetXaxis().SetTitleSize(0.11)
    subplot1.GetXaxis().SetTitleOffset(0.8)
    subplot1.GetYaxis().SetLabelFont(63)
    subplot1.GetYaxis().SetLabelSize(10)
    #subplot1.GetYaxis().SetTitle("MC generator / Powheg+Pythia")
    #subplot1.GetYaxis().SetTitle("Powheg+Herwig/ Powheg+Pythia")
    #subplot1.GetYaxis().SetTitle("aMC@NLO+Pythia / Powheg+Pythia")
    subplot1.GetYaxis().SetTitle("Powheg+Pythia, varied hdamp / Powheg+Pythia")
    subplot1.GetYaxis().SetTitleOffset(0.5)
    subplot1.GetYaxis().SetTitleSize(0.05)
    subplot1.GetYaxis().SetNdivisions(207)
    #subplot1.GetYaxis().SetRangeUser(0.987,1.013)
    subplot1.GetYaxis().SetRangeUser(0.995,1.005)
    subplot1.Draw("E1")
    #subplot2.Draw("E1 same")
    #subplot3.Draw("E1 same")
    line = r.TLine(355,1,2000,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.cd()
    canvas_overlayed.Draw()
    canvas_overlayed.SaveAs("../histogram_images/mt_Vs_pT/overlayed_mt_Vs_pT_full_PPvsPH_NoHigherPtBins.pdf")
    #canvas_overlayed.SaveAs("../histogram_images/mt_Vs_pT/overlayed_mt_Vs_pT_reduced_PPvsPH_NoHigherPtBins.pdf")


######
###### Main body of code
######

# Make and save histograms
MakeHisto()

filenames = ["ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]
bin_boundaries = array('f', [355.0, 381.0, 420.0, 478.0, 549.0, 633.0, 720.0, 836.0, 2000.0])
bin_boundaries_string = ["355","381","420","478","549","633","720","836","2000"]

""" bin_boundaries = array('f', [355.0, 381.0, 420.0, 478.0, 549.0, 633.0, 720.0])
bin_boundaries_string = ["355","381","420","478","549","633","720"] """
number_of_bins = len(bin_boundaries)-1

""" for i in range(0,len(filenames)):

    mW_values = array('f')
    mW_errors = array('f')

    for j in range(1,len(bin_boundaries)):

        histogram_file_path = "../output_root_files/mt_Vs_pT_PL/output_mt_JSF_1_00_mt_172_5_" + filenames[i] + "_pT_" + bin_boundaries_string[j-1] + "_" + bin_boundaries_string[j] + ".root"
        histfile = r.TFile.Open(histogram_file_path)
        hist = histfile.Get("nominal")
        hist.SetDirectory(0)
        histfile.Close()
        #hist.GetXaxis().SetRangeUser(60,110)
        mW_values.append(hist.GetMean())
        mW_errors.append(hist.GetMeanError())

    #MakePlot_withFits(number_of_bins,mW_values,mW_errors,filenames[i])
    MakePlot(number_of_bins,mW_values,mW_errors,filenames[i]) """
    
#MakeOverlayedHisto()

print('')
print('Script has finished!')
print('')
