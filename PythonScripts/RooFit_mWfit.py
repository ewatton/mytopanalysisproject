# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np

# Set up model
# ---------------------------------------------------------------------------------------------------------------

useDataLikeErrors = True # Choose whether to use data-like errors (True) or not (False)

# Declare observable mW
mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)

# Delcare JSF and constants in relation mean1 = A_2 + B_2*JSF
JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)
A_2 = ROOT.RooRealVar("A_2","A_2",13.4973)
B_2 = ROOT.RooRealVar("B_2","B_2",70.3441)

# Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and their parameters
mean1 = ROOT.RooFormulaVar("mean1","mean of first gaussian","@0 + @1*@2", ROOT.RooArgList(A_2,B_2,JSF)) # Actual 83.8226
mean2 = ROOT.RooRealVar("mean2", "mean of second gaussian", 81.6166)
sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.9132)
sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.750)
 
gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
 
# Sum the signal components into a composite signal p.d.f.
gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0, 1.0)
combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))
 
# Import TH1D
# ----------------------------------------------------------------------------------------------------------------
 
histogram_file_path = "../output_root_files/mW_distributions_varied_JSF/After_setting_Sumw2(True)/output_mW_JSF_1_00_mt_172_5_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
hist = histfile.Get("nominal")
hist.SetDirectory(0)
histfile.Close()

if useDataLikeErrors == True: # If using data-like errors
    for bin_number in range(0,201):
        hist.SetBinError(bin_number,np.sqrt(hist.GetBinContent(bin_number)))

dataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist, 1.0)
 
# Construct a chi^2 of the data and the model.
# ---------------------------------------------------------------------------------------------------------------

ll = ROOT.RooLinkedList()
cmd = ROOT.RooFit.Save()
ll.Add(cmd)
fitResult = combinedGauss.chi2FitTo(dataHist, ll)

# Print fit results
# ---------------------------------------------------------------------------------------------------------------

fitResult.Print("v")
print("\nInitial value of floating parameters")
fitResult.floatParsInit().Print("s")
print("\nFinal value of floating parameters")
fitResult.floatParsFinal().Print("s")
print("\nValue of constant parameters")
fitResult.constPars().Print("s")

# Plot and save the fit to the data
# ---------------------------------------------------------------------------------------------------------------

mWframe = mW.frame()
dataHist.plotOn(mWframe,ROOT.RooFit.MarkerSize(0.05))
combinedGauss.plotOn(mWframe,ROOT.RooFit.LineColor(2),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(3))
mWframe.GetYaxis().SetTitle("Entries")
mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
canvas = ROOT.TCanvas("canvas")
mWframe.Draw()
canvas.SaveAs("../histogram_images/mW_distributions_varied_JSF/Fitting/TwoGaussians/RooFit/RooFit_test.pdf")