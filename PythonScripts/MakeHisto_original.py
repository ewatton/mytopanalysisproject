# Small macro to make some plots from the output of Jonathan's analysis
#
# To run you first need to setup Root (via the ATLAS software):
""" export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup 21.2.146,AnalysisBase """
#

# import ROOT classes
from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

print('')

def FillHisto_mJ(tree_name,hist_name):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.weight_normalise
    
        # access a variable - MUST BE CHANGED TO SPECIFIC VARIABLE WANTED
        variable = tree_name.topjet_m # topjet_m

        # fill histogram with entires with weight
        hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling mJ histogram')
    print('')

def FillHisto_mJ_PL(tree_name,hist_name):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.mcEventNormWeight
    
        # access a variable - MUST BE CHANGED TO SPECIFIC VARIABLE WANTED
        variable = tree_name.topjet_m # topjet_m

        # fill histogram with entires with weight
        hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling mJ histogram')
    print('')

def FillHisto_mW(tree_name,hist_name):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.weight_normalise
    
        # access a variable
        variable = tree_name.topjet_Subjet_invMass
        #variable = tree_name.topjet_Subjet_invMass_lightjets
        #variable = tree_name.topjet_Subjet_invMass_ClosestToW

        # fill histogram with entires with weight
        hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling mW histogram')
    print('')

def FillHisto_weights(tree_name,hist_name):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.weight_normalise

        # fill histogram with entires with weight
        hist_name.Fill(weight)
    
    print('Finished filling weight histogram')
    print('')

def FillHisto_mW_PL(tree_name,hist_name):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.mcEventNormWeight
    
        # access a variable
        variable = tree_name.topjet_Subjet_invMass
        #variable = tree_name.topjet_Subjet_invMass_lightjets
        #variable = tree_name.topjet_Subjet_invMass_ClosestToW

        # fill histogram with entires with weight
        hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling mW histogram')
    print('')


def SaveHisto(hist_name,hist_save_name):

    print('Saving the following histogram as a png file:')
    print(hist_name)

    canvas = r.TCanvas("canvas")
    hist_name.Draw()
    canvas.SaveAs("../histogram_images/Weight_Distributions/ttallpp8/" + hist_save_name + ".pdf")

    print('')

def MakeHisto(iterator):

    input_file_path_JSF_0_97 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_16_ttbarttppmJSFvariedFastSim/JSF_1_00_ttppm/AllYears/ttbarBoosted/ttpp8m169.root"
    input_file_path_JSF_0_98 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_16_ttbarttppmJSFvariedFastSim/JSF_1_00_ttppm/AllYears/ttbarBoosted/ttpp8m176.root"
    input_file_path_JSF_0_99 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_06_01_ttbarNominalFastSim/JSF_1_00_ttpp8AF2/AllYears/ttbarBoosted/ttpp8AF2.root"
    #input_file_path_JSF_1_00 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_25_ttbarNominalFullSim/JSF_1_00_ttallpp8/AllYears/ttbarBoosted/ttallpp8.root"
    #input_file_path_JSF_1_01 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_16_ttbarttppmJSFvariedFastSim/JSF_1_01_ttppm/AllYears/ttbarBoosted/ttpp8m169.root"
    #input_file_path_JSF_1_02 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_16_ttbarttppmJSFvariedFastSim/JSF_1_02_ttppm/AllYears/ttbarBoosted/ttpp8m169.root"
    #input_file_path_JSF_1_03 = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_05_16_ttbarttppmJSFvariedFastSim/JSF_1_03_ttppm/AllYears/ttbarBoosted/ttpp8m169.root"
    input_file_paths = [input_file_path_JSF_0_97,input_file_path_JSF_0_98,input_file_path_JSF_0_99]

    output_file_path_JSF_0_97 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m169/mJ/JSF_1_00_ttpp8m169.root"
    output_file_path_JSF_0_98 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m176/mJ/JSF_1_00_ttpp8m176.root"
    output_file_path_JSF_0_99 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8AF2/mJ/JSF_1_00_ttpp8AF2.root"
    #output_file_path_JSF_1_00 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttallpp8/JetResponse/JSF_1_00_ttallpp8.root"
    #output_file_path_JSF_1_01 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m169/mW/JSF_1_01_ttpp8m169.root"
    #output_file_path_JSF_1_02 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m169/mW/JSF_1_02_ttpp8m169.root"
    #output_file_path_JSF_1_03 = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m169/mW/JSF_1_03_ttpp8m169.root"
    output_file_paths = [output_file_path_JSF_0_97,output_file_path_JSF_0_98,output_file_path_JSF_0_99]

    
    #JSF = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
    JSF = ["1_00"]

    for i in range(0,len(input_file_paths)):
        
        #JSF_value = JSF[i]

        print('')
        print('Grabbing the TTree from file %s' %(input_file_paths[i]))
        print('')
        
        infile = TFile.Open(input_file_paths[i],"READ");

        # Get nominal tree
        trees = []
        for h in infile.GetListOfKeys():
            h = h.ReadObj()
            if (h.ClassName() == 'TTree'):
                tree = h
                trees.append(tree)

        print('')
        print(trees)
        print('')

        # open output file to hold histograms
        outfile = TFile.Open(output_file_paths[i],"RECREATE")

        # Turn on proper stat uncertainties in Root histograms
        TH1D.SetDefaultSumw2(True)

        """ histograms_list = []
        histogram_savenames = []
        for i in range(0,len(trees)):
            name = trees[i].GetName()
            hist = TH1D(name, "h_weights; Weight; Events", 100, -1, 1)
            histograms_list.append(hist)
            histogram_savenames.append(name)

        for j in range(0,len(histograms_list)):
            FillHisto_weights(trees[j],histograms_list[j])
            histograms_list[j].Write()
            SaveHisto(histograms_list[j],"JSF_" + JSF_value + "_mt_172_5_ttallpp8") """

        # Make empty mJ histograms
        histograms_mJ = []
        for i in range(0,len(trees)):
            name = trees[i].GetName()
            hist = TH1D(name, "h_topjet_m; Mass [GeV/c^{2}]; Events", 50, 120.0, 220.0) # 100, 135.0, 205.0
            histograms_mJ.append(hist)

        """ # Make empty mW histograms
        histograms_mW = []
        for j in range(0,len(trees)):
            #name = "mW"
            name = trees[j].GetName()
            #name2 = name + "_no_neg"
            #name3 = name2 + "_peak"
            hist = TH1D(name, "h_topjet_Subjets_invMass_largestPT; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0)
            #hist = TH1D(name, "h_topjet_Subjets_invMass_largestPT; Mass [GeV/c^{2}]; Events", 200, -30.0, 200.0);
            #hist2 = TH1D(name2, "h_topjet_Subjets_invMass_largestPT; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0);
            #hist3 = TH1D(name3, "h_topjet_Subjets_invMass_largestPT; Mass [GeV/c^{2}]; Events", 100, 60.0, 100.0);
            #hist = TH1D(name, "h_topjet_Subjets_invMass_alllightjets; Mass [GeV/c^{2}]; Events", 200, -30.0, 200.0);
            #hist2 = TH1D(name2, "h_topjet_Subjets_invMass_alllightjets; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0);
            #hist3 = TH1D(name3, "h_topjet_Subjets_invMass_alllightjets; Mass [GeV/c^{2}]; Events", 100, 60.0, 100.0);
            #hist = TH1D(name, "h_topjet_Subjets_invMass_ClosestToW; Mass [GeV/c^{2}]; Events", 200, -30.0, 200.0);
            #hist2 = TH1D(name2, "h_topjet_Subjets_invMass_ClosestToW; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0);
            #hist3 = TH1D(name3, "h_topjet_Subjets_invMass_ClosestToW; Mass [GeV/c^{2}]; Events", 100, 60.0, 100.0);
            histograms_mW.append(hist)
            #histograms.append(hist2)
            #histograms.append(hist3) """

        # Fill empty mJ histograms
        for j in range(0,len(histograms_mJ)):
            FillHisto_mJ(trees[j],histograms_mJ[j])
            histograms_mJ[j].Write()
        

        """ # Fill empty mW histograms
        for j in range(0,len(histograms_mW)):
            if i > 3:
                FillHisto_mW_PL(trees[j],histograms_mW[j])
                histograms_mW[j].Write()
            else:
                FillHisto_mW(trees[j],histograms_mW[j])
                histograms_mW[j].Write() """

        """ # Fill empty mW histograms
        for j in range(0,len(histograms_mW)):
            FillHisto_mW(trees[j],histograms_mW[j])
            histograms_mW[j].Write() """

        # SAVE HISTOGRAMS INTO OUTPUTFILE
        outfile.Close()

######
###### Main body of code
######

MakeHisto(" ")

print('')
print('Histograms have been saved to the output_root_files folder, and also as images in the histogram_images folder')
print('')
