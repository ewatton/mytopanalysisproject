from optparse import OptionParser

def check(inputFilename):

    outputfile = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/OutputTextFiles/checkMergedOutput.txt"

    file1 = open(inputFilename, 'r')
    Lines = file1.readlines()
    f = open(outputfile, 'a')
    goodFile = True
    for line in Lines:
        if "Warning in <TTreeReader::SetEntryBase()>: The TTree / TChain has an associated TEntryList. TTreeReader ignores TEntryLists unless you construct the TTreeReader passing a TEntryList." in line:
            continue
        else:
            goodFile = False
    
    if goodFile == True:
        f.write('OK:\t')
    else:
        f.write('ERROR:\t')
    f.write('%s' %inputFilename)
    f.write('\n')
    f.close()
            


def main():
  
    parser = OptionParser()
    parser.add_option("-i", "--inputFilename",action="store", type="string", dest="inputFilename")
    (options, args) = parser.parse_args()   
  
    print("\nChecking the following log file:\n")
    print(options.inputFilename)
    check(options.inputFilename)
    print("\ndone\n")

if __name__ == "__main__":
  main()