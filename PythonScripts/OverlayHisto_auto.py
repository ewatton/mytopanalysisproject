# Macro that will overlay histograms and save them to PDFs
# First created by Elliot Watton - 11/10/2021
# Last editted by Elliot Watton - 11/01/2022

import ROOT as r
r.TH1.SetDefaultSumw2(True)
print('')

# samplesFullSim = ["ttpp8Var","ttph721"]
# samplesFastSim = ["ttph713","ttnfaMCatNLO","ttpp8CR","ttpp8RTT"]

# histogram_types = [ 
        # "_topjet_pt",
        # "_topjet_eta",
        # "_topjet_phi",
        # "_topjet_m",
        # "_topjet_m_50bins",
        # "_topjet_y",
        # "_topjet_nSubjets",
        # "_topjet_avgSubjet_pt",
        # "_topjet_avgSubjet_eta",
        # "_topjet_W_invMass",
        # "_topjet_nbtaggedSubjets",
        # "_topjet_Subjet_eta",
        # "_topjet_Subjet_phi",
        # "_topjet_Subjet_pT",
        # "_topjet_Subjet_mass",
        # "_topjet_Subjet_type",
        # "_topjet_Subjet_DeltaR",
        # "_topjet_Subjet_isMatched",
        # "_topjet_Subjet_jetresponse",
        # "_topjet_Subjet_bjetresponse",
        # "_topjet_Subjet_lightjetresponse",
        # "_topjet_m_50bins",
        # "_leadadd_top_deltaR",
        # "_leadadd_top_deltaPhi",
        # "_subleadadd_hadtop_deltaR",
        # "_subleadadd_hadtop_deltaPhi", 
        # "_tmm_leadadd_leptop_deltaR",
        # "_tmm_leadadd_leptop_deltaPhi",
        # "_tmm_subleadadd_leptop_deltaR",
        # "_tmm_subleadadd_leptop_deltaPhi"
        # ]

# for i in range(0,len(histogram_types)):

#     #####################################################
#     ##################################################### Flavour vs nominal
#     #####################################################

#     histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root")
#     histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8RTT/All/JSF_1_00_ttpp8RTT.root")
#     histfile3 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttph713/All/JSF_1_00_ttph713.root")

#     hist1 = histfile1.Get("nominal" + histogram_types[i])
#     hist1.SetDirectory(0)
#     histfile1.Close()
#     hist1.SetStats(0)
#     hist1.SetLineColor(r.kBlack)
#     hist1.SetLineWidth(2)
#     hist1.GetYaxis().SetTitle("Normalised events")
#     hist1.SetTitle("")

#     hist2 = histfile2.Get("nominal" + histogram_types[i])
#     hist2.SetDirectory(0)
#     histfile2.Close()
#     hist2.SetStats(0)
#     hist2.SetLineColor(r.kBlue)
#     hist2.SetLineWidth(2)
#     hist2.GetYaxis().SetTitle("Normalised events")
#     hist2.SetTitle("")

#     hist3 = histfile3.Get("nominal" + histogram_types[i])
#     hist3.SetDirectory(0)
#     histfile3.Close()
#     hist3.SetStats(0)
#     hist3.SetLineColor(r.kRed)
#     hist3.SetLineWidth(2)
#     hist3.GetYaxis().SetTitle("Normalised events")
#     hist3.SetTitle("")
    
#     hist1.Scale(1./hist1.Integral(), "width")
#     hist2.Scale(1./hist2.Integral(), "width")
#     hist3.Scale(1./hist3.Integral(), "width")

#     minimums = []
#     maximums = []
#     minimums.append(hist1.GetMinimum())
#     minimums.append(hist2.GetMinimum())
#     minimums.append(hist3.GetMinimum())
#     maximums.append(hist1.GetMaximum())
#     maximums.append(hist2.GetMaximum())
#     maximums.append(hist3.GetMaximum())
#     globalMinY = min(minimums)
#     globalMaxY = max(maximums)

#     # Make a canvas that the histograms will be overlayed onto
#     canvas_overlayed = r.TCanvas("canvas_overlayed")
#     pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
#     pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
#     pad1.SetBottomMargin(0.00001)
#     pad1.SetBorderMode(0)
#     pad2.SetTopMargin(0.00001)
#     pad2.SetBottomMargin(0.2)
#     pad2.SetBorderMode(0)
#     pad1.Draw()
#     pad2.Draw()
#     pad1.cd()

#     hist1.Draw()
#     hist2.Draw("same")
#     hist3.Draw("same")

#     hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     hist3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

#     if "deltaR" in histogram_types[i]:
#         latex = r.TLatex()
#         latex.SetNDC()
#         latex.SetTextSize(0.03)
#         latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#         latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#         latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
#         latex.DrawText(0.56,0.85,"Powheg+Pythia8, nominal mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
#         latex.DrawText(0.56,0.81,"Powheg+Pythia8, recoil-to-top mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))
#         latex.DrawText(0.56,0.77,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist3.GetMean(), hist3.GetMeanError()))

#         legend = r.TLegend(0.66,0.68,0.88,0.48)
#         legend.AddEntry(hist1, "Powheg+Pythia8, nominal")
#         legend.AddEntry(hist2, "Powheg+Pythia8, recoil-to-top")
#         legend.AddEntry(hist3, "Powheg+Herwig7.1.3")
#         legend.SetLineWidth(0)
#         legend.SetFillStyle(0)
#         legend.Draw("same")
#     else:
#         latex = r.TLatex()
#         latex.SetNDC()
#         latex.SetTextSize(0.03)
#         latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#         latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#         latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
#         latex.DrawText(0.56,0.15,"Powheg+Pythia8, nominal mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
#         latex.DrawText(0.56,0.10,"Powheg+Pythia8, recoil-to-top mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))
#         latex.DrawText(0.56,0.05,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist3.GetMean(), hist3.GetMeanError()))

#         legend = r.TLegend(0.1,0.05,0.3,0.15)
#         legend.AddEntry(hist1, "Powheg+Pythia8, nominal")
#         legend.AddEntry(hist2, "Powheg+Pythia8, recoil-to-top")
#         legend.AddEntry(hist3, "Powheg+Herwig7.1.3")
#         legend.SetLineWidth(0)
#         legend.SetFillStyle(0)
#         legend.Draw("same")

#     pad2.cd()
#     r.gStyle.SetOptStat(0)

#     subplot1 = hist2.Clone()
#     subplot2 = hist3.Clone()

#     subplot1.Divide(hist1)
#     subplot1.SetTitle("")
#     subplot1.GetXaxis().SetLabelFont(63)
#     subplot1.GetXaxis().SetLabelSize(10)
#     subplot1.GetXaxis().SetTitle(histogram_types[i])
#     subplot1.GetXaxis().SetTitleSize(0.11)
#     subplot1.GetXaxis().SetTitleOffset(0.8)
#     subplot1.GetYaxis().SetLabelFont(63)
#     subplot1.GetYaxis().SetLabelSize(10)
#     subplot1.GetYaxis().SetTitle("MC setup / nominal")
#     subplot1.GetYaxis().SetTitleOffset(0.38)
#     subplot1.GetYaxis().SetTitleSize(0.07)
#     subplot1.GetYaxis().SetNdivisions(207)
#     subplot1.GetYaxis().SetRangeUser(0.75,1.25)
#     subplot1.Draw("E1")

#     subplot2.Divide(hist1)
#     subplot2.SetTitle("")
#     subplot2.GetXaxis().SetLabelFont(63)
#     subplot2.GetXaxis().SetLabelSize(10)
#     subplot2.GetXaxis().SetTitle(histogram_types[i])
#     subplot2.GetXaxis().SetTitleSize(0.11)
#     subplot2.GetXaxis().SetTitleOffset(0.8)
#     subplot2.GetYaxis().SetLabelFont(63)
#     subplot2.GetYaxis().SetLabelSize(10)
#     subplot2.GetYaxis().SetTitle("MC setup / nominal")
#     subplot2.GetYaxis().SetTitleOffset(0.38)
#     subplot2.GetYaxis().SetTitleSize(0.07)
#     subplot2.GetYaxis().SetNdivisions(207)
#     subplot2.GetYaxis().SetRangeUser(0.75,1.25)
#     subplot2.Draw("same")

#     line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
#     line.SetLineColor(r.kBlack)
#     line.SetLineWidth(2)
#     line.Draw("same")
#     canvas_overlayed.cd()
#     canvas_overlayed.Draw()

#     canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/NewObservable/2022_11_29" + histogram_types[i] + ".pdf")

    #####################################################
    ##################################################### Different MC setups vs Nominal
    #####################################################

    # for j in range(0,len(samplesFastSim)):

    #     histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_Nominal.root")
    #     histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + samplesFastSim[j] + "/All/JSF_1_00_" + samplesFastSim[j] + ".root")

    #     hist1 = histfile1.Get("nominal" + histogram_types[i])
    #     hist1.SetDirectory(0)
    #     histfile1.Close()
    #     hist1.SetStats(0)
    #     hist1.SetLineColor(r.kBlue)
    #     hist1.SetLineWidth(2)
    #     hist1.GetYaxis().SetTitle("Normalised events")
    #     hist1.SetTitle("")

    #     hist2 = histfile2.Get("nominal" + histogram_types[i])
    #     hist2.SetDirectory(0)
    #     histfile2.Close()
    #     hist2.SetStats(0)
    #     hist2.SetLineColor(r.kRed)
    #     hist2.SetLineWidth(2)
    #     hist2.GetYaxis().SetTitle("Normalised events")
    #     hist2.SetTitle("")

    #     if ("_topjet_W_invMass" in histogram_types[i]):
    #         hist1.GetXaxis().SetRangeUser(60,110)
    #         hist2.GetXaxis().SetRangeUser(60,110)

    #     hist1.Scale(1./hist1.Integral(), "width")
    #     hist2.Scale(1./hist2.Integral(), "width")

    #     # Make a canvas that the histograms will be overlayed onto
    #     canvas_overlayed = r.TCanvas("canvas_overlayed")
    #     pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    #     pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    #     pad1.SetBottomMargin(0.00001)
    #     pad1.SetBorderMode(0)
    #     pad2.SetTopMargin(0.00001)
    #     pad2.SetBottomMargin(0.2)
    #     pad2.SetBorderMode(0)
    #     pad1.Draw()
    #     pad2.Draw()
    #     pad1.cd()

    #     minimums = []
    #     maximums = []
    #     minimums.append(hist1.GetMinimum())
    #     maximums.append(hist1.GetMaximum())
    #     minimums.append(hist2.GetMinimum())
    #     maximums.append(hist2.GetMaximum())
    #     globalMinY = min(minimums)
    #     globalMaxY = max(maximums)

    #     hist1.Draw()
    #     hist2.Draw("same")

    #     hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
    #     hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

    #     legend = r.TLegend(0.11,0.58,0.4,0.68)
    #     legend.AddEntry(hist1, "Nominal")
    #     legend.AddEntry(hist2, samplesFastSim[j])
    #     legend.SetLineWidth(0)
    #     legend.SetFillStyle(0)
    #     legend.Draw("same")

    #     latex = r.TLatex()
    #     latex.SetNDC()
    #     latex.SetTextSize(0.03)
    #     latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    #     latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    #     latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    #     latex.DrawText(0.56,0.85,"Nominal mean = %.4f +- %.4f" %(hist1.GetMean(), hist1.GetMeanError()))
    #     latex.DrawText(0.56,0.81,samplesFastSim[j] + " mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))


    #     pad2.cd()
    #     r.gStyle.SetOptStat(0)
    #     subplot1 = hist2.Clone()

    #     subplot1.Divide(hist1)
    #     subplot1.SetTitle("")
    #     subplot1.GetXaxis().SetLabelFont(63)
    #     subplot1.GetXaxis().SetLabelSize(10)
    #     subplot1.GetXaxis().SetTitle("Response")
    #     subplot1.GetXaxis().SetTitleSize(0.11)
    #     subplot1.GetXaxis().SetTitleOffset(0.8)
    #     subplot1.GetYaxis().SetLabelFont(63)
    #     subplot1.GetYaxis().SetLabelSize(10)
    #     subplot1.GetYaxis().SetTitle("MC setup / Nominal")
    #     subplot1.GetYaxis().SetTitleOffset(0.38)
    #     subplot1.GetYaxis().SetTitleSize(0.07)
    #     subplot1.GetYaxis().SetNdivisions(207)
    #     subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    #     subplot1.Draw("E1")

    #     if ("_topjet_W_invMass" in histogram_types[i]):
    #         line = r.TLine(60,1,110,1)
    #     else:
    #         line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
    #     line.SetLineColor(r.kBlue)
    #     line.SetLineWidth(2)
    #     line.Draw("same")
    #     canvas_overlayed.cd()
    #     canvas_overlayed.Draw()

    #     canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_10_26" + histogram_types[i] + "_" + samplesFastSim[j] + "_vs_nominal.pdf")
    
    # for j in range(0,len(samplesFullSim)):

    #     histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8_Nominal.root")
    #     histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + samplesFullSim[j] + "/All/JSF_1_00_" + samplesFullSim[j] + ".root")

    #     hist1 = histfile1.Get("nominal" + histogram_types[i])
    #     hist1.SetDirectory(0)
    #     histfile1.Close()
    #     hist1.SetStats(0)
    #     hist1.SetLineColor(r.kBlue)
    #     hist1.SetLineWidth(2)
    #     hist1.GetYaxis().SetTitle("Normalised events")
    #     hist1.SetTitle("")

    #     hist2 = histfile2.Get("nominal" + histogram_types[i])
    #     hist2.SetDirectory(0)
    #     histfile2.Close()
    #     hist2.SetStats(0)
    #     hist2.SetLineColor(r.kRed)
    #     hist2.SetLineWidth(2)
    #     hist2.GetYaxis().SetTitle("Normalised events")
    #     hist2.SetTitle("")

    #     if ("_topjet_W_invMass" in histogram_types[i]):
    #         hist1.GetXaxis().SetRangeUser(60,110)
    #         hist2.GetXaxis().SetRangeUser(60,110)

    #     hist1.Scale(1./hist1.Integral(), "width")
    #     hist2.Scale(1./hist2.Integral(), "width")

    #     # Make a canvas that the histograms will be overlayed onto
    #     canvas_overlayed = r.TCanvas("canvas_overlayed")
    #     pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    #     pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    #     pad1.SetBottomMargin(0.00001)
    #     pad1.SetBorderMode(0)
    #     pad2.SetTopMargin(0.00001)
    #     pad2.SetBottomMargin(0.2)
    #     pad2.SetBorderMode(0)
    #     pad1.Draw()
    #     pad2.Draw()
    #     pad1.cd()

    #     minimums = []
    #     maximums = []
    #     minimums.append(hist1.GetMinimum())
    #     maximums.append(hist1.GetMaximum())
    #     minimums.append(hist2.GetMinimum())
    #     maximums.append(hist2.GetMaximum())
    #     globalMinY = min(minimums)
    #     globalMaxY = max(maximums)

    #     hist1.Draw()
    #     hist2.Draw("same")

    #     hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
    #     hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

    #     legend = r.TLegend(0.11,0.58,0.4,0.68)
    #     legend.AddEntry(hist1, "Nominal")
    #     legend.AddEntry(hist2, samplesFullSim[j])
    #     legend.SetLineWidth(0)
    #     legend.SetFillStyle(0)
    #     legend.Draw("same")

    #     latex = r.TLatex()
    #     latex.SetNDC()
    #     latex.SetTextSize(0.03)
    #     latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    #     latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    #     latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    #     latex.DrawText(0.56,0.85,"Nominal mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
    #     latex.DrawText(0.56,0.81,samplesFullSim[j] + " mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))


    #     pad2.cd()
    #     r.gStyle.SetOptStat(0)
    #     subplot1 = hist2.Clone()

    #     subplot1.Divide(hist1)
    #     subplot1.SetTitle("")
    #     subplot1.GetXaxis().SetLabelFont(63)
    #     subplot1.GetXaxis().SetLabelSize(10)
    #     subplot1.GetXaxis().SetTitle("Response")
    #     subplot1.GetXaxis().SetTitleSize(0.11)
    #     subplot1.GetXaxis().SetTitleOffset(0.8)
    #     subplot1.GetYaxis().SetLabelFont(63)
    #     subplot1.GetYaxis().SetLabelSize(10)
    #     subplot1.GetYaxis().SetTitle("MC setup / Nominal")
    #     subplot1.GetYaxis().SetTitleOffset(0.38)
    #     subplot1.GetYaxis().SetTitleSize(0.07)
    #     subplot1.GetYaxis().SetNdivisions(207)
    #     subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    #     subplot1.Draw("E1")

    #     if ("_topjet_W_invMass" in histogram_types[i]):
    #         line = r.TLine(60,1,110,1)
    #     else:
    #         line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
    #     line.SetLineColor(r.kBlue)
    #     line.SetLineWidth(2)
    #     line.Draw("same")
    #     canvas_overlayed.cd()
    #     canvas_overlayed.Draw()

    #     canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_10_26" + histogram_types[i] + "_" + samplesFullSim[j] + "_vs_nominal.pdf")

    #####################################################
    ##################################################### ttph713 vs ttph721
    #####################################################

    # histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root")
    # histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root")

    # hist1 = histfile1.Get("nominal" + histogram_types[i])
    # hist1.SetDirectory(0)
    # histfile1.Close()
    # hist1.SetStats(0)
    # hist1.SetLineColor(r.kBlue)
    # hist1.SetLineWidth(2)
    # hist1.GetYaxis().SetTitle("Normalised events")
    # hist1.SetTitle("")

    # hist2 = histfile2.Get("nominal" + histogram_types[i])
    # hist2.SetDirectory(0)
    # histfile2.Close()
    # hist2.SetStats(0)
    # hist2.SetLineColor(r.kRed)
    # hist2.SetLineWidth(2)
    # hist2.GetYaxis().SetTitle("Normalised events")
    # hist2.SetTitle("")

    # if ("_topjet_W_invMass" in histogram_types[i]):
    #     hist1.GetXaxis().SetRangeUser(60,110)
    #     hist2.GetXaxis().SetRangeUser(60,110)

    # hist1.Scale(1./hist1.Integral(), "width")
    # hist2.Scale(1./hist2.Integral(), "width")

    # # Make a canvas that the histograms will be overlayed onto
    # canvas_overlayed = r.TCanvas("canvas_overlayed")
    # pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    # pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    # pad1.SetBottomMargin(0.00001)
    # pad1.SetBorderMode(0)
    # pad2.SetTopMargin(0.00001)
    # pad2.SetBottomMargin(0.2)
    # pad2.SetBorderMode(0)
    # pad1.Draw()
    # pad2.Draw()
    # pad1.cd()

    # minimums = []
    # maximums = []
    # minimums.append(hist1.GetMinimum())
    # maximums.append(hist1.GetMaximum())
    # minimums.append(hist2.GetMinimum())
    # maximums.append(hist2.GetMaximum())
    # globalMinY = min(minimums)
    # globalMaxY = max(maximums)

    # hist1.Draw()
    # hist2.Draw("same")

    # hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
    # hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

    # legend = r.TLegend(0.11,0.58,0.4,0.68)
    # legend.AddEntry(hist1, "PWG+PY8, Full sim")
    # legend.AddEntry(hist2, "PWG+PY8, Fast sim")
    # legend.SetLineWidth(0)
    # legend.SetFillStyle(0)
    # legend.Draw("same")

    # latex = r.TLatex()
    # latex.SetNDC()
    # latex.SetTextSize(0.03)
    # latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    # latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    # latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    # latex.DrawText(0.62,0.85,"Full sim mean = %.4f +- %.4f" %(hist1.GetMean(), hist1.GetMeanError()))
    # latex.DrawText(0.62,0.81,"Fast sim mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))


    # pad2.cd()
    # r.gStyle.SetOptStat(0)
    # subplot1 = hist2.Clone()

    # subplot1.Divide(hist1)
    # subplot1.SetTitle("")
    # subplot1.GetXaxis().SetLabelFont(63)
    # subplot1.GetXaxis().SetLabelSize(10)
    # subplot1.GetXaxis().SetTitle("Mass [GeV]")
    # subplot1.GetXaxis().SetTitleSize(0.11)
    # subplot1.GetXaxis().SetTitleOffset(0.8)
    # subplot1.GetYaxis().SetLabelFont(63)
    # subplot1.GetYaxis().SetLabelSize(10)
    # subplot1.GetYaxis().SetTitle("Fast sim / Full Sim")
    # subplot1.GetYaxis().SetTitleOffset(0.38)
    # subplot1.GetYaxis().SetTitleSize(0.07)
    # subplot1.GetYaxis().SetNdivisions(207)
    # subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    # subplot1.Draw("E1")

    # if ("_topjet_W_invMass" in histogram_types[i]):
    #     line = r.TLine(60,1,110,1)
    # else:
    #     line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
    # line.SetLineColor(r.kBlue)
    # line.SetLineWidth(2)
    # line.Draw("same")
    # canvas_overlayed.cd()
    # canvas_overlayed.Draw()

    # canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_11_30" + histogram_types[i] + "_Full_vs_Fast.pdf")

#####################################################
##################################################### FSR and ISR vs nominal
#####################################################

# histfile_FSR_UP = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_fsr_up_50bins.root")
# histfile_ISR_UP = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_radiation_up_50bins.root")
# histfile_FSR_DOWN = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_fsr_down_50bins.root")
# histfile_ISR_DOWN = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_radiation_down_50bins.root")
# histfile_nominal = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/All/JSF_1_00_ttallpp8.root")

# # histfile_FSR_UP = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_fsr_up.root")
# # histfile_ISR_UP = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_radiation_up.root")
# # histfile_FSR_DOWN = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_fsr_down.root")
# # histfile_ISR_DOWN = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_radiation_down.root")
# # histfile_nominal = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root")

# hist_FSR_UP = histfile_FSR_UP.Get("nominal_topjet_m_50bins")
# hist_ISR_UP = histfile_ISR_UP.Get("nominal_topjet_m_50bins")
# hist_FSR_DOWN = histfile_FSR_DOWN.Get("nominal_topjet_m_50bins")
# hist_ISR_DOWN = histfile_ISR_DOWN.Get("nominal_topjet_m_50bins")
# hist_nominal = histfile_nominal.Get("nominal_topjet_m_50bins")

# # hist_FSR_UP = histfile_FSR_UP.Get("nominal_topjet_W_invMass")
# # hist_ISR_UP = histfile_ISR_UP.Get("nominal_topjet_W_invMass")
# # hist_FSR_DOWN = histfile_FSR_DOWN.Get("nominal_topjet_W_invMass")
# # hist_ISR_DOWN = histfile_ISR_DOWN.Get("nominal_topjet_W_invMass")
# # hist_nominal = histfile_nominal.Get("nominal_topjet_W_invMass")

# hist_FSR_UP.SetDirectory(0)
# hist_ISR_UP.SetDirectory(0)
# hist_FSR_DOWN.SetDirectory(0)
# hist_ISR_DOWN.SetDirectory(0)
# hist_nominal.SetDirectory(0)

# histfile_FSR_UP.Close()
# histfile_ISR_UP.Close()
# histfile_FSR_DOWN.Close()
# histfile_ISR_DOWN.Close()
# histfile_nominal.Close()

# hist_FSR_UP.SetStats(0)
# hist_ISR_UP.SetStats(0)
# hist_FSR_DOWN.SetStats(0)
# hist_ISR_DOWN.SetStats(0)
# hist_nominal.SetStats(0)

# hist_FSR_UP.SetLineColor(r.kBlue)
# hist_ISR_UP.SetLineColor(r.kBlue)
# hist_FSR_DOWN.SetLineColor(r.kRed)
# hist_ISR_DOWN.SetLineColor(r.kRed)

# hist_FSR_UP.SetLineWidth(2)
# hist_ISR_UP.SetLineWidth(2)
# hist_FSR_DOWN.SetLineWidth(2)
# hist_ISR_DOWN.SetLineWidth(2)
# hist_nominal.SetLineWidth(2)

# hist_FSR_UP.GetYaxis().SetTitle("Normalised events")
# hist_ISR_UP.GetYaxis().SetTitle("Normalised events")
# hist_FSR_DOWN.GetYaxis().SetTitle("Normalised events")
# hist_ISR_DOWN.GetYaxis().SetTitle("Normalised events")
# hist_nominal.GetYaxis().SetTitle("Normalised events")

# hist_FSR_UP.SetTitle("")
# hist_ISR_UP.SetTitle("")
# hist_FSR_DOWN.SetTitle("")
# hist_ISR_DOWN.SetTitle("")
# hist_nominal.SetTitle("")

# # hist_FSR_UP.GetXaxis().SetRangeUser(60,110)
# # hist_ISR_UP.GetXaxis().SetRangeUser(60,110)
# # hist_FSR_DOWN.GetXaxis().SetRangeUser(60,110)
# # hist_ISR_DOWN.GetXaxis().SetRangeUser(60,110)
# # hist_nominal.GetXaxis().SetRangeUser(60,110)

# hist_FSR_UP.Scale(1./hist_FSR_UP.Integral(), "width")
# hist_ISR_UP.Scale(1./hist_ISR_UP.Integral(), "width")
# hist_FSR_DOWN.Scale(1./hist_FSR_DOWN.Integral(), "width")
# hist_ISR_DOWN.Scale(1./hist_ISR_DOWN.Integral(), "width")
# hist_nominal.Scale(1./hist_nominal.Integral(), "width")

# # Make a canvas that the histograms will be overlayed onto
# canvas_overlayed = r.TCanvas("canvas_overlayed")
# pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
# pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
# pad1.SetBottomMargin(0.00001)
# pad1.SetBorderMode(0)
# pad2.SetTopMargin(0.00001)
# pad2.SetBottomMargin(0.2)
# pad2.SetBorderMode(0)
# pad1.Draw()
# pad2.Draw()
# pad1.cd()

# minimums = []
# maximums = []
# minimums.append(hist_FSR_UP.GetMinimum())
# maximums.append(hist_FSR_UP.GetMaximum())
# minimums.append(hist_FSR_DOWN.GetMinimum())
# maximums.append(hist_FSR_DOWN.GetMaximum())
# minimums.append(hist_nominal.GetMinimum())
# maximums.append(hist_nominal.GetMaximum())
# globalMinY = min(minimums)
# globalMaxY = max(maximums)

# hist_nominal.Draw()
# hist_FSR_UP.Draw("same")
# hist_FSR_DOWN.Draw("same")

# hist_FSR_UP.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
# hist_FSR_DOWN.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
# hist_nominal.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

# legend = r.TLegend(0.11,0.58,0.4,0.68)
# legend.AddEntry(hist_nominal, "Nominal")
# legend.AddEntry(hist_FSR_UP, "FSR_up")
# legend.AddEntry(hist_FSR_DOWN, "FSR_down")
# legend.SetLineWidth(0)
# legend.SetFillStyle(0)
# legend.Draw("same")

# latex = r.TLatex()
# latex.SetNDC()
# latex.SetTextSize(0.03)
# latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
# latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
# latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
# latex.DrawText(0.62,0.85,"Nominal mean = (%.4f +- %.4f) GeV " %(hist_nominal.GetMean(), hist_nominal.GetMeanError()))
# latex.DrawText(0.62,0.81,"FSR_up mean = (%.4f +- %.4f) GeV" %(hist_FSR_UP.GetMean(), hist_FSR_UP.GetMeanError()))
# latex.DrawText(0.62,0.77,"FSR_down mean = (%.4f +- %.4f) GeV" %(hist_FSR_DOWN.GetMean(), hist_FSR_DOWN.GetMeanError()))


# pad2.cd()
# r.gStyle.SetOptStat(0)
# subplot1 = hist_FSR_UP.Clone()
# subplot2 = hist_FSR_DOWN.Clone()

# subplot1.Divide(hist_nominal)
# subplot2.Divide(hist_nominal)

# subplot1.SetTitle("")
# subplot1.GetXaxis().SetLabelFont(63)
# subplot1.GetXaxis().SetLabelSize(10)
# subplot1.GetXaxis().SetTitle("Mass [GeV]")
# subplot1.GetXaxis().SetTitleSize(0.11)
# subplot1.GetXaxis().SetTitleOffset(0.8)
# subplot1.GetYaxis().SetLabelFont(63)
# subplot1.GetYaxis().SetLabelSize(10)
# subplot1.GetYaxis().SetTitle("FSR / Nominal")
# subplot1.GetYaxis().SetTitleOffset(0.38)
# subplot1.GetYaxis().SetTitleSize(0.07)
# subplot1.GetYaxis().SetNdivisions(207)
# subplot1.GetYaxis().SetRangeUser(0.75,1.25)
# subplot1.Draw("E1")

# subplot2.SetTitle("")
# subplot2.GetXaxis().SetLabelFont(63)
# subplot2.GetXaxis().SetLabelSize(10)
# subplot2.GetXaxis().SetTitle("Mass [GeV]")
# subplot2.GetXaxis().SetTitleSize(0.11)
# subplot2.GetXaxis().SetTitleOffset(0.8)
# subplot2.GetYaxis().SetLabelFont(63)
# subplot2.GetYaxis().SetLabelSize(10)
# subplot2.GetYaxis().SetTitle("FSR / Nominal")
# subplot2.GetYaxis().SetTitleOffset(0.38)
# subplot2.GetYaxis().SetTitleSize(0.07)
# subplot2.GetYaxis().SetNdivisions(207)
# subplot2.GetYaxis().SetRangeUser(0.75,1.25)
# subplot2.Draw("same")

# # line = r.TLine(60,1,110,1)
# line = r.TLine(hist_nominal.GetXaxis().GetXmin(),1,hist_nominal.GetXaxis().GetXmax(),1)

# line.SetLineColor(r.kBlack)
# line.SetLineWidth(2)
# line.Draw("same")
# canvas_overlayed.cd()
# canvas_overlayed.Draw()

# canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_11_30_FSR_vs_Nominal_mJ.pdf")
# # canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_11_30_FSR_vs_Nominal_mW.pdf")

# # Make a canvas that the histograms will be overlayed onto
# canvas_overlayed = r.TCanvas("canvas_overlayed")
# pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
# pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
# pad1.SetBottomMargin(0.00001)
# pad1.SetBorderMode(0)
# pad2.SetTopMargin(0.00001)
# pad2.SetBottomMargin(0.2)
# pad2.SetBorderMode(0)
# pad1.Draw()
# pad2.Draw()
# pad1.cd()

# minimums = []
# maximums = []
# minimums.append(hist_ISR_UP.GetMinimum())
# maximums.append(hist_ISR_UP.GetMaximum())
# minimums.append(hist_ISR_DOWN.GetMinimum())
# maximums.append(hist_ISR_DOWN.GetMaximum())
# minimums.append(hist_nominal.GetMinimum())
# maximums.append(hist_nominal.GetMaximum())
# globalMinY = min(minimums)
# globalMaxY = max(maximums)

# hist_nominal.Draw()
# hist_ISR_UP.Draw("same")
# hist_ISR_DOWN.Draw("same")

# hist_ISR_UP.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
# hist_ISR_DOWN.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
# hist_nominal.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

# legend = r.TLegend(0.11,0.58,0.4,0.68)
# legend.AddEntry(hist_nominal, "Nominal")
# legend.AddEntry(hist_ISR_UP, "ISR_up")
# legend.AddEntry(hist_ISR_DOWN, "ISR_down")
# legend.SetLineWidth(0)
# legend.SetFillStyle(0)
# legend.Draw("same")

# latex = r.TLatex()
# latex.SetNDC()
# latex.SetTextSize(0.03)
# latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
# latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
# latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
# latex.DrawText(0.62,0.85,"Nominal mean = (%.4f +- %.4f) GeV " %(hist_nominal.GetMean(), hist_nominal.GetMeanError()))
# latex.DrawText(0.62,0.81,"ISR_up mean = (%.4f +- %.4f) GeV" %(hist_ISR_UP.GetMean(), hist_ISR_UP.GetMeanError()))
# latex.DrawText(0.62,0.77,"ISR_down mean = (%.4f +- %.4f) GeV" %(hist_ISR_DOWN.GetMean(), hist_ISR_DOWN.GetMeanError()))


# pad2.cd()
# r.gStyle.SetOptStat(0)
# subplot1 = hist_ISR_UP.Clone()
# subplot2 = hist_ISR_DOWN.Clone()

# subplot1.Divide(hist_nominal)
# subplot2.Divide(hist_nominal)

# subplot1.SetTitle("")
# subplot1.GetXaxis().SetLabelFont(63)
# subplot1.GetXaxis().SetLabelSize(10)
# subplot1.GetXaxis().SetTitle("Mass [GeV]")
# subplot1.GetXaxis().SetTitleSize(0.11)
# subplot1.GetXaxis().SetTitleOffset(0.8)
# subplot1.GetYaxis().SetLabelFont(63)
# subplot1.GetYaxis().SetLabelSize(10)
# subplot1.GetYaxis().SetTitle("ISR / Nominal")
# subplot1.GetYaxis().SetTitleOffset(0.38)
# subplot1.GetYaxis().SetTitleSize(0.07)
# subplot1.GetYaxis().SetNdivisions(207)
# subplot1.GetYaxis().SetRangeUser(0.75,1.25)
# subplot1.Draw("E1")

# subplot2.SetTitle("")
# subplot2.GetXaxis().SetLabelFont(63)
# subplot2.GetXaxis().SetLabelSize(10)
# subplot2.GetXaxis().SetTitle("Mass [GeV]")
# subplot2.GetXaxis().SetTitleSize(0.11)
# subplot2.GetXaxis().SetTitleOffset(0.8)
# subplot2.GetYaxis().SetLabelFont(63)
# subplot2.GetYaxis().SetLabelSize(10)
# subplot2.GetYaxis().SetTitle("ISR / Nominal")
# subplot2.GetYaxis().SetTitleOffset(0.38)
# subplot2.GetYaxis().SetTitleSize(0.07)
# subplot2.GetYaxis().SetNdivisions(207)
# subplot2.GetYaxis().SetRangeUser(0.75,1.25)
# subplot2.Draw("same")

# # line = r.TLine(60,1,110,1)
# line = r.TLine(hist_nominal.GetXaxis().GetXmin(),1,hist_nominal.GetXaxis().GetXmax(),1)

# line.SetLineColor(r.kBlack)
# line.SetLineWidth(2)
# line.Draw("same")
# canvas_overlayed.cd()
# canvas_overlayed.Draw()

# canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_11_30_ISR_vs_Nominal_mJ_50bins.pdf")
# # canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/MCGenComparison/2022_11_02_ISR_vs_Nominal_mW.pdf")

#####################################################
##################################################### mlb
#####################################################

histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL_100bins.root")
histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8RTT/All/JSF_1_00_ttpp8RTT_ALL_100bins.root")
histfile3 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m173/All/JSF_1_00_ttpp8m173_ALL_100bins.root")

hist1 = histfile1.Get("nominal_mlb")
hist1.SetDirectory(0)
histfile1.Close()
hist1.SetStats(0)
hist1.SetLineColor(r.kBlack)
hist1.SetLineWidth(2)
hist1.GetYaxis().SetTitle("Normalised events")
hist1.SetTitle("")

hist2 = histfile2.Get("nominal_mlb")
hist2.SetDirectory(0)
histfile2.Close()
hist2.SetStats(0)
hist2.SetLineColor(r.kBlue)
hist2.SetLineWidth(2)
hist2.GetYaxis().SetTitle("Normalised events")
hist2.SetTitle("")

hist3 = histfile3.Get("nominal_mlb")
hist3.SetDirectory(0)
histfile3.Close()
hist3.SetStats(0)
hist3.SetLineColor(r.kRed)
hist3.SetLineWidth(2)
hist3.GetYaxis().SetTitle("Normalised events")
hist3.SetTitle("")

hist1.Scale(1./hist1.Integral(), "width")
hist2.Scale(1./hist2.Integral(), "width")
hist3.Scale(1./hist3.Integral(), "width")

minimums = []
maximums = []
minimums.append(hist1.GetMinimum())
minimums.append(hist2.GetMinimum())
minimums.append(hist3.GetMinimum())
maximums.append(hist1.GetMaximum())
maximums.append(hist2.GetMaximum())
maximums.append(hist3.GetMaximum())
globalMinY = min(minimums)
globalMaxY = max(maximums)

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")
pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.Draw()
pad2.Draw()
pad1.cd()

hist1.Draw()
hist2.Draw("same")
hist3.Draw("same")

hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.03)
latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
latex.DrawText(0.56,0.85,"Nominal sample mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
latex.DrawText(0.56,0.81,"Recoil-to-top sample mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))
latex.DrawText(0.56,0.77,"173 GeV sample mean = %.4f +- %.4f" %(hist3.GetMean(), hist3.GetMeanError()))

legend = r.TLegend(0.66,0.68,0.88,0.48)
legend.AddEntry(hist1, "Nominal sample")
legend.AddEntry(hist2, "Recoil-to-top sample")
legend.AddEntry(hist3, "173 GeV sample")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)

subplot1 = hist2.Clone()
subplot2 = hist3.Clone()

subplot1.Divide(hist1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("mlb [GeV]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC setup / nominal")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")

subplot2.Divide(hist1)
subplot2.SetTitle("")
subplot2.GetXaxis().SetLabelFont(63)
subplot2.GetXaxis().SetLabelSize(10)
subplot2.GetXaxis().SetTitle("mlb [GeV]")
subplot2.GetXaxis().SetTitleSize(0.11)
subplot2.GetXaxis().SetTitleOffset(0.8)
subplot2.GetYaxis().SetLabelFont(63)
subplot2.GetYaxis().SetLabelSize(10)
subplot2.GetYaxis().SetTitle("MC setup / nominal")
subplot2.GetYaxis().SetTitleOffset(0.38)
subplot2.GetYaxis().SetTitleSize(0.07)
subplot2.GetYaxis().SetNdivisions(207)
subplot2.GetYaxis().SetRangeUser(0.75,1.25)
subplot2.Draw("same")

line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()
canvas_overlayed.Draw()

canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/NewObservable/2022_12_13_mlb.pdf")

#####################################################
##################################################### Transverse W mass
#####################################################

histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL_100bins.root")
histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8RTT/All/JSF_1_00_ttpp8RTT_ALL_100bins.root")
histfile3 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m173/All/JSF_1_00_ttpp8m173_ALL_100bins.root")

hist1 = histfile1.Get("nominal_mtw")
hist1.SetDirectory(0)
histfile1.Close()
hist1.SetStats(0)
hist1.SetLineColor(r.kBlack)
hist1.SetLineWidth(2)
hist1.GetYaxis().SetTitle("Normalised events")
hist1.SetTitle("")

hist2 = histfile2.Get("nominal_mtw")
hist2.SetDirectory(0)
histfile2.Close()
hist2.SetStats(0)
hist2.SetLineColor(r.kBlue)
hist2.SetLineWidth(2)
hist2.GetYaxis().SetTitle("Normalised events")
hist2.SetTitle("")

hist3 = histfile3.Get("nominal_mtw")
hist3.SetDirectory(0)
histfile3.Close()
hist3.SetStats(0)
hist3.SetLineColor(r.kRed)
hist3.SetLineWidth(2)
hist3.GetYaxis().SetTitle("Normalised events")
hist3.SetTitle("")

hist1.Scale(1./hist1.Integral(), "width")
hist2.Scale(1./hist2.Integral(), "width")
hist3.Scale(1./hist3.Integral(), "width")

minimums = []
maximums = []
minimums.append(hist1.GetMinimum())
minimums.append(hist2.GetMinimum())
minimums.append(hist3.GetMinimum())
maximums.append(hist1.GetMaximum())
maximums.append(hist2.GetMaximum())
maximums.append(hist3.GetMaximum())
globalMinY = min(minimums)
globalMaxY = max(maximums)

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")
pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.Draw()
pad2.Draw()
pad1.cd()

hist1.Draw()
hist2.Draw("same")
hist3.Draw("same")

hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.03)
latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
latex.DrawText(0.56,0.85,"Nominal sample mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
latex.DrawText(0.56,0.81,"Recoil-to-top sample mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))
latex.DrawText(0.56,0.77,"173 GeV sample mean = %.4f +- %.4f" %(hist3.GetMean(), hist3.GetMeanError()))

legend = r.TLegend(0.66,0.68,0.88,0.48)
legend.AddEntry(hist1, "Nominal sample")
legend.AddEntry(hist2, "Recoil-to-top sample")
legend.AddEntry(hist3, "173 GeV sample")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)

subplot1 = hist2.Clone()
subplot2 = hist3.Clone()

subplot1.Divide(hist1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Transverse W mass [GeV]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC setup / nominal")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")

subplot2.Divide(hist1)
subplot2.SetTitle("")
subplot2.GetXaxis().SetLabelFont(63)
subplot2.GetXaxis().SetLabelSize(10)
subplot2.GetXaxis().SetTitle("mlb [GeV]")
subplot2.GetXaxis().SetTitleSize(0.11)
subplot2.GetXaxis().SetTitleOffset(0.8)
subplot2.GetYaxis().SetLabelFont(63)
subplot2.GetYaxis().SetLabelSize(10)
subplot2.GetYaxis().SetTitle("Transverse W mass / nominal")
subplot2.GetYaxis().SetTitleOffset(0.38)
subplot2.GetYaxis().SetTitleSize(0.07)
subplot2.GetYaxis().SetNdivisions(207)
subplot2.GetYaxis().SetRangeUser(0.75,1.25)
subplot2.Draw("same")

line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()
canvas_overlayed.Draw()

canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/NewObservable/2022_12_13_mtw.pdf")

#####################################################
##################################################### lep_pt/lepbjet_pt
#####################################################

histfile1 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL_100bins.root")
histfile2 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8RTT/All/JSF_1_00_ttpp8RTT_ALL_100bins.root")
histfile3 = r.TFile.Open("../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m173/All/JSF_1_00_ttpp8m173_ALL_100bins.root")

hist1 = histfile1.Get("nominal_lep_pt_lepbjet_pt_ratio")
hist1.SetDirectory(0)
hist1.SetStats(0)
hist1.SetLineColor(r.kBlack)
hist1.SetLineWidth(2)
hist1.GetYaxis().SetTitle("Normalised events")
hist1.SetTitle("")
# hist1b = histfile1.Get("nominal_lepbjet_pt")
# hist1b.SetDirectory(0)
# histfile1.Close()
# hist1b.SetStats(0)
# hist1b.SetLineColor(r.kBlack)
# hist1b.SetLineWidth(2)
# hist1b.GetYaxis().SetTitle("Normalised events")
# hist1b.SetTitle("")

hist2 = histfile2.Get("nominal_lep_pt_lepbjet_pt_ratio")
hist2.SetDirectory(0)
hist2.SetStats(0)
hist2.SetLineColor(r.kBlue)
hist2.SetLineWidth(2)
hist2.GetYaxis().SetTitle("Normalised events")
hist2.SetTitle("")
# hist2b = histfile2.Get("nominal_lepbjet_pt")
# hist2b.SetDirectory(0)
# histfile2.Close()
# hist2b.SetStats(0)
# hist2b.SetLineColor(r.kBlue)
# hist2b.SetLineWidth(2)
# hist2b.GetYaxis().SetTitle("Normalised events")
# hist2b.SetTitle("")

hist3 = histfile3.Get("nominal_lep_pt_lepbjet_pt_ratio")
hist3.SetDirectory(0)
hist3.SetStats(0)
hist3.SetLineColor(r.kRed)
hist3.SetLineWidth(2)
hist3.GetYaxis().SetTitle("Normalised events")
hist3.SetTitle("")
# hist3b = histfile3.Get("nominal_lepbjet_pt")
# hist3b.SetDirectory(0)
# histfile3.Close()
# hist3b.SetStats(0)
# hist3b.SetLineColor(r.kRed)
# hist3b.SetLineWidth(2)
# hist3b.GetYaxis().SetTitle("Normalised events")
# hist3b.SetTitle("")

hist1.Scale(1./hist1.Integral(), "width")
hist2.Scale(1./hist2.Integral(), "width")
hist3.Scale(1./hist3.Integral(), "width")

# hist1b.Scale(1./hist1b.Integral(), "width")
# hist2b.Scale(1./hist2b.Integral(), "width")
# hist3b.Scale(1./hist3b.Integral(), "width")

# hist1.Divide(hist1b)
# hist2.Divide(hist2b)
# hist3.Divide(hist3b)


minimums = []
maximums = []
minimums.append(hist1.GetMinimum())
minimums.append(hist2.GetMinimum())
minimums.append(hist3.GetMinimum())
maximums.append(hist1.GetMaximum())
maximums.append(hist2.GetMaximum())
maximums.append(hist3.GetMaximum())
globalMinY = min(minimums)
globalMaxY = max(maximums)

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")
pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.Draw()
pad2.Draw()
pad1.cd()

hist1.Draw()
hist2.Draw("same")
hist3.Draw("same")

hist1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
hist3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.03)
latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
latex.DrawText(0.56,0.85,"Nominal sample mean = %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
latex.DrawText(0.56,0.81,"Recoil-to-top sample mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))
latex.DrawText(0.56,0.77,"173 GeV sample mean = %.4f +- %.4f" %(hist3.GetMean(), hist3.GetMeanError()))

legend = r.TLegend(0.66,0.68,0.88,0.48)
legend.AddEntry(hist1, "Nominal sample")
legend.AddEntry(hist2, "Recoil-to-top sample")
legend.AddEntry(hist3, "173 GeV sample")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)

subplot1 = hist2.Clone()
subplot2 = hist3.Clone()

subplot1.Divide(hist1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("lep_pt/lepbjet_pt")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC setup / nominal")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")

subplot2.Divide(hist1)
subplot2.SetTitle("")
subplot2.GetXaxis().SetLabelFont(63)
subplot2.GetXaxis().SetLabelSize(10)
subplot2.GetXaxis().SetTitle("lep_pt/lepbjet_pt")
subplot2.GetXaxis().SetTitleSize(0.11)
subplot2.GetXaxis().SetTitleOffset(0.8)
subplot2.GetYaxis().SetLabelFont(63)
subplot2.GetYaxis().SetLabelSize(10)
subplot2.GetYaxis().SetTitle("MC setup / nominal")
subplot2.GetYaxis().SetTitleOffset(0.38)
subplot2.GetYaxis().SetTitleSize(0.07)
subplot2.GetYaxis().SetNdivisions(207)
subplot2.GetYaxis().SetRangeUser(0.75,1.25)
subplot2.Draw("same")

line = r.TLine(hist1.GetXaxis().GetXmin(),1,hist1.GetXaxis().GetXmax(),1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()
canvas_overlayed.Draw()

canvas_overlayed.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/NewObservable/2022_12_13_lep_pt_lepbjet_pt_ratio.pdf")