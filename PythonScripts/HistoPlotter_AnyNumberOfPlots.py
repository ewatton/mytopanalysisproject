import ROOT as r
from array import array

r.TH1.SetDefaultSumw2(True)

samples = ["wejets","wmujets","wtaujets","wjets"]
PlotClassifier = ["","MUR05_MUF05","MUR05_MUF1","MUR2_MUF1","MUR2_MUF2","MUR1_MUF05","MUR1_MUF2"]

for i in range(0,len(samples)):
    histograms = []
    variable="nominal_topjet_W_invMass"
    for j in range(0,len(PlotClassifier)):
        if PlotClassifier[j] == "":
            histfile = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + samples[i] + "/mJ_mW.root")
        else:
            histfile = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + samples[i] + "/mJ_mW_" + PlotClassifier[j] + ".root")
        histogram = histfile.Get(variable)
        histogram.SetDirectory(0)
        histogram.SetStats(0)
        histogram.SetLineWidth(2)
        histfile.Close()
        # histogram.Scale(1./histogram.Integral(), "width")
        histogram.SetTitle("")
        histogram.GetYaxis().SetTitle("Events")
        histogram.GetXaxis().SetTitle("Mass [GeV]")
        histogram.GetYaxis().SetTitleOffset(1.4)
        if (samples[i] == "wjets"):
            histogram.GetYaxis().SetRangeUser(0,20)
        elif (samples[i] == "wejets"):
            histogram.GetYaxis().SetRangeUser(0,10)
        elif (samples[i] == "wmujets"):
            histogram.GetYaxis().SetRangeUser(0,10)
        elif (samples[i] == "wtaujets"):
            histogram.GetYaxis().SetRangeUser(0,5)
        else:
            histogram.GetYaxis().SetRangeUser(0,100)    

        if (variable == "nominal_topjet_W_invMass"):
            histogram.GetXaxis().SetRangeUser(60,105)
        histograms.append(histogram)

    canvas_overlayed = r.TCanvas("canvas_overlayed")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetTicks()
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks()
    # pad1.SetGrid()
    # pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    histograms[0].Draw()
    histograms[0].SetLineColor(r.kBlack)
    colours = [r.kRed,r.kRed,r.kBlue,r.kBlue,r.kGreen,r.kGreen]
    styles = [r.kSolid,r.kDashed,r.kSolid,r.kDashed,r.kSolid,r.kDashed]
    for k in range(1,len(histograms)):
        histograms[k].Draw("same")
        histograms[k].SetLineColor(colours[k-1])
        histograms[k].SetLineStyle(styles[k-1])

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    legend = r.TLegend(0.15,0.4,0.45,0.70)
    legend.AddEntry(histograms[0], "Nominal")
    for k in range(1,len(histograms)):
        legend.AddEntry(histograms[k], PlotClassifier[k])
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    # latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
    if samples[i] == "wejets":
        latex.DrawLatex(0.15,0.75,"#bf{Wejets MC sample}")
    elif samples[i] == "wmujets":
        latex.DrawLatex(0.15,0.75,"#bf{Wmujets MC sample}")
    elif samples[i] == "wtaujets":
        latex.DrawLatex(0.15,0.75,"#bf{Wtaujets MC sample}")
    elif samples[i] == "wjets":
        latex.DrawLatex(0.15,0.75,"#bf{W+jets MC sample}")
    else:
        print("\n\n\nSomething has gone wrong!\n\n\n")

    # latex.DrawText(0.45,0.76,"Nominal mean = %.4f +- %.4f" %(histograms[0].GetMean(),histograms[0].GetMeanError()))
    # yposition = 0.76
    # for k in range(1,len(histograms)):
    #     yposition = yposition - 0.04
    #     latex.DrawText(0.45,yposition,PlotClassifier[j] + " mean = %.4f +- %.4f" %(histograms[k].GetMean(),histograms[k].GetMeanError()))

    pad2.cd()
    ymin = 0
    ymax = 2.0
    r.gStyle.SetOptStat(0)
    subplots = []
    for k in range(1,len(histograms)):
        subplot_k = histograms[k].Clone()
        subplot_k.Divide(histograms[0])
        subplot_k.SetMarkerStyle(5)
        subplot_k.SetMarkerSize(0.5)
        subplot_k.SetTitle("")
        subplot_k.GetXaxis().SetLabelFont(63)
        subplot_k.GetXaxis().SetLabelSize(10)
        subplot_k.GetXaxis().SetTitle("Mass [GeV]")
        subplot_k.GetXaxis().SetTitleSize(0.11)
        subplot_k.GetXaxis().SetTitleOffset(0.8)
        subplot_k.GetYaxis().SetLabelFont(63)
        subplot_k.GetYaxis().SetLabelSize(10)
        subplot_k.GetYaxis().SetTitle("MC / Nominal")
        subplot_k.GetYaxis().SetTitleOffset(0.38)
        subplot_k.GetYaxis().SetTitleSize(0.11)
        subplot_k.GetYaxis().SetNdivisions(207)
        subplot_k.GetYaxis().SetRangeUser(ymin,ymax)
        subplots.append(subplot_k)

    subplots[0].Draw("E1")
    for k in range(0,len(subplots)):
        subplots[k].Draw("same")

    line = r.TLine(60,1,105,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.Draw()
    canvas_overlayed.SaveAs("../histogram_images/2023-09-18/" + samples[i] + "_NPweights_NotNormalised.pdf")

########
########
########

# # samples = ["ttpp8AF2","ttpp8FSRup","ttpp8FSRdown"]
# # samples = ["SingleTop","SingleTopH7"]
# # variableList = ["nominal_topjet_W_invMass","nominal_topjet_m"]
# # variableList = ["nominal_topjet_W_invMass"]
# variableList = ["nominal_topjet_m"]

# for j in range(0,len(variableList)):
#     variable=variableList[j]
#     histfile1 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttpp8AF2/mJ_mW_nominal20Bins.root")
#     histfile2 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttpp8RTW/mJ_mW_nominal20Bins.root")
#     histfile3 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/SingleTopDS/mJ_mW_nominal20Bins.root")
#     histogram1 = histfile1.Get(variable)
#     histogram1.SetDirectory(0)
#     histogram1.SetStats(0)
#     histogram1.SetLineWidth(2)
#     histfile1.Close()
#     histogram1.Scale(1./histogram1.Integral(), "width")
#     histogram1.SetTitle("")
#     histogram1.GetYaxis().SetTitle("Events")
#     histogram1.GetXaxis().SetTitle("Mass [GeV]")
#     histogram1.GetYaxis().SetTitleOffset(1.4)
#     histogram2 = histfile2.Get(variable)
#     histogram2.SetDirectory(0)
#     histogram2.SetStats(0)
#     histogram2.SetLineWidth(2)
#     histfile2.Close()
#     histogram2.Scale(1./histogram2.Integral(), "width")
#     histogram2.SetTitle("")
#     histogram2.GetYaxis().SetTitle("Events")
#     histogram2.GetXaxis().SetTitle("Mass [GeV]")
#     histogram2.GetYaxis().SetTitleOffset(1.4)
#     histogram3 = histfile3.Get(variable)
#     histogram3.SetDirectory(0)
#     histogram3.SetStats(0)
#     histogram3.SetLineWidth(2)
#     histfile3.Close()
#     histogram3.Scale(1./histogram3.Integral(), "width")
#     histogram3.SetTitle("")
#     histogram3.GetYaxis().SetTitle("Events")
#     histogram3.GetXaxis().SetTitle("Mass [GeV]")
#     histogram3.GetYaxis().SetTitleOffset(1.4)
#     if (variable == "nominal_topjet_W_invMass"):
#         histogram1.GetXaxis().SetRangeUser(40,120)
#         histogram2.GetXaxis().SetRangeUser(40,120)
#         histogram3.GetXaxis().SetRangeUser(40,120)
#         xmin = 40
#         xmax = 120
#     else:
#         xmin = 135
#         xmax = 205

#     minimums = []
#     maximums = []
#     minimums.append(histogram1.GetMinimum())
#     maximums.append(histogram1.GetMaximum())
#     minimums.append(histogram2.GetMinimum())
#     maximums.append(histogram2.GetMaximum())
#     minimums.append(histogram3.GetMinimum())
#     maximums.append(histogram3.GetMaximum())
#     globalMinY = min(minimums)
#     globalMaxY = max(maximums)
#     histogram1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram1.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram2.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram3.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

#     canvas_overlayed = r.TCanvas("canvas_overlayed")
#     pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
#     pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
#     pad1.SetBottomMargin(0.00001)
#     pad1.SetBorderMode(0)
#     pad2.SetTopMargin(0.00001)
#     pad2.SetBottomMargin(0.2)
#     pad2.SetBorderMode(0)
#     # pad1.SetGrid()
#     # pad2.SetGrid()
#     pad1.Draw()
#     pad2.Draw()
#     pad1.cd()

#     histogram1.Draw()
#     histogram1.SetLineColor(r.kBlack)
#     histogram2.Draw("same")
#     # histogram3.Draw("same")
#     histogram2.SetLineColor(r.kBlue)
#     # histogram3.SetLineColor(r.kBlue)

#     latex = r.TLatex()
#     latex.SetNDC()
#     latex.SetTextSize(0.04)

#     legend = r.TLegend(0.15,0.6,0.30,0.7)
#     legend.AddEntry(histogram1, "Nominal")
#     legend.AddEntry(histogram2, "Recoil-to-W")
#     # legend.AddEntry(histogram3, "SingleTopDS")
#     legend.SetLineWidth(0)
#     legend.SetFillStyle(0)
#     legend.Draw("same")

#     latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#     latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#     latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
#     latex.DrawText(0.3,0.25,variable)
#     latex.DrawText(0.3,0.20,"Nominal mean = %.4f +- %.4f" %(histogram1.GetMean(),histogram1.GetMeanError()))
#     latex.DrawText(0.3,0.15,"Recoil-to-W mean = %.4f +- %.4f" %(histogram2.GetMean(),histogram2.GetMeanError()))
#     # latex.DrawText(0.3,0.10,"SingleTopDS mean = %.4f +- %.4f" %(histogram3.GetMean(),histogram3.GetMeanError()))
#     pad2.cd()
#     ymin = 0.9
#     ymax = 1.1
#     r.gStyle.SetOptStat(0)
#     subplots = []
#     subplot = histogram2.Clone()
#     subplot.Divide(histogram1)
#     subplot.SetMarkerStyle(5)
#     subplot.SetMarkerSize(0.5)
#     subplot.SetTitle("")
#     subplot.GetXaxis().SetLabelFont(63)
#     subplot.GetXaxis().SetLabelSize(10)
#     subplot.GetXaxis().SetTitle("Mass [GeV]")
#     subplot.GetXaxis().SetTitleSize(0.11)
#     subplot.GetXaxis().SetTitleOffset(0.8)
#     subplot.GetYaxis().SetLabelFont(63)
#     subplot.GetYaxis().SetLabelSize(10)
#     subplot.GetYaxis().SetTitle("Ratio")
#     subplot.GetYaxis().SetTitleOffset(0.38)
#     subplot.GetYaxis().SetTitleSize(0.11)
#     subplot.GetYaxis().SetNdivisions(207)
#     subplot.GetYaxis().SetRangeUser(ymin,ymax)
#     subplots.append(subplot)
#     # subplot2 = histogram3.Clone()
#     # subplot2.Divide(histogram1)
#     # subplot2.SetMarkerStyle(5)
#     # subplot2.SetMarkerSize(0.5)
#     # subplot2.SetTitle("")
#     # subplot2.GetXaxis().SetLabelFont(63)
#     # subplot2.GetXaxis().SetLabelSize(10)
#     # subplot2.GetXaxis().SetTitle("Mass [GeV]")
#     # subplot2.GetXaxis().SetTitleSize(0.11)
#     # subplot2.GetXaxis().SetTitleOffset(0.8)
#     # subplot2.GetYaxis().SetLabelFont(63)
#     # subplot2.GetYaxis().SetLabelSize(10)
#     # subplot2.GetYaxis().SetTitle("Ratio")
#     # subplot2.GetYaxis().SetTitleOffset(0.38)
#     # subplot2.GetYaxis().SetTitleSize(0.11)
#     # subplot2.GetYaxis().SetNdivisions(207)
#     # subplot2.GetYaxis().SetRangeUser(ymin,ymax)
#     # subplots.append(subplot2)

#     subplots[0].Draw("E1")
#     for k in range(0,len(subplots)):
#         subplots[k].Draw("same")

#     line = r.TLine(xmin,1,xmax,1)
#     line.SetLineColor(r.kBlack)
#     line.SetLineWidth(2)
#     line.Draw("same")
#     canvas_overlayed.Draw()
#     canvas_overlayed.SaveAs("../histogram_images/2023-07-14/" + variable + "_NominalvsRTW.pdf")

######################
######################
######################

# variableList = ["nominal_lepbjet_lep_pT_ratio","nominal_tmm_leptop_addjet_m","nominal_tmm_leptop_bjet_addjet_m"]
# dRs_string = ["0-8","0-9","1-0","1-1","1-2","1-3","1-4"]

# for j in range(0,len(variableList)):
#     for i in range(0,len(dRs_string)):
#         variable=variableList[j]
#         histfile1 = r.TFile.Open("../output_root_files/2023-06-21-InvestigatingRecoil/ttpp8AF2/leptop_addjet_mass_dR" + dRs_string[i] + "_40Bins.root")
#         histfile2 = r.TFile.Open("../output_root_files/2023-06-21-InvestigatingRecoil/ttpp8RTT/leptop_addjet_mass_dR" + dRs_string[i] + "_40Bins.root")
#         histogram1 = histfile1.Get(variable)
#         histogram1.SetDirectory(0)
#         histogram1.SetStats(0)
#         histogram1.SetLineWidth(2)
#         histfile1.Close()
#         histogram1.Scale(1./histogram1.Integral(), "width")
#         histogram1.SetTitle("")
#         histogram1.GetYaxis().SetTitle("Events")

#         if variable != "nominal_lepbjet_lep_pT_ratio":
#             histogram1.GetXaxis().SetTitle("Mass [GeV]")
#         else:
#             histogram1.GetXaxis().SetTitle("lep bjet pT / lep pT")
            
#         histogram1.GetYaxis().SetTitleOffset(1.4)
#         histogram2 = histfile2.Get(variable)
#         histogram2.SetDirectory(0)
#         histogram2.SetStats(0)
#         histogram2.SetLineWidth(2)
#         histfile2.Close()
#         histogram2.Scale(1./histogram2.Integral(), "width")
#         histogram2.SetTitle("")
#         histogram2.GetYaxis().SetTitle("Events")

#         if variable != "nominal_lepbjet_lep_pT_ratio":
#             histogram2.GetXaxis().SetTitle("Mass [GeV]")
#         else:
#             histogram2.GetXaxis().SetTitle("lep bjet pT / lep pT")

#         histogram2.GetYaxis().SetTitleOffset(1.4)
        
#         if (variable != "nominal_lepbjet_lep_pT_ratio"):
#             histogram1.GetXaxis().SetRangeUser(0,600)
#             histogram2.GetXaxis().SetRangeUser(0,600)
#             xmin = 0
#             xmax = 600
#         else:
#             xmin = 0
#             xmax = 10

#         minimums = []
#         maximums = []
#         minimums.append(histogram1.GetMinimum())
#         maximums.append(histogram1.GetMaximum())
#         minimums.append(histogram2.GetMinimum())
#         maximums.append(histogram2.GetMaximum())
#         globalMinY = min(minimums)
#         globalMaxY = max(maximums)
#         histogram1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#         histogram1.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#         histogram2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#         histogram2.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

#         canvas_overlayed = r.TCanvas("canvas_overlayed")
#         pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
#         pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
#         pad1.SetBottomMargin(0.00001)
#         pad1.SetBorderMode(0)
#         pad2.SetTopMargin(0.00001)
#         pad2.SetBottomMargin(0.2)
#         pad2.SetBorderMode(0)
#         # pad1.SetGrid()
#         # pad2.SetGrid()
#         pad1.Draw()
#         pad2.Draw()
#         pad1.cd()

#         histogram1.Draw()
#         histogram1.SetLineColor(r.kBlack)
#         histogram2.Draw("same")
#         histogram2.SetLineColor(r.kRed)

#         latex = r.TLatex()
#         latex.SetNDC()
#         latex.SetTextSize(0.04)

#         legend = r.TLegend(0.15,0.6,0.30,0.7)
#         legend.AddEntry(histogram1, "Nominal")
#         legend.AddEntry(histogram2, "Recoil")
#         legend.SetLineWidth(0)
#         legend.SetFillStyle(0)
#         legend.Draw("same")

#         latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#         latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#         latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")
#         latex.DrawText(0.55,0.85,variable)
#         latex.DrawText(0.55,0.80,"Nominal mean = %.4f +- %.4f" %(histogram1.GetMean(),histogram1.GetMeanError()))
#         latex.DrawText(0.55,0.75,"Recoil mean = %.4f +- %.4f" %(histogram2.GetMean(),histogram2.GetMeanError()))
#         pad2.cd()
#         ymin = 0.75
#         ymax = 1.25
#         r.gStyle.SetOptStat(0)
#         subplot = histogram2.Clone()
#         subplot.Divide(histogram1)
#         subplot.SetMarkerStyle(5)
#         subplot.SetMarkerSize(0.5)
#         subplot.SetTitle("")
#         subplot.GetXaxis().SetLabelFont(63)
#         subplot.GetXaxis().SetLabelSize(10)
#         subplot.GetXaxis().SetTitle("Mass [GeV]")
#         subplot.GetXaxis().SetTitleSize(0.11)
#         subplot.GetXaxis().SetTitleOffset(0.8)
#         subplot.GetYaxis().SetLabelFont(63)
#         subplot.GetYaxis().SetLabelSize(10)
#         subplot.GetYaxis().SetTitle("Recoil/Nominal")
#         subplot.GetYaxis().SetTitleOffset(0.38)
#         subplot.GetYaxis().SetTitleSize(0.11)
#         subplot.GetYaxis().SetNdivisions(207)
#         subplot.GetYaxis().SetRangeUser(ymin,ymax)
#         subplot.Draw("E1")

#         line = r.TLine(xmin,1,xmax,1)
#         line.SetLineColor(r.kBlack)
#         line.SetLineWidth(2)
#         line.Draw("same")
#         canvas_overlayed.Draw()
#         canvas_overlayed.SaveAs("../histogram_images/2023-06-27/" + variable + "_NominalVsRTT_dR" + dRs_string[i] + "_40Bins.pdf")

print("\nFinished\n")