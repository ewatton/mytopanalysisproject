# A macro that will use TMinuit to minimise chi^2 while finding m_t as a fit parameter for the fit function found from Scatter.py for the input MC data.
# I assumed the relationship between average_m_j and m_t would be the same for the data, where here I am using the error in the average_m_j for data.
# First created by Elliot Watton - 14/10/2021
# Last editted by Elliot Watton - 18/10/2021

# Imports
from ROOT import TMinuit , Double , Long
import numpy as np
from array import array as arr
r.TH1.SetDefaultSumw2(True)
# Initialise global variables 
average_m_j = 0       # m_j
average_m_j_err = 0   # sigma_{m_j}
grad = 0              # s
intercept = 0         # c
npar = 1              # Number of fit parameters

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = grad * m_t + intercept
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global grad 
    grad = s
    global intercept
    intercept = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe
    
    print('')
    print('*==* MINUIT fit completed:')
    print 'error code = ', ierflg
    print 'status = ', icstat
    print('')
    print('Results:')
    print('\t m_t = (%.3f +/- %.3f) GeV/c^2' %(final_m_t, final_m_t_err))
    print('')

    return final_m_t, final_m_t_err

########
######## The following relates to when finding the values of m_t to ensure that the TMinuit method works, and to look at the statistical errors on m_t
########

# Data from excel tables

#average_m_j_AF2, average_m_j_169, average_m_j_172, average_m_j_173, average_m_j_176 = 165.80918234, 164.183495161, 165.641776774, 166.120847175, 167.505570029
#average_m_j_err_AF2, average_m_j_err_169, average_m_j_err_172, average_m_j_err_173, average_m_j_err_176 = 0.07529608, 0.07278996, 0.07479042, 0.07557325, 0.07803119
#grad = 0.474759406701
#intercept = 83.9616340778

#average_m_j_AF2_redu_axis, average_m_j_169_redu_axis, average_m_j_172_redu_axis, average_m_j_173_redu_axis, average_m_j_176_redu_axis = 168.820745518, 167.114695808, 168.625381276, 169.076304420, 170.509665558
#average_m_j_err_AF2_redu_axis, average_m_j_err_169_redu_axis, average_m_j_err_172_redu_axis, average_m_j_err_173_redu_axis, average_m_j_err_176_redu_axis = 0.06116448, 0.05885825, 0.06069796, 0.06152170, 0.06388915
#grad_redu_axis = 0.482928928781
#intercept_redu_axis = 85.5312105223

# Now find the corresponding m_t for each

#m_t_8AF2, m_t_err_8AF2 = getm_t(average_m_j_AF2, average_m_j_err_AF2, grad, intercept)
#m_t_m169, m_t_err_m169 = getm_t(average_m_j_169, average_m_j_err_169, grad, intercept)
#m_t_m172, m_t_err_m172 = getm_t(average_m_j_172, average_m_j_err_172, grad, intercept)
#m_t_m173, m_t_err_m173 = getm_t(average_m_j_173, average_m_j_err_173, grad, intercept)
#m_t_m176, m_t_err_m176 = getm_t(average_m_j_176, average_m_j_err_176, grad, intercept)

#m_t_8AF2_redu_axis, m_t_err_8AF2_redu_axis = getm_t(average_m_j_AF2_redu_axis, average_m_j_err_AF2_redu_axis, grad_redu_axis, intercept_redu_axis)
#m_t_m169_redu_axis, m_t_err_m169_redu_axis = getm_t(average_m_j_169_redu_axis, average_m_j_err_169_redu_axis, grad_redu_axis, intercept_redu_axis)
#m_t_m172_redu_axis, m_t_err_m172_redu_axis = getm_t(average_m_j_172_redu_axis, average_m_j_err_172_redu_axis, grad_redu_axis, intercept_redu_axis)
#m_t_m173_redu_axis, m_t_err_m173_redu_axis = getm_t(average_m_j_173_redu_axis, average_m_j_err_173_redu_axis, grad_redu_axis, intercept_redu_axis)
#m_t_m176_redu_axis, m_t_err_m176_redu_axis = getm_t(average_m_j_176_redu_axis, average_m_j_err_176_redu_axis, grad_redu_axis, intercept_redu_axis)

# Print results

#print('')
#print('FINAL RESULTS (all mass values in units of GeV/c^2):')
#print('')
#print('ttpp8AF2.root:  \t%.4f +/- %.4f ' %(m_t_8AF2, m_t_err_8AF2))
#print('ttpp8m169.root: \t%.4f +/- %.4f ' %(m_t_m169, m_t_err_m169))
#print('ttpp8m172.root: \t%.4f +/- %.4f ' %(m_t_m172, m_t_err_m172))
#print('ttpp8m173.root: \t%.4f +/- %.4f ' %(m_t_m173, m_t_err_m173))
#print('ttpp8m176.root: \t%.4f +/- %.4f ' %(m_t_m176, m_t_err_m176))
#print('')
#print('Now for the case when the mass range matches that in paper:')
#print('')
#print('ttpp8AF2.root:  \t%.4f +/- %.4f ' %(m_t_8AF2_redu_axis, m_t_err_8AF2_redu_axis))
#print('ttpp8m169.root: \t%.4f +/- %.4f ' %(m_t_m169_redu_axis, m_t_err_m169_redu_axis))
#print('ttpp8m172.root: \t%.4f +/- %.4f ' %(m_t_m172_redu_axis, m_t_err_m172_redu_axis))
#print('ttpp8m173.root: \t%.4f +/- %.4f ' %(m_t_m173_redu_axis, m_t_err_m173_redu_axis))
#print('ttpp8m176.root: \t%.4f +/- %.4f ' %(m_t_m176_redu_axis, m_t_err_m176_redu_axis))
#print('')

########
######## The following is using the TMinuit method to investigate the difference between using the full and fast simulations
######## The systematic uncertainties investigation will be coded in another macro
########

average_m_j_ttallpp8_nominal = 
average_m_j_ttallpp8_nominal_err = 
grad = 0.482928928781
intercept = 85.5312105223

m_t_ttallpp8_nominal, m_t_err_ttallpp8_nominal = getm_t(average_m_j_ttallpp8_nominal, average_m_j_ttallpp8_nominal_err, grad, intercept)
