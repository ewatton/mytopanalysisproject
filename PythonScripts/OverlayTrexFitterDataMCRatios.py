# Macro that will overlay histograms and save them to PDFs
# First created by Elliot Watton - 11/10/2021
# Last editted by Elliot Watton - 11/01/2022

import ROOT as r
import matplotlib.pyplot as plt
import numpy as np
r.TH1.SetDefaultSumw2(True)

# variables = ["lead_subjet_pt","lepbjet_pt","lepPhi","lepPt","met","mtw","mW","nSubjets","sublead_subjet_pt","topjet_pt","blep_hadtop_deltaPhi","ttbar_deltaPhi","leadadd_subleadadd_deltaPhi","leadadd_hadtop_deltaPhi","subleadadd_hadtop_deltaPhi"]
# # variables = ["mW"]
# # directory = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/boosted-top-mass-pfit/TRExConfigs/nominalFit/mWFit/Plots/"
# directory = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/nominalFit/mWFit/Plots/"
# for i in range(0,len(variables)):

#     prefit_file = open(directory + variables[i] + "_prefit.yaml")
#     prefit_lines = prefit_file.readlines()
#     for j in range(0, len(prefit_lines)-1):
#         line = prefit_lines[j]
#         if "Total" in line:
#             # print(prefit_lines[j+1])
#             prefit_mcYieldLine = prefit_lines[j+1].replace(prefit_lines[j+1][:12],'',1)
#             prefit_mcYieldLine = prefit_mcYieldLine.replace("]","")
#             prefit_mcYieldLine = prefit_mcYieldLine.replace(",","")
#             prefit_mcYieldLine = prefit_mcYieldLine.replace("\n","")
#             prefit_mcYeilds = list(prefit_mcYieldLine.split(" "))
#             prefit_mcYeilds = [float(x) for x in prefit_mcYeilds]
#             # print(prefit_mcYeilds)
#             # print(prefit_lines[j+2])
#             prefit_mcUncLine = prefit_lines[j+2].replace(prefit_lines[j+2][:20],'',1)
#             prefit_mcUncLine = prefit_mcUncLine.replace("]","")
#             prefit_mcUncLine = prefit_mcUncLine.replace(",","")
#             prefit_mcUncLine = prefit_mcUncLine.replace("\n","")
#             prefit_mcUncs = list(prefit_mcUncLine.split(" "))
#             prefit_mcUncs = [float(x) for x in prefit_mcUncs]
#             # print(prefit_mcUncs)
#         if "Data" in line:
#             dataYieldLine = prefit_lines[j+1].replace(prefit_lines[j+1][:12],'',1)
#             dataYieldLine = dataYieldLine.replace("]","")
#             dataYieldLine = dataYieldLine.replace(",","")
#             dataYieldLine = dataYieldLine.replace("\n","")
#             dataYields = list(dataYieldLine.split(" "))
#             dataYields = [float(x) for x in dataYields]
#             # print(dataYields)
#         if "Figure" in line:
#             binningLine = prefit_lines[j+1].replace(prefit_lines[j+1][:15],'',1)
#             binningLine = binningLine.replace("]","")
#             binningLine = binningLine.replace(",","")
#             binningLine = binningLine.replace("\n","")
#             binning = list(binningLine.split(" "))
#             binning = [float(x) for x in binning]
#             binCentres = []
#             for k in range(0,len(binning)-1):
#                 # print((binning[k]+binning[k+1])*0.5)
#                 binCentres.append((binning[k]+binning[k+1])*0.5)
#     prefit_file.close()
    
#     postfit_file = open(directory + variables[i] + "_postfit.yaml")
#     postfit_lines = postfit_file.readlines()
#     for j in range(0, len(postfit_lines)-1):
#         line = postfit_lines[j]
#         if "Total" in line:
#             # print(postfit_lines[j+1])
#             postfit_mcYieldLine = postfit_lines[j+1].replace(postfit_lines[j+1][:12],'',1)
#             postfit_mcYieldLine = postfit_mcYieldLine.replace("]","")
#             postfit_mcYieldLine = postfit_mcYieldLine.replace(",","")
#             postfit_mcYieldLine = postfit_mcYieldLine.replace("\n","")
#             postfit_mcYeilds = list(postfit_mcYieldLine.split(" "))
#             postfit_mcYeilds = [float(x) for x in postfit_mcYeilds]
#             # print(postfit_mcYeilds)
#             # print(postfit_lines[j+2])
#             postfit_mcUncLine = postfit_lines[j+2].replace(postfit_lines[j+2][:20],'',1)
#             postfit_mcUncLine = postfit_mcUncLine.replace("]","")
#             postfit_mcUncLine = postfit_mcUncLine.replace(",","")
#             postfit_mcUncLine = postfit_mcUncLine.replace("\n","")
#             postfit_mcUncs = list(postfit_mcUncLine.split(" "))
#             postfit_mcUncs = [float(x) for x in postfit_mcUncs]
#             # print(postfit_mcUncs)
#     postfit_file.close()
#     x = binCentres
#     y_prefit = []
#     y_postfit = []
#     yerr_prefit = []
#     yerr_postfit = []
#     for j in range(0,len(dataYields)):
#         y_prefit.append(dataYields[j]/prefit_mcYeilds[j])
#         y_postfit.append(dataYields[j]/postfit_mcYeilds[j])
#         yerr_prefit.append((prefit_mcUncs[j]/prefit_mcYeilds[j])*(dataYields[j]/prefit_mcYeilds[j]))
#         yerr_postfit.append((postfit_mcUncs[j]/postfit_mcYeilds[j])*(dataYields[j]/postfit_mcYeilds[j]))            
#     fig = plt.figure()
#     # plt.errorbar(x, y_prefit, yerr=yerr_prefit, marker='o' , color='b', label='Prefit', linestyle='--')
#     # plt.errorbar(x, y_postfit, yerr=yerr_postfit, marker='o' ,color='r', label='Postfit', linestyle='--')
#     plt.plot(x,y_prefit, marker='o' , color='b', label='Prefit', linestyle='--')
#     plt.plot(x,y_postfit, marker='o' ,color='r', label='Postfit', linestyle='--')
#     plt.axhline(y = 1.0, color = 'k', linestyle = '-', alpha=0.5)
#     plt.title(variables[i])
#     plt.grid(True)
#     plt.ylabel("Data/MC")
#     if ("pt" in variables[i]) or ("Pt" in variables[i]) or ("pT" in variables[i]):
#         plt.xlabel("pT [GeV]")
#     elif "Phi" in variables[i]:
#         plt.xlabel("Phi (radians)")
#     elif variables[i] == "mtw":
#         plt.xlabel("mtw [GeV]")
#     elif variables[i] == "met":
#         plt.xlabel("MET [GeV]")
#     elif variables[i] == "mW":
#         plt.xlabel("mW [GeV]")
#     elif variables[i] == "nSubjet":
#         plt.xlabel("Number of sub-jets")

#     plt.legend(loc='lower left')
#     plt.savefig(directory + variables[i] + '_comparison.pdf')

####################################################################################
####################################################################################
####################################################################################

def chi2(y,yerr):
    result = 0
    for i in range(0,len(y)):
        result += ((y[i] - 1.0)*(y[i] - 1.0))/(yerr[i]*yerr[i])
    return result

variables = ["mW","lepPhi","lepbjet_pt","topjet_pt","nSubjets","lead_subjet_pt","sublead_subjet_pt","met","mtw","lepPt","blep_hadtop_deltaPhi","ttbar_deltaPhi","leadadd_subleadadd_deltaPhi","leadadd_hadtop_deltaPhi","subleadadd_hadtop_deltaPhi"]
prefitErrorUpLineNumbers = [1,4,7,10,13,16,19,22,25,28,31,34,37,40,43]
prefitErrorDownLineNumbers = [2,5,8,11,14,17,20,23,26,29,32,35,38,41,44]
postfitErrorUpLineNumbers = [52,55,58,61,64,67,70,73,76,79,82,85,88,91,94]
postfitErrorDownLineNumbers = [53,56,59,62,65,68,71,74,77,80,83,86,89,92,95]
directory_nominal = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/nominalFit_dataFit/mWFit/Plots/"
directory_alt = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/nominalFit_HerwigNP1_dataFit/mWFit/Plots/"
outputFolderDir = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/histogram_images/2023/"
for i in range(0,len(variables)):

    prefit_file_nominal = open(directory_nominal + variables[i] + "_prefit.yaml")
    prefit_lines_nominal = prefit_file_nominal.readlines()
    for j in range(0, len(prefit_lines_nominal)-1):
        line = prefit_lines_nominal[j]
        if "Total" in line:
            prefit_mcYieldLine_nominal = prefit_lines_nominal[j+1].replace(prefit_lines_nominal[j+1][:12],'',1)
            prefit_mcYieldLine_nominal = prefit_mcYieldLine_nominal.replace("]","")
            prefit_mcYieldLine_nominal = prefit_mcYieldLine_nominal.replace(",","")
            prefit_mcYieldLine_nominal = prefit_mcYieldLine_nominal.replace("\n","")
            prefit_mcYeilds_nominal = list(prefit_mcYieldLine_nominal.split(" "))
            prefit_mcYeilds_nominal = [float(x) for x in prefit_mcYeilds_nominal]
            prefit_mcUncLine_nominal = prefit_lines_nominal[j+2].replace(prefit_lines_nominal[j+2][:20],'',1)
            prefit_mcUncLine_nominal = prefit_mcUncLine_nominal.replace("]","")
            prefit_mcUncLine_nominal = prefit_mcUncLine_nominal.replace(",","")
            prefit_mcUncLine_nominal = prefit_mcUncLine_nominal.replace("\n","")
            prefit_mcUncs_nominal = list(prefit_mcUncLine_nominal.split(" "))
            prefit_mcUncs_nominal = [float(x) for x in prefit_mcUncs_nominal]
        if "Data" in line:
            dataYieldLine_nominal = prefit_lines_nominal[j+1].replace(prefit_lines_nominal[j+1][:12],'',1)
            dataYieldLine_nominal = dataYieldLine_nominal.replace("]","")
            dataYieldLine_nominal = dataYieldLine_nominal.replace(",","")
            dataYieldLine_nominal = dataYieldLine_nominal.replace("\n","")
            dataYields_nominal = list(dataYieldLine_nominal.split(" "))
            dataYields_nominal = [float(x) for x in dataYields_nominal]
        if "Figure" in line:
            binningLine_nominal = prefit_lines_nominal[j+1].replace(prefit_lines_nominal[j+1][:15],'',1)
            binningLine_nominal = binningLine_nominal.replace("]","")
            binningLine_nominal = binningLine_nominal.replace(",","")
            binningLine_nominal = binningLine_nominal.replace("\n","")
            binning_nominal = list(binningLine_nominal.split(" "))
            binning_nominal = [float(x) for x in binning_nominal]
            binCentres_nominal = []
            for k in range(0,len(binning_nominal)-1):
                binCentres_nominal.append((binning_nominal[k]+binning_nominal[k+1])*0.5)
    prefit_file_nominal.close()
    
    postfit_file_nominal = open(directory_nominal + variables[i] + "_postfit.yaml")
    postfit_lines_nominal = postfit_file_nominal.readlines()
    for j in range(0, len(postfit_lines_nominal)-1):
        line = postfit_lines_nominal[j]
        if "Total" in line:
            postfit_mcYieldLine_nominal = postfit_lines_nominal[j+1].replace(postfit_lines_nominal[j+1][:12],'',1)
            postfit_mcYieldLine_nominal = postfit_mcYieldLine_nominal.replace("]","")
            postfit_mcYieldLine_nominal = postfit_mcYieldLine_nominal.replace(",","")
            postfit_mcYieldLine_nominal = postfit_mcYieldLine_nominal.replace("\n","")
            postfit_mcYeilds_nominal = list(postfit_mcYieldLine_nominal.split(" "))
            postfit_mcYeilds_nominal = [float(x) for x in postfit_mcYeilds_nominal]
            postfit_mcUncLine_nominal = postfit_lines_nominal[j+2].replace(postfit_lines_nominal[j+2][:20],'',1)
            postfit_mcUncLine_nominal = postfit_mcUncLine_nominal.replace("]","")
            postfit_mcUncLine_nominal = postfit_mcUncLine_nominal.replace(",","")
            postfit_mcUncLine_nominal = postfit_mcUncLine_nominal.replace("\n","")
            postfit_mcUncs_nominal = list(postfit_mcUncLine_nominal.split(" "))
            postfit_mcUncs_nominal = [float(x) for x in postfit_mcUncs_nominal]
    postfit_file_nominal.close()

    postfit_file_alt = open(directory_alt + variables[i] + "_postfit.yaml")
    postfit_lines_alt = postfit_file_alt.readlines()
    for j in range(0, len(postfit_lines_alt)-1):
        line = postfit_lines_alt[j]
        if "Total" in line:
            postfit_mcYieldLine_alt = postfit_lines_alt[j+1].replace(postfit_lines_alt[j+1][:12],'',1)
            postfit_mcYieldLine_alt = postfit_mcYieldLine_alt.replace("]","")
            postfit_mcYieldLine_alt = postfit_mcYieldLine_alt.replace(",","")
            postfit_mcYieldLine_alt = postfit_mcYieldLine_alt.replace("\n","")
            postfit_mcYeilds_alt = list(postfit_mcYieldLine_alt.split(" "))
            postfit_mcYeilds_alt = [float(x) for x in postfit_mcYeilds_alt]
            postfit_mcUncLine_alt = postfit_lines_alt[j+2].replace(postfit_lines_alt[j+2][:20],'',1)
            postfit_mcUncLine_alt = postfit_mcUncLine_alt.replace("]","")
            postfit_mcUncLine_alt = postfit_mcUncLine_alt.replace(",","")
            postfit_mcUncLine_alt = postfit_mcUncLine_alt.replace("\n","")
            postfit_mcUncs_alt = list(postfit_mcUncLine_alt.split(" "))
            postfit_mcUncs_alt = [float(x) for x in postfit_mcUncs_alt]
    postfit_file_alt.close()
    
    y_prefit_nominal = []
    y_postfit_nominal = []
    y_postfit_alt = []
    for j in range(0,len(dataYields_nominal)):
        y_prefit_nominal.append(dataYields_nominal[j]/prefit_mcYeilds_nominal[j])
        y_postfit_nominal.append(dataYields_nominal[j]/postfit_mcYeilds_nominal[j])
        y_postfit_alt.append(dataYields_nominal[j]/postfit_mcYeilds_alt[j]) 

    ratioErrors_nominal_file = open("/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/RatioErrors_nominal.txt")
    ratioErrors_nominal_lines = ratioErrors_nominal_file.readlines()
    
    prefit_line_up = ratioErrors_nominal_lines[prefitErrorUpLineNumbers[i]-1]
    prefit_yerr_up_line_nominal = prefit_line_up.replace(" \n","")
    prefit_yerr_up_nominal = list(prefit_yerr_up_line_nominal.split(" "))
    prefit_yerr_up_nominal = [float(x) for x in prefit_yerr_up_nominal]

    prefit_line_down = ratioErrors_nominal_lines[prefitErrorDownLineNumbers[i]-1]
    prefit_yerr_down_line_nominal = prefit_line_down.replace(" \n","")
    prefit_yerr_down_nominal = list(prefit_yerr_down_line_nominal.split(" "))
    prefit_yerr_down_nominal = [float(x) for x in prefit_yerr_down_nominal]
    
    postfit_line_up = ratioErrors_nominal_lines[postfitErrorUpLineNumbers[i]-1]
    postfit_yerr_up_line_nominal = postfit_line_up.replace(" \n","")
    postfit_yerr_up_nominal = list(postfit_yerr_up_line_nominal.split(" "))
    postfit_yerr_up_nominal = [float(x) for x in postfit_yerr_up_nominal]

    postfit_line_down = ratioErrors_nominal_lines[postfitErrorDownLineNumbers[i]-1]
    postfit_yerr_down_line_nominal = postfit_line_down.replace(" \n","")
    postfit_yerr_down_nominal = list(postfit_yerr_down_line_nominal.split(" "))
    postfit_yerr_down_nominal = [float(x) for x in postfit_yerr_down_nominal]
    
    postfit_file_alt.close()

    ratioErrors_alt_file = open("/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/TRExFitter/TRExFitter/RatioErrors_alt.txt")
    ratioErrors_alt_lines = ratioErrors_alt_file.readlines()
    
    postfit_line_up = ratioErrors_alt_lines[postfitErrorUpLineNumbers[i]-1]
    postfit_yerr_up_line_alt = postfit_line_up.replace(" \n","")
    postfit_yerr_up_alt = list(postfit_yerr_up_line_alt.split(" "))
    postfit_yerr_up_alt = [float(x) for x in postfit_yerr_up_alt]

    postfit_line_down = ratioErrors_alt_lines[postfitErrorDownLineNumbers[i]-1]
    postfit_yerr_down_line_alt = postfit_line_down.replace(" \n","")
    postfit_yerr_down_alt = list(postfit_yerr_down_line_alt.split(" "))
    postfit_yerr_down_alt = [float(x) for x in postfit_yerr_down_alt]
    
    postfit_file_alt.close()

    fig, ax = plt.subplots()
    
    # plt.plot(x,y_prefit_nominal, marker='o' , color='b', label='Prefit', linestyle='--')
    # plt.plot(x,y_postfit_nominal, marker='o' ,color='r', label='Postfit (nominal)', linestyle='--')
    # plt.plot(x,y_postfit_alt, marker='o' ,color='g', label='Postfit (Herwig NP fixed to +1)', linestyle='--')

    asymmetric_error_prefit = np.array(list(zip(prefit_yerr_down_nominal, prefit_yerr_up_nominal))).T
    asymmetric_error_postfit_nominal = np.array(list(zip(postfit_yerr_down_nominal, postfit_yerr_up_nominal))).T
    asymmetric_error_postfit_alt = np.array(list(zip(postfit_yerr_down_alt, postfit_yerr_up_alt))).T

    ax.errorbar(binCentres_nominal,y_prefit_nominal, yerr=asymmetric_error_prefit, marker='o' , color='b', label='Prefit', linestyle='--')
    ax.errorbar(binCentres_nominal,y_postfit_nominal, yerr=asymmetric_error_postfit_nominal, marker='o' ,color='r', label='Postfit (nominal)', linestyle='--')
    ax.errorbar(binCentres_nominal,y_postfit_alt, yerr=asymmetric_error_postfit_alt, marker='o' ,color='g', label='Postfit (Herwig NP fixed to +1)', linestyle='--')

    prefit_yerr_average = []
    postfit_nominal_yerr_average = []
    postfit_alt_yerr_average = []

    for j in range(0,len(binCentres_nominal)):
        prefit_yerr_average.append((prefit_yerr_down_nominal[j]+prefit_yerr_up_nominal[j])*0.5)
        postfit_nominal_yerr_average.append((postfit_yerr_down_nominal[j]+postfit_yerr_up_nominal[j])*0.5)
        postfit_alt_yerr_average.append((postfit_yerr_down_alt[j]+postfit_yerr_up_alt[j])*0.5)

    prefit_chi2 = chi2(y_prefit_nominal,prefit_yerr_average)
    postfit_nominal_chi2 = chi2(y_prefit_nominal,postfit_nominal_yerr_average)
    postfit_alt_chi2 = chi2(y_prefit_nominal,postfit_alt_yerr_average)

    ndof = len(binCentres_nominal) - 2

    textstr = '\n'.join((
    'Reduced $\chi^2$ respect to perfect agreement',
    r'Prefit red. $\chi^2 = %.2f$/%i' % (prefit_chi2, ndof),
    r'Postfit (nominal) red. $\chi^2 = %.2f$/%i' % (postfit_nominal_chi2, ndof),
    r'Postfit (Herwig fixed) red. $\chi^2 = %.2f$/%i' % (postfit_alt_chi2, ndof)))
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=7, verticalalignment='top', bbox=props)

    plt.axhline(y = 1.0, color = 'k', linestyle = '-', alpha=0.5)
    plt.title(variables[i])
    plt.grid(True)
    plt.ylabel("Data/MC")
    if ("pt" in variables[i]) or ("Pt" in variables[i]) or ("pT" in variables[i]):
        plt.xlabel("pT [GeV]")
    elif "Phi" in variables[i]:
        plt.xlabel("Phi (radians)")
    elif variables[i] == "mtw":
        plt.xlabel("mtw [GeV]")
    elif variables[i] == "met":
        plt.xlabel("MET [GeV]")
    elif variables[i] == "mW":
        plt.xlabel("mW [GeV]")
    elif variables[i] == "nSubjet":
        plt.xlabel("Number of sub-jets")

    plt.legend(loc='lower left')
    plt.savefig(outputFolderDir + "2023-10-27/" + variables[i] + '_HerwigNP1_comparison.pdf')
