# A macro that will find the average top jet mass from previously made histograms, and then use TMinuit to find the top quark mass.
# This will use the linear relation found previously.
# First created by Elliot Watton - 18/10/2021
# Last editted by Elliot Watton - 14/12/2021

import ROOT as r
import ROOT
from ROOT import TMinuit , Double , Long
from array import array as arr
from array import array
import math
import numpy as np
r.TH1.SetDefaultSumw2(True)

########
######## Function definitions
########

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = A + (B*(m_t - 172.5))
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global B
    B = s
    global A
    A = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe

    return final_m_t, final_m_t_err

########
######## Main body of code
########


# Initialise global variables 
average_m_j = 0       # m_j
average_m_j_err = 0   # sigma_{m_j}
A = 0
B = 0
npar = 1              # Number of fit parameters


# Values of the constants in the equations for mJ and mW
A = 168.832795
B = 0.476824

# -------------------------------------------------------------------------------------------

# Main body of code

# -------------------------------------------------------------------------------------------


# -------------------------------------------------------------------------------------------
# Non-JER systematics
# -------------------------------------------------------------------------------------------


# Get a sorted list of mJ histogram names
# -------------------------------------------------------------------------------------------

# Get nominal first
names_mJ_nominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names_mJ_nominal.append(h.GetName())
histfile.Close()

names_mJ_noNominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
        names_mJ_noNominal.append(h.GetName())
histfile.Close()

names_mJ_noNominal.sort()

names_mJ_JER = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
        names_mJ_JER.append(h.GetName().rstrip("_mJ_combined") + "_topjet_m")
histfile.Close()

names_mJ_JER.sort()


names_mJ = names_mJ_nominal + names_mJ_noNominal + names_mJ_JER 

print('')
#print(names_mJ)
print('')

# Get a sorted list of mW histogram names
# -------------------------------------------------------------------------------------------

# Get nominal first
names_mW_nominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names_mW_nominal.append(h.GetName())
histfile.Close()

names_mW_noNominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
        names_mW_noNominal.append(h.GetName())
histfile.Close()

names_mW_noNominal.sort()

names_mW_JER = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
        names_mW_JER.append(h.GetName().rstrip("_mW_combined") + "_topjet_W_invMass")
histfile.Close()

names_mW_JER.sort()


names_mW = names_mW_nominal + names_mW_noNominal + names_mW_JER

print('')
#print(names_mW)
print('')


# Get a list of mJ values and mJ errors in the SAME order as the sorted mJ names list
# -------------------------------------------------------------------------------------------

names = []
mJ_values = []
mJ_errors = []

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names.append(h.GetName().rstrip("_topjet_m"))
        hist = h
        mJ_values.append(hist.GetMean())
        mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mJ)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and (h.GetName() == names_mJ[i]) and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
            names.append(h.GetName().rstrip("_topjet_m"))
            hist = h
            mJ_values.append(hist.GetMean())
            mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mJ)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and ((h.GetName().rstrip("_mJ_combined") + "_topjet_m") == names_mJ[i]) and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
            names.append(h.GetName().rstrip("_mJ_combined"))
            hist = h
            mJ_values.append(hist.GetMean())
            mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

print('')
#print(names)
print('')


# Get a list of mW histograms in the SAME order as the sorted mW names list
# -------------------------------------------------------------------------------------------

histograms = []

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        hist = h
        hist.SetDirectory(0)
        histograms.append(hist)
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mW)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and (h.GetName() == names_mW[i]) and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
            hist = h
            hist.SetDirectory(0)
            histograms.append(hist)
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mW)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and ((h.GetName().rstrip("_mW_combined") + "_topjet_W_invMass") == names_mW[i]) and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
            hist = h
            hist.SetDirectory(0)
            histograms.append(hist)
histfile.Close()

JSF_values = []
JSF_errors = []
mtop_values = []
mtop_errors = []
diffFromNominal = []

# Get the results and fill lists so that the ordering of each list is the same as the initial names_mJ and names_mW ordered lists.
# ----------------------------------------------------------------------------------------------------------------------------------


for i in range(0,len(names)):
    mtop, mtop_error = getm_t(mJ_values[i], mJ_errors[i], B, A)
    mtop_values.append(mtop)
    mtop_errors.append(mtop_error)
    diffFromNominal.append(mtop-173.358951)

# Print table of results for all systematics.
# ----------------------------------------------------------------------------------------------------------------------------------

print("\n\n\nBelow is LaTeX code for table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Systematics","$\overline{\\textup{m}_\\textup{top-jet}}$ (GeV)","$\overline{\\textup{m}_\\textup{top-jet}}$ error (GeV)","m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mJ_values[i],mJ_errors[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\n\n\nBelow is LaTeX code for simplified table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccc}\\toprule")

headings = ["Systematics", "m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")


# ------------------------------------------------------------------------------------------------------
# Find positions of the different systematics
# ------------------------------------------------------------------------------------------------------


# Find the samples which do not have an up or down template, need to handle their results differently
# ------------------------------------------------------------------------------------------------------

positionsOfSystematicsWithNoUpOrDownTemplate = []
for i in range(0,len(names)):
    if ("__1u" not in names[i]) and ("__1dow" not in names[i]) and (i != 0):
        positionsOfSystematicsWithNoUpOrDownTemplate.append(i)

print('')
print("Positions where there are no up or down templates:")
print(positionsOfSystematicsWithNoUpOrDownTemplate)
print('')
print("Names of systematics samples which have no up or down templates:")
names_noUpOrDown = []
for i in range(0,len(positionsOfSystematicsWithNoUpOrDownTemplate)):
    names_noUpOrDown.append(names[positionsOfSystematicsWithNoUpOrDownTemplate[i]])
    print(names[positionsOfSystematicsWithNoUpOrDownTemplate[i]])
print('')

# Find positions of the JES systematics
# ------------------------------------------------------------------------------------------------------

positionsOfJESSystematics = []
for i in range(0,len(names)):
    if ("JET" in names[i]) and ("JER" not in names[i]):
        positionsOfJESSystematics.append(i)

print('')
print("Positions where the JES systematics are:")
print(positionsOfJESSystematics)
print('')
print("Names of JES systematics:")
names_JES = []
for i in range(0,len(positionsOfJESSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfJESSystematics[i]].partition("__")
        print(before)
        names_JES.append(before)
print('')

# Find positions of the lepton systematics
# ------------------------------------------------------------------------------------------------------

positionsOfLeptonSystematics = []
for i in range(0,len(names)):
    if ("EG" in names[i]) or ("MUON" in names[i]):
        positionsOfLeptonSystematics.append(i)

print('')
print("Positions where the lepton systematics are:")
print(positionsOfLeptonSystematics)
print('')
print("Names of lepton systematics:")
names_leptons = []
for i in range(0,len(positionsOfLeptonSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfLeptonSystematics[i]].partition("__")
        print(before)
        names_leptons.append(before)
print('')

# Find positions of the MET systematics
# ------------------------------------------------------------------------------------------------------

positionsOfMETSystematics = []
for i in range(0,len(names)):
    if ("MET" in names[i]) and (("_1up" in names[i]) or ("_1down" in names[i])):
        positionsOfMETSystematics.append(i)

print('')
print("Positions where the MET systematics are:")
print(positionsOfMETSystematics)
print('')
print("Names of MET systematics:")
names_MET = []
for i in range(0,len(positionsOfMETSystematics)):
    if (names[positionsOfMETSystematics[i]] in names_noUpOrDown):
        continue
    elif i % 2 == 0:
        before, sep, after = names[positionsOfMETSystematics[i]].partition("__")
        print(before)
        names_MET.append(before)
print('')

# Find positions of the JER systematics
# ------------------------------------------------------------------------------------------------------

positionsOfJERSystematics = []
for i in range(0,len(names)):
    if ("JER" in names[i]):
        positionsOfJERSystematics.append(i)

print('')
print("Positions where the JER systematics are:")
print(positionsOfJERSystematics)
print('')
print("Names of JER systematics:")
names_JER = []
for i in range(0,len(positionsOfJERSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfJERSystematics[i]].partition("__")
        print(before)
        names_JER.append(before)
print('')

# ------------------------------------------------------------------------------------------------------
# Work out systematic uncertiainties on mtop
# ------------------------------------------------------------------------------------------------------

# JES
# ------------------------------------------------------------------------------------------------------

JES_uncertainties = []
for i in range(0,len(positionsOfJESSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfJESSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfJESSystematics[(2*i)+1]] - mtop_values[0])
    JES_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    JES_uncertainties.append(JES_uncertainty)

JES_uncertainties_squared = []
for i in range(0,len(JES_uncertainties)):
    JES_uncertainties_squared.append(JES_uncertainties[i]*JES_uncertainties[i])

sumOfJESUncertaintiesSquared = 0
for i in range(0,len(JES_uncertainties_squared)):
    sumOfJESUncertaintiesSquared += JES_uncertainties_squared[i]

Total_JES_uncetainty = np.sqrt(sumOfJESUncertaintiesSquared)

JES_tuple = zip(JES_uncertainties, names_JES)
topthree_JES_tuple = sorted(JES_tuple, reverse=True)[:3]

# MET
# ------------------------------------------------------------------------------------------------------

MET_uncertainties = []
for i in range(0,len(positionsOfMETSystematics)):
    downDiffFromNominal = abs(mtop_values[positionsOfMETSystematics[i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfMETSystematics[i]+1] - mtop_values[0])
    MET_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    MET_uncertainties.append(MET_uncertainty)

for i in range(0,len(positionsOfSystematicsWithNoUpOrDownTemplate)):
    MET_uncertainty = abs(mtop_values[positionsOfSystematicsWithNoUpOrDownTemplate[i]] - mtop_values[0])
    MET_uncertainties.append(MET_uncertainty)

MET_uncertainties_squared = []
for i in range(0,len(MET_uncertainties)):
    MET_uncertainties_squared.append(MET_uncertainties[i]*MET_uncertainties[i])

sumOfMETUncertaintiesSquared = 0
for i in range(0,len(MET_uncertainties_squared)):
    sumOfMETUncertaintiesSquared += MET_uncertainties_squared[i]

Total_MET_uncetainty = np.sqrt(sumOfMETUncertaintiesSquared)

# Leptons
# ------------------------------------------------------------------------------------------------------

LEP_uncertainties = []
for i in range(0,len(positionsOfLeptonSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfLeptonSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfLeptonSystematics[(2*i)+1]] - mtop_values[0])
    LEP_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    LEP_uncertainties.append(LEP_uncertainty)

LEP_uncertainties_squared = []
for i in range(0,len(LEP_uncertainties)):
    LEP_uncertainties_squared.append(LEP_uncertainties[i]*LEP_uncertainties[i])

sumOfLEPUncertaintiesSquared = 0
for i in range(0,len(LEP_uncertainties_squared)):
    sumOfLEPUncertaintiesSquared += LEP_uncertainties_squared[i]

Total_LEP_uncetainty = np.sqrt(sumOfLEPUncertaintiesSquared)

# JER
# ------------------------------------------------------------------------------------------------------

JER_uncertainties = []
for i in range(0,len(positionsOfJERSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfJERSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfJERSystematics[(2*i)+1]] - mtop_values[0])
    JER_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    JER_uncertainties.append(JER_uncertainty)

JER_uncertainties_squared = []
for i in range(0,len(JER_uncertainties)):
    JER_uncertainties_squared.append(JER_uncertainties[i]*JER_uncertainties[i])

sumOfJERUncertaintiesSquared = 0
for i in range(0,len(JER_uncertainties_squared)):
    sumOfJERUncertaintiesSquared += JER_uncertainties_squared[i]

Total_JER_uncetainty = np.sqrt(sumOfJERUncertaintiesSquared)


# Now make final LaTeX summary table for systematics
# ------------------------------------------------------------------------------------------------------

print("\\begin{table}[!htb]")
print("\\centering")
print("\\begin{tabular}{lc}\\toprule")

headings = ["Systematics", "Uncertainty on m$_\\textup{top}$ (GeV)"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

Total_Systematic_Uncertainty = np.sqrt((Total_JES_uncetainty*Total_JES_uncetainty)+(Total_JER_uncetainty*Total_JER_uncetainty)+(Total_MET_uncetainty*Total_MET_uncetainty)+(Total_LEP_uncetainty*Total_LEP_uncetainty))
Total_uncertainty_names = ["JES",topthree_JES_tuple[0][1],topthree_JES_tuple[1][1],topthree_JES_tuple[2][1],"JER","MET","Leptons","Total"]
Total_uncertainties = [Total_JES_uncetainty,topthree_JES_tuple[0][0],topthree_JES_tuple[1][0],topthree_JES_tuple[2][0],Total_JER_uncetainty,Total_MET_uncetainty,Total_LEP_uncetainty,Total_Systematic_Uncertainty]

for i in range(0,len(Total_uncertainty_names)):
    linestring = ""
    lines = [Total_uncertainty_names[i],Total_uncertainties[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , " ")
            linestring += (str(lines[j]) + " & ")
            if i == len(Total_uncertainty_names)-1:
                print("\\hline")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\nScript has finished\n")



