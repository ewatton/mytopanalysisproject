import ROOT as r
from array import array
from optparse import OptionParser

r.TH1.SetDefaultSumw2(True)

def CopyTree(inputFilename,outputFilename,isFSRup,isFSRdown,ispThard,isRTT):

    if isRTT:
        DSIDs = [601356,601357]
    elif ispThard:
        DSIDs = [601491,601497]
    elif isFSRup:
        DSIDs = [601670,601672]
    elif isFSRdown:
        DSIDs = [601669,601671]

    inputFile = r.TFile.Open(inputFilename,"READ")
    inputTree = inputFile.Get("nominal")
    outputFile = r.TFile.Open(outputFilename,"update")
    outputTree = inputTree.CloneTree(0)

    for i in range(0, inputTree.GetEntries()):
        inputTree.GetEntry(i)
        DSID = getattr(inputTree, "mcChannelNumber")
        if (DSID in DSIDs):
            outputTree.Fill()
    outputTree.AutoSave()
    outputFile.Close()

def main():

    parser = OptionParser()
    
    parser.add_option("-i", "--infile",
                  action="store", type="string", dest="inputFilename")
    parser.add_option("-o", "--outfile",
                  action="store", type="string", dest="outputFilename")
    parser.add_option("-u", "--FSRup", dest="FSRup", action = "store_true", default = False,
                        help="Copy and fix FSR up files")
    parser.add_option("-d", "--FSRdown", dest="FSRdown", action = "store_true", default = False,
                        help="Copy and fix FSR down files")
    parser.add_option("-p", "--pThard", dest="pThard", action = "store_true", default = False,
                        help="Copy and fix pThard files")
    parser.add_option("-r", "--RTT", dest="RTT", action = "store_true", default = False,
                        help="Copy and fix RTT files")

    (options, args) = parser.parse_args()  
    print(options)
    print(args)
    

    CopyTree(options.inputFilename,options.outputFilename,options.FSRup,options.FSRdown,options.pThard,options.RTT)
    
if __name__ == "__main__":
    main()