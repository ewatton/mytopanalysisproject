# A macro that will find the average top jet mass from previously made histograms, and then use TMinuit to find the top quark mass.
# This will use the linear relation found previously.
# First created by Elliot Watton - 18/10/2021
# Last editted by Elliot Watton - 14/12/2021

import ROOT as r
import ROOT
from ROOT import TMinuit , Double , Long
from array import array as arr
from array import array
import math
import numpy as np
r.TH1.SetDefaultSumw2(True)

########
######## Function definitions
########

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = A + (B*(m_t - 172.5))
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global B
    B = s
    global A
    A = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe

    return final_m_t, final_m_t_err

########
######## Main body of code
########


# Initialise global variables 
average_m_j = 0       # m_j
average_m_j_err = 0   # sigma_{m_j}
A = 0
B = 0
npar = 1              # Number of fit parameters


# Values of the constants in the equations for mJ and mW
A = 168.829444
B = 0.479741

# Get nominal first
names_mJ_nominal = []
histogram_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/mJ/JSF_1_00_ttallpp8_systematics.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names_mJ_nominal.append(h.GetName())
histfile.Close()

names_mJ_noNominal = []
histogram_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/mJ/JSF_1_00_ttallpp8_otherSystematicWeights.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D'):
        names_mJ_noNominal.append(h.GetName())
histfile.Close()
#names_mJ_noNominal.sort()

names_mJ = names_mJ_nominal + names_mJ_noNominal

print('')
#print(names_mJ)
print('')

# Get a list of mJ values and mJ errors in the SAME order as the sorted mJ names list
# -------------------------------------------------------------------------------------------

names = []
mJ_values = []
mJ_errors = []

histogram_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/mJ/JSF_1_00_ttallpp8_systematics.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names.append(h.GetName().rstrip("_topjet_m"))
        hist = h
        mJ_values.append(hist.GetMean())
        mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))
histfile.Close()

histogram_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/mJ/JSF_1_00_ttallpp8_otherSystematicWeights.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mJ)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and (h.GetName() == names_mJ[i]):
            names.append(h.GetName().rstrip("_topjet_m"))
            hist = h
            mJ_values.append(hist.GetMean())
            mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

print('')
#print(names)
print('')

mtop_values = []
mtop_errors = []
diffFromNominal = []

# Get the results and fill lists so that the ordering of each list is the same as the initial names_mJ and names_mW ordered lists.
# ----------------------------------------------------------------------------------------------------------------------------------


for i in range(0,len(names)):
    mtop, mtop_error = getm_t(mJ_values[i], mJ_errors[i], B, A)
    mtop_values.append(mtop)
    mtop_errors.append(mtop_error)
    diffFromNominal.append(mtop-173.362876)

# Print table of results for all systematics.
# ----------------------------------------------------------------------------------------------------------------------------------

print("\n\n\nBelow is LaTeX code for table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Systematics","$\overline{\\textup{m}_\\textup{top-jet}}$ (GeV)","$\overline{\\textup{m}_\\textup{top-jet}}$ error (GeV)","JSF", "JSF error", "m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mJ_values[i],mJ_errors[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\n\n\nBelow is LaTeX code for simplified table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Systematics", "m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")


# ------------------------------------------------------------------------------------------------------
# Find positions of the different systematics
# ------------------------------------------------------------------------------------------------------


# Find positions of the b-tagging systematics
# ------------------------------------------------------------------------------------------------------

positionsOfbTaggingSystematics = []
for i in range(0,len(names)):
    if ("bTagSF" in names[i]):
        positionsOfbTaggingSystematics.append(i)

print('')
print("Positions where the b-tagging systematics are:")
print(positionsOfbTaggingSystematics)
print('')
print("Names of b-tagging systematics:")
names_bTagSF = []
for i in range(0,len(positionsOfbTaggingSystematics)):
    if (i % 2 == 0):
        before, sep, after = names[positionsOfbTaggingSystematics[i]].partition("_down")
        print(before)
        names_bTagSF.append(before)
print('')

# Find positions of the JVT systematics
# ------------------------------------------------------------------------------------------------------

positionsOfJVTSystematics = []
for i in range(0,len(names)):
    if ("jvt" in names[i]):
        positionsOfJVTSystematics.append(i)

print('')
print("Positions where the JVT systematics are:")
print(positionsOfJVTSystematics)
print('')
print("Names of JVT systematics:")
names_JVT = []
for i in range(0,len(positionsOfJVTSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfJVTSystematics[i]].partition("_DOWN")
        print(before)
        names_JVT.append(before)
print('')

# Find positions of the leptonSF systematics
# ------------------------------------------------------------------------------------------------------

positionsOfLeptonSFSystematics = []
for i in range(0,len(names)):
    if ("leptonSF" in names[i]):
        positionsOfLeptonSFSystematics.append(i)

print('')
print("Positions where the leptonSF systematics are:")
print(positionsOfLeptonSFSystematics)
print('')
print("Names of leptonSF systematics:")
names_leptonSF = []
for i in range(0,len(positionsOfLeptonSFSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfLeptonSFSystematics[i]].partition("_DOWN")
        print(before)
        names_leptonSF.append(before)
print('')

# Find positions of the pileup systematics
# ------------------------------------------------------------------------------------------------------

positionsOfPUSystematics = []
for i in range(0,len(names)):
    if ("pileup" in names[i]):
        positionsOfPUSystematics.append(i)

print('')
print("Positions where the PU systematics are:")
print(positionsOfPUSystematics)
print('')
print("Names of PU systematics:")
names_PU = []
for i in range(0,len(positionsOfPUSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfPUSystematics[i]].partition("_DOWN")
        print(before)
        names_PU.append(before)
print('')

# ------------------------------------------------------------------------------------------------------
# Work out systematic uncertiainties on mtop
# ------------------------------------------------------------------------------------------------------

# b-tagging
# ------------------------------------------------------------------------------------------------------

bTagSF_uncertainties = []
for i in range(0,len(positionsOfbTaggingSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfbTaggingSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfbTaggingSystematics[(2*i)+1]] - mtop_values[0])
    bTagSF_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    bTagSF_uncertainties.append(bTagSF_uncertainty)

bTagSF_uncertainties_squared = []
for i in range(0,len(bTagSF_uncertainties)):
    bTagSF_uncertainties_squared.append(bTagSF_uncertainties[i]*bTagSF_uncertainties[i])

sumOfbTagSFUncertaintiesSquared = 0
for i in range(0,len(bTagSF_uncertainties_squared)):
    sumOfbTagSFUncertaintiesSquared += bTagSF_uncertainties_squared[i]

Total_bTagSF_uncetainty = np.sqrt(sumOfbTagSFUncertaintiesSquared)

print("Total_bTagSF_uncetainty:")
print(Total_bTagSF_uncetainty)

# JVT
# ------------------------------------------------------------------------------------------------------

JVT_uncertainties = []
for i in range(0,len(positionsOfJVTSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfJVTSystematics[i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfJVTSystematics[(2*i)+1]] - mtop_values[0])
    JVT_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    JVT_uncertainties.append(JVT_uncertainty)

JVT_uncertainties_squared = []
for i in range(0,len(JVT_uncertainties)):
    JVT_uncertainties_squared.append(JVT_uncertainties[i]*JVT_uncertainties[i])

sumOfJVTUncertaintiesSquared = 0
for i in range(0,len(JVT_uncertainties_squared)):
    sumOfJVTUncertaintiesSquared += JVT_uncertainties_squared[i]

Total_JVT_uncetainty = np.sqrt(sumOfJVTUncertaintiesSquared)

print("Total_JVT_uncetainty:")
print(Total_JVT_uncetainty)

# LeptonSF
# ------------------------------------------------------------------------------------------------------

LEP_uncertainties = []
for i in range(0,len(positionsOfLeptonSFSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfLeptonSFSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfLeptonSFSystematics[(2*i)+1]] - mtop_values[0])
    LEP_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    LEP_uncertainties.append(LEP_uncertainty)

LEP_uncertainties_squared = []
for i in range(0,len(LEP_uncertainties)):
    LEP_uncertainties_squared.append(LEP_uncertainties[i]*LEP_uncertainties[i])

sumOfLEPUncertaintiesSquared = 0
for i in range(0,len(LEP_uncertainties_squared)):
    sumOfLEPUncertaintiesSquared += LEP_uncertainties_squared[i]

Total_LEP_uncetainty = np.sqrt(sumOfLEPUncertaintiesSquared)

print("Total_LEP_uncetainty:")
print(Total_LEP_uncetainty)

# Pileup
# ------------------------------------------------------------------------------------------------------

PU_uncertainties = []
for i in range(0,len(positionsOfPUSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfPUSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfPUSystematics[(2*i)+1]] - mtop_values[0])
    PU_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    PU_uncertainties.append(PU_uncertainty)

PU_uncertainties_squared = []
for i in range(0,len(PU_uncertainties)):
    PU_uncertainties_squared.append(PU_uncertainties[i]*PU_uncertainties[i])

sumOfPUUncertaintiesSquared = 0
for i in range(0,len(PU_uncertainties_squared)):
    sumOfPUUncertaintiesSquared += PU_uncertainties_squared[i]

Total_PU_uncetainty = np.sqrt(sumOfPUUncertaintiesSquared)

print("Total_PU_uncetainty:")
print(Total_PU_uncetainty)

# Now make final LaTeX summary table for systematics
# ------------------------------------------------------------------------------------------------------

print("\\begin{table}[!htb]")
print("\\centering")
print("\\begin{tabular}{lc}\\toprule")

headings = ["Systematics", "Uncertainty on m$_\\textup{top}$ (GeV)"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

Total_Systematic_Uncertainty = np.sqrt((Total_bTagSF_uncetainty*Total_bTagSF_uncetainty)+(Total_JVT_uncetainty*Total_JVT_uncetainty)+(Total_LEP_uncetainty*Total_LEP_uncetainty)+(Total_PU_uncetainty*Total_PU_uncetainty))
Total_uncertainty_names = ["b-tagging","JVT","LeptonSF","Pileup","Total"]
Total_uncertainties = [Total_bTagSF_uncetainty,Total_JVT_uncetainty,Total_LEP_uncetainty,Total_PU_uncetainty,Total_Systematic_Uncertainty]

for i in range(0,len(Total_uncertainty_names)):
    linestring = ""
    lines = [Total_uncertainty_names[i],Total_uncertainties[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , " ")
            linestring += (str(lines[j]) + " & ")
            if i == len(Total_uncertainty_names)-1:
                print("\\hline")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\nScript has finished\n")

