from __future__ import print_function
import ROOT

# This is our observable, mean(top jet mass)
meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)

nominalMean = 168.83
errorOnMean = 0.3 # To be fixed to correct value

# JSF
JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)

# Fixed Parameter relating JSF to mean(top jet mass)
aJSF = ROOT.RooRealVar("aJSF","aJSF",78.17)
aJSF.setConstant(True)

# code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + C*(JSF-1)
meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2 - 1.0)", ROOT.RooArgList(ROOT.RooRealConstant.value(nominalMean),aJSF,JSF))

# Assume are measurements of the mean top-jet mass (we will have only 1) are distributed according to a gaussian with mean related to JSF.
# The sigma of the gaussian is fixed to the error on the mean
pdf_topJetMass = ROOT.RooGaussian("pdf_topJetMass","pdf_topJetMass",meanTopJetMass, meanFormula, ROOT.RooRealConstant.value(errorOnMean))

# Create data - just one measurement of mean(top jet mass)
toyData = ROOT.RooDataSet("d","d",ROOT.RooArgSet(meanTopJetMass))
meanTopJetMass.setVal(nominalMean)
toyData.add(ROOT.RooArgSet(meanTopJetMass))
toyData.get(0).Print("V")

# fit and draw
fitRes = pdf_topJetMass.fitTo(toyData)

frame1 = meanTopJetMass.frame(ROOT.RooFit.Bins(15)) 
toyData.plotOn(frame1) 
pdf_topJetMass.plotOn(frame1) 

c = ROOT.TCanvas()
frame1.Draw()
c.SaveAs("../histogram_images/mW_distributions_varied_JSF/Fitting/TwoGaussians/RooFit/RooFit_test.pdf")

# pull test, we generate 5000 mean top-jet mass values gaussian distributed according to the error on the mean
# we check the JSF comes back unbiased with good estimate of mean
import random
random.seed()  
h_JSF_pull = ROOT.TH1D("h_JSF_pull","h_JSF_pull;(JSF - 1)/#sigma(JSF)Toys",100,-5.0,5.0)
for i in range(0,5000):
    tempData = ROOT.RooDataSet("td","td",ROOT.RooArgSet(meanTopJetMass))
    meanTopJetMass.setVal( random.gauss(nominalMean, errorOnMean) )
    tempData.add( ROOT.RooArgSet(meanTopJetMass) )
    pdf_topJetMass.fitTo(tempData, ROOT.RooFit.PrintLevel(-1))
    h_JSF_pull.Fill( (JSF.getVal()-1.0) / JSF.getError() )

c = ROOT.TCanvas()
h_JSF_pull.Draw()
h_JSF_pull.Fit("gaus")
c.SaveAs("../histogram_images/mW_distributions_varied_JSF/Fitting/TwoGaussians/RooFit/RooFit_test2.pdf")
