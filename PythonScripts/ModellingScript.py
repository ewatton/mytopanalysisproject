# Macro created to automate more of the process of getting values from histograms in root files and find a linear relationship between points
# First created by Elliot Watton - 17/01/2022
# Last editted by Elliot Watton - 17/01/2022

# Imports
from ROOT import TMinuit , Double , Long
from array import array as arr
import ROOT as r
import math
from array import array
r.TH1.SetDefaultSumw2(True)
# Initialise global variables 
mJ = 0                          # mJ
mJ_error = 0                    # sigma_{mJ}
mW = 0                          # mW
mW_err = 0                      # sigma_{mW}
A, B, C, D, E = 0, 0, 0, 0, 0   # Constants in equations: mJ = A + B(m_t - 172.5) + C(JSF-1) and mW = D + E(JSF-1)
npar = 2                        # Number of fit parameters

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    JSF = apar[1]
    mW_fit_value, mJ_fit_value = Double(0), Double(0)
    mJ_fit_value = A + (B * (m_t - 172.5)) + (C * (JSF - 1))
    mW_fit_value = D + (E * (JSF - 1))
    chisq = ((mW - mW_fit_value)*(mW - mW_fit_value)) / (mW_err * mW_err) + ((mJ - mJ_fit_value)*(mJ - mJ_fit_value)) / (mJ_err * mJ_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getJSFandTopMass(m_W, sigma_m_W, m_J, m_J_err, A_val, B_val, C_val, D_val, E_val):
    
    # Define data
    global mW
    mW = m_W
    global mW_err 
    mW_err = sigma_m_W
    global mJ
    mJ = m_J
    global mJ_err
    mJ_err = m_J_err
    global A 
    A = A_val
    global B
    B = B_val
    global C
    C = C_val
    global D
    D = D_val
    global E
    E = E_val
    
    # Set up fit parameters, starting points and step size
    name = ["m_t","JSF"]
    vstart = arr( 'd', (172, 1.00) )
    step = arr( 'd' , (0.001 , 0.001) )
    npar =len(name)
    
    # Set up Minuit
    myMinuit = TMinuit(2)         # Initialise TMinuit with maximum of parameters (2 for this case as we have m_t and JSF)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Define the parameters for the fit
    for i in range(0,npar):
        myMinuit.mnparm(i, name[i], vstart[i], step[i], 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    finalPar = []
    finalParErr = []
    p, pe = Double(0), Double(0)
    for i in range(0,npar):
        myMinuit.GetParameter(i, p, pe)
        finalPar.append(float(p))
        finalParErr.append(float(pe))
    
    """ # get covariance matrix
    buf = arr('d', npar*npar*[0.])
    myMinuit.mnemat(buf,npar) # retrieve error matrix
    emat = np.array(buf).reshape(npar,npar)

    # --> provide formatted output of results
    print "\n"
    print "*==* MINUIT fit completed:"
    print ' fcn@minimum = %.3g' %(amin)," error code =",ierflg," status =",icstat print " Results: \t value error corr. mat."
    for i in range(0,npar):
        print ' %s: \t%10.3e +/- %.1e '%(name[i] ,finalPar[i] ,finalParErr[i])
        for j in range (0,i):
            print '%+.3g ' %(emat[i][j]/np.sqrt(emat[i][i])/np.sqrt(emat[j][j]))
        printf ' ' """
    
    print('')
    print('*==* MINUIT fit completed:')
    print 'error code = ', ierflg
    print 'status = ', icstat
    print('')
    print('Results:')
    print('\t m_t = (%f +/- %f) GeV/c^2' %(finalPar[0], finalParErr[0]))
    print('')
    print('\t JSF = (%f +/- %f) ' %(finalPar[1], finalParErr[1]))
    print('')

    return finalPar[0], finalParErr[0], finalPar[1], finalParErr[1]

# Function that will make the scatter plot and find the relation between the points
def yVsJSF(n,x,y,xerr,yerr,definition,xaxisrange):

    canvas_scatter = r.TCanvas("canvas_scatter")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    # For 7 equally spaced bins
    errorbar_width = (x[number_of_points-1]-x[0])/(number_of_points-1)
    lower_bin_edge = x[0]-(errorbar_width/2)
    upper_bin_edge = x[number_of_points-1]+(errorbar_width/2)
    graph = r.TH1D("graph","graph title",7,lower_bin_edge,upper_bin_edge)

    for i in range(1,number_of_points+1):
        graph.SetBinContent(i,y[i-1])
        graph.SetBinError(i,yerr[i-1])
    r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("E1")


    # Changes the title of the scatter plot depending on what type of invariant mass calculation is being used
    if (definition == "_largestPT_"): 
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        graph.SetTitle("Average reconstructed W boson mass vs JSF value")
    elif (definition == "_alllightjets_"):
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        graph.SetTitle("Average reconstructed W boson mass vs JSF value, where invariant mass found by using all light jets")
    elif (definition == "_ClosestToW_"):
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        graph.SetTitle("Average reconstructed W boson mass vs JSF value, where invariant mass found by light jets with combined mass closest to W mass")
    else:
        graph.SetTitle("Default title")
    
    graph.GetYaxis().SetTitleOffset(1.2)
    graph.GetYaxis().SetLabelFont(62)

    # Applies a linear fit between the ranges specified
    fit = r.TF1("fit","[0]+([1]*(x-1))",x[0],x[number_of_points-1])
    graph.Fit("fit","R")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()
    par = fit.GetParameters()
    par_error = fit.GetParErrors()

    latex.DrawText(0.55,0.20,"Fitting function: mW = D + E(JSF-1)")
    latex.DrawText(0.55,0.15,"D = %.2f +- %.2f" %(par[0],par_error[0]))
    latex.DrawText(0.55,0.10,"E = %.2f +- %.2f" %(par[1],par_error[1]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))

    print("\n\n\n\nVALUES D, E")
    print("D = %f +- %f" %(par[0],par_error[0]))
    print("E = %f +- %f" %(par[1],par_error[1]))
    print("\n\n\n\n\n")

    pad2.cd()

    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("JSF value")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    #subplot.GetYaxis().SetRangeUser(0.995,1.005)
    subplot.GetYaxis().SetRangeUser(0.9983,1.0017)
    #subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    line = r.TLine(0.97,1,1.03,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")

    canvas_scatter.cd()
    canvas_scatter.Draw()

    # Sets the save file name depending on what type of invariant mass calculation is being used
    """ if (identifier == "_largestPT_"): 
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_JSF_LargestPT_" + xaxisrange + ".pdf")
    elif (identifier == "_alllightjets_"):
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_JSF_AllJets_" + xaxisrange + ".pdf"")
    elif (identifier == "_ClosestToW_"):
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_JSF_ClosestToW_" + xaxisrange + ".pdf"")
    else:
        canvas_scatter.SaveAs("../histogram_images/mW_Vs_JSF_Default_" + xaxisrange + ".pdf"") """

    print('')

    return par[0], par[1]

###################
################### 
###################

JSF_values = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
histogram_types = ["_largestPT_","_alllightjets_","_ClosestToW_"]
xaxisrange = ["full","reduced"]
number_of_points = len(JSF_values)
outputfile = '../OutputTextFiles/MinimisationResults.txt'

JSF_values_errors = array('f')
for i in range(0,number_of_points):
    JSF_values_errors.append(0)

for i in range(0,len(histogram_types)):
    for j in range(0,len(xaxisrange)):


        #######
        ####### FINDING D AND E
        #######

        y_values = array('f')
        y_errors = array('f')

        for k in range(0,len(JSF_values)):

            histogram_path = "../output_root_files/output_Subjet_invMass" + histogram_types[i] + "JSF_" + JSF_values_string[k] + "_ttpp8AF2_more_events_SkipTruth_1.root"
            histogram_root_file = r.TFile.Open(histogram_path)
            for h in histogram_root_file.GetListOfKeys():
                h = h.ReadObj()
                if (h.GetName() == "nominal"):
                    hist = histogram_root_file.Get(h.GetName())

            if xaxisrange[j] == "reduced":
                hist.GetXaxis().SetRangeUser(60,110)

            hist_mean = hist.GetMean()
            hist_mean_error = hist.GetMeanError()
            y_values.append(hist_mean)
            y_errors.append(hist_mean_error)

        D, E = yVsJSF(number_of_points, JSF_values, y_values, JSF_values_errors, y_errors, histogram_types[i], xaxisrange[j])

        ######
        ###### TMINUIT PROCESS
        ######

        mW_types = [" ","AllJets","ClosestToW"]
        filenames = ["ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]

        for m in range(0,len(filenames)):

            f = open(outputfile, 'a')
            f.write('\n')
            f.close()
            
            # Finding mW_value and mW_error
            if (filenames[m] == "ttallpp8") and (histogram_types[i] == "_largestPT_"):
                histogram_path = "../output_root_files/output_systematics_mW_JSF_1_00_mt_172_5_ttallpp8.root"
            elif (filenames[m] != "ttallpp8") and (histogram_types[i] == "_largestPT_"):
                histogram_path = "../output_root_files/output_GeneratorSamples_mW_JSF_1_00_mt_172_5_" + filenames[m] + ".root"
            elif (filenames[m] != "ttallpp8") and (histogram_types[i] != "_largestPT_"):
                histogram_path = "../output_root_files/output_GeneratorSamples_mW_JSF_1_00_mt_172_5_" + filenames[m] + "_" + mW_types[i] + ".root"
            elif (filenames[m] == "ttallpp8") and (histogram_types[i] != "_largestPT_"):
                histogram_path = "../output_root_files/output_systematics_mW_JSF_1_00_mt_172_5_ttallpp8_" + mW_types[i] + ".root"

            histogram_root_file = r.TFile.Open(histogram_path)
            for h in histogram_root_file.GetListOfKeys():
                h = h.ReadObj()
                if (h.GetName() == "nominal"):
                    hist = histogram_root_file.Get(h.GetName())

            if xaxisrange[j] == "reduced":
                hist.GetXaxis().SetRangeUser(60,110)
            
            mW_value_ = hist.GetMean()
            mW_error_ = hist.GetStdDev()/math.sqrt(hist.Integral())

            # Finding mJ and mJ_err
            if (filenames[m] == "ttallpp8"):
                histogram_path = "../output_root_files/output_systematics_mJ_JSF_1_00_mt_172_5_ttallpp8.root"
            else:
                histogram_path = "../output_root_files/output_GeneratorSamples_mJ_JSF_1_00_mt_172_5_" + filenames[m] + ".root"
            
            histogram_root_file = r.TFile.Open(histogram_path)
            for h in histogram_root_file.GetListOfKeys():
                h = h.ReadObj()
                if h.GetName() == "nominal":
                    hist = histogram_root_file.Get(h.GetName())
                    
            mJ_value_ = hist.GetMean()
            mJ_error_ = hist.GetStdDev()/math.sqrt(hist.Integral())

            A = 168.825399
            B = 0.482998
            C = 78.169244

            m_t, m_t_err, JSF, JSF_err = getJSFandTopMass(mW_value_, mW_error_, mJ_value_, mJ_error_, A, B, C, D, E)
            lines = [mJ_value_, mJ_error_, mW_value_, mW_error_, m_t, m_t_err, JSF, JSF_err]
            f = open(outputfile, 'a')
            f.write(histogram_types[i])
            f.write('\t')
            f.write(xaxisrange[j])
            f.write('\t')
            f.write(filenames[m])
            f.write('\t')
            for line in lines:
                f.write('%f' %line)
                f.write('\t')
            f.write('\n')
            f.close()


print('')
print('Script has finished')    
print('')