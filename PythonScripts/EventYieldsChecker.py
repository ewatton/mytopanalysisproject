from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

ntupleDirectoryPaths = ["../output_root_files/2022-11-15-NewJESFlavourUncertainties/"]
samples = ["ttpp8CR","ttpp8CR0","ttpp8CR1","ttpp8CR2"]
JSFs = ["1_00"]

nominalRootFilePath = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL.root"
nominalRootFile = r.TFile.Open(nominalRootFilePath)
print("\n==================================================================================================")
print("\nGetting nominal yeild from " + nominalRootFilePath + "\n")

hist = ""
hist = nominalRootFile.Get("nominal_lep_phi")
if hist == "":
    print("\nError! No nominal_lep_phi histogram for ttpp8AF2!\n")
    exit()

nominalYeild = hist.Integral()
print("Nominal yeild is " +str(nominalYeild) + "\n")
print("==================================================================================================\n")

sampleYields = []
yieldDiffs = []

for i in range(0,len(samples)):
    rootFilePath = ntupleDirectoryPaths[0] + samples[i] + "/All/JSF_" + JSFs[0] + "_" + samples[i] + ".root"
    rootFile = r.TFile.Open(rootFilePath)
    print("\nChecking file " + rootFilePath + "\n")
    hist = ""
    hist = rootFile.Get("nominal_lep_phi")
    if hist == "":
        print("\nError! No nominal_lep_phi histogram for this sample!\n")
        exit()

    sampleYields.append(hist.Integral())
    yieldDiffs.append(hist.Integral() - nominalYeild)

print("\n\n\n====================================")
print("Sample Yield NominalYield Difference\n")
for i in range(0,len(samples)):
    print("%s %.0f %.0f %.0f" %(samples[i],sampleYields[i],nominalYeild,yieldDiffs[i]))
print("====================================")