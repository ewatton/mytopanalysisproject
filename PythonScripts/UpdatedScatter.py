# Macro created to automate more of the process of getting values from histograms in root files and find a linear relationship between points
# First created by Elliot Watton - 15/11/2021
# Last editted by Elliot Watton - 11/01/2022

# imports
from ROOT import TFile, TH1, TH1D
import ROOT as r
from array import array
import math
r.TH1.SetDefaultSumw2(True)
# Function that will make the scatter plot and find the relation between the points
def yVsJSF(n,x,y,xerr,yerr,identifier):

    canvas_scatter = r.TCanvas("canvas_scatter")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    # For 7 equally spaced bins
    errorbar_width = (x[number_of_points-1]-x[0])/(number_of_points-1)
    lower_bin_edge = x[0]-(errorbar_width/2)
    upper_bin_edge = x[number_of_points-1]+(errorbar_width/2)
    graph = r.TH1D("graph","graph title",7,lower_bin_edge,upper_bin_edge)

    for i in range(1,number_of_points+1):
        graph.SetBinContent(i,y[i-1])
        graph.SetBinError(i,yerr[i-1])
    r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("E1")


    # Changes the title of the scatter plot depending on what type of invariant mass calculation is being used
    if (identifier == "_largestPT_"): 
        graph.GetYaxis().SetTitle("Average reconstructed W boson mass [GeV]")
        graph.SetTitle("Average reconstructed W boson mass vs JSF value for fast sim")
    elif (identifier == "_alllightjets_"):
        graph.GetYaxis().SetTitle("Average sub-jet invariant mass [GeV]")
        graph.SetTitle("Average sub-jet invariant mass vs JSF value, where invariant mass found by using all light jets")
    elif (identifier == "_ClosestToW_"):
        graph.GetYaxis().SetTitle("Average sub-jet invariant mass [GeV]")
        graph.SetTitle("Average sub-jet invariant mass vs JSF value, where invariant mass found by light jets with combined mass closest to W mass")
    elif (identifier == "topjet_m"):
        graph.SetTitle("Average top jet invariant mass vs JSF value")
        graph.GetYaxis().SetTitle("Average top jet invariant mass [GeV]")
    else:
        graph.SetTitle("Gaussian 1 mean vs JSF for ttpp8AF2")
        graph.GetYaxis().SetTitle("Parameter value [GeV]")
    
    graph.GetXaxis().SetTitle("JSF")
    graph.GetYaxis().SetTitleOffset(1.2)
    graph.GetYaxis().SetLabelFont(62)

    # Applies a linear fit between the ranges specified
    fit = r.TF1("fit","[0]+([1]*x)",x[0],x[number_of_points-1])
    # fit = r.TF1("fit","[0]+([1]*(x-1))",x[0],x[number_of_points-1])
    graph.Fit("fit","R")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)

    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()
    par = fit.GetParameters()
    par_error = fit.GetParErrors()

    grad_error = fit.GetParError(1)
    intercept_error = fit.GetParError(0)
    latex.DrawText(0.65,0.15,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))
    latex.DrawText(0.65,0.10,"Gradient = %.2f +- %.2f" %(par[1],grad_error))
    latex.DrawText(0.65,0.05,"y-intercept = %.2f +- %.2f" %(par[0],intercept_error))

    # latex.DrawText(0.55,0.20,"Fitting function: mW = D + E(JSF-1)")
    # latex.DrawText(0.55,0.15,"D = %.2f +- %.2f" %(par[0],par_error[0]))
    # latex.DrawText(0.55,0.10,"E = %.2f +- %.2f" %(par[1],par_error[1]))
    # latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))

    # print("\n\n\n\nVALUES D, E")
    # print("D = %f +- %f" %(par[0],par_error[0]))
    # print("E = %f +- %f" %(par[1],par_error[1]))
    # print("\n\n\n\n\n")

    pad2.cd()

    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("JSF value")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    #subplot.GetYaxis().SetRangeUser(0.995,1.005)
    subplot.GetYaxis().SetRangeUser(0.9983,1.0017)
    #subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    line = r.TLine(0.97,1,1.03,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")

    canvas_scatter.cd()
    canvas_scatter.Draw()

    # Sets the save file name depending on what type of invariant mass calculation is being used
    if (identifier == "_largestPT_"): 
        canvas_scatter.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/mWvsJSF_D&E_ttpp8AF2.pdf")
    elif (identifier == "_alllightjets_"):
        canvas_scatter.SaveAs("../histogram_images/JSFvsSubjetInvMass_AllLightJets_withSubplot_ttpp8AF2.pdf")
    elif (identifier == "_ClosestToW_"):
        canvas_scatter.SaveAs("../histogram_images/JSFvsSubjetInvMass_ClosestToW_withSubplot_ttpp8AF2.pdf")
    elif (identifier == "topjet_m"):
        canvas_scatter.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/mJvsJSF_ttpp8AF2.pdf")
    else:
        #canvas_scatter.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/mWvsJSF.pdf")
        canvas_scatter.SaveAs("../histogram_images/2023-01-13/mW_twoGaussFit_mean1vsJSF_ttallpp8.pdf")

    print('')

def simpleScatter(n,x,y,xerr,yerr):
    
    canvas_scatter = r.TCanvas("canvas_scatter")
    graph = r.TGraphErrors(n,x,y,xerr,yerr)
    r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("AP")
    graph.SetTitle("Average sub-jet invariant mass vs top quark mass, where sub-jet invariant mass found by using largest PT light jets")
    graph.GetYaxis().SetTitle("Average sub-jet invariant mass [GeV/c^{2}]")
    graph.GetXaxis().SetTitle("Top quark invariant mass [GeV/c^{2}]")
    graph.GetYaxis().SetTitleOffset(1.4)
    #graph.GetYaxis().SetRangeUser(84,85)
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("histogram_images/TopMassVsSubjetInvMass_LargestPT_ttpp8m.pdf")
    print('')

def TGraph2DErrorsFit(n,x,xerr,y,yerr,z,zerr,x_2,xerr_2,z_2,zerr_2):
    
    canvas_scatter = r.TCanvas("canvas_scatter")
    fit_2D = r.TF2("fit_2D","[0] + ((x-172.5)*[1]) + ((y-1)*[2])",169.0,176.0,0.97,1.03)
    graph = r.TGraph2DErrors(n) # x = m(top), y = JSF, z = mJ

    print("\n\nn = " + str(n) + "\n\n")

    for i in range(0,n):
        graph.SetPoint(i,x[i],y[i],z[i])
        graph.SetPointError(i,xerr[i],yerr[i],zerr[i])
    graph.Fit(fit_2D)

    fit_2D.Draw("surf4")
    fit_2D.SetTitle("Average top jet mass vs JSF value & top quark mass; Top quark mass [GeV]; JSF value; Average top jet mass [GeV]")
    fit_2D.GetXaxis().SetTitleOffset(1.8)
    fit_2D.GetYaxis().SetTitleOffset(2.0)
    fit_2D.GetZaxis().SetTitleOffset(1.5)
    fit_2D.GetXaxis().SetLabelSize(0.03)
    fit_2D.GetYaxis().SetLabelSize(0.03)
    fit_2D.GetZaxis().SetLabelSize(0.03)
    fit_2D.SetLineWidth(1)
    fit_2D.SetLineColor(r.kGray+1)
    
    graph.Draw("same err P")
    graph.SetFillColor(29)
    graph.SetMarkerSize(0.8)
    graph.SetMarkerStyle(20)
    graph.SetMarkerColor(r.kRed)
    graph.SetLineColor(r.kBlue-3)
    graph.SetLineWidth(1)

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.02)

    chi2_2D = fit_2D.GetChisquare()
    ndof_2D = fit_2D.GetNDF()
    par_2D = fit_2D.GetParameters()
    par_errors_2D = fit_2D.GetParErrors()

    latex.DrawText(0.01,0.90,"Fit function: mJ = A + B(mt - 172.5) + C(JSF - 1)")
    latex.DrawText(0.01,0.875,"A = %.2f +- %.2f" %(par_2D[0],par_errors_2D[0]))
    latex.DrawText(0.01,0.85,"B = %.2f +- %.2f" %(par_2D[1],par_errors_2D[1]))
    latex.DrawText(0.01,0.825,"C = %.2f +- %.2f" %(par_2D[2],par_errors_2D[2]))
    latex.DrawText(0.01,0.80,"chi^{2}/ndof = %.2f/%.0f" %(chi2_2D,ndof_2D))

    print("\n\n\n\nVALUES A, B, C")
    print("A = %f +- %f" %(par_2D[0],par_errors_2D[0]))
    print("B = %f +- %f" %(par_2D[1],par_errors_2D[1]))
    print("C = %f +- %f" %(par_2D[2],par_errors_2D[2]))
    print("\n\n\n\n\n")

    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-01-13/mJ_vs_JSF_and_TopMass.pdf")
    print('')

    # Now make projections - mJ vs JSF
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_JSF = r.TF1("fit_1D_JSF","[0] + ((x-1)*[1])",0.97,1.03)
    fit_1D_JSF.FixParameter(0,par_2D[0])
    fit_1D_JSF.SetParError(0,par_errors_2D[0])
    fit_1D_JSF.FixParameter(1,par_2D[2])
    fit_1D_JSF.SetParError(1,par_errors_2D[2])
    graph_1D_JSF = r.TGraphErrors(7)
    for i in range(0,7):
        graph_1D_JSF.SetPoint(i,y[i],z[i])
        graph_1D_JSF.SetPointError(i,yerr[i],zerr[i])
    r.gStyle.SetErrorX(0.)
    graph_1D_JSF.SetMarkerStyle(5)
    graph_1D_JSF.SetMarkerSize(0.5)
    graph_1D_JSF.Fit(fit_1D_JSF, "RB")
    graph_1D_JSF.Draw("AP")
    graph_1D_JSF.SetTitle("Average top jet invariant mass vs JSF value")
    graph_1D_JSF.GetYaxis().SetTitle("Average top jet invariant mass [GeV]")
    graph_1D_JSF.GetXaxis().SetTitle("JSF value")
    graph_1D_JSF.GetYaxis().SetTitleOffset(1.4)
    graph_1D_JSF.GetYaxis().SetRangeUser(165.5,172)
    graph_1D_JSF.GetXaxis().SetRangeUser(0.965,1.035)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_JSF = fit_1D_JSF.GetChisquare()
    ndof_1D_JSF = fit_1D_JSF.GetNDF()
    par_1D_JSF = fit_1D_JSF.GetParameters()
    par_errors_1D_JSF = fit_1D_JSF.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mJ = A + C(JSF - 1)")
    latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_JSF[0],par_errors_2D[0])) # CHECK THESE ERRORS I changed them to the previous ones as I used fix params
    latex.DrawText(0.55,0.10,"C = %.2f +- %.2f" %(par_1D_JSF[1],par_errors_2D[2]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_JSF,ndof_1D_JSF))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    errorbar_width = (y[6]-y[0])/6
    lower_bin_edge = y[0]-(errorbar_width/2)
    upper_bin_edge = y[6]+(errorbar_width/2)
    graph = r.TH1D("graph","graph title",7,lower_bin_edge,upper_bin_edge)
    graph.GetXaxis().SetRangeUser(0.975,1.025)
    for i in range(1,8):
        graph.SetBinContent(i,z[i-1])
        graph.SetBinError(i,zerr[i-1])
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    r.gStyle.SetErrorX(0.)
    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit_1D_JSF)
    subplot.GetXaxis().SetRangeUser(0.975,1.025)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("JSF value")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    line = r.TLine(0.97,1,1.03,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-01-13/mJ_vs_JSF_and_TopMass_1D_projection_JSF.pdf")
    print('')

    # Now make projections - mJ vs mt
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_mt = r.TF1("fit_1D_mt","[0] + ((x-172.5)*[1])",169.0,176.0)
    fit_1D_mt.FixParameter(0,par_2D[0])
    fit_1D_mt.SetParError(0,par_errors_2D[0])
    fit_1D_mt.FixParameter(1,par_2D[1])
    fit_1D_mt.SetParError(1,par_errors_2D[1])
    graph_1D_mt = r.TGraphErrors(7)
    print("")
    for i in range(0,7):
        print("i = " + str(i))
        print("x_2[" + str(i+5) + "] = " + str(x_2[i+5]))
        print("xerr_2[" + str(i+5) + "] = " + str(xerr_2[i+5]))
        print("z_2[" + str(i+5) + "] = " + str(z_2[i+5]))
        print("zerr_2[" + str(i+5) + "] = " + str(zerr_2[i+5]))
        print("")
        graph_1D_mt.SetPoint(i,x_2[i+6],z_2[i+6])
        graph_1D_mt.SetPointError(i,xerr_2[i+6],zerr_2[i+6])
    r.gStyle.SetErrorX(0.)
    graph_1D_mt.SetMarkerStyle(5)
    graph_1D_mt.SetMarkerSize(0.5)
    graph_1D_mt.Fit(fit_1D_mt, "RB")
    graph_1D_mt.Draw("AP")
    graph_1D_mt.SetTitle("Average top jet invariant mass vs Top quark mass")
    graph_1D_mt.GetYaxis().SetTitle("Average top jet invariant mass [GeV]")
    graph_1D_mt.GetXaxis().SetTitle("Top quark mass [GeV]")
    graph_1D_mt.GetYaxis().SetTitleOffset(1.4)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_mt = fit_1D_mt.GetChisquare()
    ndof_1D_mt = fit_1D_mt.GetNDF()
    par_1D_mt = fit_1D_mt.GetParameters()
    par_errors_1D_mt = fit_1D_mt.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mJ = A + B(mt - 172.5)")
    latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_mt[0],par_errors_2D[0]))
    latex.DrawText(0.55,0.10,"B = %.2f +- %.2f" %(par_1D_mt[1],par_errors_2D[1]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_mt,ndof_1D_mt))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    edges = array('f',[167.75, 170.25, 171.75, 172.25, 172.75, 173.25, 174.75, 177.25])
    subplot2 = r.TH1D("graph","graph title",8,edges)
    for i in range(1,8):
        subplot2.SetBinContent(i,z_2[i+5])
        subplot2.SetBinError(i,zerr_2[i+5])
    subplot2.Divide(fit_1D_mt)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,8):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    for i in range(6,13):
        print("x_2[" + str(i) + "] = " + str(z_2[i]))
        print("xerr_2[" + str(i) + "] = " + str(zerr_2[i]))
        subplot_x_values.append(x_2[i])
        subplot_x_errors.append(xerr_2[i])
    subplot = r.TGraphErrors(7,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    print("")
    print("subplot_x_values:")
    print(subplot_x_values)
    print("subplot_x_errors:")
    print(subplot_x_errors)
    print("subplot_y_values:")
    print(subplot_y_values)
    print("subplot_y_errors:")
    print(subplot_y_errors)
    print("")
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("Top quark mass [GeV]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9987,1.0013)
    line = r.TLine(169,1,176,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2023-01-13/mJ_vs_JSF_and_TopMass_1D_projection_TopMass.pdf")
    print('')

def TGraph2DErrorsFit_FullSim(n,x,xerr,y,yerr,z,zerr,x_2,xerr_2,z_2,zerr_2):
    
    canvas_scatter = r.TCanvas("canvas_scatter")
    fit_2D = r.TF2("fit_2D","[0] + ((x-172.5)*[1]) + ((y-1)*[2])",171.0,174.0,0.97,1.03)
    graph = r.TGraph2DErrors(n) # x = m(top), y = JSF, z = mJ

    # print("\n\nn = " + str(n) + "\n\n")
    # print("\n\nx = " + str(x) + "\n\n")
    # print("\n\nxerr = " + str(xerr) + "\n\n")
    # print("\n\ny = " + str(y) + "\n\n")
    # print("\n\nyerr = " + str(yerr) + "\n\n")
    # print("\n\nz = " + str(z) + "\n\n")
    # print("\n\nzerr = " + str(zerr) + "\n\n")
    # print("\n\nx_2 = " + str(x_2) + "\n\n")
    # print("\n\nxerr_2 = " + str(xerr_2) + "\n\n")
    # print("\n\nz_2 = " + str(z_2) + "\n\n")
    # print("\n\nzerr_2 = " + str(zerr_2) + "\n\n")

    for i in range(0,n):
        graph.SetPoint(i,x[i],y[i],z[i])
        graph.SetPointError(i,xerr[i],yerr[i],zerr[i])
    graph.Fit(fit_2D)

    fit_2D.Draw("surf4")
    fit_2D.SetTitle("Average top jet mass vs JSF value & top quark mass; Top quark mass [GeV]; JSF value; Average top jet mass [GeV]")
    fit_2D.GetXaxis().SetTitleOffset(1.8)
    fit_2D.GetYaxis().SetTitleOffset(2.0)
    fit_2D.GetZaxis().SetTitleOffset(1.5)
    fit_2D.GetXaxis().SetLabelSize(0.03)
    fit_2D.GetYaxis().SetLabelSize(0.03)
    fit_2D.GetZaxis().SetLabelSize(0.03)
    fit_2D.SetLineWidth(1)
    fit_2D.SetLineColor(r.kGray+1)
    
    graph.Draw("same err P")
    graph.SetFillColor(29)
    graph.SetMarkerSize(0.8)
    graph.SetMarkerStyle(20)
    graph.SetMarkerColor(r.kRed)
    graph.SetLineColor(r.kBlue-3)
    graph.SetLineWidth(1)

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.02)

    chi2_2D = fit_2D.GetChisquare()
    ndof_2D = fit_2D.GetNDF()
    par_2D = fit_2D.GetParameters()
    par_errors_2D = fit_2D.GetParErrors()

    latex.DrawText(0.01,0.90,"Fit function: mJ = A + B(mt - 172.5) + C(JSF - 1)")
    latex.DrawText(0.01,0.875,"A = %.2f +- %.2f" %(par_2D[0],par_errors_2D[0]))
    latex.DrawText(0.01,0.85,"B = %.2f +- %.2f" %(par_2D[1],par_errors_2D[1]))
    latex.DrawText(0.01,0.825,"C = %.2f +- %.2f" %(par_2D[2],par_errors_2D[2]))
    latex.DrawText(0.01,0.80,"chi^{2}/ndof = %.2f/%.0f" %(chi2_2D,ndof_2D))

    print("\n\n\n\nVALUES A, B, C")
    print("A = %f +- %f" %(par_2D[0],par_errors_2D[0]))
    print("B = %f +- %f" %(par_2D[1],par_errors_2D[1]))
    print("C = %f +- %f" %(par_2D[2],par_errors_2D[2]))
    print("\n\n\n\n\n")

    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/mJ_vs_JSF_and_TopMass_FullSim.pdf")
    print('')

    # Now make projections - mJ vs JSF
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_JSF = r.TF1("fit_1D_JSF","[0] + ((x-1)*[1])",0.97,1.03)
    fit_1D_JSF.FixParameter(0,par_2D[0])
    fit_1D_JSF.SetParError(0,par_errors_2D[0])
    fit_1D_JSF.FixParameter(1,par_2D[2])
    fit_1D_JSF.SetParError(1,par_errors_2D[2])
    graph_1D_JSF = r.TGraphErrors(7)
    for i in range(0,7):
        graph_1D_JSF.SetPoint(i,y[i],z[i])
        graph_1D_JSF.SetPointError(i,yerr[i],zerr[i])
    r.gStyle.SetErrorX(0.)
    graph_1D_JSF.SetMarkerStyle(5)
    graph_1D_JSF.SetMarkerSize(0.5)
    graph_1D_JSF.Fit(fit_1D_JSF, "RB")
    graph_1D_JSF.Draw("AP")
    graph_1D_JSF.SetTitle("Average top jet invariant mass vs JSF value")
    graph_1D_JSF.GetYaxis().SetTitle("Average top jet invariant mass [GeV]")
    graph_1D_JSF.GetXaxis().SetTitle("JSF value")
    graph_1D_JSF.GetYaxis().SetTitleOffset(1.4)
    graph_1D_JSF.GetYaxis().SetRangeUser(165.5,172)
    graph_1D_JSF.GetXaxis().SetRangeUser(0.965,1.035)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_JSF = fit_1D_JSF.GetChisquare()
    ndof_1D_JSF = fit_1D_JSF.GetNDF()
    par_1D_JSF = fit_1D_JSF.GetParameters()
    par_errors_1D_JSF = fit_1D_JSF.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mJ = A + C(JSF - 1)")
    latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_JSF[0],par_errors_2D[0])) # CHECK THESE ERRORS I changed them to the previous ones as I used fix params
    latex.DrawText(0.55,0.10,"C = %.2f +- %.2f" %(par_1D_JSF[1],par_errors_2D[2]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_JSF,ndof_1D_JSF))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    errorbar_width = (y[6]-y[0])/6
    lower_bin_edge = y[0]-(errorbar_width/2)
    upper_bin_edge = y[6]+(errorbar_width/2)
    graph = r.TH1D("graph","graph title",7,lower_bin_edge,upper_bin_edge)
    graph.GetXaxis().SetRangeUser(0.975,1.025)
    for i in range(1,8):
        graph.SetBinContent(i,z[i-1])
        graph.SetBinError(i,zerr[i-1])
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    r.gStyle.SetErrorX(0.)
    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit_1D_JSF)
    subplot.GetXaxis().SetRangeUser(0.975,1.025)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("JSF value")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    line = r.TLine(0.97,1,1.03,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/mJ_vs_JSF_and_TopMass_1D_projection_JSF_FullSim.pdf")
    print('')

    # Now make projections - mJ vs mt
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_mt = r.TF1("fit_1D_mt","[0] + ((x-172.5)*[1])",171.0,174.0)
    fit_1D_mt.FixParameter(0,par_2D[0])
    fit_1D_mt.SetParError(0,par_errors_2D[0])
    fit_1D_mt.FixParameter(1,par_2D[1])
    fit_1D_mt.SetParError(1,par_errors_2D[1])
    graph_1D_mt = r.TGraphErrors(3)
    print("")
    for i in range(0,3):
        print("i = " + str(i))
        print("x_2[" + str(i+5) + "] = " + str(x_2[i+5]))
        print("xerr_2[" + str(i+5) + "] = " + str(xerr_2[i+5]))
        print("z_2[" + str(i+5) + "] = " + str(z_2[i+5]))
        print("zerr_2[" + str(i+5) + "] = " + str(zerr_2[i+5]))
        print("")
        graph_1D_mt.SetPoint(i,x_2[i+6],z_2[i+6])
        graph_1D_mt.SetPointError(i,xerr_2[i+6],zerr_2[i+6])
    r.gStyle.SetErrorX(0.)
    graph_1D_mt.SetMarkerStyle(5)
    graph_1D_mt.SetMarkerSize(0.5)
    graph_1D_mt.Fit(fit_1D_mt, "RB")
    graph_1D_mt.Draw("AP")
    graph_1D_mt.SetTitle("Average top jet invariant mass vs Top quark mass")
    graph_1D_mt.GetYaxis().SetTitle("Average top jet invariant mass [GeV]")
    graph_1D_mt.GetXaxis().SetTitle("Top quark mass [GeV]")
    graph_1D_mt.GetYaxis().SetTitleOffset(1.4)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_mt = fit_1D_mt.GetChisquare()
    ndof_1D_mt = fit_1D_mt.GetNDF()
    par_1D_mt = fit_1D_mt.GetParameters()
    par_errors_1D_mt = fit_1D_mt.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mJ = A + B(mt - 172.5)")
    latex.DrawText(0.55,0.15,"A = %.2f +- %.2f" %(par_1D_mt[0],par_errors_2D[0]))
    latex.DrawText(0.55,0.10,"B = %.2f +- %.2f" %(par_1D_mt[1],par_errors_2D[1]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_mt,ndof_1D_mt))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    edges = array('f',[170.5, 171.5, 173.5, 174.5])
    subplot2 = r.TH1D("graph","graph title",4,edges)
    for i in range(1,4):
        subplot2.SetBinContent(i,z_2[i+5])
        subplot2.SetBinError(i,zerr_2[i+5])
    subplot2.Divide(fit_1D_mt)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,4):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    for i in range(6,9):
        print("x_2[" + str(i) + "] = " + str(z_2[i]))
        print("xerr_2[" + str(i) + "] = " + str(zerr_2[i]))
        subplot_x_values.append(x_2[i])
        subplot_x_errors.append(xerr_2[i])
    subplot = r.TGraphErrors(3,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    print("")
    print("subplot_x_values:")
    print(subplot_x_values)
    print("subplot_x_errors:")
    print(subplot_x_errors)
    print("subplot_y_values:")
    print(subplot_y_values)
    print("subplot_y_errors:")
    print(subplot_y_errors)
    print("")
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("Top quark mass [GeV]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9987,1.0013)
    line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/mJ_vs_JSF_and_TopMass_1D_projection_TopMass_FullSim.pdf")
    print('')

def yVsTopMass(n,x,y,xerr,yerr):

    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph = r.TGraphErrors(n,x,y,xerr,yerr)
    r.gStyle.SetErrorX(0.)
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    graph.Draw("AP")
    graph.SetTitle("Average top jet mass vs Top quark mass used in MC simulation")
    graph.GetYaxis().SetTitle("Average top jet mass [GeV]")
    graph.GetXaxis().SetTitle("Top quark mass used in MC simulation [GeV]")
    graph.GetYaxis().SetTitleOffset(1.2)
    graph.GetYaxis().SetLabelFont(62)
    #graph.GetYaxis().SetRangeUser(82,87)

    fit = r.TF1("fit","[0]+x*[1]",169,176)
    graph.Fit("fit","R")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2 = fit.GetChisquare()
    ndof = fit.GetNDF()
    par = fit.GetParameters()
    grad_error = fit.GetParError(1)
    intercept_error = fit.GetParError(0)
    latex.DrawText(0.65,0.25,"chi^{2}/ndof = %.2f/%.0f" %(chi2,ndof))
    latex.DrawText(0.65,0.2,"Gradient = %.2f +- %.2f" %(par[1],grad_error))
    latex.DrawText(0.65,0.15,"y-intercept = %.2f +- %.2f" %(par[0],intercept_error))

    pad2.cd()
    r.gStyle.SetOptStat(0)

    edges = array('f',[166.25, 171.75, 172.25, 172.75, 173.25, 178.75])
    subplot2 = r.TH1D("graph","graph title",5,edges)
    for i in range(1,number_of_points+1):
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
    subplot2.Divide(fit)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,6):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    subplot = r.TGraphErrors(n,x,subplot_y_values,xerr,subplot_y_errors)
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("Top quark mass used in MC simulation [GeV]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9993,1.0007)
    #subplot.GetYaxis().SetRangeUser(0.9983,1.0017)
    #subplot.GetYaxis().SetRangeUser(0.993,1.007)
    line = r.TLine(169,1,176,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")

    canvas_scatter.cd()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/mJvsmt_JSF_1_00.pdf")
    print('')

def TGraph2DErrorsFit_mW(n,x,xerr,y,yerr,z,zerr,x_2,xerr_2,z_2,zerr_2):
    
    canvas_scatter = r.TCanvas("canvas_scatter")
    fit_2D = r.TF2("fit_2D","[0] + ((x-172.5)*[1]) + ((y-1)*[2])",169.0,176.0,0.97,1.03)
    graph = r.TGraph2DErrors(n) # x = mt, y = JSF, z = mW, x_2 = mt (second one)    
    for i in range(0,n):
        graph.SetPoint(i,x[i],y[i],z[i])
        graph.SetPointError(i,xerr[i],yerr[i],zerr[i])
    graph.Fit(fit_2D)

    fit_2D.Draw("surf4")
    fit_2D.SetTitle("Average sub-jet mass vs JSF value & top quark mass; Top quark mass [GeV/c^{2}]; JSF value; Average sub-jet mass [GeV/c^{2}]")
    fit_2D.GetXaxis().SetTitleOffset(1.8)
    fit_2D.GetYaxis().SetTitleOffset(2.0)
    fit_2D.GetZaxis().SetTitleOffset(1.5)
    fit_2D.GetXaxis().SetLabelSize(0.03)
    fit_2D.GetYaxis().SetLabelSize(0.03)
    fit_2D.GetZaxis().SetLabelSize(0.03)
    fit_2D.SetLineWidth(1)
    fit_2D.SetLineColor(r.kGray+1)
    
    graph.Draw("same err P")
    graph.SetFillColor(29)
    graph.SetMarkerSize(0.08)
    graph.SetMarkerStyle(20)
    graph.SetMarkerColor(r.kRed)
    graph.SetLineColor(r.kBlue-3)
    graph.SetLineWidth(1)

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.02)

    chi2_2D = fit_2D.GetChisquare()
    ndof_2D = fit_2D.GetNDF()
    par_2D = fit_2D.GetParameters()
    par_errors_2D = fit_2D.GetParErrors()

    latex.DrawText(0.01,0.90,"Fit function: mW = D + E(mt - 172.5) + F(JSF - 1)")
    latex.DrawText(0.01,0.875,"D = %.2f +- %.2f" %(par_2D[0],par_errors_2D[0]))
    latex.DrawText(0.01,0.85,"E = %.2f +- %.2f" %(par_2D[1],par_errors_2D[1]))
    latex.DrawText(0.01,0.825,"F = %.2f +- %.2f" %(par_2D[2],par_errors_2D[2]))
    latex.DrawText(0.01,0.80,"chi^{2}/ndof = %.2f/%.0f" %(chi2_2D,ndof_2D))

    print("\n\n\n\nVALUES D, E, F")
    print("D = %f +- %f" %(par_2D[0],par_errors_2D[0]))
    print("E = %f +- %f" %(par_2D[1],par_errors_2D[1]))
    print("F = %f +- %f" %(par_2D[2],par_errors_2D[2]))
    print("\n\n\n\n\n")

    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("histogram_images/mW_vs_JSF_and_TopMass.pdf")
    print('')

    # Now make projections - mJ vs JSF
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_JSF = r.TF1("fit_1D_JSF","[0] + ((x-1)*[1])",0.97,1.03)
    fit_1D_JSF.FixParameter(0,par_2D[0])
    fit_1D_JSF.SetParError(0,par_errors_2D[0])
    fit_1D_JSF.FixParameter(1,par_2D[2])
    fit_1D_JSF.SetParError(1,par_errors_2D[2])
    graph_1D_JSF = r.TGraphErrors(7)
    for i in range(0,7):
        graph_1D_JSF.SetPoint(i,y[i],z[i])
        graph_1D_JSF.SetPointError(i,yerr[i],zerr[i])
    r.gStyle.SetErrorX(0.)
    graph_1D_JSF.SetMarkerStyle(5)
    graph_1D_JSF.SetMarkerSize(0.5)
    graph_1D_JSF.Fit(fit_1D_JSF, "RB")
    graph_1D_JSF.Draw("AP")
    graph_1D_JSF.SetTitle("Average sub-jet invariant mass vs JSF value")
    graph_1D_JSF.GetYaxis().SetTitle("Average sub-jet invariant mass [GeV/c^{2}]")
    graph_1D_JSF.GetXaxis().SetTitle("JSF value")
    graph_1D_JSF.GetYaxis().SetTitleOffset(1.4)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_JSF = fit_1D_JSF.GetChisquare()
    ndof_1D_JSF = fit_1D_JSF.GetNDF()
    par_1D_JSF = fit_1D_JSF.GetParameters()
    par_errors_1D_JSF = fit_1D_JSF.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mW = D + F(JSF - 1)")
    latex.DrawText(0.55,0.15,"D = %.2f +- %.2f" %(par_1D_JSF[0],par_errors_2D[0])) # CHECK THESE ERRORS I changed them to the previous ones as I used fix params
    latex.DrawText(0.55,0.10,"F = %.2f +- %.2f" %(par_1D_JSF[1],par_errors_2D[2]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_JSF,ndof_1D_JSF))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    errorbar_width = (y[6]-y[0])/6
    lower_bin_edge = y[0]-(errorbar_width/2)
    upper_bin_edge = y[6]+(errorbar_width/2)
    graph = r.TH1D("graph","graph title",7,lower_bin_edge,upper_bin_edge)
    for i in range(1,8):
        graph.SetBinContent(i,z[i-1])
        graph.SetBinError(i,zerr[i-1])
    graph.SetMarkerStyle(5)
    graph.SetMarkerSize(0.5)
    r.gStyle.SetErrorX(0.)
    r.gStyle.SetOptStat(0)
    subplot = graph.Clone()
    subplot.Divide(fit_1D_JSF)
    subplot.Draw("E1")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("JSF value")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9983,1.0017)
    line = r.TLine(0.97,1,1.03,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("histogram_images/mW_vs_JSF_and_TopMass_1D_projection_JSF.pdf")
    print('')

    # Now make projections - mJ vs mt
    canvas_scatter = r.TCanvas("canvas_scatter")
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    fit_1D_mt = r.TF1("fit_1D_mt","[0] + ((x-172.5)*[1])",169.0,176.0)
    fit_1D_mt.FixParameter(0,par_2D[0])
    fit_1D_mt.SetParError(0,par_errors_2D[0])
    fit_1D_mt.FixParameter(1,par_2D[1])
    fit_1D_mt.SetParError(1,par_errors_2D[1])
    graph_1D_mt = r.TGraphErrors(5)
    for i in range(0,5):
        graph_1D_mt.SetPoint(i,x_2[i+6],z_2[i+6])
        graph_1D_mt.SetPointError(i,xerr_2[i+6],zerr_2[i+6])
    r.gStyle.SetErrorX(0.)
    graph_1D_mt.SetMarkerStyle(5)
    graph_1D_mt.SetMarkerSize(0.5)
    graph_1D_mt.Fit(fit_1D_mt, "RB")
    graph_1D_mt.Draw("AP")
    graph_1D_mt.SetTitle("Average sub-jet invariant mass vs Top quark mass")
    graph_1D_mt.GetYaxis().SetTitle("Average sub-jet invariant mass [GeV/c^{2}]")
    graph_1D_mt.GetXaxis().SetTitle("Top quark mass [GeV/c^{2}]")
    graph_1D_mt.GetYaxis().SetTitleOffset(1.4)
    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    chi2_1D_mt = fit_1D_mt.GetChisquare()
    ndof_1D_mt = fit_1D_mt.GetNDF()
    par_1D_mt = fit_1D_mt.GetParameters()
    par_errors_1D_mt = fit_1D_mt.GetParErrors()
    latex.DrawText(0.55,0.20,"Fit function: mJ = D + E(mt - 172.5)")
    latex.DrawText(0.55,0.15,"D = %.2f +- %.2f" %(par_1D_mt[0],par_errors_2D[0]))
    latex.DrawText(0.55,0.10,"E = %.2f +- %.2f" %(par_1D_mt[1],par_errors_2D[1]))
    latex.DrawText(0.55,0.05,"chi^{2}/ndof = %.2f/%.0f" %(chi2_1D_mt,ndof_1D_mt))
    pad2.cd()
    r.gStyle.SetOptStat(0)
    edges = array('f',[166.25, 171.75, 172.25, 172.75, 173.25, 178.75])
    subplot2 = r.TH1D("graph","graph title",5,edges)
    for i in range(1,6):
        subplot2.SetBinContent(i,z_2[i+5])
        subplot2.SetBinError(i,zerr_2[i+5])
    subplot2.Divide(fit_1D_mt)
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,6):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    for i in range(6,11):
        subplot_x_values.append(x_2[i])
        subplot_x_errors.append(xerr_2[i])
    subplot = r.TGraphErrors(5,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    r.gStyle.SetErrorX(0.)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("Top quark mass [GeV/c^{2}]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / fit")
    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9983,1.0017)
    line = r.TLine(169,1,176,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.SetGrid()
    canvas_scatter.Draw()
    canvas_scatter.SaveAs("histogram_images/mW_vs_JSF_and_TopMass_1D_projection_TopMass.pdf")
    print('')

###################
################### For JSF vs mJ 
###################

""" # These should be parameters that you need to edit
JSF_values = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]

# Number of points is length of x values (JSF_values here)
number_of_points = len(JSF_values)

# Assign an error of 0.0 to each JSF value
JSF_values_errors = array('f')
for i in range(0,number_of_points):
    JSF_values_errors.append(0)

# Create empty arrays of type float that will hold the y values and y errors
y_values = array('f')
y_errors = array('f')

# Go over each JSF value, and hence each root file and histogram
for i in range(0,len(JSF_values)):

    histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8AF2/mJ/JSF_" + JSF_values_string[i] + "_mt_172_5_ttpp8AF2.root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    
    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    y_values.append(hist_mean)
    y_errors.append(hist_mean_error)

yVsJSF(number_of_points, JSF_values, y_values, JSF_values_errors, y_errors, "topjet_m")

print('')
print('Script has finished')
print('') """

 
###################
################### For JSF vs mW (largest PT method only)
###################

""" # These should be parameters that you need to edit
JSF_values = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]

# Number of points is length of x values (JSF_values here)
number_of_points = len(JSF_values)

# Assign an error of 0.0 to each JSF value
JSF_values_errors = array('f')
for i in range(0,number_of_points):
    JSF_values_errors.append(0)

# Create empty arrays of type float that will hold the y values and y errors
y_values = array('f')
y_errors = array('f')

# Go over each JSF value, and hence each root file and histogram
for i in range(0,len(JSF_values)):

    histogram_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttpp8AF2/All/JSF_" + JSF_values_string[i] + "_ttpp8AF2.root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_W_invMass"):
            hist = histogram_root_file.Get(h.GetName())
    
    hist.GetXaxis().SetRangeUser(60,110)
    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    y_values.append(hist_mean)
    y_errors.append(hist_mean_error)

yVsJSF(number_of_points, JSF_values, y_values, JSF_values_errors, y_errors, " ")

print('')
print('Script has finished')
print('') """


###################
################### For m(top) vs mW (largest PT method)
###################

""" # These should be parameters that you need to edit
JSF_values = array('f', [1.00])
JSF_values_string = ["1_00"]
topMassValues = array('f',[169.0,172.0,172.5,173.0,176.0])
topMassValues_string = ["169","172","173","176"]

# Number of points is length of x values (JSF_values here)
number_of_points = len(topMassValues)

# Assign an error of 0.0 to each JSF value
topMassValues_errors = array('f')
for i in range(0,number_of_points):
    topMassValues_errors.append(0)

# Create empty arrays of type float that will hold the y values and y errors
y_values = array('f')
y_errors = array('f')

# Go over each JSF value, and hence each root file and histogram
for i in range(0,2):

    # Allows automation of finding the root file containing the histogram
    histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m" + topMassValues_string[i] + "/mW/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()

        # This may need to be changed depending on the name of the histogram
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    
    # Get values to use for y and y error
    hist.GetXaxis().SetRangeUser(60,110)
    hist_mean = hist.GetMean()
    hist_mean_error = hist.GetMeanError()

    hist_integral = hist.Integral()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

    #Append these to the arrays
    y_values.append(hist_mean)
    y_errors.append(hist_mean_error)

histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8AF2/mW/JSF_1_00_mt_172_5_ttpp8AF2.root"
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal"):
        hist = histogram_root_file.Get(h.GetName())

# Get values to use for y and y error
hist.GetXaxis().SetRangeUser(60,110)
hist_mean = hist.GetMean()
hist_mean_error = hist.GetMeanError()

hist_integral = hist.Integral()
#hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

#Append these to the arrays
y_values.append(hist_mean)
y_errors.append(hist_mean_error)

for i in range(2,4):

    # Allows automation of finding the root file containing the histogram
    histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m" + topMassValues_string[i] + "/mW/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()

        # This may need to be changed depending on the name of the histogram
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())
    
    # Get values to use for y and y error
    hist.GetXaxis().SetRangeUser(60,110)
    hist_mean = hist.GetMean()
    hist_mean_error = hist.GetMeanError()

    hist_integral = hist.Integral()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

    #Append these to the arrays
    y_values.append(hist_mean)
    y_errors.append(hist_mean_error)

yVsTopMass(number_of_points, topMassValues, y_values, topMassValues_errors, y_errors)

print('')
print('Script has finished')    
print('') """

###################
################### For m(top) vs mJ
###################

""" topMassValues = array('f',[169.0,172.0,172.5,173.0,176.0])
topMassValues_string = ["169","172","173","176"]

number_of_points = len(topMassValues)

topMassValues_errors = array('f')
for i in range(0,number_of_points):
    topMassValues_errors.append(0)

mJ_values = array('f')
mJ_errors = array('f')

for i in range(0,2):

    histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m" + topMassValues_string[i] + "/mJ/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    
    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    
    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)

histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8AF2/mJ/JSF_1_00_mt_172_5_ttpp8AF2.root"
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal"):
        hist = histogram_root_file.Get(h.GetName())

hist_mean = hist.GetMean()
hist_integral = hist.Integral()
hist_mean_error = hist.GetMeanError()

mJ_values.append(hist_mean)
mJ_errors.append(hist_mean_error)

for i in range(2,4):

    histogram_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8m" + topMassValues_string[i] + "/mJ/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal"):
            hist = histogram_root_file.Get(h.GetName())
    
    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    
    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)

yVsTopMass(number_of_points, topMassValues, mJ_values, topMassValues_errors, mJ_errors)

print('')
print('Script has finished')    
# print('') """

###################
################### 2D fit - mJ
###################

JSFvalues = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03,1.00,1.00,1.00,1.00,1.00,1.00])
topMassValues = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,172.5,169.0,171.0,172.0,173.0,174.0,176.0])         # mt array for when plotting mJ vs JSF
topMassValues_2 = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,169.0,171.0,172.0,172.5,173.0,174.0,176.0])       # mt array for when plotting mj vs mt
JSFvalues_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
topMassValues_string = ["169","171","172","173","174","176"]
numberOfPoints = len(JSFvalues)

topMassValues_errors = array('f')
topMassValues_errors_2 = array('f')
JSFvalues_errors = array('f')
for i in range(0,numberOfPoints):
    topMassValues_errors.append(0)
    topMassValues_errors_2.append(0)
    JSFvalues_errors.append(0)

# mJ arrays for when plotting mJ vs JSF
mJ_values = array('f')
mJ_errors = array('f')

# mJ arrays for when plotting mJ vs mt
mJ_values_2 = array('f')
mJ_errors_2 = array('f')

# Go over each JSF varied sample (ttpp8AF2)
for i in range(0,len(JSFvalues_string)):

    histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/mJ/JSF_" + JSFvalues_string[i] + "_ttpp8AF2.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)


# Go over each top mass varied sample (ttpp8AF2)
for i in range(0,len(topMassValues_string)):

    histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "/All/JSF_1_00_ttpp8m" + topMassValues_string[i] + "_ALL.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)


# Go over each JSF varied sample (ttpp8AF2)
for i in range(0,len(JSFvalues_string)):

    if (i != 3): # Added this in

        histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/mJ/JSF_" + JSFvalues_string[i] + "_ttpp8AF2.root"
        histogram_root_file = r.TFile.Open(histogram_path)
        for h in histogram_root_file.GetListOfKeys():
            h = h.ReadObj()
            if (h.GetName() == "nominal_topjet_m"):
                hist = histogram_root_file.Get(h.GetName())

        hist_mean = hist.GetMean()
        hist_mean_error = hist.GetMeanError()
        #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

        mJ_values_2.append(hist_mean)
        mJ_errors_2.append(hist_mean_error)


# Go over each top mass varied sample (ttpp8AF2)
for i in range(0,3):

    histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "/All/JSF_1_00_ttpp8m" + topMassValues_string[i] + "_ALL.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mJ_values_2.append(hist_mean)
    mJ_errors_2.append(hist_mean_error)


histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/mJ/JSF_1_00_ttpp8AF2.root"
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal_topjet_m"):
        hist = histogram_root_file.Get(h.GetName())

hist_mean = hist.GetMean()
hist_integral = hist.Integral()
hist_mean_error = hist.GetMeanError()
#hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

mJ_values_2.append(hist_mean)
mJ_errors_2.append(hist_mean_error)

for i in range(3,6): 

    histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "/All/JSF_1_00_ttpp8m" + topMassValues_string[i] + "_ALL.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mJ_values_2.append(hist_mean)
    mJ_errors_2.append(hist_mean_error)

TGraph2DErrorsFit(numberOfPoints, topMassValues, topMassValues_errors, JSFvalues, JSFvalues_errors, mJ_values, mJ_errors, topMassValues_2, topMassValues_errors_2, mJ_values_2, mJ_errors_2)

print('')
print('Script has finished')    
print('')

###################
################### 2D fit - mJ FULL SIM
###################

# JSFvalues = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03,1.00,1.00])
# topMassValues = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,172.5,171.0,174.0])         # mt array for when plotting mJ vs JSF
# topMassValues_2 = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,171.0,172.5,174.0])       # mt array for when plotting mj vs mt
# JSFvalues_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
# topMassValues_string = ["171","174"]
# numberOfPoints = len(JSFvalues)

# topMassValues_errors = array('f')
# topMassValues_errors_2 = array('f')
# JSFvalues_errors = array('f')
# for i in range(0,numberOfPoints):
#     topMassValues_errors.append(0)
#     topMassValues_errors_2.append(0)
#     JSFvalues_errors.append(0)

# # mJ arrays for when plotting mJ vs JSF
# mJ_values = array('f')
# mJ_errors = array('f')

# # mJ arrays for when plotting mJ vs mt
# mJ_values_2 = array('f')
# mJ_errors_2 = array('f')

# # Go over each JSF varied sample (ttpp8AF2)
# for i in range(0,len(JSFvalues_string)):

#     histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_" + JSFvalues_string[i] + "_ttallpp8.root"
#     histogram_root_file = r.TFile.Open(histogram_path)
#     for h in histogram_root_file.GetListOfKeys():
#         h = h.ReadObj()
#         if (h.GetName() == "nominal_topjet_m"):
#             hist = histogram_root_file.Get(h.GetName())

#     hist_mean = hist.GetMean()
#     hist_integral = hist.Integral()
#     hist_mean_error = hist.GetMeanError()
#     #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

#     mJ_values.append(hist_mean)
#     mJ_errors.append(hist_mean_error)


# # Go over each top mass varied sample (ttpp8AF2)
# for i in range(0,len(topMassValues_string)):

#     histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "_FullSim/mJ/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
#     histogram_root_file = r.TFile.Open(histogram_path)
#     for h in histogram_root_file.GetListOfKeys():
#         h = h.ReadObj()
#         if (h.GetName() == "nominal_topjet_m"):
#             hist = histogram_root_file.Get(h.GetName())

#     hist_mean = hist.GetMean()
#     hist_integral = hist.Integral()
#     hist_mean_error = hist.GetMeanError()
#     #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
#     mJ_values.append(hist_mean)
#     mJ_errors.append(hist_mean_error)


# # Go over each JSF varied sample (ttpp8AF2)
# for i in range(0,len(JSFvalues_string)):

#     if (i != 3): # Added this in

#         histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_" + JSFvalues_string[i] + "_ttallpp8.root"
#         histogram_root_file = r.TFile.Open(histogram_path)
#         for h in histogram_root_file.GetListOfKeys():
#             h = h.ReadObj()
#             if (h.GetName() == "nominal_topjet_m"):
#                 hist = histogram_root_file.Get(h.GetName())

#         hist_mean = hist.GetMean()
#         hist_mean_error = hist.GetMeanError()
#         #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

#         mJ_values_2.append(hist_mean)
#         mJ_errors_2.append(hist_mean_error)


# # Go over each top mass varied sample (ttpp8AF2)
# for i in range(0,1):

#     histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "_FullSim/mJ/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
#     histogram_root_file = r.TFile.Open(histogram_path)
#     for h in histogram_root_file.GetListOfKeys():
#         h = h.ReadObj()
#         if (h.GetName() == "nominal_topjet_m"):
#             hist = histogram_root_file.Get(h.GetName())

#     hist_mean = hist.GetMean()
#     hist_integral = hist.Integral()
#     hist_mean_error = hist.GetMeanError()
#     #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
#     mJ_values_2.append(hist_mean)
#     mJ_errors_2.append(hist_mean_error)


# histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
# histogram_root_file = r.TFile.Open(histogram_path)
# for h in histogram_root_file.GetListOfKeys():
#     h = h.ReadObj()
#     if (h.GetName() == "nominal_topjet_m"):
#         hist = histogram_root_file.Get(h.GetName())

# hist_mean = hist.GetMean()
# hist_integral = hist.Integral()
# hist_mean_error = hist.GetMeanError()
# #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

# mJ_values_2.append(hist_mean)
# mJ_errors_2.append(hist_mean_error)

# for i in range(1,2): 

#     histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttpp8m" + topMassValues_string[i] + "_FullSim/mJ/JSF_1_00_ttpp8m" + topMassValues_string[i] + ".root"
#     histogram_root_file = r.TFile.Open(histogram_path)
#     for h in histogram_root_file.GetListOfKeys():
#         h = h.ReadObj()
#         if (h.GetName() == "nominal_topjet_m"):
#             hist = histogram_root_file.Get(h.GetName())

#     hist_mean = hist.GetMean()
#     hist_integral = hist.Integral()
#     hist_mean_error = hist.GetMeanError()
#     #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
#     mJ_values_2.append(hist_mean)
#     mJ_errors_2.append(hist_mean_error)

# TGraph2DErrorsFit_FullSim(numberOfPoints, topMassValues, topMassValues_errors, JSFvalues, JSFvalues_errors, mJ_values, mJ_errors, topMassValues_2, topMassValues_errors_2, mJ_values_2, mJ_errors_2)

# print('')
# print('Script has finished')    
# print('')

###################
################### 2D fit - mW
###################

""" JSFvalues = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03,1.00,1.00,1.00,1.00])
topMassValues = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,172.5,169.0,172.0,173.0,176.0])         # mt array for when plotting mW vs JSF
topMassValues_2 = array('f', [172.5,172.5,172.5,172.5,172.5,172.5,169.0,172.0,172.5,173.0,176.0])       # mt array for when plotting mW vs mt
JSFvalues_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
topMassValues_string = ["169","172","173","176"]
numberOfPoints = len(JSFvalues)

topMassValues_errors = array('f')
topMassValues_errors_2 = array('f')
JSFvalues_errors = array('f')
for i in range(0,numberOfPoints):
    topMassValues_errors.append(0)
    topMassValues_errors_2.append(0)
    JSFvalues_errors.append(0)

# mJ arrays for when plotting mJ vs JSF
mW_values = array('f')
mW_errors = array('f')

# mJ arrays for when plotting mJ vs mt
mW_values_2 = array('f')
mW_errors_2 = array('f')

# Go over each JSF varied sample (ttpp8AF2)
for i in range(0,len(JSFvalues_string)):

    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_" + JSFvalues_string[i] + "_ttpp8AF2_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

    mW_values.append(hist_mean)
    mW_errors.append(hist_mean_error)


# Go over each top mass varied sample (ttpp8m*)
for i in range(0,len(topMassValues_string)):

    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m" + topMassValues_string[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mW_values.append(hist_mean)
    mW_errors.append(hist_mean_error)


# Go over each JSF varied sample (ttpp8AF2)
for i in range(0,len(JSFvalues_string)):

    if (i != 3): # Added this in

        histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_" + JSFvalues_string[i] + "_ttpp8AF2_more_events_SkipTruth_1.root"    
        histogram_root_file = r.TFile.Open(histogram_path)
        for h in histogram_root_file.GetListOfKeys():
            h = h.ReadObj()
            if (h.GetName() == "nominal_no_neg"):
                hist = histogram_root_file.Get(h.GetName())

        hist_mean = hist.GetMean()
        hist_mean_error = hist.GetMeanError()
        #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

        mW_values_2.append(hist_mean)
        mW_errors_2.append(hist_mean_error)


# Go over each top mass varied sample (ttpp8AF2)
for i in range(0,2):

    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m" + topMassValues_string[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mW_values_2.append(hist_mean)
    mW_errors_2.append(hist_mean_error)


histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8AF2_more_events_SkipTruth_1.root"    
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal_no_neg"):
        hist = histogram_root_file.Get(h.GetName())

hist_mean = hist.GetMean()
hist_integral = hist.Integral()
hist_mean_error = hist.GetMeanError()
#hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)

mW_values_2.append(hist_mean)
mW_errors_2.append(hist_mean_error)

for i in range(2,4): 

    histogram_path = "output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m" + topMassValues_string[i] + "_more_events_SkipTruth_1.root"    
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_no_neg"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
    mW_values_2.append(hist_mean)
    mW_errors_2.append(hist_mean_error)

TGraph2DErrorsFit_mW(numberOfPoints, topMassValues, topMassValues_errors, JSFvalues, JSFvalues_errors, mW_values, mW_errors, topMassValues_2, topMassValues_errors_2, mW_values_2, mW_errors_2)

print('')
print('Script has finished')    
print('') """

###################
################### For FractionOfTimesBuiltW vs Top Mass
###################

""" number_of_points = 5
y = array( 'f', [0.537843085, 0.535421953, 0.533957889, 0.532461306, 0.528088387])
x = array( 'f', [169.0, 172.0, 172.5, 173.0, 176.0])
xerr = array( 'f', [0,0,0,0,0])
yerr = array( 'f', [0.00192296, 0.001070281, 0.001182563, 0.001051477, 0.001748687])

yVsTopMass(number_of_points, x, y, xerr, yerr)

print('')
print('Script has finished')
print('') """

###################
################### For fit parameters vs JSF
###################

# number_of_points = 7
# x = array( 'f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
# y = array( 'f', [81.72496833,82.41974604,83.12386805,83.81092494,84.49637991,85.21745309,85.96474113])
# xerr = array( 'f', [0,0,0,0,0])
# yerr = array( 'f', [0.03178148,0.032745016,0.03325092,0.034148,0.034934705,0.035398208,0.0362346]) 

# yVsJSF(number_of_points, x, y, xerr, yerr, " ")

###################
################### For JSF vs mW (3 different methods of calculation) and top jet mass
###################

""" # These should be parameters that you need to edit
JSF_values = array('f', [0.97,0.98,0.99,1.00,1.01,1.02,1.03])
JSF_values_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]
histogram_types = ["_largestPT_","_alllightjets_","_ClosestToW_"]
#histogram_types = ["topjet_m"]

# Number of points is length of x values (JSF_values here)
number_of_points = len(JSF_values)

# Assign an error of 0.0 to each JSF value
JSF_values_errors = array('f')
for i in range(0,number_of_points):
    JSF_values_errors.append(0)

# Go over all invariant mass types
for i in range(0,len(histogram_types)):
    print('')

    # Create empty arrays of type float that will hold the y values and y errors
    y_values = array('f')
    y_errors = array('f')

    # Go over each JSF value, and hence each root file and histogram
    for j in range(0,len(JSF_values)):

        # Allows automation of finding the root file containing the histogram
        #histogram_path = "output_root_files/output_topjet_m_JSF_" + JSF_values_string[j] + "_more_events_SkipTruth_1.root"
        #histogram_path = "output_root_files/output_Subjet_invMass" + histogram_types[i] + "JSF_" + JSF_values_string[j] + "_more_events_SkipTruth_1.root"
        #histogram_path = "output_root_files/output_topjet_m_JSF_" + JSF_values_string[j] + "_ttpp8AF2_more_events_SkipTruth_1.root"
        histogram_path = "../output_root_files/output_Subjet_invMass" + histogram_types[i] + "JSF_" + JSF_values_string[j] + "_ttpp8AF2_more_events_SkipTruth_1.root"
        
        histogram_root_file = r.TFile.Open(histogram_path)
        for h in histogram_root_file.GetListOfKeys():
            h = h.ReadObj()

            # This may need to be changed depending on the name of the histogram
            #if (h.GetName() == "nominal"):
            if (h.GetName() == "nominal_no_neg"):
                hist = histogram_root_file.Get(h.GetName())

                hist.GetXaxis().SetRangeUser(60,110) # ADDED THIS IN FOR TEST - 17/01/2022

        # Get values to use for y and y error
        hist_mean = hist.GetMean()
        hist_mean_error = hist.GetMeanError()

        #hist_integral = hist.Integral()
        #hist_mean_error = hist.GetStdDev()/math.sqrt(hist_integral)
        
        # Append these to the arrays
        y_values.append(hist_mean)
        y_errors.append(hist_mean_error)

    yVsJSF(number_of_points, JSF_values, y_values, JSF_values_errors, y_errors, histogram_types[i])

print('')
print('Script has finished')    
print('')
 """
