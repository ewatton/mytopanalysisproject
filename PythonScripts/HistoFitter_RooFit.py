from __future__ import print_function
import ROOT
 
 
# Set up model
# ---------------------
 
# Declare observable mW
mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)
 
# Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and
# their parameters
mean1 = ROOT.RooRealVar("mean1", "mean of first gaussian", 83.8226)
mean2 = ROOT.RooRealVar("mean2", "mean of second gaussian", 81.6166)
sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.9132)
sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.750)
 
gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
 
# Sum the signal components into a composite signal p.d.f.
gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0.0, 1.0)
combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))
 
# Import TH1D
# -----------------------------------------
 
histogram_file_path = "../output_root_files/mW_distributions_varied_JSF/After_setting_Sumw2(True)/output_mW_JSF_1_00_mt_172_5_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
hist = histfile.Get("nominal")
hist.SetDirectory(0)
histfile.Close()
dataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist, 1.0)
 
# Construct a chi^2 of the data and the model.
# When a p.d.f. is used in a chi^2 fit, probability density scaled
# by the number of events in the dataset to obtain the fit function
# If model is an extended p.d.f, expected number events is used
# instead of the observed number of events.
ll = ROOT.RooLinkedList()
fitResult = combinedGauss.chi2FitTo(dataHist, ll)

mWframe = mW.frame()
dataHist.plotOn(mWframe)
#combinedGauss.plotOn(mWframe)
gauss1.plotOn(mWframe)
gauss2.plotOn(mWframe)
mWframe.GetYaxis().SetTitle("Entries")
mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
canvas = ROOT.TCanvas("canvas")
mWframe.Draw()
canvas.SaveAs("../histogram_images/mW_distributions_varied_JSF/Fitting/TwoGaussians/RooFit/RooFit_test.pdf")


# NB: It is also possible to fit a ROOT.RooAbsReal function to a ROOT.RooDataHist
# using chi2FitTo().
 
