from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    eventsWithLowMassTop = 0
    eventsWithLowMassAntiTop = 0
    eventsTotal = 0

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)
        if (isRecoLevel == True):
            #baseWeight = tree_name.weight_normalise
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight

            # if (tree_name.weight_pileup == 0):
            # continue

            wsel = ((tree_name.topjet_nSubjets>2) and (tree_name.topjet_W_invMass>60000.0) and (tree_name.topjet_W_invMass<105000.0))
            topsel = ((tree_name.topjet_nSubjets>1) and (tree_name.topjet_m>135000.0) and (tree_name.topjet_m<205000.0))
            # partontopsel = ((tree_name.parton_top_m>150000.0) and (tree_name.parton_antitop_m>150000.0))
            # recoilvar_mass_sel = ((tree_name.naddjet>0) and (tree_name.tmm_leptop_cttaddjet_dR<0.5))
            # recoilvar_dR_sel = ((tree_name.naddjet>0) and (tree_name.tmm_leptop_m<170000.0))
            # recoilvar_dR_sel = (tree_name.naddjet>0)
            # recoilvar_mass_peak_sel = ((tree_name.naddjet>0) and (tree_name.tmm_leptop_cttaddjet_m>150000.0) and (tree_name.tmm_leptop_cttaddjet_m<190000.0))
            # recoilvar_dR_peak_sel = ((tree_name.naddjet>0) and (tree_name.tmm_leptop_cttaddjet_dR<0.5))

            # if wsel:
            # if topsel:
            if wsel or topsel:
                # if topsel and partontopsel:
                # if recoilvar_mass_sel:
                # if recoilvar_dR_sel:
                # if recoilvar_mass_peak_sel:
                # if recoilvar_dR_peak_sel:
                weight = tree_name.weight_normalise
            # eventsTotal = eventsTotal + 1
            # if (variable_name == "parton_top_m"):
            #     if (tree_name.parton_top_m<150000.0):
            #         eventsWithLowMassTop = eventsWithLowMassTop + 1
            # if (variable_name == "parton_antitop_m"):
            #     if (tree_name.parton_antitop_m<150000.0):
            #         eventsWithLowMassAntiTop = eventsWithLowMassAntiTop + 1 
            else:
                continue

        else:
            #baseWeight = tree_name.mcEventNormWeight
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            # weight = tree_name.mcEventNormWeight

            wsel = ((tree_name.topjet_nSubjets>2) and (tree_name.topjet_W_invMass>60000.0) and (tree_name.topjet_W_invMass<105000.0))
            topsel = ((tree_name.topjet_nSubjets>1) and (tree_name.topjet_m>135000.0) and (tree_name.topjet_m<205000.0))
            partontopsel = ((tree_name.parton_top_m>150000.0) and (tree_name.parton_antitop_m>150000.0))

            # if wsel:
            # if topsel:
            # if wsel or topsel:
            if topsel and partontopsel:
                weight = tree_name.mcEventNormWeight
                eventsTotal = eventsTotal + 1
                # if (variable_name == "parton_top_m"):
                #     if (tree_name.parton_top_m<150000.0):
                #         eventsWithLowMassTop = eventsWithLowMassTop + 1
                # if (variable_name == "parton_antitop_m"):
                #     if (tree_name.parton_antitop_m<150000.0):
                #         eventsWithLowMassAntiTop = eventsWithLowMassAntiTop + 1 
            else:
                continue

        # nnloWeightModifier = tree_name.nnloWeight
        # weight = 1
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight)
            else:
                hist_name.Fill(variable / 1000.0, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight )
            else:
                hist_name.Fill(variable, weight)
    
    print("Finished filling histogram for variable: " + variable_name)

    print("\nNumber of events:")
    print("Events in histogram: " + str(eventsTotal))
    # print("Events with parton-level top mass < 150 GeV: " + str(eventsWithLowMassTop) + ", percentage = " + str((eventsWithLowMassTop/eventsTotal)*100.0) + "%")
    # print("Events with parton-level antitop mass < 150 GeV: " + str(eventsWithLowMassAntiTop) + ", percentage = " + str((eventsWithLowMassAntiTop/eventsTotal)*100.0) + "%")

def MakeHisto_auto(filePathSegment,sample,JSF):

    # input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/18/mergedOutput/ttbarBoosted/" + sample + ".root"
    # input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/" + sample + ".root"
    # output_file_path = "../output_root_files/2023-04-12-Profiling/" + sample + "/mcChannelNumber2018.root"
    output_file_path = "../output_root_files/2023-07-21-NewVariables/" + sample + "/mJ_mJselormWsel.root"
    # output_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample + "/All/JSF_" + JSF + "_" + sample + "_ALL_100bins.root"
    
    infile = TFile.Open(input_file_path,"READ");

    # Get trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        # if (h.ClassName() == 'TTree'):
        if (h.ClassName() == 'TTree') and ('nominal' in h.GetName()):
            tree = h
            trees.append(tree)

    print('\n==============================================================================')
    print('==============================================================================')
    print('\nSample = ' + sample + ', JSF = ' + JSF)
    print('\nTTrees from file: %s' %(input_file_path))
    print('\nOutput ROOT file: %s' %(output_file_path))
    print('\nThe list of TTrees read in is:')
    for treename in trees:
        print('')
        print(treename)
    print('\nThe total number of trees read in is: %i' %(len(trees)))

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"RECREATE")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable_dict = dict()

    # variable_dict["variable name"] = [variable type, if it needs to be converted from MeV to GeV, number of bins, low bin, high bin]

    # variable_dict["jet_pt"] = ["vector",True,50,0.0,900.0]
    # variable_dict["jet_eta"] = ["vector",False,50,-2.5,2.5]
    # variable_dict["jet_n"] = ["int",False,10,0,9]
    # variable_dict["passedPL"] = ["int",False,101,0,100]
    # variable_dict["top_lepb_deltaR"] = ["float",False,50,0.0,6.0]
    # variable_dict["top_lepb_deltaPhi"] = ["float",False,25,-3.15,3.15]
    # variable_dict["leadadd_top_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["leadadd_top_deltaPhi"] = ["vector",False,25,-3.15,3.15]
    # variable_dict["subleadadd_hadtop_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["subleadadd_hadtop_deltaPhi"] = ["vector",False,25,-3.15,3.15]
    # variable_dict["leadadd_subleadadd_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["leadadd_subleadadd_deltaPhi"] = ["vector",False,25,-3.15,3.15]
    # variable_dict["leadadd_lepb_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["leadadd_lepb_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_leadadd_leptop_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_leadadd_leptop_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_subleadadd_leptop_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_subleadadd_leptop_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_leadadd_ttbar_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_leadadd_ttbar_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_subleadadd_ttbar_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_subleadadd_ttbar_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_leadadd_leadtop_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_leadadd_leadtop_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_subleadadd_leadtop_deltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["tmm_subleadadd_leadtop_deltaPhi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["tmm_invmass_leadadd_leadtop"] = ["vector",False,50,135,205.0]
    # variable_dict["addjet_pt"] = ["vector",True,50,0.0,900.0]
    # variable_dict["addbjet_pt"] = ["vector",True,50,0.0,900.0]
    # variable_dict["naddbjet"] = ["int",False,10,0,9]
    # variable_dict["leadaddjet_pt"] = ["vector",True,25,0.0,1000.0]
    # variable_dict["leadaddjet_phi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["leadaddjet_eta"] = ["vector",False,50,-2.5,2.5]
    # variable_dict["leadaddjet_m"] = ["vector",True,50,0.0,200.0]
    # variable_dict["subleadaddjet_pt"] = ["vector",True,25,0.0,1000.0]
    # variable_dict["subleadaddjet_phi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["subleadaddjet_eta"] = ["vector",False,50,-2.5,2.5]
    # variable_dict["subleadaddjet_m"] = ["vector",True,50,0.0,200.0]
    # variable_dict["lepbjet_pt"] = ["float",True,50,0.0,900.0]
    # variable_dict["lepbjet_phi"] = ["float",False,50,-3.15,3.15]
    # variable_dict["lepbjet_eta"] = ["float",False,50,-2.5,2.5]
    # variable_dict["lepbjet_m"] = ["float",True,50,0.0,200.0]
    # variable_dict["naddjet"] = ["int",False,10,0,9]
    # variable_dict["mlb"] = ["float",True,50,0.0,200.0]
    # variable_dict["invmass_leadadd_hadtop"] = ["vector",True,25,0.0,1000.0]
    # variable_dict["invmass_subleadadd_hadtop"] = ["vector",True,50,0.0,500.]
    # variable_dict["nRCjet_afterMassWindow"] = ["int",False,10,0,9]
    # variable_dict["tmm_leptop_pt"] = ["float",True,25,0.0,1000.0]
    # variable_dict["tmm_leptop_phi"] = ["float",False,50,-3.15,3.15]
    # variable_dict["tmm_leptop_eta"] = ["float",False,50,-2.5,2.5]
    # variable_dict["tmm_leptop_m"] = ["float",True,50,0.0,250.0]
    # variable_dict["tmm_leptop_y"] = ["float",False,25,-2.5,2.5]
    # variable_dict["tmm_ttbar_deltaR"] = ["float",False,50,0.0,6.0]
    # variable_dict["tmm_ttbar_deltaPhi"] = ["float",False,25,-3.15,3.15]
    # variable_dict["tmm_ttbar_deltaEta"] = ["float",False,50,-2.5,2.5]
    # variable_dict["tmm_ttbar_invmass"] = ["float",True,25,350.0,2000.0]
    # variable_dict["tmm_ttbar_pt"] = ["float",True,25,0.0,1000.0]
    # variable_dict["tmm_ttbar_y"] = ["float",False,25,-2.5,2.5]
    # variable_dict["tmm_fulleventHT"] = ["float",True,50,0.0,2000.0]
    # variable_dict["tmm_mttbar_jets"] = ["float",True,50,0.0,2000.0]
    # variable_dict["tmm_leadtop_pt"] = ["float",True,50,0.0,900.0]
    # variable_dict["tmm_leadtop_m"] = ["float",True,50,0.0,300.0]
    # variable_dict["tmm_leadtop_y"] = ["float",False,50,-2.5,2.5]
    # variable_dict["lep_pt"] = ["vector",True,50,0.0,900.0]
    # variable_dict["lep_eta"] = ["vector",False,50,-2.5,2.5]
    # variable_dict["lep_phi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["lep_m"] = ["vector",True,50,0.0,200.0]
    # variable_dict["lep_type"] = ["vector",False,3,11,13]
    # variable_dict["met"] = ["float",True,50,0.0,1000.0]
    # variable_dict["met_phi"] = ["float",False,50,-3.15,3.15]
    # variable_dict["mtw"] = ["vector",True,50,0.0,200.0]
    # variable_dict["nbjet"] = ["int",False,10,0,9]
    # variable_dict["topjet_pt"] = ["float",True,25,0.0,1000.0]
    # variable_dict["topjet_eta"] = ["float",False,50,-2.5,2.5]
    # variable_dict["topjet_phi"] = ["float",False,50,-3.15,3.15]
    variable_dict["topjet_m"] = ["float",True,50,135.0,205.0]
    # variable_dict["topjet_m"] = ["float",True,19,135.0,205.0]
    # variable_dict["topjet_y"] = ["float",False,25,-2.5,2.5]
    # variable_dict["topjet_nSubjets"] = ["int",False,10,0,9]
    # variable_dict["topjet_avgSubjet_pt"] = ["float",True,50,0.0,900.0]
    # variable_dict["topjet_avgSubjet_eta"] = ["float",False,50,-2.5,2.5]
    # variable_dict["topjet_W_invMass"] = ["float",True,50,0.0,200.0]
    # variable_dict["topjet_W_invMass"] = ["float",True,20,40.0,120.0]
    # variable_dict["topjet_W_invMass"] = ["float",True,12,60.0,105.0]
    # variable_dict["topjet_nbtaggedSubjets"] = ["int",False,10,0,9]
    # variable_dict["topjet_Subjet_eta"] = ["vector",False,50,-2.5,2.5]
    # variable_dict["topjet_Subjet_phi"] = ["vector",False,50,-3.15,3.15]
    # variable_dict["topjet_Subjet_pT"] = ["vector",True,50,0.0,900.0]
    # variable_dict["topjet_Subjet_mass"] = ["vector",True,50,0.0,200.0]
    # variable_dict["topjet_Subjet_type"] = ["vector",False,5,-1,4]
    # variable_dict["topjet_Subjet_DeltaR"] = ["vector",False,50,0.0,6.0]
    # variable_dict["topjet_Subjet_isMatched"] = ["vector",False,5,-2,3]
    # variable_dict["topjet_Subjet_jetresponse"] = ["vector",False,50,0.0,2.0]
    # variable_dict["topjet_Subjet_bjetresponse"] = ["vector",False,50,0.0,2.0]
    # variable_dict["tmm_ttbar_invmass"] = ["float",True,50,400.0,1800.0]

    # variable_dict["lepbjet_lep_pT_ratio"] = ["float",False,100,0.0,10.0]
    # variable_dict["tmm_leptop_addjet_m"] = ["float",True,100,0.0,600.0]
    # variable_dict["tmm_leptop_bjet_addjet_m"] = ["float",True,100,0.0,600.0]

    # variable_dict["mcChannelNumber"] = ["float",False,5,601668.5,601673.5] # FSR up
    # variable_dict["mcChannelNumber"] = ["float",False,5,601667.5,601672.5] # FSR down
    # variable_dict["mcChannelNumber"] = ["float",False,8,601490.5,601498.5] # pThard
    # variable_dict["mcChannelNumber"] = ["float",False,4,601354.5,601358.5] # RTT

    # variable_dict["tmm_leptop_leadaddjet_m"] = ["float",True,20,100.0,500.0]
    # variable_dict["tmm_leptop_leadaddjet_dR"] = ["float",False,20,0.0,5.0]
    # variable_dict["tmm_leptop_bjet_leadaddjet_dR"] = ["float",False,20,0.0,5.0]
    # variable_dict["tmm_leptop_cttaddjet_m"] = ["float",True,36,100.0,280.0]
    # variable_dict["tmm_leptop_cttaddjet_m"] = ["float",True,18,100.0,280.0]
    # variable_dict["tmm_leptop_cttaddjet_dR"] = ["float",False,40,0.0,2.0]
    # variable_dict["tmm_leptop_cttaddjet_dR"] = ["float",False,20,0.0,2.0]
    # variable_dict["tmm_leptop_bjet_cttaddjet_dR"] = ["float",False,20,0.0,5.0]

    # variable_dict["parton_top_m"] = ["float",True,140,100.0,240.0]
    # variable_dict["parton_antitop_m"] = ["float",True,140,100.0,240.0]

    print('\nThe total number of histograms to create for each tree is: %i' %(len(variable_dict)))
    print('\n==============================================================================')
    print('==============================================================================\n')

    for z in range(0,len(trees)):
        histcount = 1
        for variable in variable_dict:
            name = trees[z].GetName() + "_" + variable
            # name = trees[z].GetName() + "_" + variable + "_lepbjet_pt_ratio"
            hist = TH1D(name, variable + "; variable; Events", variable_dict[variable][2], variable_dict[variable][3], variable_dict[variable][4])
            # hist = TH1D(name, variable + "; variable; Events", 100, 0, 2.5)
            FillHisto_auto(trees[z],hist,isRecoLevel,str(variable),variable_dict[variable][0],variable_dict[variable][1])
            print("Finished filling histogram number " + str(histcount) + "/" + str(len(variable_dict)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
            hist.Write()
            histcount += 1
        print("Finished filling the histograms for tree number " + str(z+1) + "/" + str(len(trees)))

    outfile.Close()


# ==========================================================================
# =========================== Main part of code ============================
# ==========================================================================

print('')
samplesToRunOn = [
    
    "ttallpp8",
    # "ttMCatNLOH713",
    # "ttnfaMCatNLO",
    # "ttph713",
    # "ttph721",
    "ttpp8AF2",
    # "ttpp8CR",
    # "ttpp8CR0",
    # "ttpp8CR1",
    # "ttpp8CR2",
    # "ttpp8m169",
    # "ttpp8m171",
    # "ttpp8m172",
    # "ttpp8m172_25",
    # "ttpp8m172_75",
    # "ttpp8m173",
    # "ttpp8m174",
    # "ttpp8m176",
    # "ttpp8MECOff",
    # "ttpp8RTT",
    # "ttpp8Var",
    # "ttpp8Var_AFII",
    # "diboson",
    # "SingleTop",
    # "SingleTopDS",
    # "SingleTopH7",
    # "ttV",
    # "wjets",
    # "wejets",
    # "wmujets",
    # "wtaujets",
    # "zjets",
    # "Fakes",
    "ttpp8FSRup",
    "ttpp8FSRdown",
    # "ttpp8pThard1",
    # "ttpmp8Lineshape",
    # "ttpp8RTW"

    ]

for i in range(0,len(samplesToRunOn)):

    MakeHisto_auto("2023_07_20_TTDIFFXSMASSv03_ntuples",samplesToRunOn[i],"1_00")
    # MakeHisto_auto("2023_09_26_TTDIFFXSMASSv03_RTT_pThard_DSIDFix/JSF_1_00_FastSim/AllYears/ttbarBoosted/",samplesToRunOn[i],"1_00")
    # MakeHisto_auto("2023_09_01_TTDIFFXSMASSv03_Nominal_PartonLevelTop/JSF_1_00_FastSim/AllYears/ttbarBoosted/",samplesToRunOn[i],"1_00")
    # MakeHisto_auto("2023_10_16_TTDIFFXSMASSv03_FSR/JSF_1_00_FastSim/AllYears/ttbarBoosted/",samplesToRunOn[i],"1_00")

print('')
print('Histograms have been saved to the output_root_files folder')
print('')