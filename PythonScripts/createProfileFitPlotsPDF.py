# A script that takes in the systematic histogram plots from TRExFitter and puts them in a PDF

import os
import img2pdf
import glob

directoryPath = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/mWFit/Systematics/"

with open("../ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/mWFit/Systematics/combinedSystematics.pdf","wb") as f:
    f.write(img2pdf.convert(glob.glob(directoryPath + "*/.png")))
