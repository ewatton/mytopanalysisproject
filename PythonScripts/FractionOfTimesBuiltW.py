# Macro to calculate the fraction of events that can build the W boson (at least 2 light quark jets)
# First created by Elliot Watton - 26/10/2021
# Last editted by Elliot Watton - 06/12/2021

import ROOT as r
r.TH1.SetDefaultSumw2(True)
print('')

def FractionOfTimesBuiltW(hist_name):
    
    total_num_of_entries_after_origin = 0
    total_num_of_entries_before_origin = 0
    bin_errors_combined_in_quad_before_origin = 0
    bin_errors_combined_in_quad_after_origin = 0
    
    print('Finding the fraction of events that are capable of building the W boson for the following histogram:')
    print(hist_name)
    
    BinNumberAtOrigin = hist_name.FindBin(0)

    # Go over all bins after origin
    for i in range(BinNumberAtOrigin, hist_name.GetNbinsX()+1):
        num_of_entries_in_bin = hist_name.GetBinContent(i)
        total_num_of_entries_after_origin += num_of_entries_in_bin
        bin_error = hist_name.GetBinError(i)
        bin_errors_combined_in_quad_after_origin += (bin_error * bin_error)

    # Go over all bins before origin
    for i in range(1, BinNumberAtOrigin):
        num_of_entries_in_bin = hist_name.GetBinContent(i)
        total_num_of_entries_before_origin += num_of_entries_in_bin
        bin_error = hist_name.GetBinError(i)
        bin_errors_combined_in_quad_before_origin += (bin_error * bin_error)

    total_num_of_entries = total_num_of_entries_after_origin + total_num_of_entries_before_origin
    total_num_of_entries_error = bin_errors_combined_in_quad_after_origin + bin_errors_combined_in_quad_before_origin
    fraction = total_num_of_entries_after_origin/total_num_of_entries
    fraction_err = fraction * ( (bin_errors_combined_in_quad_after_origin/(total_num_of_entries_after_origin**2)) + (total_num_of_entries_error/(total_num_of_entries**2)) ) ** 0.5

    print 'The total number of events is: ',total_num_of_entries
    print 'The number of events which could build the W boson is: ',total_num_of_entries_after_origin
    print 'This is a fraction of: ',fraction
    print 'The error on this fraction is: ',fraction_err
    print 'The integral is: ',hist_name.Integral()
    print 'The mean of this histogram is: ',hist_name.GetMean()
    print 'The error on the mean of this histogram is: ',hist_name.GetMeanError()
    print ('')

# Get the histograms from the file
histfile = r.TFile.Open("output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8m176_more_events_SkipTruth_1.root")
histograms = []
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == 'nominal'):
        hist = histfile.Get(h.GetName())
        histograms.append(hist)


# Change the histogram directories so that they do not disappear when histogram file is closed
for i in range(0,len(histograms)):
    histograms[i].SetDirectory(0)

# Close the histogram file
histfile.Close()

# Find and print the fraction of the events that have at least 2 light quark jets
for i in range(0,len(histograms)):
    FractionOfTimesBuiltW(histograms[i])
