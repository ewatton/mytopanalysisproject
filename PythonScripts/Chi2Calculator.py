# Created 28/10/2022
# Purpose: To check that the chi^2 calculated using TF1::GetChisquare() were actually correct

import ROOT as r
from optparse import OptionParser

def choice(userChoice):
    if (userChoice != "mtop") and (userChoice != "JSF"):
        print("You have not given a valid command line argument choice! (Use -h) Exiting program...\n")
        return exit()
    elif userChoice == "JSF":
        return mJvsJSF()
    else:
        return mJvsmtop()

def fitfunc(mtop, JSF):
    A = 168.844494
    B = 0.471483
    C = 78.18811
    return A + B*(mtop - 172.5) + C*(JSF - 1)

def calcChi2(measuredList, expectedList, errorList):
    result = 0
    for i in range(0,len(measuredList)):
        result += ((measuredList[i] - expectedList[i])*(measuredList[i] - expectedList[i]))/(errorList[i]*errorList[i])
    return result

def mJvsmtop():
    measuredValues = []
    expectedValues = []
    errorsOnMeasuredValues = []

    masses = [169,171,172,172.5,173,174,176]

    for i in range(0,len(masses)):
        if masses[i] != 172.5:
            histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8m" + str(masses[i]) + "/mJ/JSF_1_00_ttpp8m" + str(masses[i]) + ".root"
        else:
            histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root"

        histogram_root_file = r.TFile.Open(histogram_path)
        for h in histogram_root_file.GetListOfKeys():
            h = h.ReadObj()
            if (h.GetName() == "nominal_topjet_m"):
                hist = histogram_root_file.Get(h.GetName())

        measuredValues.append(hist.GetMean())
        errorsOnMeasuredValues.append(hist.GetMeanError())
        expectedValues.append(fitfunc(masses[i],1.0))

    print("Chi2 = %.2f" %(calcChi2(measuredValues,expectedValues,errorsOnMeasuredValues)))

def mJvsJSF():
    measuredValues = []
    expectedValues = []
    errorsOnMeasuredValues = []

    JSFs = [0.97,0.98,0.99,1.00,1.01,1.02,1.03]
    JSFs_string = ["0_97","0_98","0_99","1_00","1_01","1_02","1_03"]

    for i in range(0,len(JSFs)):
        histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_" + JSFs_string[i] + "_ttpp8AF2.root"
        histogram_root_file = r.TFile.Open(histogram_path)
        for h in histogram_root_file.GetListOfKeys():
            h = h.ReadObj()
            if (h.GetName() == "nominal_topjet_m"):
                hist = histogram_root_file.Get(h.GetName())

        measuredValues.append(hist.GetMean())
        errorsOnMeasuredValues.append(hist.GetMeanError())
        expectedValues.append(fitfunc(172.5,JSFs[i]))

    print("Chi2 = %.2f" %(calcChi2(measuredValues,expectedValues,errorsOnMeasuredValues)))

def main():
  
  parser = OptionParser()
  
  parser.add_option("-o", "--option", dest="option", type = "string", default = None,
                    help="Decide whether to run with --mtop (mJvsmtop) or --JSF (mJvsJSF)")
  
  (options, args) = parser.parse_args()        
  
  print("\nWelcome!")
  choice(options.option)
  print("Goodbye!\n")

if __name__ == "__main__":
  main()
