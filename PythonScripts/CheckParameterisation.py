import ROOT as r

r.TH1.SetDefaultSumw2(True)


A = 168.844494
B = 0.471483
C = 78.18811

JSFs = [1.00,1.00,1.00,1.00,1.00,1.00,1.00,0.97,0.98,0.99,1.01,1.02,1.03]
JSFvalues_string = ["0_97","0_98","0_99","1_01","1_02","1_03"]
masses = [169,171,172,172.5,173,174,176,172.5,172.5,172.5,172.5,172.5,172.5]

mJ_values = []
mJ_errors = []

# Go over each top mass varied sample (ttpp8AF2)
for i in range(0,3):

    histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8m" + str(masses[i]) + "/mJ/JSF_1_00_ttpp8m" + str(masses[i]) + ".root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)

histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root"
histogram_root_file = r.TFile.Open(histogram_path)
for h in histogram_root_file.GetListOfKeys():
    h = h.ReadObj()
    if (h.GetName() == "nominal_topjet_m"):
        hist = histogram_root_file.Get(h.GetName())

hist_mean = hist.GetMean()
hist_integral = hist.Integral()
hist_mean_error = hist.GetMeanError()
mJ_values.append(hist_mean)
mJ_errors.append(hist_mean_error)

for i in range(4,7): 

    histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8m" + str(masses[i]) + "/mJ/JSF_1_00_ttpp8m" + str(masses[i]) + ".root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()
    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)

# Go over each JSF varied sample (ttpp8AF2)
for i in range(0,len(JSFvalues_string)):

    histogram_path = "../output_root_files/2022-10-20-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_" + JSFvalues_string[i] + "_ttpp8AF2.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_m"):
            hist = histogram_root_file.Get(h.GetName())

    hist_mean = hist.GetMean()
    hist_integral = hist.Integral()
    hist_mean_error = hist.GetMeanError()

    mJ_values.append(hist_mean)
    mJ_errors.append(hist_mean_error)

mJ_values_estimate = []

for i in range(0,len(JSFs)):
    mJ_values_estimate.append(A + B*(masses[i] - 172.5) + C*(JSFs[i] - 1.00))

print("\nmJ_values:")
print(mJ_values)
print("\nmJ_values_estimate:")
print(mJ_values_estimate)
print("")

differences = []
for i in range(0,len(mJ_values)):
    differences.append(mJ_values_estimate[i] - mJ_values[i])

print("Top mass\tJSF\tActual mJ\tPredicted mJ from fit\tDifference")
for i in range(0,len(mJ_values)):
    print('%.4f\t%.4f\t%.4f\t%.4f\t%.4f' %(masses[i],JSFs[i],mJ_values[i],mJ_values_estimate[i],differences[i]))
print("")

print("")
print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Top mass","JSF","Actual $\overline{\\textup{m}_\\textup{top-jet}}$ (GeV)","Predicted $\overline{\\textup{m}_\\textup{top-jet}}$ (GeV)","Difference"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(JSFs)):
    linestring = ""
    lines = [masses[i],JSFs[i],mJ_values[i],mJ_values_estimate[i],differences[i]]
    for j in range(0,len(lines)):
        if j == 0:
            linestring += (str(round(lines[j],3)) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],3)) + "\\")
        else:
            linestring += (str(round(lines[j],3)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")