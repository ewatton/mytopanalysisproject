# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np
from array import array
import math

def RooFit_Minimisation(name,meanTopJetMassValue,meanTopJetMassError,hist):

    print("\n-----------------------------------------------------------------------------------------\n")
    print("\nFile = ttallpp8, JSF = 1.00, systematics\n")
    print("\n-----------------------------------------------------------------------------------------\n")

    # Set up model for mean top jet mass, mJ
    # ---------------------------------------------------------------------------------------------------------------

    # This is one of our observables, mean(top jet mass)
    meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)

    # Nominal mean and error (for when JSF = 1 and mtop = 172.5 as inputs to creating ntuple)
    nominalMean = meanTopJetMassValue
    errorOnMean = meanTopJetMassError

    # JSF
    JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)

    # Top quark mass
    mtop = ROOT.RooRealVar("mtop","mtop",172.5,150,200)

    # Fixed parameter in the equation for mean(top jet mass)
    amJ = ROOT.RooRealVar("amJ","amJ",168.832795)
    amJ.setConstant(True)

    # Fixed parameter relating JSF to mean(top jet mass)
    cJSF = ROOT.RooRealVar("cJSF","cJSF",78.189705)
    cJSF.setConstant(True)

    # Fixed parameter relating mtop to mean(top jet mass)
    bmtop = ROOT.RooRealVar("bmtop","bmtop",0.476824)
    bmtop.setConstant(True)

    # code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + C*(JSF-1) + B*(mtop - 172.5)
    meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2 - 1.0) + @3*(@4-172.5)",ROOT.RooArgList(amJ,cJSF,JSF,bmtop,mtop))

    # Assume are measurements of the mean top-jet mass (we will have only 1) are distributed according to a gaussian with mean related to JSF.
    # The sigma of the gaussian is fixed to the error on the mean
    pdf_topJetMass = ROOT.RooGaussian("pdf_topJetMass","pdf_topJetMass",meanTopJetMass, meanFormula, ROOT.RooRealConstant.value(errorOnMean))

    # Create data for mJ
    # ---------------------------------------------------------------------------------------------------------------

    # Create data - just one measurement of mean(top jet mass)
    toyData = ROOT.RooDataSet("d","d",ROOT.RooArgSet(meanTopJetMass))
    meanTopJetMass.setVal(nominalMean)
    toyData.add(ROOT.RooArgSet(meanTopJetMass))
    toyData.get(0).Print("V")

    # Set up model for reconstructed W boson mass, mW
    # ---------------------------------------------------------------------------------------------------------------

    useDataLikeErrors = True # Choose whether to use data-like errors (True) or not (False)

    # Declare observable mW
    mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)

    # Delcare constants in relation mean1 = A_2 + B_2*JSF
    A_2 = ROOT.RooRealVar("A_2","A_2",11.6346)
    B_2 = ROOT.RooRealVar("B_2","B_2",71.6033)

    # Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and their parameters
    mean1 = ROOT.RooFormulaVar("mean1","mean of first gaussian","@0 + @1*@2", ROOT.RooArgList(A_2,B_2,JSF)) # Actual 83.8226
    mean2 = ROOT.RooRealVar("mean2", "mean of second gaussian", 80.7167120345, 70,90)
    sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.7785340145,0,50)
    sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.6507628753,0,50)
    
    gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
    gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
    
    # Sum the signal components into a composite p.d.f.
    gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0, 1.0)
    combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))

    if useDataLikeErrors == True: # If using data-like errors
        for bin_number in range(hist.FindBin(55)-1,hist.FindBin(110)+1):
            hist.SetBinError(bin_number,np.sqrt(hist.GetBinContent(bin_number)))

    mWDataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist, 1.0)

    # Perform negative log likelihood on both PDFs 
    # ---------------------------------------------------------------------------------------------------------------

    mW_NLL = combinedGauss.createNLL(mWDataHist)
    mJ_NLL = pdf_topJetMass.createNLL(toyData)
    Added_NLL = ROOT.RooAddition("Added_NLL","Combined NLLs", ROOT.RooArgList(mW_NLL,mJ_NLL))
    Minimise_NLL = ROOT.RooMinimizer(Added_NLL)
    Minimise_NLL.minimize("Minuit")
    fitResult = Minimise_NLL.save()
    fitResult.Print("v")
    print("\nInitial value of floating parameters")
    fitResult.floatParsInit().Print("s")
    print("\nFinal value of floating parameters")
    fitResult.floatParsFinal().Print("s")
    print("\nValue of constant parameters")
    fitResult.constPars().Print("s")

    outputfile = '../OutputTextFiles/MinimisationResults.txt'
    lines = [meanTopJetMassValue,meanTopJetMassError,JSF.getVal(),JSF.getError(),gauss1frac.getVal(),gauss1frac.getError(),sigma1.getVal(),sigma1.getError(),mean2.getVal(),mean2.getError(),sigma2.getVal(),sigma2.getError(),mtop.getVal(),mtop.getError()]
    f = open(outputfile, 'a')
    f.write(name)
    f.write('\t')
    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')
    f.close()

    # Save the fit result
    # ---------------------------------------------------------------------------------------------------------------

    """  mWframe = mW.frame()
    mWDataHist.plotOn(mWframe,ROOT.RooFit.MarkerSize(0.05))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("combinedGauss"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(1))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss1"),ROOT.RooFit.LineColor(4),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(2),ROOT.RooFit.Components("gauss1"))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss2"),ROOT.RooFit.LineColor(8),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(3),ROOT.RooFit.Components("gauss2"))
    mWframe.GetYaxis().SetTitle("Entries")
    mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
    canvas = ROOT.TCanvas("canvas")
    mWframe.Draw()

    legend = ROOT.TLegend(0.12,0.72,0.4,0.89)
    legend.AddEntry("gauss1", "Gaussian 1", "lp")
    legend.AddEntry("gauss2", "Gaussian 2", "lp")
    legend.AddEntry("combinedGauss", "Final fit", "lp")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    canvas.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/mW_twoGaussFit_SimultaneousFittingJSFmtop_ttallpp8_" + name + "_JERsystematic.pdf") """

    return JSF.getVal(),JSF.getError(),mtop.getVal(),mtop.getError()

# -------------------------------------------------------------------------------------------

# Main body of code

# -------------------------------------------------------------------------------------------


# -------------------------------------------------------------------------------------------
# Non-JER systematics
# -------------------------------------------------------------------------------------------


# Get a sorted list of mJ histogram names
# -------------------------------------------------------------------------------------------

# Get nominal first
names_mJ_nominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names_mJ_nominal.append(h.GetName())
histfile.Close()

names_mJ_noNominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
        names_mJ_noNominal.append(h.GetName())
histfile.Close()

names_mJ_noNominal.sort()

names_mJ_JER = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
        names_mJ_JER.append(h.GetName().rstrip("_mJ_combined") + "_topjet_m")
histfile.Close()

names_mJ_JER.sort()


names_mJ = names_mJ_nominal + names_mJ_noNominal + names_mJ_JER 

print('')
#print(names_mJ)
print('')

# Get a sorted list of mW histogram names
# -------------------------------------------------------------------------------------------

# Get nominal first
names_mW_nominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names_mW_nominal.append(h.GetName())
histfile.Close()

names_mW_noNominal = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
        names_mW_noNominal.append(h.GetName())
histfile.Close()

names_mW_noNominal.sort()

names_mW_JER = []
histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
        names_mW_JER.append(h.GetName().rstrip("_mW_combined") + "_topjet_W_invMass")
histfile.Close()

names_mW_JER.sort()


names_mW = names_mW_nominal + names_mW_noNominal + names_mW_JER

print('')
#print(names_mW)
print('')


# Get a list of mJ values and mJ errors in the SAME order as the sorted mJ names list
# -------------------------------------------------------------------------------------------

names = []
mJ_values = []
mJ_errors = []

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        names.append(h.GetName().rstrip("_topjet_m"))
        hist = h
        mJ_values.append(hist.GetMean())
        mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mJ)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and (h.GetName() == names_mJ[i]) and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
            names.append(h.GetName().rstrip("_topjet_m"))
            hist = h
            mJ_values.append(hist.GetMean())
            mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mJ)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and ((h.GetName().rstrip("_mJ_combined") + "_topjet_m") == names_mJ[i]) and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
            names.append(h.GetName().rstrip("_mJ_combined"))
            hist = h
            mJ_values.append(hist.GetMean())
            mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

print('')
#print(names)
print('')


# Get a list of mW histograms in the SAME order as the sorted mW names list
# -------------------------------------------------------------------------------------------

histograms = []

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("nominal" in h.GetName()):
        hist = h
        hist.SetDirectory(0)
        histograms.append(hist)
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mW)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and (h.GetName() == names_mW[i]) and ("nominal" not in h.GetName()) and ("JER" not in h.GetName()):
            hist = h
            hist.SetDirectory(0)
            histograms.append(hist)
histfile.Close()

histogram_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for i in range(0,len(names_mW)):
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TH1D') and ((h.GetName().rstrip("_mW_combined") + "_topjet_W_invMass") == names_mW[i]) and ("nominal" not in h.GetName()) and ("JER" in h.GetName()):
            hist = h
            hist.SetDirectory(0)
            histograms.append(hist)
histfile.Close()

JSF_values = []
JSF_errors = []
mtop_values = []
mtop_errors = []
diffFromNominal = []

# Get the results and fill lists so that the ordering of each list is the same as the initial names_mJ and names_mW ordered lists.
# ----------------------------------------------------------------------------------------------------------------------------------

for i in range(0,len(names)):
    print("")
    print(len(names))
    print(len(mJ_values))
    print(len(mJ_errors))
    print(len(histograms))
    print("")
    JSF, JSF_error, mtop, mtop_error = RooFit_Minimisation(names[i],mJ_values[i],mJ_errors[i],histograms[i])
    JSF_values.append(JSF)
    JSF_errors.append(JSF_error)
    mtop_values.append(mtop)
    mtop_errors.append(mtop_error)
    diffFromNominal.append(mtop-172.043683)

# Print table of results for all systematics.
# ----------------------------------------------------------------------------------------------------------------------------------

print("\n\n\nBelow is LaTeX code for table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Systematics","$\overline{\\textup{m}_\\textup{top-jet}}$ (GeV)","$\overline{\\textup{m}_\\textup{top-jet}}$ error (GeV)","JSF", "JSF error", "m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mJ_values[i],mJ_errors[i],JSF_values[i],JSF_errors[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\n\n\nBelow is LaTeX code for simplified table of results for all systematics:\n\n\n")

print("\\begin{table}[!htb]")
print("\\small")
print("\\centering")
print("\\begin{tabular}{lccccccc}\\toprule")

headings = ["Systematics", "m$_\\textup{top}$ (GeV)", "m$_\\textup{top}$ error", "Difference from nominal"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

for i in range(0,len(names)):
    linestring = ""
    lines = [names[i],mtop_values[i],mtop_errors[i],diffFromNominal[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , "\\_")
            linestring += (str(lines[j]) + " & ")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")


# ------------------------------------------------------------------------------------------------------
# Find positions of the different systematics
# ------------------------------------------------------------------------------------------------------


# Find the samples which do not have an up or down template, need to handle their results differently
# ------------------------------------------------------------------------------------------------------

positionsOfSystematicsWithNoUpOrDownTemplate = []
for i in range(0,len(names)):
    if ("__1u" not in names[i]) and ("__1dow" not in names[i]) and (i != 0):
        positionsOfSystematicsWithNoUpOrDownTemplate.append(i)

print('')
print("Positions where there are no up or down templates:")
print(positionsOfSystematicsWithNoUpOrDownTemplate)
print('')
print("Names of systematics samples which have no up or down templates:")
names_noUpOrDown = []
for i in range(0,len(positionsOfSystematicsWithNoUpOrDownTemplate)):
    names_noUpOrDown.append(names[positionsOfSystematicsWithNoUpOrDownTemplate[i]])
    print(names[positionsOfSystematicsWithNoUpOrDownTemplate[i]])
print('')

# Find positions of the JES systematics
# ------------------------------------------------------------------------------------------------------

positionsOfJESSystematics = []
for i in range(0,len(names)):
    if ("JET" in names[i]) and ("JER" not in names[i]):
        positionsOfJESSystematics.append(i)

print('')
print("Positions where the JES systematics are:")
print(positionsOfJESSystematics)
print('')
print("Names of JES systematics:")
names_JES = []
for i in range(0,len(positionsOfJESSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfJESSystematics[i]].partition("__")
        print(before)
        names_JES.append(before)
print('')

# Find positions of the lepton systematics
# ------------------------------------------------------------------------------------------------------

positionsOfLeptonSystematics = []
for i in range(0,len(names)):
    if ("EG" in names[i]) or ("MUON" in names[i]):
        positionsOfLeptonSystematics.append(i)

print('')
print("Positions where the lepton systematics are:")
print(positionsOfLeptonSystematics)
print('')
print("Names of lepton systematics:")
names_leptons = []
for i in range(0,len(positionsOfLeptonSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfLeptonSystematics[i]].partition("__")
        print(before)
        names_leptons.append(before)
print('')

# Find positions of the MET systematics
# ------------------------------------------------------------------------------------------------------

positionsOfMETSystematics = []
for i in range(0,len(names)):
    if ("MET" in names[i]) and (("_1up" in names[i]) or ("_1down" in names[i])):
        positionsOfMETSystematics.append(i)

print('')
print("Positions where the MET systematics are:")
print(positionsOfMETSystematics)
print('')
print("Names of MET systematics:")
names_MET = []
for i in range(0,len(positionsOfMETSystematics)):
    if (names[positionsOfMETSystematics[i]] in names_noUpOrDown):
        continue
    elif i % 2 == 0:
        before, sep, after = names[positionsOfMETSystematics[i]].partition("__")
        print(before)
        names_MET.append(before)
print('')

# Find positions of the JER systematics
# ------------------------------------------------------------------------------------------------------

positionsOfJERSystematics = []
for i in range(0,len(names)):
    if ("JER" in names[i]):
        positionsOfJERSystematics.append(i)

print('')
print("Positions where the JER systematics are:")
print(positionsOfJERSystematics)
print('')
print("Names of JER systematics:")
names_JER = []
for i in range(0,len(positionsOfJERSystematics)):
    if i % 2 == 0:
        before, sep, after = names[positionsOfJERSystematics[i]].partition("__")
        print(before)
        names_JER.append(before)
print('')

# ------------------------------------------------------------------------------------------------------
# Work out systematic uncertiainties on mtop
# ------------------------------------------------------------------------------------------------------

# JES
# ------------------------------------------------------------------------------------------------------

JES_uncertainties = []
for i in range(0,len(positionsOfJESSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfJESSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfJESSystematics[(2*i)+1]] - mtop_values[0])
    JES_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    JES_uncertainties.append(JES_uncertainty)

JES_uncertainties_squared = []
for i in range(0,len(JES_uncertainties)):
    JES_uncertainties_squared.append(JES_uncertainties[i]*JES_uncertainties[i])

sumOfJESUncertaintiesSquared = 0
for i in range(0,len(JES_uncertainties_squared)):
    sumOfJESUncertaintiesSquared += JES_uncertainties_squared[i]

Total_JES_uncetainty = np.sqrt(sumOfJESUncertaintiesSquared)

JES_tuple = zip(JES_uncertainties, names_JES)
topthree_JES_tuple = sorted(JES_tuple, reverse=True)[:3]

# MET
# ------------------------------------------------------------------------------------------------------

MET_uncertainties = []
for i in range(0,len(positionsOfMETSystematics)):
    downDiffFromNominal = abs(mtop_values[positionsOfMETSystematics[i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfMETSystematics[i]+1] - mtop_values[0])
    MET_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    MET_uncertainties.append(MET_uncertainty)

for i in range(0,len(positionsOfSystematicsWithNoUpOrDownTemplate)):
    MET_uncertainty = abs(mtop_values[positionsOfSystematicsWithNoUpOrDownTemplate[i]] - mtop_values[0])
    MET_uncertainties.append(MET_uncertainty)

MET_uncertainties_squared = []
for i in range(0,len(MET_uncertainties)):
    MET_uncertainties_squared.append(MET_uncertainties[i]*MET_uncertainties[i])

sumOfMETUncertaintiesSquared = 0
for i in range(0,len(MET_uncertainties_squared)):
    sumOfMETUncertaintiesSquared += MET_uncertainties_squared[i]

Total_MET_uncetainty = np.sqrt(sumOfMETUncertaintiesSquared)

# Leptons
# ------------------------------------------------------------------------------------------------------

LEP_uncertainties = []
for i in range(0,len(positionsOfLeptonSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfLeptonSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfLeptonSystematics[(2*i)+1]] - mtop_values[0])
    LEP_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    LEP_uncertainties.append(LEP_uncertainty)

LEP_uncertainties_squared = []
for i in range(0,len(LEP_uncertainties)):
    LEP_uncertainties_squared.append(LEP_uncertainties[i]*LEP_uncertainties[i])

sumOfLEPUncertaintiesSquared = 0
for i in range(0,len(LEP_uncertainties_squared)):
    sumOfLEPUncertaintiesSquared += LEP_uncertainties_squared[i]

Total_LEP_uncetainty = np.sqrt(sumOfLEPUncertaintiesSquared)

# JER
# ------------------------------------------------------------------------------------------------------

JER_uncertainties = []
for i in range(0,len(positionsOfJERSystematics)/2):
    downDiffFromNominal = abs(mtop_values[positionsOfJERSystematics[2*i]] - mtop_values[0])
    upDiffFromNominal = abs(mtop_values[positionsOfJERSystematics[(2*i)+1]] - mtop_values[0])
    JER_uncertainty = (upDiffFromNominal + downDiffFromNominal)/2
    JER_uncertainties.append(JER_uncertainty)

JER_uncertainties_squared = []
for i in range(0,len(JER_uncertainties)):
    JER_uncertainties_squared.append(JER_uncertainties[i]*JER_uncertainties[i])

sumOfJERUncertaintiesSquared = 0
for i in range(0,len(JER_uncertainties_squared)):
    sumOfJERUncertaintiesSquared += JER_uncertainties_squared[i]

Total_JER_uncetainty = np.sqrt(sumOfJERUncertaintiesSquared)

# Now make final LaTeX summary table for systematics
# ------------------------------------------------------------------------------------------------------

print("\\begin{table}[!htb]")
print("\\centering")
print("\\begin{tabular}{lc}\\toprule")

headings = ["Systematics", "Uncertainty on m$_\\textup{top}$ (GeV)"]
outputheadingstring = ""
for i in range(0,len(headings)):
    if i == len(headings)-1:
        outputheadingstring += str(headings[i] + "\\\\")
    else:
        outputheadingstring += (str(headings[i]) + " & ")
print(outputheadingstring)
print("\\midrule")

Total_Systematic_Uncertainty = np.sqrt((Total_JES_uncetainty*Total_JES_uncetainty)+(Total_JER_uncetainty*Total_JER_uncetainty)+(Total_MET_uncetainty*Total_MET_uncetainty)+(Total_LEP_uncetainty*Total_LEP_uncetainty))
Total_uncertainty_names = ["JES",topthree_JES_tuple[0][1],topthree_JES_tuple[1][1],topthree_JES_tuple[2][1],"JER","MET","Leptons","Total"]
Total_uncertainties = [Total_JES_uncetainty,topthree_JES_tuple[0][0],topthree_JES_tuple[1][0],topthree_JES_tuple[2][0],Total_JER_uncetainty,Total_MET_uncetainty,Total_LEP_uncetainty,Total_Systematic_Uncertainty]

for i in range(0,len(Total_uncertainty_names)):
    linestring = ""
    lines = [Total_uncertainty_names[i],Total_uncertainties[i]]
    for j in range(0,len(lines)):
        if j == 0:
            lines[j] = lines[j].replace("_" , " ")
            linestring += (str(lines[j]) + " & ")
            if i == len(Total_uncertainty_names)-1:
                print("\\hline")
        elif j == len(lines)-1:
            linestring += (str(round(lines[j],4)) + "\\")
        else:
            linestring += (str(round(lines[j],4)) + " & ")
    print(linestring + "\\")

print("\\bottomrule")
print("\\end{tabular}")
print("\\caption{}")
print("\\label{}")
print("\\end{table}")

print("\nScript has finished\n")

