from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand,dR_type,dR_limit):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)
        if (isRecoLevel == True):
            weight = tree_name.weight_normalise
        else:
            weight = tree_name.mcEventNormWeight

        variable = getattr(tree_name, variable_name)

        if dR_type != None:

            if dR_type == "leptop_addjet":
                dR_difference = getattr(tree_name, "tmm_leptop_addjet_dR")
            elif dR_type == "leptop_bjet_addjet":
                dR_difference = getattr(tree_name, "tmm_leptop_bjet_addjet_dR")

            if dR_difference <= dR_limit:

                # fill histogram with entires with weight
                if (isNeedingDivisionByOneThousand == True):
                    if (variableType == "vector"):
                        for j in range(0,len(variable)):
                            hist_name.Fill( variable[j] / 1000.0, weight )
                    else:
                        hist_name.Fill(variable / 1000.0, weight)
                else:
                    if (variableType == "vector"):
                        for j in range(0,len(variable)):
                            hist_name.Fill( variable[j], weight )
                    else:
                        hist_name.Fill(variable, weight)

            else:
                continue

        else:
            # fill histogram with entires with weight
            if (isNeedingDivisionByOneThousand == True):
                if (variableType == "vector"):
                    for j in range(0,len(variable)):
                        hist_name.Fill( variable[j] / 1000.0, weight )
                else:
                    hist_name.Fill(variable / 1000.0, weight)
            else:
                if (variableType == "vector"):
                    for j in range(0,len(variable)):
                        hist_name.Fill( variable[j], weight )
                else:
                    hist_name.Fill(variable, weight)
    
    print("Finished filling histogram for variable: " + variable_name)


def MakeHisto_auto(filePathSegment,sample,JSF,dR_limit_string,dR_limit):

    # input_file_path = "~/ATLAS/GlasgowAna-2023-03-06/glasgow-ana/run/output/ttbarBoosted/ttallpp8.root"
    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    output_file_path = "../output_root_files/2023-06-21-InvestigatingRecoil/" + sample + "/leptop_addjet_mass_dR" + dR_limit_string + "_40Bins.root"
    
    infile = TFile.Open(input_file_path,"READ");

    # Get trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree') and ('nominal' in h.GetName()):
            tree = h
            trees.append(tree)

    print('\n==============================================================================')
    print('==============================================================================')
    print('\nSample = ' + sample + ', JSF = ' + JSF)
    print('\nTTrees from file: %s' %(input_file_path))
    print('\nOutput ROOT file: %s' %(output_file_path))
    print('\nThe list of TTrees read in is:')
    for treename in trees:
        print('')
        print(treename)
    print('\nThe total number of trees read in is: %i' %(len(trees)))

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable_dict = dict()

    # variable_dict["variable name"] = [variable type, if it needs to be converted from MeV to GeV, number of bins, low bin, high bin]

    variable_dict["lepbjet_lep_pT_ratio"] = ["float",False,40,0.0,10.0]
    variable_dict["tmm_leptop_addjet_m"] = ["float",True,40,0.0,600.0]
    variable_dict["tmm_leptop_bjet_addjet_m"] = ["float",True,40,0.0,600.0]


    print('\nThe total number of histograms to create for each tree is: %i' %(len(variable_dict)))
    print('\n==============================================================================')
    print('==============================================================================\n')

    for z in range(0,len(trees)):
        histcount = 1
        for variable in variable_dict:
            name = trees[z].GetName() + "_" + variable
            hist = TH1D(name, variable + "; variable; Events", variable_dict[variable][2], variable_dict[variable][3], variable_dict[variable][4])

            if variable == "lepbjet_lep_pT_ratio":
                FillHisto_auto(trees[z],hist,isRecoLevel,str(variable),variable_dict[variable][0],variable_dict[variable][1],None,dR_limit)
            elif variable == "tmm_leptop_addjet_m":
                FillHisto_auto(trees[z],hist,isRecoLevel,str(variable),variable_dict[variable][0],variable_dict[variable][1],"leptop_addjet",dR_limit)
            elif variable == "tmm_leptop_bjet_addjet_m":
                FillHisto_auto(trees[z],hist,isRecoLevel,"tmm_leptop_addjet_m",variable_dict[variable][0],variable_dict[variable][1],"leptop_bjet_addjet",dR_limit)

            print("Finished filling histogram number " + str(histcount) + "/" + str(len(variable_dict)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
            hist.Write()
            histcount += 1
        print("Finished filling the histograms for tree number " + str(z+1) + "/" + str(len(trees)))

    outfile.Close()


# ==========================================================================
# =========================== Main part of code ============================
# ==========================================================================

print('')
samplesToRunOn = [
    
    "ttpp8AF2",
    "ttpp8RTT",

    ]

dR_limits_string = ["0-8","0-9","1-0","1-1","1-2","1-3","1-4"]
dR_limits = [0.8,0.9,1.0,1.1,1.2,1.3,1.4]

for i in range(0,len(samplesToRunOn)):
    for j in range(0,len(dR_limits_string)):
        JSFtoUse = "1_00"
        if samplesToRunOn[i] == "ttpp8RTT":
            # filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim" # Not extended
            filePathSegment = "2023_06_27_TTDIFFXSMASSv03_AFII_RTT_Leptop_Addjet_m/JSF_1_00_FastSim" # Extended
            MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse,dR_limits_string[j],dR_limits[j])
        elif samplesToRunOn[i] == "ttpp8AF2":
            filePathSegment = "2023_06_27_TTDIFFXSMASSv03_AFII_RTT_Leptop_Addjet_m/JSF_1_00_FastSim"
            MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse,dR_limits_string[j],dR_limits[j])
        else:
            print("\nSomething has gone terribly wrong!\n")
            exit()

print('')
print('Histograms have been saved to the output_root_files folder')
print('')