# Macro to fit a histogram with a generic function
# First created by Elliot Watton - 03/02/2022
# Last editted by Elliot Watton - 28/02/2022

import ROOT as r
from ROOT import TMath
from array import array
import numpy as np
import csv
r.TH1.SetDefaultSumw2(True)

def lorentzianPeak(x,p):
    return p[0] * 1/np.pi * (0.5 * p[1] / ((x[0]-p[2])*(x[0]-p[2]) + 0.25*p[1]*p[1]))

def lognormal(x,p):
    return p[0] * 1/(x[0]*p[1]*np.sqrt(np.pi*2)) * np.exp( ( -1 * (np.log(x[0])-p[2]) * (np.log(x[0])-p[2]) ) / ( 2 * p[1] * p[1] ) )

def breitWigner(x,p):
    bw_A = 2 * np.sqrt(2) * p[0] * p[1] * np.sqrt(p[0] * p[0] * (p[0] * p[0] + p[1] * p[1]))
    bw_B = np.pi * np.sqrt(p[0] * p[0] + np.sqrt(p[0] * p[0] * (p[0] * p[0] + p[1] * p[1])))
    bw_C = (x[0] * x[0] - p[0]* p[0]) * (x[0] * x[0] - p[0] * p[0]) + p[0] * p[0] * p[1] * p[1]
    bw = p[2] * ( bw_A / bw_B ) / bw_C
    return bw

def lorentzianPeak_and_two_gaus(x,p):
    lorentzian = p[0] * 1/np.pi * (0.5 * p[1] / ((x[0]-p[2]) * (x[0]-p[2]) + 0.25 * p[1] * p[1]))
    gaus1 = p[3] * np.exp(-0.5 * ((x[0] - p[4]) / p[5]) * ((x[0] - p[4]) / p[5]) )
    gaus2 = p[6] * np.exp(-0.5 * ((x[0] - p[7]) / p[8]) * ((x[0] - p[7]) / p[8]) )
    return lorentzian + gaus1 + gaus2

def lorentzianPeak_and_one_gaus(x,p):
    lorentzian = p[0] * 1/np.pi * (0.5 * p[1] / ((x[0]-p[2]) * (x[0]-p[2]) + 0.25 * p[1] * p[1]))
    gaus1 = p[3] * np.exp(-0.5 * ((x[0] - p[4]) / p[5]) * ((x[0] - p[4]) / p[5]) )
    return lorentzian + gaus1

def lorentzianPeak_and_one_gaus_and_one_exp(x,p):
    lorentzian = p[0] * 1/np.pi * (0.5 * p[1] / ((x[0]-p[2]) * (x[0]-p[2]) + 0.25 * p[1] * p[1]))
    gaus1 = p[3] * np.exp(-0.5 * ((x[0] - p[2]) / p[4]) * ((x[0] - p[2]) / p[4]) )
    expo1 = p[5] * np.exp(-abs(x[0] - p[2]) / p[6])
    return lorentzian + gaus1 + expo1

def my_expo(x,p):
    return p[0] * np.exp(-abs(x[0] - p[1]) / p[2])


def crystalball1(x, alpha, n, sigma, mean):
    
    if (sigma < 0.):
        return 0.0

    z = (x - mean)/sigma

    if (alpha < 0): 
        z = -z 

    abs_alpha = abs(alpha)

    if (z  > - abs_alpha):
        return np.exp(- 0.5 * z * z)
    else:
        nDivAlpha = n/abs_alpha
        AA =  np.exp(-0.5*abs_alpha*abs_alpha)
        B = nDivAlpha -abs_alpha
        arg = nDivAlpha/(B-z)
        return AA * pow(arg,n)


def crystalball(x, p):
    return (p[0] * crystalball1(x[0], p[3], p[4], p[2], p[1]))

def crystalball_and_lorentzian_and_one_gaus(x, p):
    crys = (p[0] * crystalball1(x[0], p[3], p[4], p[2], p[1]))
    lorentzian = p[5] * 1/np.pi * (0.5 * p[6] / ((x[0]-p[7]) * (x[0]-p[7]) + 0.25 * p[6] * p[6]))
    gaus1 = p[8] * np.exp(-0.5 * ((x[0] - p[9]) / p[10]) * ((x[0] - p[9]) / p[10]) )
    return crys + lorentzian + gaus1

def crystalball_and_two_gaus(x, p):
    crys = (p[0] * crystalball1(x[0], p[3], p[4], p[2], p[1]))
    gaus1 = p[5] * np.exp(-0.5 * ((x[0] - p[6]) / p[7]) * ((x[0] - p[6]) / p[7]) )
    gaus2 = p[8] * np.exp(-0.5 * ((x[0] - p[9]) / p[10]) * ((x[0] - p[9]) / p[10]) )
    return crys + gaus1 + gaus2

def crystalball_and_three_gaus(x, p):
    crys = (p[0] * crystalball1(x[0], p[3], p[4], p[2], p[1]))
    gaus1 = p[5] * np.exp(-0.5 * ((x[0] - p[6]) / p[7]) * ((x[0] - p[6]) / p[7]) )
    gaus2 = p[8] * np.exp(-0.5 * ((x[0] - p[9]) / p[10]) * ((x[0] - p[9]) / p[10]) )
    gaus3 = p[11] * np.exp(-0.5 * ((x[0] - p[12]) / p[13]) * ((x[0] - p[12]) / p[13]) )
    return crys + gaus1 + gaus2 + gaus3

def one_gaus_with_replaced_gauss_mean(x,p):
    # Gaussian 1 mean = A + B(JSF)
    A = 13.4773
    B = 70.3641

    gaus1 = p[0] * np.exp(-0.5 * ((x[0] - (A + B*p[1])) / p[2]) * ((x[0] - (A + B*p[1])) / p[2])) 
    return gaus1

def two_gaus_with_replaced_gauss_mean(x,p):
    # Gaussian 1 mean = A + B(JSF) data-like errors
    #A = 13.4973
    #B = 70.3441

    # Old values before setting r.TH1.SetDefaultSumw2(True)
    #A = 13.6209
    #B = 70.2163

    # New values after setting r.TH1.SetDefaultSumw2(True)
    A = 13.4773
    B = 70.3641

    gaus1 = p[0] * np.exp(-0.5 * ((x[0] - (A + B*p[1])) / p[2]) * ((x[0] - (A + B*p[1])) / p[2]))
    gaus2 = p[3] * np.exp(-0.5 * ((x[0] - p[4]) / p[5]) * ((x[0] - p[4]) / p[5]) )
    return gaus1 + gaus2

#input_histogram_ttpp8m169 = "../output_root_files/mW_distributions_varied_mtop/output_mW_JSF_1_00_mt_169.root"
#input_histogram_ttpp8m172 = "../output_root_files/mW_distributions_varied_mtop/output_mW_JSF_1_00_mt_172.root"
#input_histogram_ttpp8AF2 = "../output_root_files/mW_distributions_varied_mtop/output_mW_JSF_1_00_mt_172_5.root"
#input_histogram_ttpp8m173 = "../output_root_files/mW_distributions_varied_mtop/output_mW_JSF_1_00_mt_173.root"
#input_histogram_ttpp8m176 = "../output_root_files/mW_distributions_varied_mtop/output_mW_JSF_1_00_mt_176.root"
#input_histograms = [input_histogram_ttpp8m169,input_histogram_ttpp8m172,input_histogram_ttpp8AF2,input_histogram_ttpp8m173,input_histogram_ttpp8m176]
#histo_name = ["169","172","172.5","173","176"]
#save_name = ["mt_169","mt_172","mt_AFII","mt_173","mt_176"]

#sample_name = ["ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]
sample_name = ["ttallpp8"]

JSF_results = array('d',[])
JSF_error_results = array('d',[])
P0_results = array('d',[])
P0_error_results = array('d',[])
P2_results = array('d',[])
P2_error_results = array('d',[])
P3_results = array('d',[]) 
P3_error_results = array('d',[])
P4_results = array('d',[])
P4_error_results = array('d',[])
P5_results = array('d',[])
P5_error_results = array('d',[])

for j in range(0,len(sample_name)):
    input_histogram_JSF_0_97 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_0_97_" + sample_name[j] + ".root"
    input_histogram_JSF_0_98 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_0_98_" + sample_name[j] + ".root"
    input_histogram_JSF_0_99 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_0_99_" + sample_name[j] + ".root"
    input_histogram_JSF_1_00 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_1_00_" + sample_name[j] + ".root"
    input_histogram_JSF_1_01 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_1_01_" + sample_name[j] + ".root"
    input_histogram_JSF_1_02 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_1_02_" + sample_name[j] + ".root"
    input_histogram_JSF_1_03 = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + sample_name[j] + "/mW/JSF_1_03_" + sample_name[j] + ".root"

    input_histograms = [input_histogram_JSF_0_97,input_histogram_JSF_0_98,input_histogram_JSF_0_99,input_histogram_JSF_1_00,input_histogram_JSF_1_01,input_histogram_JSF_1_02,input_histogram_JSF_1_03]
    histo_name = ["0.97","0.98","0.99","1.00","1.01","1.02","1.03"]
    
    #save_name = ["mW_twoGaussFit_JSF_0_97_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_0_98_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_0_99_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_1_00_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_1_01_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_1_02_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF","mW_twoGaussFit_JSF_1_03_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF"]
    save_name = ["mW_twoGaussFit_JSF_0_97_" + sample_name[j], "mW_twoGaussFit_JSF_0_98_" + sample_name[j], "mW_twoGaussFit_JSF_0_99_" + sample_name[j], "mW_twoGaussFit_JSF_1_00_" + sample_name[j], "mW_twoGaussFit_JSF_1_01_" + sample_name[j], "mW_twoGaussFit_JSF_1_02_" + sample_name[j], "mW_twoGaussFit_JSF_1_03_" + sample_name[j]]
    
    #save_name = ["JSF_0_97_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_0_98_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_0_99_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_1_00_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_1_01_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_1_02_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors","JSF_1_03_mt_172_5_" + sample_name[j] + "_Gauss_1_mean_swapped_with_JSF_datalike_errors"]
    #save_name = ["JSF_0_97_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_0_98_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_0_99_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_1_00_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_1_01_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_1_02_mt_172_5_" + sample_name[j] + "_datalike_errors", "JSF_1_03_mt_172_5_" + sample_name[j] + "_datalike_errors"]

    for i in range(0,len(input_histograms)):
        #if histo_name[i] == "1.00":
        # Retreive histogram to be fited
        histogram_file_path = input_histograms[i]
        histfile = r.TFile.Open(histogram_file_path)
        hist1 = histfile.Get("nominal_topjet_W_invMass")
        hist1.SetDirectory(0)
        histfile.Close()

        numberOfBinsWithNegativeEntries = 0
        binsWithNegativeEntries = array('i',[])
        positionsOfBinsWithNegativeEntries = array('f',[])
        contentOfBinsWithNegativeEntries = array('f',[])

        hist = hist1.Clone()
        """ for bin_number in range(0,201):
            if hist.GetBinContent(bin_number) < 0:
                numberOfBinsWithNegativeEntries += 1
                binsWithNegativeEntries.append(bin_number)
                positionsOfBinsWithNegativeEntries.append(hist.GetBinCenter(bin_number))
                contentOfBinsWithNegativeEntries.append(hist.GetBinContent(bin_number))
            hist.SetBinError(bin_number,np.sqrt(hist.GetBinContent(bin_number))) """

        print("\n\nFor samples: %s, with JSF = %s\n\n" %(sample_name[j],histo_name[i]))
        print("Number of bins with negative entries:")
        print("%i\n\n" %(numberOfBinsWithNegativeEntries))
        print("Array of bins with negative entries:")
        print(binsWithNegativeEntries)
        print("\n\nPosition of bin centres with negative entries:")
        print(positionsOfBinsWithNegativeEntries)
        print("\n\nContent of bin centres with negative entries:")
        print(contentOfBinsWithNegativeEntries)
        print("\n\n")

        # Setup the canvas
        canvas = r.TCanvas("canvas_scatter")
        pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
        pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
        pad1.SetBottomMargin(0.00001)
        pad1.SetBorderMode(0)
        pad2.SetTopMargin(0.00001)
        pad2.SetBottomMargin(0.2)
        pad2.SetBorderMode(0)
        #pad1.SetGrid()
        #pad2.SetGrid()
        pad1.Draw()
        pad2.Draw()
        pad1.cd()
        r.gStyle.SetOptStat(0)

        # Draw and configure histogram
        hist.Draw()
        hist.SetTitle("")
        hist.GetYaxis().SetTitleOffset(0.8)
        #hist.GetYaxis().SetLabelFont(62)
        hist.GetYaxis().SetTitleSize(0.05)
        hist.GetYaxis().SetLabelSize(0.05)

        #####################################################################
        ######################### Crystal ball ##############################
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit = r.TF1("fit",crystalball,fitBeginning,fitEnding,10)
        fit.SetParameter(0,1000)
        fit.SetParameter(1,80)
        fit.SetParameter(2,20)
        fit.SetParameter(3,-1)
        fit.SetParameter(4,2)
        hist.Fit(fit,"R") """

        #####################################################################
        ############## Crystal ball, lorentzian peak, gaussian ##############
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit1 = r.TF1("fit1",crystalball,100,180,5)
        fit2 = r.TF1("fit2",lorentzianPeak,60,100,3)
        fit3 = r.TF1("fit3","gaus",20,60)

        fit1.SetParameter(0,100)
        fit1.SetParameter(1,80)
        fit1.SetParameter(2,10)
        fit1.SetParameter(3,-1)
        fit1.SetParameter(4,2)

        fit2.SetParameter(0,1000)
        fit2.SetParameter(1,20)
        fit2.SetParameter(2,80)

        fit1.SetLineColor(r.kBlack)
        fit2.SetLineColor(r.kCyan-3)
        fit3.SetLineColor(r.kGreen-3)

        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        hist.Fit(fit3,"R+")

        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        par3 = fit3.GetParameters()
        fit = r.TF1("fit",crystalball_and_lorentzian_and_one_gaus,fitBeginning,fitEnding,11)
        par = array('d',11*[0.])
        par[0], par[1], par[2], par[3], par[4] = par1[0], par1[1], par1[2], par1[3], par1[4]
        par[5], par[6], par[7] = par2[0], par2[1], par2[2]
        par[8], par[9], par[10] = par3[0], par3[1], par3[2]
        fit.SetParameters(par)
        hist.Fit(fit,"R")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1",crystalball,20,180,5)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.FixParameter(3,PAR[3])
        fitt1.FixParameter(4,PAR[4])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2",lorentzianPeak,20,180,3)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[5])
        fitt2.FixParameter(1,PAR[6])
        fitt2.FixParameter(2,PAR[7])
        fitt2.Draw("same")
        fitt3 = r.TF1("fitt3","gaus",20,180)
        fitt3.SetLineColor(r.kOrange)
        fitt3.FixParameter(0,PAR[8])
        fitt3.FixParameter(1,PAR[9])
        fitt3.FixParameter(2,PAR[10])
        fitt3.Draw("same") """

        #####################################################################
        ######################## Two Gaussians ##############################
        #####################################################################

        hist.GetXaxis().SetRangeUser(55,110)

        fitBeginning = 55
        fitEnding = 110
        fit1 = r.TF1("fit1","gaus",70,90)
        fit2 = r.TF1("fit2","gaus",55,110)
        fit1.SetLineColor(r.kBlue-4)
        fit2.SetLineColor(r.kGreen-3)
        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        fit = r.TF1("fit","gaus(0)+gaus(3)",fitBeginning,fitEnding,6)
        par = array('d',6*[0.])
        par[0], par[1], par[2] = par1[0], par1[1], par1[2]
        par[3], par[4], par[5] = par2[0], par2[1], par2[2]
        fit.SetParameters(par)
        FitResultPtr = hist.Fit(fit,"RS")
        FitResultPtr.Print("V")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1","gaus",55,110)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",55,110)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[3])
        fitt2.FixParameter(1,PAR[4])
        fitt2.FixParameter(2,PAR[5])
        fitt2.Draw("same")

        #####################################################################
        ################# Two Gaussians with JSF included ###################
        #####################################################################

        """ hist.GetXaxis().SetRangeUser(55,110)

        fitBeginning = 55
        fitEnding = 110
        fit1 = r.TF1("fit1",one_gaus_with_replaced_gauss_mean,70,90,3)
        fit2 = r.TF1("fit2","gaus",55,110)
        fit1.SetLineColor(r.kBlue-4)
        fit2.SetLineColor(r.kGreen-3)

        par_gauss = array('d',3*[0.])
        par_gauss[0], par_gauss[1], par_gauss[2] = 1000, 1.00, 7
        fit1.SetParameters(par_gauss)

        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        fit = r.TF1("fit",two_gaus_with_replaced_gauss_mean,fitBeginning,fitEnding,6)
        par = array('d',6*[0.])
        par[0], par[1], par[2] = par1[0], par1[1], par1[2]
        par[3], par[4], par[5] = par2[0], par2[1], par2[2]
        fit.SetParameters(par)
        fit.SetParLimits(2,0,100)
        fit.SetParLimits(5,0,100)
        FitResultPtr = hist.Fit(fit,"RS")
        FitResultPtr.Print("V")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1",one_gaus_with_replaced_gauss_mean,55,110,3)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",55,110)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[3])
        fitt2.FixParameter(1,PAR[4])
        fitt2.FixParameter(2,PAR[5])
        fitt2.Draw("same") """

        #####################################################################
        ################## Crystal ball and two Gaussians ###################
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit1 = r.TF1("fit1",crystalball,100,180,5)
        fit2 = r.TF1("fit2","gaus",60,100)
        fit3 = r.TF1("fit3","gaus",20,60)

        fit1.SetParameter(0,1000)
        fit1.SetParameter(1,40)
        fit1.SetParameter(2,10)
        fit1.SetParameter(3,-1)
        fit1.SetParameter(4,2)

        fit1.SetLineColor(r.kBlack)
        fit2.SetLineColor(r.kCyan-3)
        fit3.SetLineColor(r.kGreen-3)

        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        hist.Fit(fit3,"R+")

        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        par3 = fit3.GetParameters()
        fit = r.TF1("fit",crystalball_and_two_gaus,fitBeginning,fitEnding,11)
        par = array('d',11*[0.])
        par[0], par[1], par[2], par[3], par[4] = par1[0], par1[1], par1[2], par1[3], par1[4]
        par[5], par[6], par[7] = par2[0], par2[1], par2[2]
        par[8], par[9], par[10] = par3[0], par3[1], par3[2]
        fit.SetParameters(par)
        hist.Fit(fit,"R")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1",crystalball,fitBeginning,fitEnding,5)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.FixParameter(3,PAR[3])
        fitt1.FixParameter(4,PAR[4])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",fitBeginning,fitEnding)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[5])
        fitt2.FixParameter(1,PAR[6])
        fitt2.FixParameter(2,PAR[7])
        fitt2.Draw("same")
        fitt3 = r.TF1("fitt3","gaus",fitBeginning,fitEnding)
        fitt3.SetLineColor(r.kOrange)
        fitt3.FixParameter(0,PAR[8])
        fitt3.FixParameter(1,PAR[9])
        fitt3.FixParameter(2,PAR[10])
        fitt3.Draw("same") """

        #####################################################################
        ####################### Three Gaussians #############################
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit1 = r.TF1("fit1","gaus",60,90)
        fit2 = r.TF1("fit2","gaus",20,60)
        fit3 = r.TF1("fit3","gaus",20,180)
        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        hist.Fit(fit3,"R+")
        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        par3 = fit3.GetParameters()
        fit = r.TF1("fit","gaus(0)+gaus(3)+gaus(6)",fitBeginning,fitEnding,9)
        par = array('d',9*[0.])
        par[0], par[1], par[2] = par1[0], par1[1], par1[2]
        par[3], par[4], par[5] = par2[0], par2[1], par2[2]
        par[6], par[7], par[8] = par3[0], par3[1], par3[2]
        fit.SetParameters(par)
        hist.Fit(fit,"R")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1","gaus",fitBeginning,fitEnding)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",fitBeginning,fitEnding)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[3])
        fitt2.FixParameter(1,PAR[4])
        fitt2.FixParameter(2,PAR[5])
        fitt2.Draw("same")
        fitt3= r.TF1("fitt3","gaus",fitBeginning,fitEnding)
        fitt3.SetLineColor(r.kOrange)
        fitt3.FixParameter(0,PAR[6])
        fitt3.FixParameter(1,PAR[7])
        fitt3.FixParameter(2,PAR[8])
        fitt3.Draw("same") """

        #####################################################################
        ######################## Four Gaussians #############################
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit1 = r.TF1("fit1","gaus",60,90)
        fit2 = r.TF1("fit2","gaus",20,60)
        fit3 = r.TF1("fit3","gaus",20,180)
        fit4 = r.TF1("fit4","gaus",80,90)
        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        hist.Fit(fit3,"R+")
        hist.Fit(fit4,"R+")
        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        par3 = fit3.GetParameters()
        par4 = fit4.GetParameters()
        fit = r.TF1("fit","gaus(0)+gaus(3)+gaus(6)+gaus(9)",fitBeginning,fitEnding,12)
        par = array('d',12*[0.])
        par[0], par[1], par[2] = par1[0], par1[1], par1[2]
        par[3], par[4], par[5] = par2[0], par2[1], par2[2]
        par[6], par[7], par[8] = par3[0], par3[1], par3[2]
        par[9], par[10], par[11] = par4[0], par4[1], par4[2]
        fit.SetParameters(par)
        hist.Fit(fit,"R")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1","gaus",fitBeginning,fitEnding)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",fitBeginning,fitEnding)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[3])
        fitt2.FixParameter(1,PAR[4])
        fitt2.FixParameter(2,PAR[5])
        fitt2.Draw("same")
        fitt3= r.TF1("fitt3","gaus",fitBeginning,fitEnding)
        fitt3.SetLineColor(r.kOrange)
        fitt3.FixParameter(0,PAR[6])
        fitt3.FixParameter(1,PAR[7])
        fitt3.FixParameter(2,PAR[8])
        fitt3.Draw("same")
        fitt4= r.TF1("fitt4","gaus",fitBeginning,fitEnding)
        fitt4.SetLineColor(r.kCyan-3)
        fitt4.FixParameter(0,PAR[9])
        fitt4.FixParameter(1,PAR[10])
        fitt4.FixParameter(2,PAR[11])
        fitt4.Draw("same") """

        #####################################################################
        ################# Crystal ball and three Gaussians ##################
        #####################################################################

        """ fitBeginning = 20
        fitEnding = 180
        fit1 = r.TF1("fit1",crystalball,100,180,5)
        fit2 = r.TF1("fit2","gaus",20,180)
        fit3 = r.TF1("fit3","gaus",20,60)
        fit4 = r.TF1("fit4","gaus",80,90)

        fit1.SetParameter(0,1000)
        fit1.SetParameter(1,40)
        fit1.SetParameter(2,10)
        fit1.SetParameter(3,-1)
        fit1.SetParameter(4,2)

        hist.Fit(fit1,"R")
        hist.Fit(fit2,"R+")
        hist.Fit(fit3,"R+")
        hist.Fit(fit4,"R+")

        par1 = fit1.GetParameters()
        par2 = fit2.GetParameters()
        par3 = fit3.GetParameters()
        par4 = fit4.GetParameters()
        fit = r.TF1("fit",crystalball_and_three_gaus,fitBeginning,fitEnding,14)
        par = array('d',14*[0.])
        par[0], par[1], par[2], par[3], par[4] = par1[0], par1[1], par1[2], par1[3], par1[4]
        par[5], par[6], par[7] = par2[0], par2[1], par2[2]
        par[8], par[9], par[10] = par3[0], par3[1], par3[2]
        par[11], par[12], par[13] = par4[0], par4[1], par4[2]
        fit.SetParameters(par)
        hist.Fit(fit,"R")

        PAR = fit.GetParameters()
        fitt1 = r.TF1("fitt1",crystalball,fitBeginning,fitEnding,5)
        fitt1.SetLineColor(r.kBlue-4)
        fitt1.FixParameter(0,PAR[0])
        fitt1.FixParameter(1,PAR[1])
        fitt1.FixParameter(2,PAR[2])
        fitt1.FixParameter(3,PAR[3])
        fitt1.FixParameter(4,PAR[4])
        fitt1.Draw("same")
        fitt2 = r.TF1("fitt2","gaus",fitBeginning,fitEnding)
        fitt2.SetLineColor(r.kGreen-3)
        fitt2.FixParameter(0,PAR[5])
        fitt2.FixParameter(1,PAR[6])
        fitt2.FixParameter(2,PAR[7])
        fitt2.Draw("same")
        fitt3 = r.TF1("fitt3","gaus",fitBeginning,fitEnding)
        fitt3.SetLineColor(r.kOrange)
        fitt3.FixParameter(0,PAR[8])
        fitt3.FixParameter(1,PAR[9])
        fitt3.FixParameter(2,PAR[10])
        fitt3.Draw("same")
        fitt4 = r.TF1("fitt4","gaus",fitBeginning,fitEnding)
        fitt4.SetLineColor(r.kCyan-3)
        fitt4.FixParameter(0,PAR[11])
        fitt4.FixParameter(1,PAR[12])
        fitt4.FixParameter(2,PAR[13])
        fitt4.Draw("same") """

        #hist.GetXaxis().SetRangeUser(55,110)

        # Grab and print fit parameters
        #latex = r.TLatex()
        #latex.SetNDC()
        #latex.SetTextSize(0.03)
        chi2 = fit.GetChisquare()
        chi2prob = FitResultPtr.Prob()
        ndof = fit.GetNDF()
        par = fit.GetParameters()
        par_error = fit.GetParErrors()
        #latex.DrawText(0.55,0.82,"Fitted function: f(x) = ")
        #latex.DrawText(0.12,0.85,"Gaussian 1 amplitude  = %.2f +- %.2f" %(par[0],par_error[0]))
        #latex.DrawText(0.12,0.79,"JSF = %.4f +- %.4f" %(par[1],par_error[1]))
        #latex.DrawText(0.12,0.79,"Gaussian 1 mean = %.4f +- %.4f" %(par[1],par_error[1]))
        #latex.DrawText(0.12,0.73,"Gaussian 1 std dev = %.2f +- %.2f" %(par[2],par_error[2]))
        #latex.DrawText(0.12,0.67,"Gaussian 2 amplitude  = %.2f +- %.2f" %(par[3],par_error[3]))
        #latex.DrawText(0.12,0.61,"Gaussian 2 mean = %.2f +- %.2f" %(par[4],par_error[4]))
        #latex.DrawText(0.12,0.55,"Gaussian 2 std dev = %.2f +- %.2f" %(par[5],par_error[5]))
        #latex.DrawText(0.12,0.49,"chi^{2}/ndof = %.2f/%.0f = %.2f" %(chi2,ndof,chi2/ndof))

        latex = r.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.04)
        latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
        latex.DrawLatex(0.15,0.79,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
        latex.DrawLatex(0.15,0.73,"#bf{t#bar{t} MC sample}")
        latex.DrawLatex(0.15,0.67,"#bf{chi^{2}/ndof = %.2f/%.0f}" %(chi2,ndof))

        print("")
        print(par[0])
        print(par[1])
        print(par[2])
        print(par[3])
        print(par[4])
        print(par[5])
        print("")
        print(par_error[0])
        print(par_error[1])
        print(par_error[2])
        print(par_error[3])
        print(par_error[4])
        print(par_error[5])
        print("")

        """ JSF_results.append(par[1])
        JSF_error_results.append(par_error[1])
        P0_results.append(par[0])
        P0_error_results.append(par_error[0])
        P2_results.append(par[2])
        P2_error_results.append(par_error[2])
        P3_results.append(par[3])
        P3_error_results.append(par_error[3])
        P4_results.append(par[4])
        P4_error_results.append(par_error[4])
        P5_results.append(par[5])
        P5_error_results.append(par_error[5]) """

        legend = r.TLegend(0.65,0.65,0.89,0.89)
        #legend.AddEntry(fitt1, "Crystal Ball")
        legend.AddEntry(fitt1, "Gaussian 1")
        legend.AddEntry(fitt2, "Gaussian 2")
        #legend.AddEntry(fitt4, "Gaussian 3")
        legend.AddEntry(fit,"Final fit")
        legend.SetLineWidth(0)
        legend.SetFillStyle(0)
        legend.Draw("same")

        # Make and configure subplot
        pad2.cd()
        r.gStyle.SetOptStat(0)
        subplot = hist.Clone()
        subplot.Divide(fit)
        subplot.Draw()
        subplot.SetTitle("")
        #subplot.GetXaxis().SetLabelFont(63)
        subplot.GetXaxis().SetLabelSize(0.13)
        subplot.GetXaxis().SetTitle("m_{W} [GeV]")
        subplot.GetXaxis().SetTitleSize(0.11)
        subplot.GetXaxis().SetTitleOffset(0.8)
        #subplot.GetYaxis().SetLabelFont(63)
        subplot.GetYaxis().SetLabelSize(0.13)
        subplot.GetYaxis().SetTitle("Data points / fit")
        subplot.GetYaxis().SetTitleOffset(0.38)
        subplot.GetYaxis().SetTitleSize(0.11)
        subplot.GetYaxis().SetNdivisions(207)
        subplot.GetYaxis().SetRangeUser(0.87,1.13)
        line = r.TLine(fitBeginning,1,fitEnding,1)
        line.SetLineColor(r.kRed)
        line.SetLineWidth(2)
        line.Draw("same")

        # Draw canvas and save as a PDF
        canvas.Draw()
        canvas.SaveAs("../histogram_images/2022-11-15-NewJESFlavourUncertainties/" + save_name[i] + ".pdf")

        print('')
        print('Finished fitting the mW histogram for JSF = %s' %(histo_name[i]))
        print('')

""" with open('FitResults.csv', mode='w') as results:
    results_writer = csv.writer(results, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in range(0,len(JSF_results)):
        if (i == 6) or (i == 13) or (i == 20):
            results_writer.writerow([P0_results[i],P0_error_results[i],JSF_results[i],JSF_error_results[i],P2_results[i],P2_error_results[i],P3_results[i],P3_error_results[i],P4_results[i],P4_error_results[i],P5_results[i],P5_error_results[i]])
            results_writer.writerow([' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '])
        else:
            results_writer.writerow([P0_results[i],P0_error_results[i],JSF_results[i],JSF_error_results[i],P2_results[i],P2_error_results[i],P3_results[i],P3_error_results[i],P4_results[i],P4_error_results[i],P5_results[i],P5_error_results[i]])
 """
