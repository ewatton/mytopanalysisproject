# import ROOT classes
from ROOT import TFile, TH1, TH1D
import ROOT as r

histogram_to_delete = [

    "nominal_topjet_m_50bins;1",

    ]

sample_name = [

    # "ttpp8AF2",
    # "ttpp8m173",
    "ttallpp8",

    ]

for i in range(0,len(sample_name)):
    for j in range(0,len(histogram_to_delete)):
        root_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + sample_name[i] + "/mJ/JSF_1_00_" + sample_name[i] + ".root"
        root_file = TFile.Open(root_file_path,"update")
        root_file.Delete(histogram_to_delete[j])
        root_file.Close()

        print('')
        print("Deleted the histogram: " + histogram_to_delete[j] + " from the output root file for sample: " + sample_name[i])
        print('')

print('')
print("Done")
print('')
