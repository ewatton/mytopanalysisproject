import numpy as np
import os

jsonFilesPath = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/boosted-top-mass-pfit/RooFitUtilsScripts/impacts"

files = [f for f in os.listdir(jsonFilesPath) if f != "jobs.txt"]

mtopDict_up = {}
for f in files:
    if ".dn" in f:
        continue
    npName = f
    npName = npName.replace('impact.','')
    npName = npName.replace('.json','')
    npName = npName.replace('.up','_up')
    with open(jsonFilesPath + "/" + f ,"r") as infile:
        lines = infile.readlines()
        for i in range(0, len(lines)-1):
            if 'mtop_mJ' in lines[i]:
                lineToSave = lines[i+1]
        lineToSave = lineToSave.replace(lineToSave[:23],'',1)
        lineToSave = lineToSave.replace('\n','')
        mtop_val = float(lineToSave)
        mtopDict_up[npName] = mtop_val

mtopDict_down = {}
for f in files:
    if ".up" in f:
        continue
    npName = f
    npName = npName.replace('impact.','')
    npName = npName.replace('.json','')
    npName = npName.replace('.dn','_dn')
    with open(jsonFilesPath + "/" + f ,"r") as infile:
        lines = infile.readlines()
        for i in range(0, len(lines)-1):
            if 'mtop_mJ' in lines[i]:
                lineToSave = lines[i+1]
        lineToSave = lineToSave.replace(lineToSave[:23],'',1)
        lineToSave = lineToSave.replace('\n','')
        mtop_val = float(lineToSave)
        mtopDict_down[npName] = mtop_val

impactsDict_up = {}
for np, mt in mtopDict_up.items():
    impact = mt - mtopDict_up['nominal']
    impactsDict_up[np] = impact


impactsDict_down = {}
for np, mt in mtopDict_down.items():
    impact = mt - mtopDict_down['nominal']
    impactsDict_down[np] = impact

averageImpacts = {}
for np, impact in impactsDict_up.items():
    npname = np
    npname = npname.replace('_up','')
    npname_down = np.replace('_up','_dn')
    average = (abs(impact) + abs(impactsDict_down[npname_down]))*0.5
    averageImpacts[npname] = average
sortedImpacts = sorted(averageImpacts.items(), key=lambda x:x[1], reverse=True)

rankedNPs = {}
for np in sortedImpacts:
    if np[0] == 'nominal': 
        continue
    impact_up = impactsDict_up[np[0]+'_up']
    impact_down = impactsDict_down[np[0]+'_dn']  
    rankedNPs[np[0]] = [impact_up,impact_down]

print("\nAll impacts:\n")
for np, impacts in rankedNPs.items():
    print(np, impacts)
print("\n")
