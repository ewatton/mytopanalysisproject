import ROOT as r
r.TH1.SetDefaultSumw2(True)

# Open the files containing previously made histograms and grab them
histfile_mJ_nominal = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root")
hist_mJ_nominal = histfile_mJ_nominal.Get("nominal_topjet_m")
hist_mJ_nominal.SetDirectory(0)
histfile_mJ_nominal.Close()

histfile_mW_nominal = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root")
hist_mW_nominal = histfile_mW_nominal.Get("nominal_topjet_W_invMass")
hist_mW_nominal.SetDirectory(0)
histfile_mW_nominal.Close()

# Remove the stats boxes from each histogram
hist_mJ_nominal.SetStats(0)
hist_mW_nominal.SetStats(0)

# Change the x-axis scale
hist_mW_nominal.GetXaxis().SetRangeUser(60,110)


histfile_mJ = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root")
histogram_names = []
histograms_mJ = []
for h in histfile_mJ.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("JER" in h.GetName()):
        histogram = h
        histogram.SetDirectory(0)
        histograms_mJ.append(histogram)
    if (h.ClassName() == 'TH1D') and ("JER" in h.GetName()) and ("Pseudo" not in h.GetName()):
        histogram_names.append(h.GetName().rstrip("_topjet_m"))
histfile_mJ.Close()

histograms_mJ.sort()
histogram_names.sort()

print("\nThe mJ histograms")
for i in range(0,len(histograms_mJ)):
    print(histograms_mJ[i])
print("\nThe names:")
for i in range(0,len(histogram_names)):
    print(histogram_names[i])

histfile_mW = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root")
histograms_mW = []
for h in histfile_mW.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D') and ("JER" in h.GetName()):
        histogram = h
        histogram.SetDirectory(0)
        histogram.GetXaxis().SetRangeUser(60,110)
        histograms_mW.append(histogram)
histfile_mW.Close()

histograms_mW.sort()

print("\nThe mW histograms")
for i in range(0,len(histograms_mW)):
    print(histograms_mW[i])

output_file_path_mJ = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms_test.root"
output_file_path_mW = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms_test.root"

print('')
print('Saving the combined mJ histograms to file: %s' %(output_file_path_mJ))
print('')
    
outfile_mJ = r.TFile.Open(output_file_path_mJ,"RECREATE")

numberOfCombinedHistogramsToMake = len(histograms_mJ)/2

for i in range(0,numberOfCombinedHistogramsToMake): # Now perform nominal + MCsmear - PDsmear
    
    combinedHistogram_mJ = hist_mJ_nominal.Clone()
    combinedHistogram_mJ.Add(histograms_mJ[2*i],-1) # PDsmear always before MCsmear in the list
    combinedHistogram_mJ.Add(histograms_mJ[(2*i)+1])
    combinedHistogram_mJ.SetName(histogram_names[i] + "_mJ_combined")
    combinedHistogram_mJ.Write()

    print("\nCombining histogram:")
    print(hist_mJ_nominal)
    print("with PDsmear:")
    print(histograms_mJ[2*i])
    print("and MCsmear:")
    print(histograms_mJ[(2*i)+1])
    print("to make:")
    print(combinedHistogram_mJ)

outfile_mJ.Close()

print('')
print('mJ histograms have been combined!')
print('')

print('')
print('Saving the combined mW histograms to file: %s' %(output_file_path_mW))
print('')

outfile_mW = r.TFile.Open(output_file_path_mW,"RECREATE")

for i in range(0,numberOfCombinedHistogramsToMake): # Now perform nominal + MCsmear - PDsmear

    combinedHistogram_mW = hist_mW_nominal.Clone()
    combinedHistogram_mW.Add(histograms_mW[2*i],-1) # PDsmear always before MCsmear in the list
    combinedHistogram_mW.Add(histograms_mW[(2*i)+1])
    combinedHistogram_mW.SetName(histogram_names[i] + "_mW_combined")
    combinedHistogram_mW.Write()

    print("\nCombining histogram:")
    print(hist_mW_nominal)
    print("with PDsmear:")
    print(histograms_mW[2*i])
    print("and MCsmear:")
    print(histograms_mW[(2*i)+1])
    print("to make:")
    print(combinedHistogram_mW)

outfile_mW.Close()

print('')
print('mW histograms have been combined!')
print('')
