# Macro that will overlay histograms and save them to PDFs
# First created by Elliot Watton - 11/10/2021
# Last editted by Elliot Watton - 11/01/2022

import ROOT as r
r.TH1.SetDefaultSumw2(True)
print('')

#########
######### Normalised mJ distributions for different generators / generator setups (full sim)
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttallpp8/mJ/JSF_1_00_ttallpp8.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttph7 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttph7/mJ/JSF_1_00_ttph7.root")
hist_ttph7= histfile_ttph7.Get("nominal")
hist_ttph7.SetDirectory(0)
histfile_ttph7.Close()

histfile_ttaMCatNLO = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttaMCatNLO/mJ/JSF_1_00_ttaMCatNLO.root")
hist_ttaMCatNLO = histfile_ttaMCatNLO.Get("nominal")
hist_ttaMCatNLO.SetDirectory(0)
histfile_ttaMCatNLO.Close()

histfile_ttpp8Var = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8Var/mJ/JSF_1_00_ttpp8Var.root")
hist_ttpp8Var = histfile_ttpp8Var.Get("nominal")
hist_ttpp8Var.SetDirectory(0)
histfile_ttpp8Var.Close()

histfile_ttpp8MECOff = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8MECOff/mJ/JSF_1_00_ttpp8MECOff.root")
hist_ttpp8MECOff = histfile_ttpp8MECOff.Get("nominal")
hist_ttpp8MECOff.SetDirectory(0)
histfile_ttpp8MECOff.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttph7.SetStats(0)
hist_ttaMCatNLO.SetStats(0)
hist_ttpp8Var.SetStats(0)
hist_ttpp8MECOff.SetStats(0)

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttph7.SetLineColor(r.kRed-3)
hist_ttaMCatNLO.SetLineColor(r.kCyan-3)
hist_ttpp8Var.SetLineColor(r.kGreen-3)
hist_ttpp8MECOff.SetLineColor(r.kYellow-3)

# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttph7.SetLineWidth(2)
hist_ttaMCatNLO.SetLineWidth(2)
hist_ttpp8Var.SetLineWidth(2)
hist_ttpp8MECOff.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8.GetXaxis().SetRangeUser(150,200)
hist_ttph7.GetXaxis().SetRangeUser(150,200)
hist_ttaMCatNLO.GetXaxis().SetRangeUser(150,200)
hist_ttpp8Var.GetXaxis().SetRangeUser(150,200)
hist_ttpp8MECOff.GetXaxis().SetRangeUser(150,200)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed reconstructed top jet mass distributions")
hist_ttallpp8.GetYaxis().SetTitle("Normalised events")

# Overlay the histograms onto the canvas (when normalised)
h1 = hist_ttallpp8.Clone()
h2 = hist_ttph7.Clone()
h3 = hist_ttaMCatNLO.Clone()
h4 = hist_ttpp8Var.Clone()
h5 = hist_ttpp8MECOff.Clone()
h1.Scale(1./h1.Integral(), "width")
h2.Scale(1./h2.Integral(), "width")
h3.Scale(1./h3.Integral(), "width")
h4.Scale(1./h4.Integral(), "width")
h5.Scale(1./h5.Integral(), "width")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h2.Draw("same")
h3.Draw("same")
h4.Draw("same")
h5.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.3,0.12,0.7,0.3)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h2, "Powheg+Herwig")
legend.AddEntry(h3, "aMC@NLO+Pythia")
legend.AddEntry(h4, "Powheg+Pythia, with varied hdamp parameter")
legend.AddEntry(h5, "Powheg+Pythia, with ME corrections turned off")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h2.Clone()
subplot2 = h3.Clone()
subplot3 = h4.Clone()
subplot4 = h5.Clone()
subplot1.Divide(h1)
subplot2.Divide(h1)
subplot3.Divide(h1)
subplot4.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC generator / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
subplot2.Draw("E1 same")
subplot3.Draw("E1 same")
subplot4.Draw("E1 same")
line = r.TLine(150,1,200,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/overlayed_modelling_mJ_histograms.pdf") """

######
###### Overlayed mW plots (comparing two MC generators at a time)
######

""" # Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h2.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.65,0.65,0.89,0.89)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h2, "Powheg+Herwig")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h2.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("Powheg+Herwig / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
line = r.TLine(150,1,200,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms_peak_ttph7.pdf")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h3.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.65,0.65,0.89,0.89)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h3, "aMC@NLO+Pythia")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h3.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("aMC@NLO+Pythia / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.06)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
line = r.TLine(150,1,200,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms_peak_ttaMCatNLO.pdf")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h4.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.3,0.15,0.7,0.33)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h4, "Powheg+Pythia, with varied hdamp parameter")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h4.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("Powheg+Pythia, varied hdamp / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.5)
subplot1.GetYaxis().SetTitleSize(0.05)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.86,1.14)
subplot1.Draw("E1")
line = r.TLine(150,1,200,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mJ_mass_histograms_peak_ttpp8Var.pdf") """


#########
######### Normalised mJ distributions for different generators / generator setups (full sim)
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttallpp8/mW/JSF_1_00_ttallpp8.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttph7 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttph7/mW/JSF_1_00_ttph7.root")
hist_ttph7= histfile_ttph7.Get("nominal")
hist_ttph7.SetDirectory(0)
histfile_ttph7.Close()

histfile_ttaMCatNLO = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttaMCatNLO/mW/JSF_1_00_ttaMCatNLO.root")
hist_ttaMCatNLO = histfile_ttaMCatNLO.Get("nominal")
hist_ttaMCatNLO.SetDirectory(0)
histfile_ttaMCatNLO.Close()

histfile_ttpp8Var = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8Var/mW/JSF_1_00_ttpp8Var.root")
hist_ttpp8Var = histfile_ttpp8Var.Get("nominal")
hist_ttpp8Var.SetDirectory(0)
histfile_ttpp8Var.Close()

histfile_ttpp8MECOff = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8MECOff/mW/JSF_1_00_ttpp8MECOff.root")
hist_ttpp8MECOff = histfile_ttpp8MECOff.Get("nominal")
hist_ttpp8MECOff.SetDirectory(0)
histfile_ttpp8MECOff.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttph7.SetStats(0)
hist_ttaMCatNLO.SetStats(0)
hist_ttpp8Var.SetStats(0)
hist_ttpp8MECOff.SetStats(0)

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttph7.SetLineColor(r.kRed-3)
hist_ttaMCatNLO.SetLineColor(r.kCyan-3)
hist_ttpp8Var.SetLineColor(r.kGreen-3)
hist_ttpp8MECOff.SetLineColor(r.kYellow-3)

# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttph7.SetLineWidth(2)
hist_ttaMCatNLO.SetLineWidth(2)
hist_ttpp8Var.SetLineWidth(2)
hist_ttpp8MECOff.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8.GetXaxis().SetRangeUser(60,110)
hist_ttph7.GetXaxis().SetRangeUser(60,110)
hist_ttaMCatNLO.GetXaxis().SetRangeUser(60,110)
hist_ttpp8Var.GetXaxis().SetRangeUser(60,110)
hist_ttpp8MECOff.GetXaxis().SetRangeUser(60,110)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed reconstructed W boson mass distributions")
hist_ttallpp8.GetYaxis().SetTitle("Normalised events")

# Overlay the histograms onto the canvas (when normalised)
h1 = hist_ttallpp8.Clone()
h2 = hist_ttph7.Clone()
h3 = hist_ttaMCatNLO.Clone()
h4 = hist_ttpp8Var.Clone()
h5 = hist_ttpp8MECOff.Clone()
h1.Scale(1./h1.Integral(), "width")
h2.Scale(1./h2.Integral(), "width")
h3.Scale(1./h3.Integral(), "width")
h4.Scale(1./h4.Integral(), "width")
h5.Scale(1./h5.Integral(), "width")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h2.Draw("same")
h3.Draw("same")
h4.Draw("same")
h5.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.3,0.12,0.7,0.3)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h2, "Powheg+Herwig")
legend.AddEntry(h3, "aMC@NLO+Pythia")
legend.AddEntry(h4, "Powheg+Pythia, with varied hdamp parameter")
legend.AddEntry(h5, "Powheg+Pythia, with ME corrections turned off")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.02)
latex.DrawText(0.58,0.85,"Powheg+Pythia mean = (%.2f +- %.2f) GeV" %(h1.GetMean(), h1.GetMeanError()))
latex.DrawText(0.58,0.81,"Powheg+Herwig mean = (%.2f +- %.2f) GeV" %(h2.GetMean(), h2.GetMeanError()))
latex.DrawText(0.58,0.77,"aMC@NLO+Pythia mean = (%.2f +- %.2f) GeV" %(h3.GetMean(), h3.GetMeanError()))
latex.DrawText(0.58,0.73,"Powheg+Pythia, with varied hdamp parameter mean = (%.2f +- %.2f) GeV" %(h4.GetMean(), h4.GetMeanError()))
latex.DrawText(0.58,0.69,"Powheg+Pythia, with ME corrections turned off mean = (%.2f +- %.2f) GeV" %(h5.GetMean(), h5.GetMeanError()))

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h2.Clone()
subplot2 = h3.Clone()
subplot3 = h4.Clone()
subplot4 = h5.Clone()
subplot1.Divide(h1)
subplot2.Divide(h1)
subplot3.Divide(h1)
subplot4.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC generator / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
subplot2.Draw("E1 same")
subplot3.Draw("E1 same")
subplot4.Draw("E1 same")
line = r.TLine(60,1,110,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/overlayed_modelling_mW_histograms.pdf") """

######
###### Overlayed mW plots (comparing two MC generators at a time)
######

""" # Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h2.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.65,0.65,0.89,0.89)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h2, "Powheg+Herwig")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h2.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("Powheg+Herwig / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
line = r.TLine(60,1,110,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms_peak_ttph7.pdf")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h3.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.65,0.65,0.89,0.89)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h3, "aMC@NLO+Pythia")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h3.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("aMC@NLO+Pythia / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.06)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.Draw("E1")
line = r.TLine(60,1,110,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms_peak_ttaMCatNLO.pdf")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h4.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.3,0.15,0.7,0.33)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h4, "Powheg+Pythia, with varied hdamp parameter")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

pad2.cd()
r.gStyle.SetOptStat(0)
subplot1 = h4.Clone()
subplot1.Divide(h1)
subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV/c^{2}]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("Powheg+Pythia, varied hdamp / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.5)
subplot1.GetYaxis().SetTitleSize(0.05)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.86,1.14)
subplot1.Draw("E1")
line = r.TLine(60,1,110,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms_peak_ttpp8Var.pdf") """


#########
######### mJ distributions for fast vs full sim comparison (ttpp8AF2 vs ttallpp8)
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/output_systematics_mJ_JSF_1_00_mt_172_5_ttallpp8.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttpp8AF2 = r.TFile.Open("../output_root_files/output_topjet_m_JSF_1_00_ttpp8AF2_more_events_SkipTruth_1.root")
hist_ttpp8AF2= histfile_ttpp8AF2.Get("nominal")
hist_ttpp8AF2.SetDirectory(0)
histfile_ttpp8AF2.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttpp8AF2.SetStats(0)

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

# Overlay the histograms onto the canvas
hist_ttallpp8.Draw()
hist_ttpp8AF2.Draw("same")

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttpp8AF2.SetLineColor(r.kRed-3)

# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttpp8AF2.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8_mean = hist_ttallpp8.GetMean()
hist_ttallpp8_mean_error = hist_ttallpp8.GetMeanError()
hist_ttpp8AF2_mean = hist_ttpp8AF2.GetMean()
hist_ttpp8AF2_mean_error = hist_ttpp8AF2.GetMeanError()
hist_ttallpp8.GetXaxis().SetRangeUser(150,200)
hist_ttpp8AF2.GetXaxis().SetRangeUser(150,200)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed reconstructed top jet mass distributions")
hist_ttallpp8.GetXaxis().SetTitle("Mass [GeV]")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.75,0.75,0.89,0.89)
legend.AddEntry(hist_ttallpp8, "Full sim")
legend.AddEntry(hist_ttpp8AF2, "Fast sim")
legend.SetLineWidth(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.03)
latex.DrawText(0.15,0.23,"Full sim distribution mean = (%.2f +- %.2f) GeV" %(hist_ttallpp8_mean, hist_ttallpp8_mean_error))
latex.DrawText(0.15,0.19,"Fast sim distribution mean = (%.2f +- %.2f) GeV" %(hist_ttpp8AF2_mean, hist_ttpp8AF2_mean_error))
latex.DrawText(0.15,0.15,"Powheg+Pythia used for both")

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_FastSimVsFullSim_mJ_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_FastSimVsFullSim_mJ_mass_histograms_peak.pdf") """

#########
######### mW distributions for fast vs full sim comparison (ttpp8AF2 vs ttallpp8)
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/output_systematics_mW_JSF_1_00_mt_172_5_ttallpp8.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttpp8AF2 = r.TFile.Open("../output_root_files/output_Subjet_invMass_largestPT_JSF_1_00_ttpp8AF2_more_events_SkipTruth_1.root")
hist_ttpp8AF2= histfile_ttpp8AF2.Get("nominal_no_neg")
hist_ttpp8AF2.SetDirectory(0)
histfile_ttpp8AF2.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttpp8AF2.SetStats(0)

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

# Overlay the histograms onto the canvas
hist_ttallpp8.Draw()
hist_ttpp8AF2.Draw("same")

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttpp8AF2.SetLineColor(r.kRed-3)

# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttpp8AF2.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8_mean = hist_ttallpp8.GetMean()
hist_ttallpp8_mean_error = hist_ttallpp8.GetMeanError()
hist_ttpp8AF2_mean = hist_ttpp8AF2.GetMean()
hist_ttpp8AF2_mean_error = hist_ttpp8AF2.GetMeanError()
hist_ttallpp8.GetXaxis().SetRangeUser(60,110)
hist_ttpp8AF2.GetXaxis().SetRangeUser(60,110)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed reconstructed W mass distributions")
hist_ttallpp8.GetXaxis().SetTitle("Mass [GeV]")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.75,0.75,0.89,0.89)
legend.AddEntry(hist_ttallpp8, "Full sim")
legend.AddEntry(hist_ttpp8AF2, "Fast sim")
legend.SetLineWidth(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.03)
latex.DrawText(0.28,0.23,"Full sim distribution mean = (%.2f +- %.2f) GeV" %(hist_ttallpp8_mean, hist_ttallpp8_mean_error))
latex.DrawText(0.28,0.19,"Fast sim distribution mean = (%.2f +- %.2f) GeV" %(hist_ttpp8AF2_mean, hist_ttpp8AF2_mean_error))
latex.DrawText(0.28,0.15,"Powheg+Pythia used for both")

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_FastSimVsFullSim_mW_mass_histograms.pdf")
canvas_overlayed.SaveAs("../histogram_images/overlayed_FastSimVsFullSim_mW_mass_histograms_peak.pdf") """

#########
######### Normalised Jet Response distributions for different generators / generator setups (full sim)
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal_topjet_Subjet_jetresponse")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttph7 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttph7/All/JSF_1_00_ttph7.root")
hist_ttph7= histfile_ttph7.Get("nominal_topjet_Subjet_jetresponse")
hist_ttph7.SetDirectory(0)
histfile_ttph7.Close()

histfile_ttaMCatNLO = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttaMCatNLO/All/JSF_1_00_ttaMCatNLO.root")
hist_ttaMCatNLO = histfile_ttaMCatNLO.Get("nominal_topjet_Subjet_jetresponse")
hist_ttaMCatNLO.SetDirectory(0)
histfile_ttaMCatNLO.Close()

histfile_ttpp8Var = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8Var/All/JSF_1_00_ttpp8Var.root")
hist_ttpp8Var = histfile_ttpp8Var.Get("nominal_topjet_Subjet_jetresponse")
hist_ttpp8Var.SetDirectory(0)
histfile_ttpp8Var.Close()

histfile_ttpp8AF2 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root")
hist_ttpp8AF2 = histfile_ttpp8AF2.Get("nominal_topjet_Subjet_jetresponse")
hist_ttpp8AF2.SetDirectory(0)
histfile_ttpp8AF2.Close()

histfile_ttph713 = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttph713/All/JSF_1_00_ttph713.root")
hist_ttph713 = histfile_ttph713.Get("nominal_topjet_Subjet_jetresponse")
hist_ttph713.SetDirectory(0)
histfile_ttph713.Close()

histfile_ttnfaMCatNLO = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttnfaMCatNLO/All/JSF_1_00_ttnfaMCatNLO.root")
hist_ttnfaMCatNLO = histfile_ttnfaMCatNLO.Get("nominal_topjet_Subjet_jetresponse")
hist_ttnfaMCatNLO.SetDirectory(0)
histfile_ttnfaMCatNLO.Close()

histfile_ttpp8MECOff = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttpp8MECOff/All/JSF_1_00_ttpp8MECOff.root")
hist_ttpp8MECOff = histfile_ttpp8MECOff.Get("nominal_topjet_Subjet_jetresponse")
hist_ttpp8MECOff.SetDirectory(0)
histfile_ttpp8MECOff.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttph7.SetStats(0)
hist_ttaMCatNLO.SetStats(0)
hist_ttpp8Var.SetStats(0)
hist_ttpp8AF2.SetStats(0)
hist_ttph713.SetStats(0)
hist_ttnfaMCatNLO.SetStats(0)
hist_ttpp8MECOff.SetStats(0)

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttph7.SetLineColor(r.kRed-3)
hist_ttaMCatNLO.SetLineColor(r.kCyan-3)
hist_ttpp8Var.SetLineColor(r.kGreen-3)

hist_ttpp8AF2.SetLineColor(r.kBlack)
hist_ttph713.SetLineColor(r.kRed-3)
hist_ttnfaMCatNLO.SetLineColor(r.kCyan-3)
hist_ttpp8MECOff.SetLineColor(r.kGreen-3)

# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttph7.SetLineWidth(2)
hist_ttaMCatNLO.SetLineWidth(2)
hist_ttpp8Var.SetLineWidth(2)
hist_ttpp8AF2.SetLineWidth(2)
hist_ttph713.SetLineWidth(2)
hist_ttnfaMCatNLO.SetLineWidth(2)
hist_ttpp8MECOff.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8.GetXaxis().SetRangeUser(60,110)
hist_ttph7.GetXaxis().SetRangeUser(60,110)
hist_ttaMCatNLO.GetXaxis().SetRangeUser(60,110)
hist_ttpp8Var.GetXaxis().SetRangeUser(60,110)
hist_ttpp8AF2.GetXaxis().SetRangeUser(60,110)
hist_ttph713.GetXaxis().SetRangeUser(60,110)
hist_ttnfaMCatNLO.GetXaxis().SetRangeUser(60,110)
hist_ttpp8MECOff.GetXaxis().SetRangeUser(60,110)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed jet response distributions for full sim samples")
hist_ttallpp8.GetYaxis().SetTitle("Normalised events")
hist_ttpp8AF2.SetTitle("Overlayed jet response distributions for fast sim samples")
hist_ttpp8AF2.GetYaxis().SetTitle("Normalised events")

# Overlay the histograms onto the canvas (when normalised)
h1 = hist_ttallpp8.Clone()
h2 = hist_ttph7.Clone()
h3 = hist_ttaMCatNLO.Clone()
h4 = hist_ttpp8Var.Clone()
h5 = hist_ttpp8AF2.Clone()
h6 = hist_ttph713.Clone()
h7 = hist_ttnfaMCatNLO.Clone()
h8 = hist_ttpp8MECOff.Clone()

h1.Scale(1./h1.Integral(), "width")
h2.Scale(1./h2.Integral(), "width")
h3.Scale(1./h3.Integral(), "width")
h4.Scale(1./h4.Integral(), "width")
h5.Scale(1./h5.Integral(), "width")
h6.Scale(1./h6.Integral(), "width")
h7.Scale(1./h7.Integral(), "width")
h8.Scale(1./h8.Integral(), "width")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()
h2.Draw("same")
h3.Draw("same")
h4.Draw("same")

h5.Draw()
h6.Draw("same")
h7.Draw("same")
h8.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.11,0.68,0.47,0.88)
legend.AddEntry(h1, "Powheg+Pythia")
legend.AddEntry(h2, "Powheg+Herwig7")
legend.AddEntry(h3, "aMC@NLO+Pythia")
legend.AddEntry(h4, "Powheg+Pythia, with varied hdamp parameter")
legend.AddEntry(h5, "Powheg+Pythia")
legend.AddEntry(h6, "Powheg+Herwig7.1.3")
legend.AddEntry(h7, "aMC@NLO+Pythia")
legend.AddEntry(h8, "Powheg+Pythia, with ME corrections turned off")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.02)

latex.DrawText(0.58,0.85,"Powheg+Pythia mean = %.4f +- %.4f" %(h1.GetMean(), h1.GetMeanError()))
latex.DrawText(0.58,0.81,"Powheg+Herwig7 mean = %.4f +- %.4f" %(h2.GetMean(), h2.GetMeanError()))
latex.DrawText(0.58,0.77,"aMC@NLO+Pythia mean = %.4f +- %.4f" %(h3.GetMean(), h3.GetMeanError()))
latex.DrawText(0.58,0.73,"Powheg+Pythia, with varied hdamp parameter mean = %.4f +- %.4f" %(h4.GetMean(), h4.GetMeanError()))

latex.DrawText(0.58,0.85,"Powheg+Pythia mean = %.4f +- %.4f" %(h5.GetMean(), h5.GetMeanError()))
latex.DrawText(0.58,0.81,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(h6.GetMean(), h6.GetMeanError()))
latex.DrawText(0.58,0.77,"aMC@NLO+Pythia mean = %.4f +- %.4f" %(h7.GetMean(), h7.GetMeanError()))
latex.DrawText(0.58,0.73,"Powheg+Pythia, with ME corrections turned off mean = %.4f +- %.4f" %(h8.GetMean(), h8.GetMeanError()))

pad2.cd()
r.gStyle.SetOptStat(0)

subplot1 = h2.Clone()
subplot2 = h3.Clone()
subplot3 = h4.Clone()
subplot1.Divide(h1)
subplot2.Divide(h1)
subplot3.Divide(h1)

subplot1 = h6.Clone()
subplot2 = h7.Clone()
subplot3 = h8.Clone()
subplot1.Divide(h5)
subplot2.Divide(h5)
subplot3.Divide(h5)

subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Response")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC setup / Powheg+Pythia")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
subplot1.GetYaxis().SetRangeUser(0.5,1.5)
subplot1.Draw("E1")
subplot2.Draw("E1 same")
subplot3.Draw("E1 same")
line = r.TLine(0,1,2,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
#canvas_overlayed.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/overlayed_modelling_lightJetResponse_histograms_PPvsPPfastsim.pdf")
canvas_overlayed.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/overlayed_JR_histograms_fastsim.pdf") """

#########
#########
#########

""" # Open the file containing previously made histograms and grab them
histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/mW/JSF_1_00_ttallpp8_systematics.root")
hist_ttallpp8 = histfile_ttallpp8.Get("nominal_topjet_W_invMass")
hist_ttallpp8.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8_systematics_isr_up.root")
hist_ttallpp8_isr_up = histfile_ttallpp8.Get("nominal_topjet_W_invMass")
hist_ttallpp8_isr_up.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8_systematics_isr_down.root")
hist_ttallpp8_isr_down = histfile_ttallpp8.Get("nominal_topjet_W_invMass")
hist_ttallpp8_isr_down.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8_systematics_fsr_up.root")
hist_ttallpp8_fsr_up = histfile_ttallpp8.Get("nominal_topjet_W_invMass")
hist_ttallpp8_fsr_up.SetDirectory(0)
histfile_ttallpp8.Close()

histfile_ttallpp8 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8_systematics_fsr_down.root")
hist_ttallpp8_fsr_down = histfile_ttallpp8.Get("nominal_topjet_W_invMass")
hist_ttallpp8_fsr_down.SetDirectory(0)
histfile_ttallpp8.Close()

# Remove the stats boxes from each histogram
hist_ttallpp8.SetStats(0)
hist_ttallpp8_isr_up.SetStats(0)
hist_ttallpp8_isr_down.SetStats(0)
hist_ttallpp8_fsr_up.SetStats(0)
hist_ttallpp8_fsr_down.SetStats(0)

# Change the colour of the different histograms
hist_ttallpp8.SetLineColor(r.kBlack)
hist_ttallpp8_isr_up.SetLineColor(r.kRed-3)
hist_ttallpp8_isr_down.SetLineColor(r.kCyan-3)
hist_ttallpp8_fsr_up.SetLineColor(r.kRed-3)
hist_ttallpp8_fsr_down.SetLineColor(r.kCyan-3)


# Change the width of the lines so they can be easily seen
hist_ttallpp8.SetLineWidth(2)
hist_ttallpp8_isr_up.SetLineWidth(2)
hist_ttallpp8_isr_down.SetLineWidth(2)
hist_ttallpp8_fsr_up.SetLineWidth(2)
hist_ttallpp8_fsr_down.SetLineWidth(2)

# Change the x-axis scale
hist_ttallpp8.GetXaxis().SetRangeUser(60,110)
hist_ttallpp8_isr_up.GetXaxis().SetRangeUser(60,110)
hist_ttallpp8_isr_down.GetXaxis().SetRangeUser(60,110)
hist_ttallpp8_fsr_up.GetXaxis().SetRangeUser(60,110)
hist_ttallpp8_fsr_down.GetXaxis().SetRangeUser(60,110)

# Add a title to the first histogram, so that it will show on the canvas
hist_ttallpp8.SetTitle("Overlayed W mass distributions")
hist_ttallpp8.GetYaxis().SetTitle("Normalised events")

# Overlay the histograms onto the canvas (when normalised)
h1 = hist_ttallpp8.Clone()
h2 = hist_ttallpp8_isr_up.Clone()
h3 = hist_ttallpp8_isr_down.Clone()
h4 = hist_ttallpp8_fsr_up.Clone()
h5 = hist_ttallpp8_fsr_down.Clone()

h1.Scale(1./h1.Integral(), "width")
h2.Scale(1./h2.Integral(), "width")
h3.Scale(1./h3.Integral(), "width")
h4.Scale(1./h4.Integral(), "width")
h5.Scale(1./h5.Integral(), "width")

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Draw()

h2.Draw("same")
h3.Draw("same")

h4.Draw("same")
h5.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.11,0.68,0.4,0.88)
legend.AddEntry(h1, "Powheg+Pythia nominal")
legend.AddEntry(h2, "Powheg+Pythia ISR_up")
legend.AddEntry(h3, "Powheg+Pythia ISR_down")
legend.AddEntry(h4, "Powheg+Pythia FSR_up")
legend.AddEntry(h5, "Powheg+Pythia FSR_down")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.025)

latex.DrawText(0.58,0.85,"Powheg+Pythia nominal mean = (%.4f +- %.4f) GeV" %(h1.GetMean(), h1.GetMeanError()))
latex.DrawText(0.58,0.81,"Powheg+Pythia ISR_up mean = (%.4f +- %.4f) GeV" %(h2.GetMean(), h2.GetMeanError()))
latex.DrawText(0.58,0.77,"Powheg+Pythia ISR_down mean = (%.4f +- %.4f) GeV" %(h3.GetMean(), h3.GetMeanError()))

latex.DrawText(0.58,0.85,"Powheg+Pythia nominal mean = (%.4f +- %.4f) GeV" %(h1.GetMean(), h1.GetMeanError()))
latex.DrawText(0.58,0.81,"Powheg+Pythia FSR_up mean = (%.4f +- %.4f) GeV" %(h4.GetMean(), h4.GetMeanError()))
latex.DrawText(0.58,0.77,"Powheg+Pythia FSR_down mean = (%.4f +- %.4f) GeV" %(h5.GetMean(), h5.GetMeanError()))

pad2.cd()
r.gStyle.SetOptStat(0)

subplot1 = h2.Clone()
subplot2 = h3.Clone()

subplot1 = h4.Clone()
subplot2 = h5.Clone()

subplot1.Divide(h1)
subplot2.Divide(h1)

subplot1.SetTitle("")
subplot1.GetXaxis().SetLabelFont(63)
subplot1.GetXaxis().SetLabelSize(10)
subplot1.GetXaxis().SetTitle("Mass [GeV]")
subplot1.GetXaxis().SetTitleSize(0.11)
subplot1.GetXaxis().SetTitleOffset(0.8)
subplot1.GetYaxis().SetLabelFont(63)
subplot1.GetYaxis().SetLabelSize(10)
subplot1.GetYaxis().SetTitle("MC setup / Nominal")
subplot1.GetYaxis().SetTitleOffset(0.38)
subplot1.GetYaxis().SetTitleSize(0.07)
subplot1.GetYaxis().SetNdivisions(207)
#subplot1.GetYaxis().SetRangeUser(0.75,1.25)
subplot1.GetYaxis().SetRangeUser(0.9,1.1)
#subplot1.GetYaxis().SetRangeUser(0.5,1.5)
subplot1.Draw("E1")
subplot2.Draw("E1 same")
line = r.TLine(60,1,110,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
#canvas_overlayed.SaveAs("../histogram_images/overlayed_modelling_mW_mass_histograms.pdf")
#canvas_overlayed.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/overlayed_modelling_lightJetResponse_histograms_PPvsPPfastsim.pdf")
canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/overlayed_mW_histograms_ISR_Manchester.pdf")

print('')
print('All overlay plots made!')
print('') """

###############
###############
###############

""" histogram_types = ["nominal_topjet_m_50bins","nominal_topjet_W_invMass","nominal_topjet_Subjet_bjetresponse","nominal_topjet_Subjet_lightjetresponse"]
#histogram_types = ["nominal_topjet_Subjet_lightjetresponse","nominal_topjet_Subjet_bjetresponse"]

for i in range(0,len(histogram_types)):

    histfile0 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root")
    hist0 = histfile0.Get(histogram_types[i])
    hist0.SetDirectory(0)
    histfile0.Close()
    
    histfile1 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttph713/All/JSF_1_00_ttph713.root")
    hist1 = histfile1.Get(histogram_types[i])
    hist1.SetDirectory(0)
    histfile1.Close()

    histfile1_MC2MC = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttph713_MC2MC/All/JSF_1_00_ttph713.root")
    hist1_MC2MC = histfile1_MC2MC.Get(histogram_types[i])
    hist1_MC2MC.SetDirectory(0)
    histfile1_MC2MC.Close()

    # histfile2 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttMCatNLOH713/All/JSF_1_00_ttMCatNLOH713.root")
    # hist2 = histfile2.Get(histogram_types[i])
    # hist2.SetDirectory(0)
    # histfile2.Close()

    # histfile2_MC2MC = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttMCatNLOH713_MC2MC/All/JSF_1_00_ttMCatNLOH713.root")
    # hist2_MC2MC = histfile2_MC2MC.Get(histogram_types[i])
    # hist2_MC2MC.SetDirectory(0)
    # histfile2_MC2MC.Close()

    # Remove the stats boxes from each histogram
    hist0.SetStats(0)
    hist1.SetStats(0)
    hist1_MC2MC.SetStats(0)
    # hist2.SetStats(0)
    # hist2_MC2MC.SetStats(0)

    # Change the colour of the different histograms
    hist0.SetLineColor(r.kBlack)
    hist1.SetLineColor(r.kRed-3)
    hist1_MC2MC.SetLineColor(r.kRed-3)
    # hist2.SetLineColor(r.kCyan-3)
    # hist2_MC2MC.SetLineColor(r.kCyan-3)


    # Change the width of the lines so they can be easily seen
    hist0.SetLineWidth(2)
    hist1.SetLineWidth(2)
    hist1_MC2MC.SetLineWidth(2)
    # hist2.SetLineWidth(2)
    # hist2_MC2MC.SetLineWidth(2)

    if (histogram_types[i] == "nominal_topjet_W_invMass"):
        hist0.GetXaxis().SetRangeUser(60,110)
        hist1.GetXaxis().SetRangeUser(60,110)
        hist1_MC2MC.GetXaxis().SetRangeUser(60,110)
        # hist2.GetXaxis().SetRangeUser(60,110)
        # hist2_MC2MC.GetXaxis().SetRangeUser(60,110)

    if (histogram_types[i] == "nominal_topjet_W_invMass"):
        hist0.SetTitle("Overlayed W boson mass distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed W boson mass distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist1_MC2MC.SetTitle("Overlayed W boson mass distributions")
        hist1_MC2MC.GetYaxis().SetTitle("Normalised events")
        # hist2.SetTitle("Overlayed W boson mass distributions")
        # hist2.GetYaxis().SetTitle("Normalised events")
        # hist2_MC2MC.SetTitle("Overlayed W boson mass distributions")
        # hist2_MC2MC.GetYaxis().SetTitle("Normalised events")
    elif (histogram_types[i] == "nominal_topjet_m_50bins"):
        hist0.SetTitle("Overlayed top jet mass distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed top jet mass distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist1_MC2MC.SetTitle("Overlayed top jet mass distributions")
        hist1_MC2MC.GetYaxis().SetTitle("Normalised events")
        # hist2.SetTitle("Overlayed top jet mass distributions")
        # hist2.GetYaxis().SetTitle("Normalised events")
        # hist2_MC2MC.SetTitle("Overlayed top jet mass distributions")
        # hist2_MC2MC.GetYaxis().SetTitle("Normalised events")
    elif (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse") or (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        hist0.SetTitle("Overlayed W jet response distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed W jet response distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist1_MC2MC.SetTitle("Overlayed W jet response distributions")
        hist1_MC2MC.GetYaxis().SetTitle("Normalised events")
        # hist2.SetTitle("Overlayed W jet response distributions")
        # hist2.GetYaxis().SetTitle("Normalised events")
        # hist2_MC2MC.SetTitle("Overlayed W jet response distributions")
        # hist2_MC2MC.GetYaxis().SetTitle("Normalised events")

    hist0.Scale(1./hist0.Integral(), "width")
    hist1.Scale(1./hist1.Integral(), "width")
    hist1_MC2MC.Scale(1./hist1_MC2MC.Integral(), "width")
    # hist2.Scale(1./hist2.Integral(), "width")
    # hist2_MC2MC.Scale(1./hist2_MC2MC.Integral(), "width")

    # Make a canvas that the histograms will be overlayed onto
    canvas_overlayed = r.TCanvas("canvas_overlayed")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    #pad1.SetGrid()
    #pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    hist0.Draw()
    hist1.Draw("same")

    # Add a legend so that the different overlayed histograms can be identified
    legend = r.TLegend(0.11,0.58,0.4,0.68)
    legend.AddEntry(hist0, "Powheg+Pythia8")
    legend.AddEntry(hist1, "Powheg+Herwig7.1.3")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.03)
    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")


    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        latex.DrawText(0.56,0.85,"Powheg+Pythia8 mean = (%.4f +- %.4f) GeV" %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.56,0.81,"Powheg+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist1.GetMean(), hist1.GetMeanError()))
    else:
        latex.DrawText(0.56,0.85,"Powheg+Pythia8 mean = %.4f +- %.4f" %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.56,0.81,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist1.GetMean(), hist1.GetMeanError()))


    pad2.cd()
    r.gStyle.SetOptStat(0)

    subplot1 = hist1.Clone()
    subplot1.Divide(hist0)

    subplot1.SetTitle("")
    subplot1.GetXaxis().SetLabelFont(63)
    subplot1.GetXaxis().SetLabelSize(10)
    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        subplot1.GetXaxis().SetTitle("Mass (GeV)")
    else:
        subplot1.GetXaxis().SetTitle("Response")
    subplot1.GetXaxis().SetTitleSize(0.11)
    subplot1.GetXaxis().SetTitleOffset(0.8)
    subplot1.GetYaxis().SetLabelFont(63)
    subplot1.GetYaxis().SetLabelSize(10)
    subplot1.GetYaxis().SetTitle("Powheg+Herwig7.1.3 / Powheg+Pythia8")
    subplot1.GetYaxis().SetTitleOffset(0.38)
    subplot1.GetYaxis().SetTitleSize(0.07)
    subplot1.GetYaxis().SetNdivisions(207)
    subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    subplot1.Draw("E1")
    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse") or (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        line = r.TLine(0,1,2,1)
    elif (histogram_types[i] == "nominal_topjet_m_50bins"):
        line = r.TLine(120,1,220,1)
    elif (histogram_types[i] == "nominal_topjet_W_invMass"):
        line = r.TLine(60,1,110,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.cd()

    # Draw the canvas to the screen (not really neccessary)
    canvas_overlayed.Draw()

    # Save the canvas to a PDF so that it can be viewed once script is complete
    canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_PHvsPP_NEW.pdf")

    canvas_overlayed = r.TCanvas("canvas_overlayed")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    #pad1.SetGrid()
    #pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    hist0.Draw()
    hist1_MC2MC.Draw("same")

    # Add a legend so that the different overlayed histograms can be identified
    legend = r.TLegend(0.11,0.58,0.4,0.68)
    legend.AddEntry(hist0, "Powheg+Pythia8")
    legend.AddEntry(hist1_MC2MC, "Powheg+Herwig7.1.3 with MC2MC SF")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.03)
    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")


    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        latex.DrawText(0.56,0.85,"Powheg+Pythia8 mean = (%.4f +- %.4f) GeV" %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.56,0.81,"Powheg+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist1_MC2MC.GetMean(), hist1_MC2MC.GetMeanError()))
    else:
        latex.DrawText(0.56,0.85,"Powheg+Pythia8 mean = %.4f +- %.4f" %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.56,0.81,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist1_MC2MC.GetMean(), hist1_MC2MC.GetMeanError()))


    pad2.cd()
    r.gStyle.SetOptStat(0)

    subplot1 = hist1_MC2MC.Clone()
    subplot1.Divide(hist0)

    subplot1.SetTitle("")
    subplot1.GetXaxis().SetLabelFont(63)
    subplot1.GetXaxis().SetLabelSize(10)
    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        subplot1.GetXaxis().SetTitle("Mass (GeV)")
    else:
        subplot1.GetXaxis().SetTitle("Response")
    subplot1.GetXaxis().SetTitleSize(0.11)
    subplot1.GetXaxis().SetTitleOffset(0.8)
    subplot1.GetYaxis().SetLabelFont(63)
    subplot1.GetYaxis().SetLabelSize(10)
    subplot1.GetYaxis().SetTitle("Powheg+Herwig7.1.3 / Powheg+Pythia8")
    subplot1.GetYaxis().SetTitleOffset(0.38)
    subplot1.GetYaxis().SetTitleSize(0.05)
    subplot1.GetYaxis().SetNdivisions(207)
    subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    subplot1.Draw("E1")
    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse") or (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        line = r.TLine(0,1,2,1)
    elif (histogram_types[i] == "nominal_topjet_m_50bins"):
        line = r.TLine(120,1,220,1)
    elif (histogram_types[i] == "nominal_topjet_W_invMass"):
        line = r.TLine(60,1,110,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.cd()

    # Draw the canvas to the screen (not really neccessary)
    canvas_overlayed.Draw()

    # Save the canvas to a PDF so that it can be viewed once script is complete
    canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_PHwMC2MCSFvsPP_NEW.pdf")

    # canvas_overlayed = r.TCanvas("canvas_overlayed")

    # pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    # pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    # pad1.SetBottomMargin(0.00001)
    # pad1.SetBorderMode(0)
    # pad2.SetTopMargin(0.00001)
    # pad2.SetBottomMargin(0.2)
    # pad2.SetBorderMode(0)
    # #pad1.SetGrid()
    # #pad2.SetGrid()
    # pad1.Draw()
    # pad2.Draw()
    # pad1.cd()

    # hist1.Draw()
    # hist2.Draw("same")

    # # Add a legend so that the different overlayed histograms can be identified
    # r.TLegend(0.11,0.58,0.47,0.68)
    # legend.AddEntry(hist1, "Powheg+Herwig7.1.3")
    # legend.AddEntry(hist2, "aMC@NLO+Herwig7.1.3")
    # legend.SetLineWidth(0)
    # legend.SetFillStyle(0)
    # legend.Draw("same")

    # latex = r.TLatex()
    # latex.SetNDC()
    # latex.SetTextSize(0.03)
    # latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    # latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    # latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")

    # if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
    #     latex.DrawText(0.55,0.85,"Powheg+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist1.GetMean(), hist1.GetMeanError()))
    #     latex.DrawText(0.55,0.81,"aMC@NLO+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist2.GetMean(), hist2.GetMeanError()))
    # else:
    #     latex.DrawText(0.55,0.85,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist1.GetMean(), hist1.GetMeanError()))
    #     latex.DrawText(0.55,0.81,"aMC@NLO+Herwig7.1.3 mean = %.4f +- %.4f" %(hist2.GetMean(), hist2.GetMeanError()))


    # pad2.cd()
    # r.gStyle.SetOptStat(0)

    # subplot1 = hist1.Clone()
    # subplot1.Divide(hist2)

    # subplot1.SetTitle("")
    # subplot1.GetXaxis().SetLabelFont(63)
    # subplot1.GetXaxis().SetLabelSize(10)
    # if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
    #     subplot1.GetXaxis().SetTitle("Mass (GeV)")
    # else:
    #     subplot1.GetXaxis().SetTitle("Response")
    # subplot1.GetXaxis().SetTitleSize(0.11)
    # subplot1.GetXaxis().SetTitleOffset(0.8)
    # subplot1.GetYaxis().SetLabelFont(63)
    # subplot1.GetYaxis().SetLabelSize(10)
    # subplot1.GetYaxis().SetTitle("Powheg+Herwig7.1.3 / aMC@NLO+Herwig7.1.3")
    # subplot1.GetYaxis().SetTitleOffset(0.38)
    # subplot1.GetYaxis().SetTitleSize(0.05)
    # subplot1.GetYaxis().SetNdivisions(207)
    # subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    # subplot1.Draw("E1")
    # if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse") or (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
    #     line = r.TLine(0,1,2,1)
    # elif (histogram_types[i] == "nominal_topjet_m_50bins"):
    #     line = r.TLine(120,1,220,1)
    # elif (histogram_types[i] == "nominal_topjet_W_invMass"):
    #     line = r.TLine(60,1,110,1)
    # line.SetLineColor(r.kCyan-3)
    # line.SetLineWidth(2)
    # line.Draw("same")
    # canvas_overlayed.cd()

    # # Draw the canvas to the screen (not really neccessary)
    # canvas_overlayed.Draw()

    # # Save the canvas to a PDF so that it can be viewed once script is complete
    # canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_PHvsMGH_NEW.pdf")

    # canvas_overlayed = r.TCanvas("canvas_overlayed")

    # pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    # pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    # pad1.SetBottomMargin(0.00001)
    # pad1.SetBorderMode(0)
    # pad2.SetTopMargin(0.00001)
    # pad2.SetBottomMargin(0.2)
    # pad2.SetBorderMode(0)
    # #pad1.SetGrid()
    # #pad2.SetGrid()
    # pad1.Draw()
    # pad2.Draw()
    # pad1.cd()

    # hist1_MC2MC.Draw()
    # hist2_MC2MC.Draw("same")

    # # Add a legend so that the different overlayed histograms can be identified
    # r.TLegend(0.11,0.58,0.47,0.68)
    # legend.AddEntry(hist1, "Powheg+Herwig7.1.3 with MC2MC SF")
    # legend.AddEntry(hist2, "aMC@NLO+Herwig7.1.3 with MC2MC SF")
    # legend.SetLineWidth(0)
    # legend.SetFillStyle(0)
    # legend.Draw("same")

    # latex = r.TLatex()
    # latex.SetNDC()
    # latex.SetTextSize(0.03)
    # latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    # latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    # latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")

    # if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
    #     latex.DrawText(0.55,0.85,"Powheg+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist1_MC2MC.GetMean(), hist1_MC2MC.GetMeanError()))
    #     latex.DrawText(0.55,0.81,"aMC@NLO+Herwig7.1.3 mean = (%.4f +- %.4f) GeV" %(hist2_MC2MC.GetMean(), hist2_MC2MC.GetMeanError()))
    # else:
    #     latex.DrawText(0.55,0.85,"Powheg+Herwig7.1.3 mean = %.4f +- %.4f" %(hist1_MC2MC.GetMean(), hist1_MC2MC.GetMeanError()))
    #     latex.DrawText(0.55,0.81,"aMC@NLO+Herwig7.1.3 mean = %.4f +- %.4f" %(hist2_MC2MC.GetMean(), hist2_MC2MC.GetMeanError()))


    # pad2.cd()
    # r.gStyle.SetOptStat(0)

    # subplot1 = hist1_MC2MC.Clone()
    # subplot1.Divide(hist2_MC2MC)

    # subplot1.SetTitle("")
    # subplot1.GetXaxis().SetLabelFont(63)
    # subplot1.GetXaxis().SetLabelSize(10)
    # if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
    #     subplot1.GetXaxis().SetTitle("Mass (GeV)")
    # else:
    #     subplot1.GetXaxis().SetTitle("Response")
    # subplot1.GetXaxis().SetTitleSize(0.11)
    # subplot1.GetXaxis().SetTitleOffset(0.8)
    # subplot1.GetYaxis().SetLabelFont(63)
    # subplot1.GetYaxis().SetLabelSize(10)
    # subplot1.GetYaxis().SetTitle("Powheg+Herwig7.1.3 / aMC@NLO+Herwig7.1.3")
    # subplot1.GetYaxis().SetTitleOffset(0.38)
    # subplot1.GetYaxis().SetTitleSize(0.05)
    # subplot1.GetYaxis().SetNdivisions(207)
    # subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    # subplot1.Draw("E1")
    # if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse") or (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
    #     line = r.TLine(0,1,2,1)
    # elif (histogram_types[i] == "nominal_topjet_m_50bins"):
    #     line = r.TLine(120,1,220,1)
    # elif (histogram_types[i] == "nominal_topjet_W_invMass"):
    #     line = r.TLine(60,1,110,1)
    # line.SetLineColor(r.kCyan-3)
    # line.SetLineWidth(2)
    # line.Draw("same")
    # canvas_overlayed.cd()

    # # Draw the canvas to the screen (not really neccessary)
    # canvas_overlayed.Draw()

    # # Save the canvas to a PDF so that it can be viewed once script is complete
    # canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_PHvsMGH_bothWithMC2MCSF_NEW.pdf")

print('')
print('All overlay plots made!')
print('') """

##################
##################
##################

""" #histogram_types = ["nominal_topjet_m","nominal_topjet_W_invMass","nominal_topjet_Subjet_bjetresponse","nominal_topjet_Subjet_lightjetresponse"]
histogram_types = ["nominal_topjet_Subjet_bjetresponse","nominal_topjet_Subjet_lightjetresponse"]

for i in range(0,len(histogram_types)):

    histfile0 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttpp8AF2/All/JSF_1_00_ttpp8AF2.root")
    hist0 = histfile0.Get(histogram_types[i])
    hist0.SetDirectory(0)
    histfile0.Close()
    
    histfile4 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttMCatNLOH713_MC2MC/All/JSF_1_00_ttMCatNLOH713.root")
    hist4 = histfile4.Get(histogram_types[i])
    hist4.SetDirectory(0)
    histfile4.Close()

    histfile2 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttpp8MECOff/All/JSF_1_00_ttpp8MECOff.root")
    hist2 = histfile2.Get(histogram_types[i])
    hist2.SetDirectory(0)
    histfile2.Close()

    histfile3 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttnfaMCatNLO/All/JSF_1_00_ttnfaMCatNLO.root")
    hist3 = histfile3.Get(histogram_types[i])
    hist3.SetDirectory(0)
    histfile3.Close()

    histfile1 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttph713_MC2MC/All/JSF_1_00_ttph713.root")
    hist1 = histfile1.Get(histogram_types[i])
    hist1.SetDirectory(0)
    histfile1.Close()

    histfile5 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttallpp8/jetresponse/JSF_1_00_ttallpp8_systematics.root")
    hist5 = histfile5.Get(histogram_types[i])
    hist5.SetDirectory(0)
    histfile5.Close()

    histfile6 = r.TFile.Open("../output_root_files/2022-07-06-CorrectedOutputROOTFiles/ttph721/All/JSF_1_00_ttph721.root")
    hist6 = histfile6.Get(histogram_types[i])
    hist6.SetDirectory(0)
    histfile6.Close()

    # Remove the stats boxes from each histogram
    hist0.SetStats(0)
    hist1.SetStats(0)
    hist2.SetStats(0)
    hist3.SetStats(0)
    hist4.SetStats(0)
    hist5.SetStats(0)
    hist6.SetStats(0)

    # Change the colour of the different histograms
    hist0.SetLineColor(r.kBlack)
    hist1.SetLineColor(r.kRed-3)
    hist2.SetLineColor(r.kYellow-3)
    hist3.SetLineColor(r.kCyan-3)
    hist4.SetLineColor(r.kGreen-3)

    hist5.SetLineColor(r.kBlack)
    hist6.SetLineColor(r.kRed-3)




    # Change the width of the lines so they can be easily seen
    hist0.SetLineWidth(2)
    hist1.SetLineWidth(2)
    hist2.SetLineWidth(2)
    hist3.SetLineWidth(2)
    hist4.SetLineWidth(2)
    hist5.SetLineWidth(2)
    hist6.SetLineWidth(2)

    if (histogram_types[i] == "nominal_topjet_W_invMass"):
        hist0.GetXaxis().SetRangeUser(60,110)
        hist1.GetXaxis().SetRangeUser(60,110)
        hist2.GetXaxis().SetRangeUser(60,110)
        hist3.GetXaxis().SetRangeUser(60,110)
        hist4.GetXaxis().SetRangeUser(60,110)
        hist5.GetXaxis().SetRangeUser(60,110)
        hist6.GetXaxis().SetRangeUser(60,110)

    if (histogram_types[i] == "nominal_topjet_W_invMass"):
        hist0.SetTitle("Overlayed W boson mass distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed W boson mass distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist2.SetTitle("Overlayed W boson mass distributions")
        hist2.GetYaxis().SetTitle("Normalised events")
        hist3.SetTitle("Overlayed W boson mass distributions")
        hist3.GetYaxis().SetTitle("Normalised events")
        hist4.SetTitle("Overlayed W boson mass distributions")
        hist4.GetYaxis().SetTitle("Normalised events")
        hist5.SetTitle("Overlayed W boson mass distributions")
        hist5.GetYaxis().SetTitle("Normalised events")
        hist6.SetTitle("Overlayed W boson mass distributions")
        hist6.GetYaxis().SetTitle("Normalised events")

    elif (histogram_types[i] == "nominal_topjet_m"):
        hist0.SetTitle("Overlayed top jet mass distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed top jet mass distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist2.SetTitle("Overlayed top jet mass distributions")
        hist2.GetYaxis().SetTitle("Normalised events")
        hist3.SetTitle("Overlayed top jet mass distributions")
        hist3.GetYaxis().SetTitle("Normalised events")
        hist4.SetTitle("Overlayed top jet mass distributions")
        hist4.GetYaxis().SetTitle("Normalised events")
        hist5.SetTitle("Overlayed top jet mass distributions")
        hist5.GetYaxis().SetTitle("Normalised events")
        hist6.SetTitle("Overlayed top jet mass distributions")
        hist6.GetYaxis().SetTitle("Normalised events")

    elif (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse"):
        hist0.SetTitle("Overlayed W jet response distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed W jet response distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist2.SetTitle("Overlayed W jet response distributions")
        hist2.GetYaxis().SetTitle("Normalised events")
        hist3.SetTitle("Overlayed W jet response distributions")
        hist3.GetYaxis().SetTitle("Normalised events")
        hist4.SetTitle("Overlayed W jet response distributions")
        hist4.GetYaxis().SetTitle("Normalised events")
        hist5.SetTitle("Overlayed W jet response distributions")
        hist5.GetYaxis().SetTitle("Normalised events")
        hist6.SetTitle("Overlayed W jet response distributions")
        hist6.GetYaxis().SetTitle("Normalised events")

    elif (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        hist0.SetTitle("Overlayed b jet response distributions")
        hist0.GetYaxis().SetTitle("Normalised events")
        hist1.SetTitle("Overlayed b jet response distributions")
        hist1.GetYaxis().SetTitle("Normalised events")
        hist2.SetTitle("Overlayed b jet response distributions")
        hist2.GetYaxis().SetTitle("Normalised events")
        hist3.SetTitle("Overlayed b jet response distributions")
        hist3.GetYaxis().SetTitle("Normalised events")
        hist4.SetTitle("Overlayed b jet response distributions")
        hist4.GetYaxis().SetTitle("Normalised events")
        hist5.SetTitle("Overlayed b jet response distributions")
        hist5.GetYaxis().SetTitle("Normalised events")
        hist6.SetTitle("Overlayed b jet response distributions")
        hist6.GetYaxis().SetTitle("Normalised events")

    hist0.Scale(1./hist0.Integral(), "width")
    hist1.Scale(1./hist1.Integral(), "width")
    hist2.Scale(1./hist2.Integral(), "width")
    hist3.Scale(1./hist3.Integral(), "width")
    hist4.Scale(1./hist4.Integral(), "width")
    hist5.Scale(1./hist5.Integral(), "width")
    hist6.Scale(1./hist6.Integral(), "width")

    # Make a canvas that the histograms will be overlayed onto
    canvas_overlayed = r.TCanvas("canvas_overlayed")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    hist0.Draw()
    hist1.Draw("same")
    hist2.Draw("same")
    hist3.Draw("same")
    hist4.Draw("same")

    # Add a legend so that the different overlayed histograms can be identified
    legend = r.TLegend(0.11,0.58,0.40,0.88)
    legend.AddEntry(hist0, "Powheg+Pythia8")
    legend.AddEntry(hist1, "Powheg+Herwig7.1.3 with MC2MC SF")
    legend.AddEntry(hist2, "Powheg+Pythia8 with MECOff")
    legend.AddEntry(hist3, "aMC@NLO+Pythia8")
    legend.AddEntry(hist4, "aMC@NLO+Herwig7.1.3 with MC2MC SF")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.025)

    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse"):
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean =  %.4f +- %.4f " %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.1.3 with MC2MC SF mean =  %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
        latex.DrawText(0.63,0.77,"Powheg+Pythia8 with MECOff mean =  %.4f +- %.4f " %(hist2.GetMean(), hist2.GetMeanError()))
        latex.DrawText(0.63,0.73,"aMC@NLO+Pythia8 mean =  %.4f +- %.4f " %(hist3.GetMean(), hist3.GetMeanError()))
        latex.DrawText(0.63,0.69,"aMC@NLO+Herwig7.1.3 with MC2MC SF mean =  %.4f +- %.4f " %(hist4.GetMean(), hist4.GetMeanError()))
    elif ((histogram_types[i] == "nominal_topjet_Subjet_bjetresponse")):
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean =  %.4f +- %.4f " %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.1.3 with MC2MC SF mean =  %.4f +- %.4f " %(hist1.GetMean(), hist1.GetMeanError()))
        latex.DrawText(0.63,0.77,"Powheg+Pythia8 with MECOff mean =  %.4f +- %.4f " %(hist2.GetMean(), hist2.GetMeanError()))
        latex.DrawText(0.63,0.73,"aMC@NLO+Pythia8 mean =  %.4f +- %.4f " %(hist3.GetMean(), hist3.GetMeanError()))
        latex.DrawText(0.63,0.69,"aMC@NLO+Herwig7.1.3 with MC2MC SF mean =  %.4f +- %.4f " %(hist4.GetMean(), hist4.GetMeanError()))
        
    else:
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean = (%.4f +- %.4f) GeV" %(hist0.GetMean(), hist0.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.1.3 with MC2MC SF mean = (%.4f +- %.4f) GeV" %(hist1.GetMean(), hist1.GetMeanError()))
        latex.DrawText(0.63,0.77,"Powheg+Pythia8 with MECOff mean = (%.4f +- %.4f) GeV" %(hist2.GetMean(), hist2.GetMeanError()))
        latex.DrawText(0.63,0.73,"aMC@NLO+Pythia8 mean = (%.4f +- %.4f) GeV" %(hist3.GetMean(), hist3.GetMeanError()))
        latex.DrawText(0.63,0.69,"aMC@NLO+Herwig7.1.3 with MC2MC SF mean = (%.4f +- %.4f) GeV" %(hist4.GetMean(), hist4.GetMeanError()))

    pad2.cd()
    r.gStyle.SetOptStat(0)

    subplot1 = hist1.Clone()
    subplot2 = hist2.Clone()
    subplot3 = hist3.Clone()
    subplot4 = hist4.Clone()
    subplot1.Divide(hist0)
    subplot2.Divide(hist0)
    subplot3.Divide(hist0)
    subplot4.Divide(hist0)

    subplot1.SetTitle("")
    subplot1.GetXaxis().SetLabelFont(63)
    subplot1.GetXaxis().SetLabelSize(10)
    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        subplot1.GetXaxis().SetTitle("Mass (GeV)")
    else:
        subplot1.GetXaxis().SetTitle("Response")
    subplot1.GetXaxis().SetTitleSize(0.11)
    subplot1.GetXaxis().SetTitleOffset(0.8)
    subplot1.GetYaxis().SetLabelFont(63)
    subplot1.GetYaxis().SetLabelSize(10)
    subplot1.GetYaxis().SetTitle("MC setup / Powheg+Pythia8")
    subplot1.GetYaxis().SetTitleOffset(0.38)
    subplot1.GetYaxis().SetTitleSize(0.07)
    subplot1.GetYaxis().SetNdivisions(207)
    subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    subplot1.Draw("E1")
    subplot2.Draw("same")
    subplot3.Draw("same")
    subplot4.Draw("same")
    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse"):
        line = r.TLine(0,1,2,1)
    if (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        line = r.TLine(0,1,2,1)
    elif (histogram_types[i] == "nominal_topjet_m"):
        line = r.TLine(135,1,205,1)
    elif (histogram_types[i] == "nominal_topjet_W_invMass"):
        line = r.TLine(60,1,110,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.cd()

    # Draw the canvas to the screen (not really neccessary)
    canvas_overlayed.Draw() 

    # Save the canvas to a PDF so that it can be viewed once script is complete
    canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_FastSim.pdf")

    # Make a canvas that the histograms will be overlayed onto
    canvas_overlayed = r.TCanvas("canvas_overlayed")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetGrid()
    pad2.SetGrid()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    hist5.Draw()
    hist6.Draw("same")

    # Add a legend so that the different overlayed histograms can be identified
    legend = r.TLegend(0.11,0.78,0.40,0.88)
    legend.AddEntry(hist5, "Powheg+Pythia8")
    legend.AddEntry(hist6, "Powheg+Herwig7.2.1")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.025)

    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse"):
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean =  %.4f +- %.4f " %(hist5.GetMean(), hist5.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.2.1 mean =  %.4f +- %.4f " %(hist6.GetMean(), hist6.GetMeanError()))
    elif (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean =  %.4f +- %.4f " %(hist5.GetMean(), hist5.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.2.1 mean =  %.4f +- %.4f " %(hist6.GetMean(), hist6.GetMeanError()))
    else:
        latex.DrawText(0.63,0.85,"Powheg+Pythia8 mean = (%.4f +- %.4f) GeV" %(hist5.GetMean(), hist5.GetMeanError()))
        latex.DrawText(0.63,0.81,"Powheg+Herwig7.2.1 mean = (%.4f +- %.4f) GeV" %(hist6.GetMean(), hist6.GetMeanError()))

    pad2.cd()
    r.gStyle.SetOptStat(0)

    subplot1 = hist6.Clone()
    subplot1.Divide(hist5)

    subplot1.SetTitle("")
    subplot1.GetXaxis().SetLabelFont(63)
    subplot1.GetXaxis().SetLabelSize(10)
    if (histogram_types[i] != "nominal_topjet_Subjet_lightjetresponse") and (histogram_types[i] != "nominal_topjet_Subjet_bjetresponse"):
        subplot1.GetXaxis().SetTitle("Mass (GeV)")
    else:
        subplot1.GetXaxis().SetTitle("Response")
    subplot1.GetXaxis().SetTitleSize(0.11)
    subplot1.GetXaxis().SetTitleOffset(0.8)
    subplot1.GetYaxis().SetLabelFont(63)
    subplot1.GetYaxis().SetLabelSize(10)
    subplot1.GetYaxis().SetTitle("Powheg+Herwig7.2.1 / Powheg+Pythia8")
    subplot1.GetYaxis().SetTitleOffset(0.38)
    subplot1.GetYaxis().SetTitleSize(0.05)
    subplot1.GetYaxis().SetNdivisions(207)
    subplot1.GetYaxis().SetRangeUser(0.75,1.25)
    subplot1.Draw("E1")
    if (histogram_types[i] == "nominal_topjet_Subjet_lightjetresponse"):
        line = r.TLine(0,1,2,1)
    if (histogram_types[i] == "nominal_topjet_Subjet_bjetresponse"):
        line = r.TLine(0,1,2,1)
    elif (histogram_types[i] == "nominal_topjet_m"):
        line = r.TLine(135,1,205,1)
    elif (histogram_types[i] == "nominal_topjet_W_invMass"):
        line = r.TLine(60,1,110,1)
    line.SetLineColor(r.kBlack)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_overlayed.cd()

    # Draw the canvas to the screen (not really neccessary)
    canvas_overlayed.Draw()

    # Save the canvas to a PDF so that it can be viewed once script is complete
    canvas_overlayed.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/" + histogram_types[i] + "_PPvsPH721.pdf")

print('')
print('All overlay plots made!')
print('') """

####################
####################
####################


# histfile = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL.root")

# hnom = histfile.Get("nominal_topjet_W_invMass")
# hnom.SetDirectory(0)
# hnom.SetLineColor(r.kBlack)
# hnom.SetLineWidth(2)

# hup = histfile.Get("JET_EtaIntercalibration_TotalStat__1up_topjet_W_invMass")
# # hup = histfile.Get("JET_EtaIntercalibration_NonClosure_highE__1up_topjet_W_invMass")
# hup.SetDirectory(0)
# hup.SetLineColor(r.kRed)
# hup.SetLineWidth(2)

# hdown = histfile.Get("JET_EtaIntercalibration_TotalStat__1down_topjet_W_invMass")
# # hup = histfile.Get("JET_EtaIntercalibration_NonClosure_highE__1down_topjet_W_invMass")
# hdown.SetDirectory(0)
# hdown.SetLineColor(r.kBlue)
# hdown.SetLineWidth(2)

# histfile.Close()

# # Make a canvas that the histograms will be overlayed onto
# canvas_overlayed = r.TCanvas("canvas_overlayed")

# pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
# pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
# pad1.SetBottomMargin(0.00001)
# pad1.SetBorderMode(0)
# pad2.SetTopMargin(0.00001)
# pad2.SetBottomMargin(0.2)
# pad2.SetBorderMode(0)
# pad1.SetGrid()
# pad2.SetGrid()
# pad1.Draw()
# pad2.Draw()
# pad1.cd()

# # up = rfile.Get("JET_EtaIntercalibration_NonClosure_highE__1down")
# # down = rfile.Get("JET_EtaIntercalibration_NonClosure_highE__1up")

# # hnom.Scale(1./hnom.Integral(), "width")
# # hup.Scale(1./hup.Integral(), "width")
# # hdown.Scale(1./hdown.Integral(), "width")

# hnom.Draw()
# hup.Draw("same")
# hdown.Draw("same")

# # Add a legend so that the different overlayed histograms can be identified
# legend = r.TLegend(0.11,0.78,0.40,0.88)
# legend.AddEntry(hnom, "Nominal")
# # legend.AddEntry(hup, "JET_EtaIntercalibration_NonClosure_highE__1up")
# # legend.AddEntry(hdown, "JET_EtaIntercalibration_NonClosure_highE__1down")
# legend.AddEntry(hup, "JET_EtaIntercalibration_TotalStat__1up")
# legend.AddEntry(hdown, "JET_EtaIntercalibration_TotalStat__1down")
# legend.SetLineWidth(0)
# legend.SetFillStyle(0)
# legend.Draw("same")

# pad2.cd()
# r.gStyle.SetOptStat(0)

# subplotup = hup.Clone()
# subplotupnom = hnom.Clone()
# subplotup.Add(subplotupnom,-1)
# subplotup.Divide(subplotupnom)

# subplotdown = hdown.Clone()
# subplotdownnom = hnom.Clone()
# subplotdown.Add(subplotdownnom,-1)
# subplotdown.Divide(subplotdownnom)

# subplotup.SetTitle("")
# subplotup.GetXaxis().SetLabelFont(63)
# subplotup.GetXaxis().SetLabelSize(10)
# subplotup.GetXaxis().SetTitle("Mass (GeV)")
# subplotup.GetXaxis().SetTitleSize(0.11)
# subplotup.GetXaxis().SetTitleOffset(0.8)
# subplotup.GetYaxis().SetLabelFont(63)
# subplotup.GetYaxis().SetLabelSize(10)
# subplotup.GetYaxis().SetTitle("Syst - Nominal / Nominal")
# subplotup.GetYaxis().SetTitleOffset(0.38)
# subplotup.GetYaxis().SetTitleSize(0.05)
# subplotup.GetYaxis().SetNdivisions(207)
# subplotup.GetYaxis().SetRangeUser(-1,1)
# subplotup.Draw("E1")

# subplotdown.SetTitle("")
# subplotdown.GetXaxis().SetLabelFont(63)
# subplotdown.GetXaxis().SetLabelSize(10)
# subplotdown.GetXaxis().SetTitle("Mass (GeV)")
# subplotdown.GetXaxis().SetTitleSize(0.11)
# subplotdown.GetXaxis().SetTitleOffset(0.8)
# subplotdown.GetYaxis().SetLabelFont(63)
# subplotdown.GetYaxis().SetLabelSize(10)
# subplotdown.GetYaxis().SetTitle("Syst - Nominal / Nominal")
# subplotdown.GetYaxis().SetTitleOffset(0.38)
# subplotdown.GetYaxis().SetTitleSize(0.05)
# subplotdown.GetYaxis().SetNdivisions(207)
# subplotdown.GetYaxis().SetRangeUser(-1,1)
# subplotdown.Draw("same")
# # line = r.TLine(40,1,120,1)
# # line.SetLineColor(r.kBlack)
# # line.SetLineWidth(2)
# # line.Draw("same")
# canvas_overlayed.cd()

# # Draw the canvas to the screen (not really neccessary)
# canvas_overlayed.Draw()

# # Save the canvas to a PDF so that it can be viewed once script is complete
# canvas_overlayed.SaveAs("../ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/mWFit/RecreatedSmoothingPlots.pdf")

####################
####################
####################


histfile = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/All/JSF_1_00_ttpp8AF2_ALL.root")
h1 = histfile.Get("nominal_topjet_Subjet_bjetresponse")
h1.SetDirectory(0)
h1.SetLineColor(r.kBlack)
h1.SetLineWidth(2)
histfile.Close()

histfile = r.TFile.Open("../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8RTT/All/JSF_1_00_ttpp8RTT_ALL.root")
h2 = histfile.Get("nominal_topjet_Subjet_bjetresponse")
h2.SetDirectory(0)
h2.SetLineColor(r.kRed)
h2.SetLineWidth(2)
histfile.Close()

# Make a canvas that the histograms will be overlayed onto
canvas_overlayed = r.TCanvas("canvas_overlayed")

pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
pad1.SetBottomMargin(0.00001)
pad1.SetBorderMode(0)
pad2.SetTopMargin(0.00001)
pad2.SetBottomMargin(0.2)
pad2.SetBorderMode(0)
pad1.SetGrid()
pad2.SetGrid()
pad1.Draw()
pad2.Draw()
pad1.cd()

h1.Scale(1./h1.Integral(), "width")
h2.Scale(1./h2.Integral(), "width")

h1.Draw()
h2.Draw("same")

# Add a legend so that the different overlayed histograms can be identified
legend = r.TLegend(0.11,0.78,0.40,0.88)
legend.AddEntry(h1, "PWG+PY8")
legend.AddEntry(h2, "Recoil sample")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.04)
latex.DrawText(0.55,0.85,"Nominal sample mean =  %.4f +- %.4f " %(h1.GetMean(), h1.GetMeanError()))
latex.DrawText(0.55,0.80,"Recoil sample mean =  %.4f +- %.4f " %(h2.GetMean(), h2.GetMeanError()))

pad2.cd()
r.gStyle.SetOptStat(0)

subplot = h2.Clone()
subplot.Divide(h1)

subplot.SetTitle("")
subplot.GetXaxis().SetLabelFont(63)
subplot.GetXaxis().SetLabelSize(10)
subplot.GetXaxis().SetTitle("Response")
subplot.GetXaxis().SetTitleSize(0.11)
subplot.GetXaxis().SetTitleOffset(0.8)
subplot.GetYaxis().SetLabelFont(63)
subplot.GetYaxis().SetLabelSize(10)
subplot.GetYaxis().SetTitle("Recoil sample / Nominal")
subplot.GetYaxis().SetTitleOffset(0.38)
subplot.GetYaxis().SetTitleSize(0.05)
subplot.GetYaxis().SetNdivisions(207)
subplot.GetYaxis().SetRangeUser(0.71,1.29)
subplot.Draw("E1")

line = r.TLine(0,1,2,1)
line.SetLineColor(r.kBlack)
line.SetLineWidth(2)
line.Draw("same")
canvas_overlayed.cd()

# Draw the canvas to the screen (not really neccessary)
canvas_overlayed.Draw()

# Save the canvas to a PDF so that it can be viewed once script is complete
# canvas_overlayed.SaveAs("../ProfileLikelihoodFitting/boosted-top-mass-pfit/TRExConfigs/mWFit/RecreatedSmoothingPlots.pdf")
canvas_overlayed.SaveAs("../histogram_images/2023-01-13/RecoilVsNominal_bjetresponse.pdf")
