from ROOT import TFile, TH1, TH1D
import ROOT as r
from optparse import OptionParser

r.TH1.SetDefaultSumw2(True)

def FillHisto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand,ISRorFSRTemplateToUse):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        if isRecoLevel:
            baseWeight = tree_name.weight_normalise
        else:
            baseWeight = tree_name.mcEventNormWeight

        if ISRorFSRTemplateToUse != "":
            specificWeight = getattr(tree_name, ISRorFSRTemplateToUse)
            weight = baseWeight*specificWeight
        else:
            weight = tree_name.weight_normalise
    
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight )
            else:
                hist_name.Fill(variable / 1000.0, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight )
            else:
                hist_name.Fill(variable, weight)
    
    print("Finished filling histogram for variable: " + variable_name)

def FillHisto_systematicWeights(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand,weightToSwapOutName,systematicWeightName,systematicWeightNamesVectorElement):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())
    print('Swapping out weight:')
    print(weightToSwapOutName)
    print('with:')
    print(systematicWeightName)
    print("")
   
    hadToSkipEvents = False
    numberOfEventsSkipped = 0

    if ("eigen" in systematicWeightName):
        systematicWeightName = systematicWeightName.replace("_" + systematicWeightNamesVectorElement + "_", "_")

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        
        tree_name.GetEntry(i)
        
        if (isRecoLevel == True):
            
            baseWeight = tree_name.weight_normalise
            weightToSwapOut = getattr(tree_name, weightToSwapOutName)
            systematicWeight = getattr(tree_name, systematicWeightName)

            if ("eigen" in systematicWeightName):
                systematicWeight = systematicWeight[int(systematicWeightNamesVectorElement)]

            if weightToSwapOut == 0: # Had to put this in for some of the weight_pileup values for entries
                numberOfEventsSkipped = numberOfEventsSkipped + 1
                hadToSkipEvents = True
                continue

            weight = (baseWeight/weightToSwapOut) * systematicWeight
        else:
            baseWeight = tree_name.weight_normalise
            weightToSwapOut = getattr(tree_name, weightToSwapOutName)
            systematicWeight = getattr(tree_name, systematicWeightName)
            weight = (baseWeight/weightToSwapOut) * systematicWeight
    
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight )
            else:
                hist_name.Fill(variable / 1000.0, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight )
            else:
                hist_name.Fill(variable, weight)

    if hadToSkipEvents == True:
        print("WARNING There were %i events with 0 weight. These were skipped to avoid division by zero errors.\n" %(numberOfEventsSkipped))

def MakeHisto(doSystematics,input_file_path,output_file_path,variablesToMakeHistogramsFor,ISRorFSRTemplateToUse):

    print('')
    print('Grabbing the TTree from file: %s' %(input_file_path))
    print('')
    print('Saving the histograms to file: %s' %(output_file_path))
    print('')

    if ISRorFSRTemplateToUse != "":
        output_file_path = output_file_path.rstrip(".root") + "_" + ISRorFSRTemplateToUse + ".root"
    
    infile = TFile.Open(input_file_path,"READ")

    trees = []
    if doSystematics:
        for h in infile.GetListOfKeys():
            h = h.ReadObj()
            if (h.ClassName() == 'TTree'):
                tree = h
                trees.append(tree)
    else:
        for h in infile.GetListOfKeys():
            h = h.ReadObj()
            if (h.ClassName() == 'TTree') and (h.GetName() == 'nominal'):
                tree = h
                trees.append(tree)

    print('')
    print('The list of TTrees read in from %s:\n' %(input_file_path))
    for treename in trees:
        print(treename)
        print('')
    print('The total number of trees read in is: %i' %(len(trees)))

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    for z in range(0,len(trees)):
        for i in range(0,len(variablesToMakeHistogramsFor)):
            name = trees[z].GetName() + "_" + variablesToMakeHistogramsFor[i]

            if (variablesToMakeHistogramsFor[i] == "topjet_pt"):
                hist = TH1D(name, "h_topjet_pt; pT [GeV]; Events", 200, 355.0, 1000.0)
                variableType = "float"
                isNeedingDivisionByOneThousand = True

            elif (variablesToMakeHistogramsFor[i] == "topjet_eta"):
                hist = TH1D(name, "h_topjet_eta; eta; Events", 200, -2.5, 2.5)
                variableType = "float"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] == "topjet_phi"):
                hist = TH1D(name, "h_topjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                variableType = "float"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] == "topjet_m"):
                # hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 100, 135.0, 205.0)
                name = trees[z].GetName() + "_" + variablesToMakeHistogramsFor[i] + "_50bins"
                hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 50, 120.0, 220.0)
                variableType = "float"
                isNeedingDivisionByOneThousand = True
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_y"):
                hist = TH1D(name, "h_topjet_y; y; Events", 200, -2.5, 2.5)
                variableType = "float"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_nSubjets"):
                hist = TH1D(name, "h_topjet_nSubjets; Number of sub-jets; Events", 8, 0, 7)
                variableType = "int"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_avgSubjet_pt"):
                hist = TH1D(name, "h_topjet_avgSubjet_pt; pT [GeV]; Events", 200, 0.0, 900.0)
                variableType = "float"
                isNeedingDivisionByOneThousand = True
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_avgSubjet_eta"):
                hist = TH1D(name, "h_topjet_avgSubjet_eta; eta; Events", 200, -2.5, 2.5)
                variableType = "float"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_W_invMass"):
                hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV]; Events", 200, 0.0, 200.0)
                variableType = "float"
                isNeedingDivisionByOneThousand = True
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_nbtaggedSubjets"):
                hist = TH1D(name, "h_topjet_nbtaggedSubjets; Number of b tagged jets; Events", 6, 0.0, 5)
                variableType = "int"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_eta"):
                hist = TH1D(name, "h_topjet_Subjet_eta; eta; Events", 200, -2.5, 2.5)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_phi"):
                hist = TH1D(name, "h_topjet_Subjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_pT"):
                hist = TH1D(name, "h_topjet_Subjet_pT; pT [GeV]; Events", 200, 0.0, 900.0)
                variableType = "vector"
                isNeedingDivisionByOneThousand = True
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_mass"):
                hist = TH1D(name, "h_topjet_Subjet_mass; Mass [GeV]; Events", 200, 0.0, 180.0)
                variableType = "vector"
                isNeedingDivisionByOneThousand = True
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_type"):
                hist = TH1D(name, "h_topjet_Subjet_type; Type scoring; Events", 5, -1, 4)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_DeltaR"):
                hist = TH1D(name, "h_topjet_Subjet_DeltaR; Delta R; Events", 100, 0.0, 0.5)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_isMatched"):
                hist = TH1D(name, "h_topjet_Subjet_isMatched; Matching score; Events", 5, -2, 3)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_jetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_jetresponse; Response; Events", 200, 0.0, 2.0)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_bjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_bjetresponse; Response; Events", 200, 0.0, 2.0)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
                
            elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_lightjetresponse"):
                hist = TH1D(name, "h_topjet_Subjet_lightjetresponse; Response; Events", 200, 0.0, 2.0)
                variableType = "vector"
                isNeedingDivisionByOneThousand = False
            
            FillHisto(trees[z],hist,isRecoLevel,variablesToMakeHistogramsFor[i],variableType,isNeedingDivisionByOneThousand,ISRorFSRTemplateToUse)
            print("Finished filling histogram number " + str(i+1) + "/" + str(len(variablesToMakeHistogramsFor)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
            hist.Write()
        print("Finished filling the histograms for tree number " + str(z+1) + "/" + str(len(trees)))

    outfile.Close()


def MakeHisto_systematicWeights(input_file_path,output_file_path,variablesToMakeHistogramsFor):

    print('\nGrabbing the TTree from file: %s' %(input_file_path))
    print('\nSaving the histograms to file: %s\n' %(output_file_path))
    
    infile = TFile.Open(input_file_path,"READ")

    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree') and (h.GetName() == 'nominal'):
            tree = h
            trees.append(tree)

    print('The list of TTrees read in from %s:\n' %(input_file_path))
    for treename in trees:
        print(treename)
        print('')
    print('The total number of trees read in is: %i' %(len(trees)))

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    weightsToSwapOutNames = [
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_jvt",
        "weight_jvt",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_pileup",
        "weight_pileup",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
    ]

    systematicWeightNames = [
        "weight_bTagSF_DL1r_77_extrapolation_down",
        "weight_bTagSF_DL1r_77_extrapolation_up",
        "weight_bTagSF_DL1r_77_extrapolation_from_charm_down",
        "weight_bTagSF_DL1r_77_extrapolation_from_charm_up",
        "weight_jvt_DOWN",
        "weight_jvt_UP",
        "weight_leptonSF_EL_SF_ID_DOWN",
        "weight_leptonSF_EL_SF_ID_UP",
        "weight_leptonSF_EL_SF_Isol_DOWN",
        "weight_leptonSF_EL_SF_Isol_UP",
        "weight_leptonSF_EL_SF_Reco_DOWN",
        "weight_leptonSF_EL_SF_Reco_UP",
        "weight_leptonSF_EL_SF_Trigger_DOWN",
        "weight_leptonSF_EL_SF_Trigger_UP",
        "weight_leptonSF_MU_SF_ID_STAT_DOWN",
        "weight_leptonSF_MU_SF_ID_STAT_UP",
        "weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
        "weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP",
        "weight_leptonSF_MU_SF_ID_SYST_DOWN",
        "weight_leptonSF_MU_SF_ID_SYST_UP",
        "weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
        "weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP",
        "weight_leptonSF_MU_SF_Isol_STAT_DOWN",
        "weight_leptonSF_MU_SF_Isol_STAT_UP",
        "weight_leptonSF_MU_SF_Isol_SYST_DOWN",
        "weight_leptonSF_MU_SF_Isol_SYST_UP",
        "weight_leptonSF_MU_SF_TTVA_STAT_DOWN",
        "weight_leptonSF_MU_SF_TTVA_STAT_UP",
        "weight_leptonSF_MU_SF_TTVA_SYST_DOWN",
        "weight_leptonSF_MU_SF_TTVA_SYST_UP",
        "weight_leptonSF_MU_SF_Trigger_STAT_DOWN",
        "weight_leptonSF_MU_SF_Trigger_STAT_UP",
        "weight_leptonSF_MU_SF_Trigger_SYST_DOWN",
        "weight_leptonSF_MU_SF_Trigger_SYST_UP",
        "weight_pileup_DOWN",
        "weight_pileup_UP",
        "weight_bTagSF_DL1r_77_eigenvars_B_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_3_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_4_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_4_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_5_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_5_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_6_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_6_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_7_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_7_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_8_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_8_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_3_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_3_up"
    ]

    systematicWeightNamesVectorElement = [
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3",
        "4",
        "4",
        "5",
        "5",
        "6",
        "6",
        "7",
        "7",
        "8",
        "8",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3"
    ]

    for z in range(0,len(trees)):
        for k in range(0,len(systematicWeightNames)):            
            histograms = []
            for i in range(0,len(variable)):
                name = trees[z].GetName() + "_" + variable[i] + systematicWeightNames[k]

                if (variablesToMakeHistogramsFor[i] == "topjet_pt"):
                    hist = TH1D(name, "h_topjet_pt; pT [GeV]; Events", 200, 355.0, 1000.0)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = True

                elif (variablesToMakeHistogramsFor[i] == "topjet_eta"):
                    hist = TH1D(name, "h_topjet_eta; eta; Events", 200, -2.5, 2.5)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] == "topjet_phi"):
                    hist = TH1D(name, "h_topjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] == "topjet_m"):
                    # hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 100, 135.0, 205.0)
                    name = trees[z].GetName() + "_" + variablesToMakeHistogramsFor[i] + "_50bins"
                    hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 50, 120.0, 220.0)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = True
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_y"):
                    hist = TH1D(name, "h_topjet_y; y; Events", 200, -2.5, 2.5)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_nSubjets"):
                    hist = TH1D(name, "h_topjet_nSubjets; Number of sub-jets; Events", 8, 0, 7)
                    variableType = "int"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_avgSubjet_pt"):
                    hist = TH1D(name, "h_topjet_avgSubjet_pt; pT [GeV]; Events", 200, 0.0, 900.0)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = True
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_avgSubjet_eta"):
                    hist = TH1D(name, "h_topjet_avgSubjet_eta; eta; Events", 200, -2.5, 2.5)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_W_invMass"):
                    hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV]; Events", 200, 0.0, 200.0)
                    variableType = "float"
                    isNeedingDivisionByOneThousand = True
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_nbtaggedSubjets"):
                    hist = TH1D(name, "h_topjet_nbtaggedSubjets; Number of b tagged jets; Events", 6, 0.0, 5)
                    variableType = "int"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_eta"):
                    hist = TH1D(name, "h_topjet_Subjet_eta; eta; Events", 200, -2.5, 2.5)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_phi"):
                    hist = TH1D(name, "h_topjet_Subjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_pT"):
                    hist = TH1D(name, "h_topjet_Subjet_pT; pT [GeV]; Events", 200, 0.0, 900.0)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = True
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_mass"):
                    hist = TH1D(name, "h_topjet_Subjet_mass; Mass [GeV]; Events", 200, 0.0, 180.0)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = True
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_type"):
                    hist = TH1D(name, "h_topjet_Subjet_type; Type scoring; Events", 5, -1, 4)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_DeltaR"):
                    hist = TH1D(name, "h_topjet_Subjet_DeltaR; Delta R; Events", 100, 0.0, 0.5)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_isMatched"):
                    hist = TH1D(name, "h_topjet_Subjet_isMatched; Matching score; Events", 5, -2, 3)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_jetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_jetresponse; Response; Events", 200, 0.0, 2.0)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_bjetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_bjetresponse; Response; Events", 200, 0.0, 2.0)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False
                    
                elif (variablesToMakeHistogramsFor[i] =="topjet_Subjet_lightjetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_lightjetresponse; Response; Events", 200, 0.0, 2.0)
                    variableType = "vector"
                    isNeedingDivisionByOneThousand = False

                FillHisto_systematicWeights(trees[z],hist,isRecoLevel,variablesToMakeHistogramsFor[i],variableType,isNeedingDivisionByOneThousand,weightsToSwapOutNames[k],systematicWeightNames[k],systematicWeightNamesVectorElement[k])
                print("Finished filling histogram number " + str(j+1) + "/" + str(len(variablesToMakeHistogramsFor)) + " for the systematic weight number " + str(k+1) + "/" + str(len(systematicWeightNames)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
                hist.Write()

    outfile.Close()


def main():

    parser = OptionParser()
    
    parser.add_option("-i", "--infile",
                  action="store", type="string", dest="inputFilename")
    parser.add_option("-o", "--outfile",
                  action="store", type="string", dest="outputFilename")
    parser.add_option("-s", "--systematics", dest="systematics", action = "store_true", default = False,
                        help="Create systematic histograms")
    parser.add_option("-r", "--radiation", dest="radiation", action = "store_true", default = False,
                        help="Create histograms for ISR and FSR templates")
    parser.add_option("-w", "--systematicWeights", dest="systematicWeights", action = "store_true", default = False,
                        help="Create histograms for other systematic weight templates")

    (options, args) = parser.parse_args()  
    print(options)
    print(args)

    if ((options.systematics and options.radiation) or (options.systematics and options.systematicWeights) or (options.radiation and options.systematicWeights)):
        print("Please only select one alternate option! (-s, -r, -w)")
        exit()

    doAll = False
    numOfVariables = input("\nHello! Please type the number of variables you would like to make histograms of (type 'q' to exit or 'all' all variables):  ")
    if (((numOfVariables <= 0) or (numOfVariables > 20)) and (numOfVariables != "q") and (numOfVariables != "all")):
        print("Error! Please enter a valid number of variables!")
        exit()
    elif numOfVariables == "q":
        print("\nExiting code!")
        exit()
    elif numOfVariables == "all":
        doAll = True
    
    variablesToMakeHistogramsFor = []

    variablesAvailable = [
            "topjet_pt",
            "topjet_eta",
            "topjet_phi",
            "topjet_m",
            "topjet_y",
            "topjet_nSubjets",
            "topjet_avgSubjet_pt",
            "topjet_avgSubjet_eta",
            "topjet_W_invMass",
            "topjet_nbtaggedSubjets",
            "topjet_Subjet_eta",
            "topjet_Subjet_phi",
            "topjet_Subjet_pT",
            "topjet_Subjet_mass",
            "topjet_Subjet_type",
            "topjet_Subjet_DeltaR",
            "topjet_Subjet_isMatched",
            "topjet_Subjet_jetresponse",
            "topjet_Subjet_bjetresponse",
            "topjet_Subjet_lightjetresponse"
        ]

    if doAll == False:

        print("\nThe list of variable names that you can make a histogram for is the following: ")
        print("\t")
        print("\ttopjet_pt")
        print("\ttopjet_eta")
        print("\ttopjet_phi")
        print("\ttopjet_m")
        print("\ttopjet_y")
        print("\ttopjet_nSubjets")
        print("\ttopjet_avgSubjet_pt")
        print("\ttopjet_avgSubjet_eta")
        print("\ttopjet_W_invMass")
        print("\ttopjet_nbtaggedSubjets")
        print("\ttopjet_Subjet_eta")
        print("\ttopjet_Subjet_phi")
        print("\ttopjet_Subjet_pT")
        print("\ttopjet_Subjet_mass")
        print("\ttopjet_Subjet_type")
        print("\ttopjet_Subjet_DeltaR")
        print("\ttopjet_Subjet_isMatched")
        print("\ttopjet_Subjet_jetresponse")
        print("\ttopjet_Subjet_bjetresponse")
        print("\ttopjet_Subjet_lightjetresponse")

        for i in range(0,numOfVariables):
            correctName = False
            while (correctName == False):
                userInput = input("\nType the name of the variable that you want to make a histogram for (type 'q' to exit code): ")
                if userInput in variablesAvailable:
                    variablesToMakeHistogramsFor.append(userInput)
                    correctName = True
                elif userInput == "q":
                    print("\nExiting code!")
                    exit()
                else:
                    print("You have typed in the wrong name, try again!")
    else:
        variablesToMakeHistogramsFor = variablesAvailable
        print("\nWill make histograms for all variables!")

    ISRorFSRTemplateToUse = ""
    if options.radiation:
        ISRorFSRTemplatesAvailable = ["radiation_up","radiation_down","fsr_up","fsr_down"]
        print("\nThe list of ISR and FSR templates available: ")
        print("\t")
        print("\tradiation_up")
        print("\tradiation_down")
        print("\tfsr_up")
        print("\tfsr_down")
        correctName = False
        while correctName == False:
            ISRorFSRTemplate = input("\nPlease choose an ISR or FSR template from the options above (type 'q' to exit code): ")
            if ISRorFSRTemplate in ISRorFSRTemplatesAvailable:
                ISRorFSRTemplateToUse = ISRorFSRTemplate
                correctName = True
            elif userInput == "q":
                print("\nExiting code!")
                exit()
            else:
                print("You have typed in the wrong template name, try again!")
    
    if options.systematicWeights:
        MakeHisto_systematicWeights(options.inputFilename,options.outputFilename,variablesToMakeHistogramsFor)
    else:
        MakeHisto(options.systematics,options.inputFilename,options.outputFilename,variablesToMakeHistogramsFor,ISRorFSRTemplateToUse)

if __name__ == "__main__":
    main()