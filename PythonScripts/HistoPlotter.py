import ROOT as r
from array import array

r.TH1.SetDefaultSumw2(True)

# dR = ["0-8","1-0","1-2","1-4"]
# variables = ["nominal_tmm_leptop_addjet_m","nominal_tmm_leptop_bjet_addjet_m","nominal_lepbjet_lep_pT_ratio"]
# xlabel = ["Mass [GeV]", "Mass [GeV]", "lepbjet pT / lep pT"]
# xmin = [0,0,0]
# xmax = [600,600,10]

# for i in range(0,len(dR)):
#     for j in range(0,len(variables)):

#         histfile1 = r.TFile.Open("../output_root_files/2023-02-14-Profiling/ttpp8AF2_LeptonicSideMass_dR" + dR[i] + ".root")
#         histfile2 = r.TFile.Open("../output_root_files/2023-02-14-Profiling/ttpp8RTT_LeptonicSideMass_dR" + dR[i] + ".root")
#         histogram1 = histfile1.Get(variables[j])
#         histogram2 = histfile2.Get(variables[j])
#         histogram1.SetDirectory(0)
#         histogram2.SetDirectory(0)
#         histogram1.SetStats(0)
#         histogram2.SetStats(0)
#         histogram1.SetLineWidth(2)
#         histogram2.SetLineWidth(2)
#         histfile1.Close()
#         histfile2.Close()

#         histogram1.Scale(1./histogram1.Integral(), "width")
#         histogram2.Scale(1./histogram2.Integral(), "width")

#         histogram1.SetLineColor(r.kBlack)
#         histogram2.SetLineColor(r.kRed)

#         histogram1.SetTitle("")
#         histogram1.GetYaxis().SetTitle("Events")
#         histogram1.GetXaxis().SetTitle(xlabel[j])
#         histogram1.GetYaxis().SetTitleOffset(1.4)

#         canvas_overlayed = r.TCanvas("canvas_overlayed")
#         pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
#         pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
#         pad1.SetBottomMargin(0.00001)
#         pad1.SetBorderMode(0)
#         pad2.SetTopMargin(0.00001)
#         pad2.SetBottomMargin(0.2)
#         pad2.SetBorderMode(0)
#         pad1.SetGrid()
#         pad2.SetGrid()
#         pad1.Draw()
#         pad2.Draw()
#         pad1.cd()

#         histogram1.Draw()
#         histogram2.Draw("same")

#         latex = r.TLatex()
#         latex.SetNDC()
#         latex.SetTextSize(0.04)

#         legend = r.TLegend(0.60,0.12,0.88,0.32)
#         legend.AddEntry(histogram1, "PWG+PY8, nominal")
#         legend.AddEntry(histogram2, "PWG+PY8, recoilToTop")
#         legend.SetLineWidth(0)
#         legend.SetFillStyle(0)
#         legend.Draw("same")

#         latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#         latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#         latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")

#         latex.DrawText(0.45,0.76,"Nominal mean = %.4f +- %.4f" %(histogram1.GetMean(),histogram1.GetMeanError()))
#         latex.DrawText(0.45,0.72,"RecoilToTop mean = %.4f +- %.4f" %(histogram2.GetMean(),histogram2.GetMeanError()))
        
#         pad2.cd()

#         r.gStyle.SetOptStat(0)
#         subplot = histogram2.Clone()
#         subplot.Divide(histogram1)
#         subplot.SetMarkerStyle(5)
#         subplot.SetMarkerSize(0.5)
#         subplot.Draw("E1")
#         subplot.SetTitle("")
#         subplot.GetXaxis().SetLabelFont(63)
#         subplot.GetXaxis().SetLabelSize(10)
#         subplot.GetXaxis().SetTitle(xlabel[j])
#         subplot.GetXaxis().SetTitleSize(0.11)
#         subplot.GetXaxis().SetTitleOffset(0.8)
#         subplot.GetYaxis().SetLabelFont(63)
#         subplot.GetYaxis().SetLabelSize(10)
#         subplot.GetYaxis().SetTitle("Recoil / Nominal")
#         subplot.GetYaxis().SetTitleOffset(0.38)
#         subplot.GetYaxis().SetTitleSize(0.11)
#         subplot.GetYaxis().SetNdivisions(207)
#         subplot.GetYaxis().SetRangeUser(0.5,1.5)
#         line = r.TLine(xmin[j],1,xmax[j],1)
#         line.SetLineColor(r.kBlack)
#         line.SetLineWidth(2)
#         line.Draw("same")
#         canvas_overlayed.Draw()
#         canvas_overlayed.SaveAs("../histogram_images/2023-03-22/dR" + dR[i] + "_" + variables[j] + "_NominalVsRecoil.pdf")

#############################################################
#############################################################

# variables = ["nominal_tmm_leptop_cttaddjet_dR"]
# variables = ["nominal_topjet_m","nominal_topjet_W_invMass","nominal_tmm_leptop_cttaddjet_m","nominal_tmm_leptop_leadaddjet_m","nominal_tmm_leptop_leadaddjet_dR","nominal_tmm_leptop_cttaddjet_dR","nominal_tmm_leptop_bjet_leadaddjet_dR","nominal_tmm_leptop_bjet_cttaddjet_dR"]
# variables = ["nominal_tmm_leptop_cttaddjet_dR"]
# variables = ["nominal_topjet_Subjet_jetresponse","nominal_topjet_Subjet_bjetresponse","nominal_topjet_Subjet_lightjetresponse"]

# xlabel = ["#it{#DeltaR}(lep-top,j)", "Mass [GeV]", "dR", "dR", "dR", "dR", "#it{#DeltaR}(lep-top,j)", "#it{m}_{(lep-top)+j} [GeV]"]
# xmin = [0.0, 60.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0]
# xmax = [1.5, 105.0, 500.0, 500.0, 5.0, 5.0, 5.0, 5.0]
# xmin = [400.0]
# xmax = [1800.0]
# samplePairs = [["ttpmp8Lineshape","ttpp8AF2"],["ttpp8pThard1","ttpp8AF2"],["ttph721","ttallpp8"],["ttph713","ttpp8AF2"],["ttpp8Var_AFII","ttpp8AF2"],["ttpp8RTT","ttpp8AF2"],["ttpp8RTW","ttpp8AF2"],]
samplePairs = [["ttpp8RTT","ttpp8AF2"]]


##### For Herwig variable study 
variables = [
    # "nominal_top_lepb_deltaPhi",
    # "nominal_leadadd_top_deltaPhi",
    # "nominal_subleadadd_hadtop_deltaPhi",
    # "nominal_leadadd_subleadadd_deltaPhi",
    # "nominal_leadaddjet_pt",
    # "nominal_subleadaddjet_pt",
    # "nominal_naddjet",
    # "nominal_invmass_leadadd_hadtop",
    # "nominal_tmm_leptop_pt",
    # "nominal_tmm_leptop_y",
    # "nominal_tmm_ttbar_deltaPhi",
    # "nominal_tmm_ttbar_invmass",
    # "nominal_tmm_ttbar_pt",
    # "nominal_tmm_ttbar_y",
    # "nominal_tmm_fulleventHT",
    # "nominal_tmm_mttbar_jets",
    # "nominal_topjet_pt",
    # "nominal_topjet_y",

    # "nominal_topjet_m",
    # "nominal_topjet_W_invMass",

    # "nominal_tmm_leptop_cttaddjet_m",
    "nominal_tmm_leptop_cttaddjet_dR",

    # "nominal_tmm_leptop_m",
]
xlabel = [
    # "#it{#Delta#phi}(#it{b}_{lep},top_{had}) [rads]",
    # "#it{#Delta#phi}(extra_1,top_{had}) [rads]",
    # "#it{#Delta#phi}(extra_2,top_{had}) [rads]",
    # "#it{#Delta#phi}(extra_1,extra_2) [rads]",
    # "#it{p}_{T}^{extra_1} [GeV]",
    # "#it{p}_{T}^{extra_2}}[GeV]",
    # "#it{N}^{extrajets}",
    # "#it{m}^{extra_{1},top_{had}} [GeV]",
    # "#it{p}_{T}^{top_{lep}} [GeV]",
    # "#it{y}^{lep_{top}}",
    # "#it{#Delta#phi}(top_{had},top_{lep}) [rads]",
    # "#it{m}^{t#bar{t}} [GeV]",
    # "#it{p}_{T}^{t#bar{t}} [GeV]",
    # "#it{y}^{t#bar{t}}",
    # "#it{H}_{T}^{t#bar{t}} [GeV]",
    # "#it{H}_{T}^{t#bar{t}+jets} [GeV]",
    # "#it{p}_{T}^{top_{had}}[GeV]",
    # "#it{y}_{T}^{top_{had}}",

    # "#it{m}_{j} [GeV]",
    # "#it{m}_{W} [GeV]",

    # "#it{m}_{(lep-top)+j} [GeV]",
    "#it{#DeltaR}(lep-top,j)",

    # "#it{m}_{lep-top} [GeV]",


]
xmin = [
    # -3.15,
    # -3.15,
    # -3.15,
    # -3.15,
    # 0.0,
    # 0.0,
    # 0,
    # 0.0,
    # 0.0,
    # -2.5,
    # -3.15,
    # 350.0,
    # 0.0,
    # -2.5,
    # 0.0,
    # 0.0,
    # 0.0,
    # -2.5,

    # 135.0,
    # 60.0,

    # 120.0,
    0.0,

    # 100.0,

]
xmax = [
    # 3.15,
    # 3.15,
    # 3.15,
    # 3.15,
    # 1000.0,
    # 1000.0,
    # 9,
    # 1000.0,
    # 1000.0,
    # 2.5,
    # 3.15,
    # 2000.0,
    # 1000.0,
    # 2.5,
    # 2000.0,
    # 2000.0,
    # 1000.0,
    # 2.5,

    # 205.0,
    # 105.0,

    # 280.0,
    1.5,

    # 250.0,
]
#####

for i in range(0,len(samplePairs)):

    if samplePairs[i][0] == "ttpmp8Lineshape":
        histoName2 = "Lineshape"
    elif samplePairs[i][0] == "ttph721":
        histoName2 = "PWG+HWG7"
    elif samplePairs[i][0] == "ttph713":
        histoName2 = "PWG+HWG713"
    elif samplePairs[i][0] == "ttpp8Var_AFII":
        histoName2 = "Hdamp (AFII)"
    elif samplePairs[i][0] == "ttpp8RTT":
        histoName2 = "PWG+PY8, Recoil-to-top scheme"
    elif samplePairs[i][0] == "ttpp8pThard1":
        histoName2 = "pThard=1 (New)"
    elif samplePairs[i][0] == "ttpp8RTW":
        histoName2 = "Recoil-to-W"
    elif samplePairs[i][0] == "ttallpp8":
        histoName2 = "Nominal (FS)"
    elif samplePairs[i][0] == "ttpp8FSRup":
        histoName2 = "FSR down sample (flipped) (AFII)"
    elif samplePairs[i][0] == "ttpp8FSRdown":
        histoName2 = "FSR up sample (flipped) (AFII)"

    if samplePairs[i][1] == "ttpp8AF2":
        histoName1 = "PWG+PY8, recoilToColored=ON scheme"
    elif samplePairs[i][1] == "ttallpp8":
        histoName1 = "FSR down weight (FS)"
    elif samplePairs[i][1] == "ttph721":
        histoName1 = "PWG+HWG721"
    elif samplePairs[i][1] == "ttpp8pThard1":
        histoName1 = "pThard=1 (Original)"
    elif samplePairs[i][1] == "ttpp8RTT":
        histoName1 = "Recoil-to-top (Original)"

    for j in range(0,len(variables)):

        # if variables[j] == "nominal_topjet_m":
        #     histfile_nom_FS = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/ttallpp8/Histograms.root")
        #     histogram_nom_FS = histfile_nom_FS.Get(variables[j])
        #     histogram_nom_FS.SetDirectory(0)
        #     histfile_nom_FS.Close()
        #     nom_mean_FS = histogram_nom_FS.GetMean()

        #     histfile_nom_AFII = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/ttpp8AF2/Histograms.root")
        #     histogram_nom_AFII = histfile_nom_AFII.Get(variables[j])
        #     histogram_nom_AFII.SetDirectory(0)
        #     histfile_nom_AFII.Close()
        #     nom_mean_AFII = histogram_nom_AFII.GetMean()

        histfile1 = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + samplePairs[i][1] + "/dRVariable_MassPeakEvents_0-1Bins.root")
        histfile2 = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + samplePairs[i][0] + "/dRVariable_MassPeakEvents_0-1Bins.root")
        histogram1 = histfile1.Get(variables[j])
        histogram2 = histfile2.Get(variables[j])
        histogram1.SetDirectory(0)
        histogram2.SetDirectory(0)
        histogram1.SetStats(0)
        histogram2.SetStats(0)
        histogram1.SetLineWidth(2)
        histogram2.SetLineWidth(2)
        histfile1.Close()
        histfile2.Close()

        histogram1.Scale(1./histogram1.Integral(), "width")
        histogram2.Scale(1./histogram2.Integral(), "width")

        histogram1.SetLineColor(r.kBlack)
        histogram2.SetLineColor(r.kRed)

        histogram1.SetTitle("")
        histogram1.GetYaxis().SetTitle("Normalised events")
        histogram1.GetYaxis().SetTitleSize(0.05)
        histogram1.GetXaxis().SetTitle(xlabel[j])
        histogram1.GetYaxis().SetTitleOffset(1.05) #1.05
        histogram1.GetYaxis().SetLabelSize(0.05)

        minimums = []
        maximums = []
        minimums.append(histogram1.GetMinimum())
        maximums.append(histogram1.GetMaximum())
        minimums.append(histogram2.GetMinimum())
        maximums.append(histogram2.GetMaximum())
        globalMinY = min(minimums)
        globalMaxY = max(maximums)

        histogram1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
        histogram1.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
        histogram2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
        histogram2.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

        histogram1.GetXaxis().SetRangeUser(xmin[j],xmax[j])
        histogram2.GetXaxis().SetRangeUser(xmin[j],xmax[j])

        canvas_overlayed = r.TCanvas("canvas_overlayed")
        pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
        pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
        pad1.SetBottomMargin(0.00001)
        pad1.SetBorderMode(0)
        pad1.SetTicks()
        pad2.SetTopMargin(0.00001)
        pad2.SetBottomMargin(0.35)
        pad2.SetBorderMode(0)
        pad2.SetTicks()
        # pad1.SetGrid()
        # pad2.SetGrid()
        pad1.Draw()
        pad2.Draw()
        pad1.cd()

        histogram1.Draw()
        histogram2.Draw("same")

        latex = r.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.04)

        legend = r.TLegend(0.55,0.70,0.88,0.85) # 0.15,0.50,0.35,0.60 top left; 0.55,0.70,0.88,0.85 top right; 0.5,0.10,0.88,0.25 bottom right; 0.20,0.10,0.58,0.25 bottom left;
        legend.AddEntry(histogram1, histoName1)
        legend.AddEntry(histogram2, histoName2)
        legend.SetLineWidth(0)
        legend.SetFillStyle(0)
        legend.Draw("same")

        latex.DrawLatex(0.15,0.80,"#it{ATLAS} #bf{Simlation Internal}")
        latex.DrawLatex(0.15,0.74,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
        latex.DrawLatex(0.15,0.68,"#bf{t#bar{t} MC samples}")

        # latex.DrawLatex(0.13,0.80,"#it{ATLAS} #bf{Simlation Internal}")
        # latex.DrawLatex(0.13,0.74,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
        # latex.DrawLatex(0.13,0.68,"#bf{t#bar{t} MC samples}")

        # latex.DrawText(0.15,0.20,histoName1 + " mean = (%.3f +- %.3f) GeV" %(histogram1.GetMean(),histogram1.GetMeanError()))
        # latex.DrawText(0.15,0.15,histoName2 + " mean = (%.3f +- %.3f) GeV" %(histogram2.GetMean(),histogram2.GetMeanError()))

        # if variables[j] == "nominal_topjet_m":
        
        #     latex.DrawText(0.15,0.10,"Difference in mean between "+ histoName2 + " and nominal (AFII) = %.3f GeV" %(histogram2.GetMean()-nom_mean_AFII))
        #     latex.DrawText(0.15,0.05,"Difference in mean between "+ histoName1 + " and nominal (FS) = %.3f GeV" %(histogram1.GetMean()-nom_mean_FS))
        
        # latex.DrawText(0.55,0.70,"Nominal (FS, from original file) mean = %.4f +- %.4f" %(histogram1.GetMean(),histogram1.GetMeanError()))
        # latex.DrawText(0.55,0.65,"Nominal (FS, from new file) mean = %.4f +- %.4f" %(histogram2.GetMean(),histogram2.GetMeanError()))
        
        pad2.cd()

        r.gStyle.SetOptStat(0)
        subplot = histogram2.Clone()
        subplot.Divide(histogram1)
        subplot.SetMarkerStyle(5)
        subplot.SetMarkerSize(0.5)
        subplot.Draw("E1")
        subplot.SetTitle("")

        subplot.GetXaxis().SetLabelSize(0.14)
        subplot.GetXaxis().SetTitle(xlabel[j])
        subplot.GetXaxis().SetTitleSize(0.14)
        subplot.GetXaxis().SetTitleOffset(1.15)
        
        subplot.GetYaxis().SetLabelSize(0.14)
        subplot.GetYaxis().SetTitle("Ratio")
        subplot.GetYaxis().SetTitleOffset(0.35) #0.25
        subplot.GetYaxis().SetTitleSize(0.14)


        subplot.GetYaxis().SetNdivisions(207)
        subplot.GetYaxis().SetRangeUser(0.5,1.5)
        line = r.TLine(xmin[j],1,xmax[j],1)
        line.SetLineColor(r.kBlack)
        line.SetLineWidth(2)
        line.Draw("same")
        canvas_overlayed.Draw()
        canvas_overlayed.SaveAs("../histogram_images/2023/2023-10-31/" + samplePairs[i][0] + "_vs_" + samplePairs[i][1] + "_" + variables[j] + "_MassPeakEvents.pdf")

        # canvas_ratio = r.TCanvas("canvas_ratio")
        # canvas_ratio.SetTicks()
        # canvas_ratio.cd()

        # subplot.Draw()

        # # subplot.GetXaxis().SetLabelFont(63)
        # subplot.GetXaxis().SetLabelSize(0.05)
        # subplot.GetXaxis().SetTitle(xlabel[j])
        # subplot.GetXaxis().SetTitleSize(0.04)
        # subplot.GetXaxis().SetTitleOffset(1.15)
        
        # # subplot.GetYaxis().SetLabelFont(63)
        # subplot.GetYaxis().SetLabelSize(0.05)
        # # subplot.GetYaxis().SetTitle(samplePairs[i][0] + "/" + samplePairs[i][1])
        # subplot.GetYaxis().SetTitle("Ratio")
        # subplot.GetYaxis().SetTitleOffset(1.5)
        # subplot.GetYaxis().SetTitleSize(0.035)

        # line = r.TLine(xmin[j],1,xmax[j],1)
        # line.SetLineColor(r.kBlack)
        # line.SetLineWidth(2)
        # line.Draw("same")

        # latex = r.TLatex()
        # latex.SetNDC()
        # latex.SetTextSize(0.04)

        # latex.DrawLatex(0.15,0.80,"#it{ATLAS} #bf{Simlation Internal}")
        # latex.DrawLatex(0.15,0.74,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
        # latex.DrawLatex(0.15,0.68,"#bf{t#bar{t} MC samples}")

        # legend = r.TLegend(0.15,0.50,0.35,0.60)
        # legend.AddEntry(line, histoName1)
        # legend.AddEntry(subplot, histoName2)
        # legend.SetLineWidth(0)
        # legend.SetFillStyle(0)
        # legend.Draw("same")

        # canvas_ratio.Draw()
        # canvas_ratio.SaveAs("../histogram_images/2023/2023-10-18/NotNormalised/" + samplePairs[i][0] + "_vs_" + samplePairs[i][1] + "_" + variables[j] + "_VariablesForHerwig_ratio.pdf")


########################
########################
########################

# variables = ["nominal_topjet_m","nominal_topjet_W_invMass"]

# xlabel = ["Mass [GeV]", "Mass [GeV]"]
# xmin = [135.0, 40.0]
# xmax = [205.0, 120.0]

# for j in range(0,len(variables)):
#     histfile1 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttallpp8/PlotsForComparison.root")
#     histfile2 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttpp8FSRup_weight/mJ_mW_nominal20Bins.root")
#     histfile3 = r.TFile.Open("../output_root_files/2023-04-12-Profiling/ttpp8FSRdown_weight/mJ_mW_nominal20Bins.root")
#     histogram1 = histfile1.Get(variables[j])
#     histogram2 = histfile2.Get(variables[j])
#     histogram3 = histfile3.Get(variables[j])
#     histogram1.SetDirectory(0)
#     histogram2.SetDirectory(0)
#     histogram3.SetDirectory(0)
#     histogram1.SetStats(0)
#     histogram2.SetStats(0)
#     histogram3.SetStats(0)
#     histogram1.SetLineWidth(2)
#     histogram2.SetLineWidth(2)
#     histogram3.SetLineWidth(2)
#     histfile1.Close()
#     histfile2.Close()
#     histfile3.Close()

#     histogram1.Scale(1./histogram1.Integral(), "width")
#     histogram2.Scale(1./histogram2.Integral(), "width")
#     histogram3.Scale(1./histogram3.Integral(), "width")

#     histogram1.SetLineColor(r.kBlack)
#     histogram2.SetLineColor(r.kRed)
#     histogram3.SetLineColor(r.kBlue)

#     histogram1.SetTitle("")
#     histogram1.GetYaxis().SetTitle("Normalised events")
#     histogram1.GetXaxis().SetTitle(xlabel[j])
#     histogram1.GetYaxis().SetTitleOffset(1.4)

#     minimums = []
#     maximums = []
#     minimums.append(histogram1.GetMinimum())
#     maximums.append(histogram1.GetMaximum())
#     minimums.append(histogram2.GetMinimum())
#     maximums.append(histogram2.GetMaximum())
#     minimums.append(histogram3.GetMinimum())
#     maximums.append(histogram3.GetMaximum())
#     globalMinY = min(minimums)
#     globalMaxY = max(maximums)

#     histogram1.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram1.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram2.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram2.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram3.GetYaxis().SetRangeUser(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)
#     histogram3.GetYaxis().SetLimits(globalMinY,globalMaxY + (globalMaxY-globalMinY)/10)

#     canvas_overlayed = r.TCanvas("canvas_overlayed")
#     pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
#     pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
#     pad1.SetBottomMargin(0.00001)
#     pad1.SetBorderMode(0)
#     pad2.SetTopMargin(0.00001)
#     pad2.SetBottomMargin(0.2)
#     pad2.SetBorderMode(0)
#     pad1.SetGrid()
#     pad2.SetGrid()
#     pad1.Draw()
#     pad2.Draw()
#     pad1.cd()

#     histogram1.Draw()
#     histogram2.Draw("same")
#     histogram3.Draw("same")

#     latex = r.TLatex()
#     latex.SetNDC()
#     latex.SetTextSize(0.04)

#     legend = r.TLegend(0.15,0.50,0.3,0.60)
#     legend.AddEntry(histogram1, "Nomainal (FS)")
#     legend.AddEntry(histogram2, "FSR up (weight)")
#     legend.AddEntry(histogram3, "FSR down (weight)")
#     legend.SetLineWidth(0)
#     legend.SetFillStyle(0)
#     legend.Draw("same")

#     latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
#     latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
#     latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")

#     latex.DrawText(0.55,0.70,"Nomnial mean = %.4f +- %.4f" %(histogram1.GetMean(),histogram1.GetMeanError()))
#     latex.DrawText(0.55,0.65,"FSRup mean = %.4f +- %.4f" %(histogram2.GetMean(),histogram2.GetMeanError()))
#     latex.DrawText(0.55,0.60,"FSRdown mean = %.4f +- %.4f" %(histogram3.GetMean(),histogram3.GetMeanError()))
    
#     pad2.cd()

#     r.gStyle.SetOptStat(0)
#     subplot = histogram2.Clone()
#     subplot.Divide(histogram1)
#     subplot.SetMarkerStyle(5)
#     subplot.SetMarkerSize(0.5)
#     subplot.Draw("E1")
#     subplot.SetTitle("")
#     subplot.GetXaxis().SetLabelFont(63)
#     subplot.GetXaxis().SetLabelSize(10)
#     subplot.GetXaxis().SetTitle(xlabel[j])
#     subplot.GetXaxis().SetTitleSize(0.11)
#     subplot.GetXaxis().SetTitleOffset(0.8)
#     subplot.GetYaxis().SetLabelFont(63)
#     subplot.GetYaxis().SetLabelSize(10)
#     subplot.GetYaxis().SetTitle("FSR/Nominal")
#     subplot.GetYaxis().SetTitleOffset(0.38)
#     subplot.GetYaxis().SetTitleSize(0.11)
#     subplot.GetYaxis().SetNdivisions(207)
#     subplot.GetYaxis().SetRangeUser(0.75,1.25)

#     subplot1 = histogram3.Clone()
#     subplot1.Divide(histogram1)
#     subplot1.SetMarkerStyle(5)
#     subplot1.SetMarkerSize(0.5)
#     subplot1.Draw("same")
#     subplot1.SetTitle("")
#     subplot1.GetXaxis().SetLabelFont(63)
#     subplot1.GetXaxis().SetLabelSize(10)
#     subplot1.GetXaxis().SetTitle(xlabel[j])
#     subplot1.GetXaxis().SetTitleSize(0.11)
#     subplot1.GetXaxis().SetTitleOffset(0.8)
#     subplot1.GetYaxis().SetLabelFont(63)
#     subplot1.GetYaxis().SetLabelSize(10)
#     subplot1.GetYaxis().SetTitle("FSR/Nominal")
#     subplot1.GetYaxis().SetTitleOffset(0.38)
#     subplot1.GetYaxis().SetTitleSize(0.11)
#     subplot1.GetYaxis().SetNdivisions(207)
#     subplot1.GetYaxis().SetRangeUser(0.75,1.25)

#     line = r.TLine(xmin[j],1,xmax[j],1)
#     line.SetLineColor(r.kBlack)
#     line.SetLineWidth(2)
#     line.Draw("same")
#     canvas_overlayed.Draw()
#     canvas_overlayed.SaveAs("../histogram_images/2023-06-30/FSRweight_vs_NominalFS_" + variables[j] + ".pdf")

print("\nFinished\n")