# A macro that will use TMinuit to minimise chi^2 while finding m_t as a fit parameter for the fit function found from Scatter.py for the input MC data.
# I assumed the relationship between average_m_j and m_t would be the same for the data, where here I am using the error in the average_m_j for data.
# First created by Elliot Watton - 14/10/2021
# Last editted by Elliot Watton - 15/10/2021

# Imports
from ROOT import TMinuit , Double , Long
import numpy as np
from array import array as arr
r.TH1.SetDefaultSumw2(True)
# Define data 
average_m_j = 169.23913561       # m_j
average_m_j_err = 0.061489527    # sigma_{m_j}
grad = 0.482928929               # s
intercept = 85.53121052          # c
npar = 1                         # Number of fit parameters

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = grad * m_t + intercept
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)
    
# Set up Minuit
myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
myMinuit.SetFCN(fcn)          # Sets the function to minimise
arglist = arr('d', 2*[0.01])  # Sets the error definition
ierflg = Long(0)
arglist[0] = 1.               # 1 sigma is Delta chi^2
myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

# Set starting values and step size for fit parameter, m_t
myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
arglist[0] = 6000 # Number of calls for FCN before giving up
arglist[1] = 0.3  # Tolerance
myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

# Check TMinuit status
amin, edm, errdef = Double(0.), Double(0.), Double(0.)
nvpar, nparx, icstat = Long(0), Long(0), Long(0)
myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

""" meaning of parameters:
    amin:   value of fcn distance at minimum ( = chi^2) 
    edm:    estimated distance to minimum
    errdef: delta_fcn used to define 1 sigma errors
    nvpar:  total number of parameters
    icstat: status of error matrix:
            3 = accurate
            2 = forced pos. def
            1 = approximate
            0 = not calculated
"""
myMinuit.mnprin(3,amin) # Print-out by Minuit

# Get results from Minuit
p, pe = Double(0), Double(0)
myMinuit.GetParameter(0, p, pe)
final_m_t = p
final_m_t_err = pe
    
print('')
print('*==* MINUIT fit completed:')
print 'error code = ', ierflg
print 'status = ', icstat
print('')
print('Results:')
print('\t m_t = (%.4f +/- %.4f) GeV/c^2' %(final_m_t, final_m_t_err))
print('')


