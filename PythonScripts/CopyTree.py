import ROOT as r
from array import array
from optparse import OptionParser

r.TH1.SetDefaultSumw2(True)

def CopyTree(inputFilename,outputFilename,isFSRup,isFSRdown):

    if isFSRup:
        DSIDs = [601670,601672]
    elif isFSRdown:
        DSIDs = [601669,601671]
    else:
        print("Something is wrong with the setup!")
        exit()

    inputFile = r.TFile.Open(inputFilename,"READ")
    trees = []
    for h in inputFile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree') and (('nominal' in h.GetName()) or ('particleLevel' in h.GetName()) or ('sumWeights' in h.GetName()) or ('truth' in h.GetName())):
            tree = h
            trees.append(tree)

    outputFile = r.TFile.Open(outputFilename,"update")
    for tree in trees:
        print("Processing TTree: ", tree)
        outputTree = tree.CloneTree(0)
        if 'sumWeights' in tree.GetName():
            branch = "dsid"
        else:
            branch = "mcChannelNumber"
        for i in range(0, tree.GetEntries()):
            tree.GetEntry(i)
            DSID = getattr(tree, branch)
            if (DSID in DSIDs):
                outputTree.Fill()
        outputTree.AutoSave()
    outputFile.Close()

def main():

    parser = OptionParser()
    
    parser.add_option("-i", "--infile",
                  action="store", type="string", dest="inputFilename")
    parser.add_option("-o", "--outfile",
                  action="store", type="string", dest="outputFilename")
    parser.add_option("-u", "--FSRup", dest="FSRup", action = "store_true", default = False,
                        help="Copy and fix FSR up files")
    parser.add_option("-d", "--FSRdown", dest="FSRdown", action = "store_true", default = False,
                        help="Copy and fix FSR down files")

    (options, args) = parser.parse_args()  
    print(options)    

    CopyTree(options.inputFilename,options.outputFilename,options.FSRup,options.FSRdown)
    
if __name__ == "__main__":
    main()