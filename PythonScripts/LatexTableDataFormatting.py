inputdata1 = [0.7603,0.2397,0.7120,0.0000,6.7785,0.0000,80.7167,0.0000,18.6508,0.0000,172.5000,0.0000]
inputdata2 = [0.9800,0.0011,0.5351,0.0236,6.7182,0.1512,79.7505,0.3291,19.0947,0.9529,172.4716,0.2287]
inputdata3 = [0.9900,0.0012,0.5143,0.0235,6.6611,0.1523,80.3744,0.2826,18.2692,0.7986,172.4693,0.2326]
inputdata4 = [0.9997,0.0012,0.5215,0.0232,6.7692,0.1501,80.7293,0.2971,18.6029,0.8301,172.5016,0.2330]
inputdata5 = [1.0098,0.0012,0.5109,0.0226,6.7758,0.1486,81.2801,0.2744,18.2381,0.7374,172.4198,0.2374]
inputdata6 = [1.0200,0.0012,0.5120,0.0220,6.8414,0.1465,81.8183,0.2738,18.3613,0.7220,172.4120,0.2418]
inputdata7 = [0.7603,0.2397,0.7120,0.0000,6.7785,0.0000,80.7167,0.0000,18.6508,0.0000,172.5000,0.0000]
inputdata8 = [1.0016,0.0011,0.4984,0.0213,6.5754,0.1416,81.3377,0.2242,17.7277,0.6459,168.5948,0.2181]
inputdata9 = [1.0028,0.0011,0.5072,0.0216,6.7303,0.1425,80.7238,0.2728,18.2249,0.7087,170.6616,0.2244]
inputdata10 = [1.0013,0.0010,0.5254,0.0213,6.7093,0.1426,80.7705,0.3168,18.7691,0.9105,171.9891,0.2028]
inputdata11 = [1.0002,0.0011,0.5180,0.0220,6.7078,0.1432,80.9086,0.2688,18.3828,0.7585,172.9974,0.2210]
inputdata12 = [1.0009,0.0011,0.5305,0.0217,6.7441,0.1398,80.6231,0.3003,18.7757,0.8163,173.8981,0.2213]
inputdata13 = [1.0015,0.0011,0.5016,0.0204,6.5425,0.1363,80.4791,0.2677,17.9562,0.6541,175.7243,0.2274]


inputdatalists = [
    inputdata1,
    inputdata2,
    inputdata3,
    inputdata4,
    inputdata5,
    inputdata6,
    inputdata7,
    inputdata8,
    inputdata9,
    inputdata10,
    inputdata11,
    inputdata12,
    inputdata13,
    ]

# SampleName = [
#     "Powheg+Pythia8",
#     "Powheg+Herwig7.1.3",
#     "Powheg+Pythia8, MEC turned off",
#     "Powheg+Pythia8, colour reconnection",
#     "Powheg+Pythia8, recoil-to-top",
#     "aMC@NLO+Pythia8",
#     "aMC@NLO+Herwig7.1.3",
#     "Powheg+Pythia8",
#     "Powheg+Pythia8, varied $h_\text{damp}$",
#     "Powheg+Herwig7.2.1"
#     ]


# FastorFull = [
#     "Fast",
#     "Fast",
#     "Fast",
#     "Fast",
#     "Fast",
#     "Fast",
#     "Fast",
#     "Full",
#     "Full",
#     "Full",
#     ]

# BigTalbe = False

# for j in range(0,len(inputdatalists)):

#     if BigTalbe == True:
#         if SampleName[j] != "Powheg+Pythia8, MEC turned off" and SampleName[j] != "Powheg+Pythia8, recoil-to-top" and SampleName[j] != "Powheg+Pythia8, colour reconnection" and SampleName[j] != "Powheg+Pythia8, varied $h_\text{damp}$":
#             outputstring = ""
#             for i in range(0,len(inputdatalists[j])):
#                 if i == 0:
#                     outputstring += (SampleName[j] + " & " + FastorFull[j] + " & ")
#                 if i % 2 == 0 and i != len(inputdatalists[j])-1:
#                     outputstring += (str(inputdatalists[j][i]) + "$\pm$")
#                 elif i == len(inputdatalists[j])-1:
#                     outputstring += (str(inputdatalists[j][i]) + "\\\\")
#                 else:
#                     outputstring += (str(inputdatalists[j][i]) + " & ")
#         else:
#             outputstring = "\multirow{2}{*}{"
#             for i in range(0,len(inputdatalists[j])):
#                 if i == 0:
#                     outputstring += (SampleName[j] + "} & \multirow{2}{*}{" + FastorFull[j] + "} & \multirow{2}{*}{")
#                 if i % 2 == 0 and i != len(inputdatalists[j])-1:
#                     outputstring += (str(inputdatalists[j][i]) + "$\pm$")
#                 elif i == len(inputdatalists[j])-1:
#                     outputstring += (str(inputdatalists[j][i]) + "}\\\\")
#                 else:
#                     outputstring += (str(inputdatalists[j][i]) + "} & \multirow{2}{*}{")
#     else:
#         outputstring = ""
#         for i in range(0,len(inputdatalists[j])):
#             if i == 0:
#                 outputstring += (SampleName[j] + " & " + FastorFull[j] + " & ")
#             if i % 2 == 0 and i != len(inputdatalists[j])-1:
#                 outputstring += (str(inputdatalists[j][i]) + "$\pm$")
#             elif i == len(inputdatalists[j])-1:
#                 outputstring += (str(inputdatalists[j][i]) + "\\\\")
#             else:
#                 outputstring += (str(inputdatalists[j][i]) + " & ")
    
#     print(outputstring)

for j in range(0,len(inputdatalists)):
    outputstring = ""
    for i in range(0,len(inputdatalists[j])):
        if i == 0:
            outputstring += (" &  & ")
        if i % 2 == 0 and i != len(inputdatalists[j])-1:
            outputstring += (str(inputdatalists[j][i]) + "$\pm$")
        elif i == len(inputdatalists[j])-1:
            outputstring += (str(inputdatalists[j][i]) + "\\\\")
        else:
            outputstring += (str(inputdatalists[j][i]) + " & ")
    print(outputstring)