# A macro that will find the average top jet mass from previously made histograms, and then use TMinuit to find the top quark mass.
# This will use the linear relation found previously.
# First created by Elliot Watton - 18/10/2021
# Last editted by Elliot Watton - 19/10/2021

import ROOT as r
from ROOT import TMinuit , Double , Long
import numpy as np
from array import array as arr


########
######## Function definitions
########



def CalcMeanIntegralStd(hist_name):
    
    # Make a variable that will track the sum in mean calculation
    total = 0
    total_num_of_entries = 0
    
    print('Calculating the mean, number of entries and integral for:')
    print(hist_name)
    
    # Go over all bins
    for i in range(1,hist_name.GetNbinsX()+1):
    
        # Get the midpoint of each bin for the mid value of that bin
        mid_value = hist_name.GetBinCenter(i)

        # Get how many entries there are in the bin
        num_of_entries_in_bin = hist_name.GetBinContent(i)

        # Add the number of entries in this bin to the total
        total_num_of_entries += num_of_entries_in_bin
    
        # Add the product of the previous variables to the total
        total += (mid_value * num_of_entries_in_bin)

    # When the for loop has finished, must divide the total by number of entries
    average = total / total_num_of_entries
    print('The average is:')
    print(average)
    #print('The error on this average is:')
    #print(hist_name.GetMeanError())
    print('The integral of the histogram is:')
    print(hist_name.Integral(1,hist_name.GetNbinsX()))
    print('The standard deviation is:')
    print(hist_name.GetStdDev())
    print('')
    
    # Get error on mean_m_j
    error_on_mean_m_j = hist_name.GetStdDev()/np.sqrt(hist_name.Integral(1,hist_name.GetNbinsX()))
    
    return average, error_on_mean_m_j


# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = grad * m_t + intercept
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global grad 
    grad = s
    global intercept
    intercept = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe

    return final_m_t, final_m_t_err



########
######## Main body of code
########

print('')

# Initialise global variables 
average_m_j = 0       # m_j
average_m_j_err = 0   # sigma_{m_j}
grad = 0              # s
intercept = 0         # c
npar = 1              # Number of fit parameters

# Open the file containing previously made histograms
histfile = r.TFile.Open("output_root_files/output_modelling_ttpp8Var.root")

# Get the histograms from the file
histograms = []
histogram_names = []
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    #substring = 'CategoryReduction_JET_'
    #if (substring in h.GetName()):
    hist = histfile.Get(h.GetName())
    histograms.append(hist)
    histogram_names.append(h.GetName())

# Change the histogram directories so that they do not disappear when histogram file is closed
for i in range(0,len(histograms)):
    histograms[i].SetDirectory(0)

# Close the histogram file
histfile.Close()

mean_m_j_values = []
mean_m_j_values_err = []

# Find and print the mean, number of entries used in mean calculation, integral, and then store the information
for i in range(0,len(histograms)):
    mean_m_j_to_append, mean_m_j_err_to_append = 0, 0
    mean_m_j_to_append, mean_m_j_err_to_append = CalcMeanIntegralStd(histograms[i])
    mean_m_j_values.append(mean_m_j_to_append)
    mean_m_j_values_err.append(mean_m_j_err_to_append)

# Data for gradient and y-intercept
grad = 0.482928928781
intercept = 85.5312105223

m_t_values = []
m_t_err_values = []

# Now find the corresponding m_t for each and print it to the screen
for i in range(0,len(histograms)):
    m_t, m_t_err = 0, 0
    m_t, m_t_err = getm_t(mean_m_j_values[i], mean_m_j_values_err[i], grad, intercept)
    m_t_values.append(m_t)
    m_t_err_values.append(m_t_err)

# Print results
print('')
print('FINAL RESULTS (all mass values in units of GeV/c^2):')
print('')
for i in range(0,len(m_t_values)):
    print('%s:  \t%.4f +/- %.4f ' %(histogram_names[i], m_t_values[i], m_t_err_values[i]))
print('')

# Write results to a text file called 'Systematics_results_JES.txt'
file = open("Modelling_results_ttpp8Var.txt","w")
for i in range(0,len(m_t_values)):
    file.write(("%s\t%s\t%s\n") % (histogram_names[i], m_t_values[i], m_t_err_values[i]))
file.close()
