# A macro that will find the average top jet mass from previously made histograms, and then use TMinuit to find the top quark mass.
# This will use the linear relation found previously.
# First created by Elliot Watton - 18/10/2021
# Last editted by Elliot Watton - 14/12/2021

import ROOT as r
from ROOT import TMinuit , Double , Long
from array import array as arr
from array import array
import math
r.TH1.SetDefaultSumw2(True)

########
######## Function definitions
########

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = A + (B*(m_t - 172.5))
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global B
    B = s
    global A
    A = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 172.5, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe

    return final_m_t, final_m_t_err

########
######## Main body of code
########

print('')
samples = [
    "JSF_1_00_ttpp8AF2",
    "JSF_0_97_ttpp8AF2",
    "JSF_0_98_ttpp8AF2",
    "JSF_0_99_ttpp8AF2",
    "JSF_1_01_ttpp8AF2",
    "JSF_1_02_ttpp8AF2",
    "JSF_1_03_ttpp8AF2",
    "ttpp8m169",
    "ttpp8m171",
    "ttpp8m172",
    "ttpp8m173",
    "ttpp8m174",
    "ttpp8m176",
    "ttph713",
    "ttpp8MECOff",
    "ttpp8CR0",
    "ttpp8CR1",
    "ttpp8CR2",
    "ttpp8RTT",
    "ttnfaMCatNLO",
    "ttMCatNLOH713",
    "ttpp8m172_25",
    "ttpp8m172_75",
    "ttallpp8",
    "ttpp8Var",
    "radiation_up",
    "radiation_down",
    "fsr_up",
    "fsr_down",
    "ttph721",
]

strBeginning = [
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.00 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.97 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.98 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 0.99 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.01 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.02 & Fast & ",
    "Powheg+Pythia8, 172.5 GeV, JSF = 1.03 & Fast & ",
    "Powheg+Pythia8, 169 GeV & Fast & ",
    "Powheg+Pythia8, 171 GeV & Fast & ",
    "Powheg+Pythia8, 172 GeV & Fast & ",
    "Powheg+Pythia8, 173 GeV & Fast & ",
    "Powheg+Pythia8, 174 GeV & Fast & ",
    "Powheg+Pythia8, 176 GeV & Fast & ",
    "Powheg+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, MEC turned off & Fast & ",
    "Powheg+Pythia8, CR0 & Fast & ",
    "Powheg+Pythia8, CR1 & Fast & ",
    "Powheg+Pythia8, CR2 & Fast & ",
    "Powheg+Pythia8, recoil-to-top & Fast & ",
    "aMC@NLO+Pythia8 & Fast & ",
    "aMC@NLO+Herwig7.1.3 & Fast & ",
    "Powheg+Pythia8, 172.25 GeV & Fast & ",
    "Powheg+Pythia8, 172.75 GeV & Fast & ",
    "Powheg+Pythia8 & Full & ",
    "Powheg+Pythia8, varied $h_\text{damp}$ & Full & ",
    "Powheg+Pythia8, ISR_up & Full & ",
    "Powheg+Pythia8, ISR_down & Full & ",
    "Powheg+Pythia8, FSR_up & Full & ",
    "Powheg+Pythia8, FSR_down & Full & ",
    "Powheg+Herwig7.2.1 & Full & ",
]

for q in range(0,len(samples)):

    # Initialise global variables 
    average_m_j = 0       # m_j
    average_m_j_err = 0   # sigma_{m_j}
    A = 0
    B = 0
    npar = 1              # Number of fit parameters

    mJ_array = []
    mJ_err_array = []
    mJ_err_array_MC = []
    histogram_names = []

    useDataLikeErrors = True

    # Finding mJ and mJ_err 
    if (samples[q] == "ttallpp8"):
        histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
    elif ("JSF" in samples[q]):
        histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttpp8AF2/mJ/" + samples[q] + ".root"
    elif (samples[q] == "radiation_up") or (samples[q] == "radiation_down") or (samples[q] == "fsr_up") or (samples[q] == "fsr_down"):
        histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_" + samples[q] + ".root"
    else:
        histogram_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + samples[q] + "/All/JSF_1_00_" + samples[q] + "_ALL.root"
    # histogram_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/" + samples[q] + "/mJ/JSF_1_00_" + samples[q] + "_JERSystematicsCombinedHistograms.root"
    histogram_root_file = r.TFile.Open(histogram_path)
    for h in histogram_root_file.GetListOfKeys():
        if h.GetName() == "nominal_topjet_m":
            h = h.ReadObj()
            hist = histogram_root_file.Get(h.GetName())
            histogram_names.append(h.GetName().rstrip("_topjet_m"))
            # histogram_names.append(h.GetName().rstrip("_mJ_combined"))
            mJ_array.append(hist.GetMean())
            if useDataLikeErrors == True:
                mJ_err_array.append(hist.GetStdDev()/math.sqrt(hist.Integral()))
            else:
                mJ_err_array.append(hist.GetMeanError())
            mJ_err_array_MC.append(hist.GetMeanError())

    # Values of the constants in the equations for mJ and mW
    
    # FAST SIM NUMBERS
    A = 168.832795
    B = 0.476824

    # FULL SIM NUMBER
    # A = 169.246951
    # B = 0.457656

    # Close the histogram file
    histogram_root_file.Close()

    m_t_values = []
    m_t_err_values = []
    m_t_err_values_MC = []

    # Now find the corresponding m_t for each and print it to the screen
    for i in range(0,len(mJ_array)):
        m_t, m_t_err = getm_t(mJ_array[i], mJ_err_array[i], B, A)
        m_t_values.append(m_t)
        m_t_err_values.append(m_t_err)
    
    for i in range(0,len(mJ_array)):
        m_t, m_t_err = getm_t(mJ_array[i], mJ_err_array_MC[i], B, A)
        m_t_err_values_MC.append(m_t_err)

    # # Write results to a text file
    # outputfile = "../OutputTextFiles/MinimisationResults.txt"
    # for i in range(0,len(m_t_values)):
    #     lines = [mJ_array[i], mJ_err_array[i], m_t_values[i], m_t_err_values[i]]
    #     f = open(outputfile, 'a')
    #     f.write(samples[q])
    #     f.write('\t')
    #     for line in lines:
    #         f.write('\t')
    #         f.write('%f' %line)
    #     f.write('\n')
    # f.close()

    outputfile = '../OutputTextFiles/MinimisationResults.txt'
    f = open(outputfile, 'a')
    for i in range(0,len(m_t_values)):
        f.write(strBeginning[q])
        f.write('%.3f & %.3f & %.3f\\\\' %(m_t_values[i],m_t_err_values_MC[i],m_t_err_values[i]))
        f.write('\n')
    f.close()


