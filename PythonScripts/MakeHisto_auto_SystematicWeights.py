from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand,weightToSwapOutName,systematicWeightName,systematicWeightNamesVectorElement):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())
    print('Swapping out weight:')
    print(weightToSwapOutName)
    print('with:')
    print(systematicWeightName)
    print("")
   
    hadToSkipEvents = False
    numberOfEventsSkipped = 0

    if ("eigen" in systematicWeightName):
        systematicWeightName = systematicWeightName.replace("_" + systematicWeightNamesVectorElement + "_", "_")

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        
        tree_name.GetEntry(i)
        
        if (isRecoLevel == True):
            
            baseWeight = tree_name.weight_normalise
            weightToSwapOut = getattr(tree_name, weightToSwapOutName)
            systematicWeight = getattr(tree_name, systematicWeightName)

            if ("eigen" in systematicWeightName):
                systematicWeight = systematicWeight[int(systematicWeightNamesVectorElement)]

            if weightToSwapOut == 0: # Had to put this in for some of the weight_pileup values for entries
                numberOfEventsSkipped = numberOfEventsSkipped + 1
                hadToSkipEvents = True
                continue

            #print("\nweight_normalise:\t%f" %(baseWeight))
            #print("weightToSwapOut:\t%f" %(weightToSwapOut))
            #print("systematicWeight:\t%f" %(systematicWeight))

            weight = (baseWeight/weightToSwapOut) * systematicWeight
        else:
            baseWeight = tree_name.weight_normalise
            weightToSwapOut = getattr(tree_name, weightToSwapOutName)
            systematicWeight = getattr(tree_name, systematicWeightName)
            weight = (baseWeight/weightToSwapOut) * systematicWeight
    
        variable = getattr(tree_name, variable_name)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight )
            else:
                hist_name.Fill(variable / 1000.0, weight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight )
            else:
                hist_name.Fill(variable, weight)

    if hadToSkipEvents == True:
        print("WARNING There were %i events with 0 weight. These were skipped to avoid division by zero errors.\n" %(numberOfEventsSkipped))


def MakeHisto_auto(filePathSegment,sample,useMC2MC_SF):

    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    if (useMC2MC_SF == False):
        output_file_path = "../output_root_files/2023-01-13-NewJESFlavourUncertainties/" + sample + "/mJ/JSF_1_00_" + sample + "_otherSystematicWeights_50bins.root"
    else:
        output_file_path = "../output_root_files/2022-07-06-CorrectedOutputROOTFiles/" + sample + "_MC2MC/All/JSF_1_00_" + sample + ".root"

    print('')
    print('Grabbing the TTree from file: %s' %(input_file_path))
    print('')
    print('Saving the histograms to file: %s' %(output_file_path))
    print('')
    
    infile = TFile.Open(input_file_path,"READ");

    # Get nominal tree
    """ for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree' and h.GetName() == 'nominal'):
            tree = h """

    # Get all trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree') and (h.GetName() == 'nominal'):
            tree = h
            trees.append(tree)

    print('')
    print('The list of TTrees read in from %s:\n' %(input_file_path))
    for treename in trees:
        print(treename)
        print('')
    print('The total number of trees read in is: %i' %(len(trees)))

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable = [
        # "topjet_pt",
        #"topjet_eta",
        #"topjet_phi",
        "topjet_m",
        #"topjet_y",
        #"topjet_nSubjets",
        #"topjet_avgSubjet_pt",
        #"topjet_avgSubjet_eta",
        # "topjet_W_invMass",
        #"topjet_nbtaggedSubjets",
        #"topjet_Subjet_eta",
        #"topjet_Subjet_phi",
        #"topjet_Subjet_pT",
        #"topjet_Subjet_mass",
        #"topjet_Subjet_type",
        #"topjet_Subjet_DeltaR",
        #"topjet_Subjet_isMatched",
        #"topjet_Subjet_jetresponse",
        #"topjet_Subjet_bjetresponse",
        #"topjet_Subjet_lightjetresponse"
    ]

    variableType = [
        #"float",
        #"float",
        #"float",
        "float",
        #"float",
        #"int",
        #"float",
        #"float",
        # "float",
        #"int",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector",
        #"vector"
    ]

    isNeedingDivisionByOneThousand = [
        #True,
        #False,
        #False,
        True,
        #False,
        #False,
        #True,
        #False,
        # True,
        #False,
        #False,
        #False,
        #True,
        #True,
        #False,
        #False,
        #False,
        #False,
        #False,
        #False
    ]

    weightsToSwapOutNames = [
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_jvt",
        "weight_jvt",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_leptonSF",
        "weight_pileup",
        "weight_pileup",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
        "weight_bTagSF_DL1r_77",
    ]

    systematicWeightNames = [
        "weight_bTagSF_DL1r_77_extrapolation_down",
        "weight_bTagSF_DL1r_77_extrapolation_up",
        "weight_bTagSF_DL1r_77_extrapolation_from_charm_down",
        "weight_bTagSF_DL1r_77_extrapolation_from_charm_up",
        "weight_jvt_DOWN",
        "weight_jvt_UP",
        "weight_leptonSF_EL_SF_ID_DOWN",
        "weight_leptonSF_EL_SF_ID_UP",
        "weight_leptonSF_EL_SF_Isol_DOWN",
        "weight_leptonSF_EL_SF_Isol_UP",
        "weight_leptonSF_EL_SF_Reco_DOWN",
        "weight_leptonSF_EL_SF_Reco_UP",
        "weight_leptonSF_EL_SF_Trigger_DOWN",
        "weight_leptonSF_EL_SF_Trigger_UP",
        "weight_leptonSF_MU_SF_ID_STAT_DOWN",
        "weight_leptonSF_MU_SF_ID_STAT_UP",
        "weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
        "weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP",
        "weight_leptonSF_MU_SF_ID_SYST_DOWN",
        "weight_leptonSF_MU_SF_ID_SYST_UP",
        "weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
        "weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP",
        "weight_leptonSF_MU_SF_Isol_STAT_DOWN",
        "weight_leptonSF_MU_SF_Isol_STAT_UP",
        "weight_leptonSF_MU_SF_Isol_SYST_DOWN",
        "weight_leptonSF_MU_SF_Isol_SYST_UP",
        "weight_leptonSF_MU_SF_TTVA_STAT_DOWN",
        "weight_leptonSF_MU_SF_TTVA_STAT_UP",
        "weight_leptonSF_MU_SF_TTVA_SYST_DOWN",
        "weight_leptonSF_MU_SF_TTVA_SYST_UP",
        "weight_leptonSF_MU_SF_Trigger_STAT_DOWN",
        "weight_leptonSF_MU_SF_Trigger_STAT_UP",
        "weight_leptonSF_MU_SF_Trigger_SYST_DOWN",
        "weight_leptonSF_MU_SF_Trigger_SYST_UP",
        "weight_pileup_DOWN",
        "weight_pileup_UP",
        "weight_bTagSF_DL1r_77_eigenvars_B_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_3_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_4_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_4_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_5_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_5_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_6_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_6_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_7_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_7_up",
        "weight_bTagSF_DL1r_77_eigenvars_B_8_down",
        "weight_bTagSF_DL1r_77_eigenvars_B_8_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_C_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_C_3_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_0_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_0_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_1_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_1_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_2_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_2_up",
        "weight_bTagSF_DL1r_77_eigenvars_Light_3_down",
        "weight_bTagSF_DL1r_77_eigenvars_Light_3_up"
    ]

    systematicWeightNamesVectorElement = [
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3",
        "4",
        "4",
        "5",
        "5",
        "6",
        "6",
        "7",
        "7",
        "8",
        "8",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3",
        "0",
        "0",
        "1",
        "1",
        "2",
        "2",
        "3",
        "3"
    ]

    for z in range(0,len(trees)):
        for k in range(0,len(systematicWeightNames)):
        #for k in range(0,1):
            
            histograms = []
            
            for i in range(0,len(variable)):
                name = trees[z].GetName() + "_" + variable[i] + systematicWeightNames[k]

                if (variable[i] == "topjet_pt"):
                    hist = TH1D(name, "h_topjet_pt; pT [GeV]; Events", 200, 355.0, 1000.0)
                elif (variable[i] == "topjet_eta"):
                    hist = TH1D(name, "h_topjet_eta; eta; Events", 200, -2.5, 2.5)
                elif (variable[i] == "topjet_phi"):
                    hist = TH1D(name, "h_topjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                elif (variable[i] == "topjet_m"):
                    hist = TH1D(name, "h_topjet_m; Mass [GeV]; Events", 50, 135.0, 205.0)
                elif (variable[i] =="topjet_y"):
                    hist = TH1D(name, "h_topjet_y; y; Events", 200, -2.5, 2.5)
                elif (variable[i] =="topjet_nSubjets"):
                    hist = TH1D(name, "h_topjet_nSubjets; Number of sub-jets; Events", 8, 0, 7)
                elif (variable[i] =="topjet_avgSubjet_pt"):
                    hist = TH1D(name, "h_topjet_avgSubjet_pt; pT [GeV]; Events", 200, 0.0, 900.0)
                elif (variable[i] =="topjet_avgSubjet_eta"):
                    hist = TH1D(name, "h_topjet_avgSubjet_eta; eta; Events", 200, -2.5, 2.5)
                elif (variable[i] =="topjet_W_invMass"):
                    hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV]; Events", 200, 0.0, 200.0)
                elif (variable[i] =="topjet_nbtaggedSubjets"):
                    hist = TH1D(name, "h_topjet_nbtaggedSubjets; Number of b tagged jets; Events", 6, 0.0, 5)
                elif (variable[i] =="topjet_Subjet_eta"):
                    hist = TH1D(name, "h_topjet_Subjet_eta; eta; Events", 200, -2.5, 2.5)
                elif (variable[i] =="topjet_Subjet_phi"):
                    hist = TH1D(name, "h_topjet_Subjet_phi; phi [radians]; Events", 200, -3.15, 3.15)
                elif (variable[i] =="topjet_Subjet_pT"):
                    hist = TH1D(name, "h_topjet_Subjet_pT; pT [GeV]; Events", 200, 0.0, 900.0)
                elif (variable[i] =="topjet_Subjet_mass"):
                    hist = TH1D(name, "h_topjet_Subjet_mass; Mass [GeV]; Events", 200, 0.0, 180.0)
                elif (variable[i] =="topjet_Subjet_type"):
                    hist = TH1D(name, "h_topjet_Subjet_type; Type scoring; Events", 5, -1, 4)
                elif (variable[i] =="topjet_Subjet_DeltaR"):
                    hist = TH1D(name, "h_topjet_Subjet_DeltaR; Delta R; Events", 100, 0.0, 0.5)
                elif (variable[i] =="topjet_Subjet_isMatched"):
                    hist = TH1D(name, "h_topjet_Subjet_isMatched; Matching score; Events", 5, -2, 3)
                elif (variable[i] =="topjet_Subjet_jetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_jetresponse; Response; Events", 200, 0.0, 2.0)
                elif (variable[i] =="topjet_Subjet_bjetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_bjetresponse; Response; Events", 200, 0.0, 2.0)
                elif (variable[i] =="topjet_Subjet_lightjetresponse"):
                    hist = TH1D(name, "h_topjet_Subjet_lightjetresponse; Response; Events", 200, 0.0, 2.0)

                histograms.append(hist)

            for j in range(0,len(histograms)):
                FillHisto_auto(trees[z],histograms[j],isRecoLevel,variable[j],variableType[j],isNeedingDivisionByOneThousand[j],weightsToSwapOutNames[k],systematicWeightNames[k],systematicWeightNamesVectorElement[k])
                print("Finished filling histogram number " + str(j+1) + "/" + str(len(histograms)) + " for the systematic weight number " + str(k+1) + "/" + str(len(systematicWeightNames)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
                histograms[j].Write()

    outfile.Close()


######
###### Main part of code
######

print('')
samplesToRunOn = ["ttallpp8"]
filePathSegments = ["2022_11_15_ttbar_NewFlavourScheme_FullSim/JSF_1_00_FullSim"]
MC2MC_SF = [False]

for i in range(0,len(samplesToRunOn)):
    MakeHisto_auto(filePathSegments[0],samplesToRunOn[0],MC2MC_SF[0])

print('')
print('Done')
print('')