from ROOT import TFile, TH1, TH1D
import ROOT as r

r.TH1.SetDefaultSumw2(True)

def FillHisto_auto(tree_name,hist_name,isRecoLevel,variable_name,variableType,isNeedingDivisionByOneThousand,weightModifier):

    print('\nFilling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)
        if (isRecoLevel == True):
            #baseWeight = tree_name.weight_normalise
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.weight_normalise
        else:
            #baseWeight = tree_name.mcEventNormWeight
            #specificWeight = tree_name.fsr_down
            #weight = baseWeight*specificWeight
            weight = tree_name.mcEventNormWeight
    
        variable = getattr(tree_name, variable_name)
        additionalWeight = getattr(tree_name, weightModifier)

        # fill histogram with entires with weight
        if (isNeedingDivisionByOneThousand == True):
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j] / 1000.0, weight*additionalWeight )
            else:
                hist_name.Fill(variable / 1000.0, weight*additionalWeight)
        else:
            if (variableType == "vector"):
                for j in range(0,len(variable)):
                    hist_name.Fill( variable[j], weight*additionalWeight )
            else:
                hist_name.Fill(variable, weight*additionalWeight)
    
    print("Finished filling histogram for variable " + variable_name + " with weight modified by " + weightModifier)


def MakeHisto_auto(filePathSegment,sample,JSF,weight):

    input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/" + filePathSegment + "/AllYears/ttbarBoosted/" + sample + ".root"
    output_file_path = "../output_root_files/2023-04-12-Profiling/" + sample + "/mJ_mW_" + weight + ".root"
    
    infile = TFile.Open(input_file_path,"READ");

    # Get trees
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        # if (h.ClassName() == 'TTree'):
        if (h.ClassName() == 'TTree') and ('nominal' in h.GetName()):
            tree = h
            trees.append(tree)

    print('\n==============================================================================')
    print('==============================================================================')
    print('\nSample = ' + sample + ', JSF = ' + JSF)
    print('\nTTrees from file: %s' %(input_file_path))
    print('\nOutput ROOT file: %s' %(output_file_path))
    print('\nThe list of TTrees read in is:')
    for treename in trees:
        print('')
        print(treename)
    print('\nThe total number of trees read in is: %i' %(len(trees)))
    print('\n==============================================================================')
    print('==============================================================================\n')

    # open output file to hold histograms
    outfile = TFile.Open(output_file_path,"update")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    isRecoLevel = True

    variable_dict = dict()

    # variable_dict["variable name"] = [variable type, if it needs to be converted from MeV to GeV, number of bins, low bin, high bin]

    # variable_dict["jet_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["jet_eta"] = ["vector",False,200,-2.5,2.5]
    # variable_dict["jet_n"] = ["int",False,10,0,9]
    # variable_dict["passedPL"] = ["int",False,101,0,100]
    # variable_dict["top_lepb_deltaR"] = ["float",False,200,0.0,6.0]
    # variable_dict["top_lepb_deltaPhi"] = ["float",False,200,-3.15,3.15]
    # variable_dict["leadadd_top_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["leadadd_top_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["subleadadd_hadtop_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["subleadadd_hadtop_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["leadadd_subleadadd_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["leadadd_subleadadd_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["leadadd_lepb_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["leadadd_lepb_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_leadadd_leptop_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_leadadd_leptop_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_subleadadd_leptop_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_subleadadd_leptop_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_leadadd_ttbar_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_leadadd_ttbar_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_subleadadd_ttbar_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_subleadadd_ttbar_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_leadadd_leadtop_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_leadadd_leadtop_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_subleadadd_leadtop_deltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["tmm_subleadadd_leadtop_deltaPhi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["tmm_invmass_leadadd_leadtop"] = ["vector",False,200,135,205.0]
    # variable_dict["addjet_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["addbjet_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["naddbjet"] = ["int",False,10,0,9]
    # variable_dict["leadaddjet_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["leadaddjet_phi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["leadaddjet_eta"] = ["vector",False,200,-2.5,2.5]
    # variable_dict["leadaddjet_m"] = ["vector",True,200,0.0,200.0]
    # variable_dict["subleadaddjet_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["subleadaddjet_phi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["subleadaddjet_eta"] = ["vector",False,200,-2.5,2.5]
    # variable_dict["subleadaddjet_m"] = ["vector",True,200,0.0,200.0]
    # variable_dict["lepbjet_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["lepbjet_phi"] = ["float",False,200,-3.15,3.15]
    # variable_dict["lepbjet_eta"] = ["float",False,200,-2.5,2.5]
    # variable_dict["lepbjet_m"] = ["float",True,200,0.0,200.0]
    # variable_dict["naddjet"] = ["int",False,10,0,9]
    # variable_dict["mlb"] = ["float",True,200,0.0,200.0]
    # variable_dict["invmass_leadadd_hadtop"] = ["vector",True,200,0.0,500.0]
    # variable_dict["invmass_subleadadd_hadtop"] = ["vector",True,200,0.0,500.]
    # variable_dict["nRCjet_afterMassWindow"] = ["int",False,10,0,9]
    # variable_dict["tmm_leptop_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["tmm_leptop_phi"] = ["float",False,200,-3.15,3.15]
    # variable_dict["tmm_leptop_eta"] = ["float",False,200,-2.5,2.5]
    # variable_dict["tmm_leptop_m"] = ["float",True,200,0.0,300.0]
    # variable_dict["tmm_leptop_y"] = ["float",False,200,-2.5,2.5]
    # variable_dict["tmm_ttbar_deltaR"] = ["float",False,200,0.0,6.0]
    # variable_dict["tmm_ttbar_deltaPhi"] = ["float",False,200,-3.15,3.15]
    # variable_dict["tmm_ttbar_deltaEta"] = ["float",False,200,-2.5,2.5]
    # variable_dict["tmm_ttbar_invmass"] = ["float",True,200,0.0,1000.0]
    # variable_dict["tmm_ttbar_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["tmm_ttbar_y"] = ["float",False,200,-2.5,2.5]
    # variable_dict["tmm_leadtop_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["tmm_leadtop_m"] = ["float",True,200,0.0,300.0]
    # variable_dict["tmm_leadtop_y"] = ["float",False,200,-2.5,2.5]
    # variable_dict["lep_pt"] = ["vector",True,200,0.0,900.0]
    # variable_dict["lep_eta"] = ["vector",False,200,-2.5,2.5]
    variable_dict["lep_phi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["lep_m"] = ["vector",True,200,0.0,200.0]
    # variable_dict["lep_type"] = ["vector",False,3,11,13]
    # variable_dict["met"] = ["float",True,200,0.0,1000.0]
    # variable_dict["met_phi"] = ["float",False,200,-3.15,3.15]
    # variable_dict["mtw"] = ["vector",True,200,0.0,200.0]
    # variable_dict["nbjet"] = ["int",False,10,0,9]
    # variable_dict["topjet_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["topjet_eta"] = ["float",False,200,-2.5,2.5]
    # variable_dict["topjet_phi"] = ["float",False,200,-3.15,3.15]
    variable_dict["topjet_m"] = ["float",True,200,135.0,205.0]
    # variable_dict["topjet_y"] = ["float",False,200,-2.5,2.5]
    # variable_dict["topjet_nSubjets"] = ["int",False,10,0,9]
    # variable_dict["topjet_avgSubjet_pt"] = ["float",True,200,0.0,900.0]
    # variable_dict["topjet_avgSubjet_eta"] = ["float",False,200,-2.5,2.5]
    variable_dict["topjet_W_invMass"] = ["float",True,20,40.0,120.0]
    # variable_dict["topjet_nbtaggedSubjets"] = ["int",False,10,0,9]
    # variable_dict["topjet_Subjet_eta"] = ["vector",False,200,-2.5,2.5]
    # variable_dict["topjet_Subjet_phi"] = ["vector",False,200,-3.15,3.15]
    # variable_dict["topjet_Subjet_pT"] = ["vector",True,200,0.0,900.0]
    # variable_dict["topjet_Subjet_mass"] = ["vector",True,200,0.0,200.0]
    # variable_dict["topjet_Subjet_type"] = ["vector",False,5,-1,4]
    # variable_dict["topjet_Subjet_DeltaR"] = ["vector",False,200,0.0,6.0]
    # variable_dict["topjet_Subjet_isMatched"] = ["vector",False,5,-2,3]
    # variable_dict["topjet_Subjet_jetresponse"] = ["vector",False,200,0.0,2.0]
    # variable_dict["topjet_Subjet_bjetresponse"] = ["vector",False,200,0.0,2.0]
    # variable_dict["topjet_Subjet_lightjetresponse"] = ["vector",False,200,0.0,2.0]
    # variable_dict["lepbjet_lep_pT_ratio"] = ["float",False,100,0.0,10.0]
    # variable_dict["tmm_leptop_addjet_m"] = ["float",True,100,0.0,600.0]
    # variable_dict["tmm_leptop_bjet_addjet_m"] = ["float",True,100,0.0,600.0]

    for z in range(0,len(trees)):
        histcount = 1
        for variable in variable_dict:
            name = trees[z].GetName() + "_" + variable
            # name = trees[z].GetName() + "_" + variable + "_lepbjet_pt_ratio"
            hist = TH1D(name, variable + "; variable; Events", variable_dict[variable][2], variable_dict[variable][3], variable_dict[variable][4])
            # hist = TH1D(name, variable + "; variable; Events", 100, 0, 2.5)
            FillHisto_auto(trees[z],hist,isRecoLevel,str(variable),variable_dict[variable][0],variable_dict[variable][1],weight)
            print("Finished filling histogram number " + str(histcount) + "/" + str(len(variable_dict)) + " for tree number " + str(z+1) + "/" + str(len(trees)))
            hist.Write()
            histcount += 1
        print("Finished filling the histograms for tree number " + str(z+1) + "/" + str(len(trees)))

    outfile.Close()


# ==========================================================================
# =========================== Main part of code ============================
# ==========================================================================

print('')
samplesToRunOn = [
    
    # "ttallpp8",
    # "ttMCatNLOH713",
    # "ttnfaMCatNLO",
    # "ttph713",
    # "ttph721",
    # "ttpp8AF2",
    # "ttpp8CR",
    # "ttpp8CR0",
    # "ttpp8CR1",
    # "ttpp8CR2",
    # "ttpp8m169",
    # "ttpp8m171",
    # "ttpp8m172",
    # "ttpp8m172_25",
    # "ttpp8m172_75",
    # "ttpp8m173",
    # "ttpp8m174",
    # "ttpp8m176",
    # "ttpp8MECOff",
    # "ttpp8RTT",
    # "ttpp8Var",
    # "diboson",
    # "SingleTop",
    # "SingleTopDS",
    # "SingleTopH7",
    # "ttV",
    "wejets",
    "wmujets",
    "wtaujets",
    # "zjets",
    # "Fakes"

    ]

weights = [
    "MUR05_MUF05",
    "MUR05_MUF1",
    "MUR2_MUF1",
    "MUR2_MUF2",
    "MUR1_MUF2",
    "MUR1_MUF05"
]

for i in range(0,len(samplesToRunOn)):
    for j in range(0,len(weights)):
        JSFtoUse = "1_00"
        if samplesToRunOn[i] == "ttallpp8":
            filePathSegment = "2023_04_12_TTDIFFXSMASSv02_Nominal_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttMCatNLOH713":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttnfaMCatNLO":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttph713":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif "ttpp8CR" in samplesToRunOn[i]:
            filePathSegment = "2022_12_07_ttbar_CR_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8m169":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8m171":
            filePathSegment = "2023_04_12_TTDIFFXSMASSv02_Parameterisation_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8m172":
            filePathSegment = "2023_04_12_TTDIFFXSMASSv02_Parameterisation_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8m172_25":
            filePathSegment = "2022_12_16_ttbar_ExtraMassPoints_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8m172_75":
            filePathSegment = "2022_12_16_ttbar_ExtraMassPoints_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8m173":
            filePathSegment = "2023_04_12_TTDIFFXSMASSv02_Parameterisation_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8m174":
            filePathSegment = "2023_04_12_TTDIFFXSMASSv02_Parameterisation_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8m176":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8MECOff":
            filePathSegment = "2023_01_13_ttbar_FastSimAlternative_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttpp8RTT":
            filePathSegment = "2023_03_07_ttpp8AF2_leptonicSideMass_FastSim/JSF_1_00_FastSim"
        elif samplesToRunOn[i] == "ttph721":
            filePathSegment = "2022_11_15_ttbar_FullSimAlternative_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8Var":
            filePathSegment = "2022_11_15_ttbar_FullSimAlternative_FullSim/JSF_1_00_FullSim"
        elif samplesToRunOn[i] == "ttpp8AF2":
            # filePathSegment = "2022_11_24_ttbar_AF2_JSFvaried_FastSim/JSF_" + JSFs[k] + "_FastSim"
            filePathSegment = "2023_03_07_ttpp8AF2_leptonicSideMass_FastSim/JSF_1_00_FastSim"
        elif ((samplesToRunOn[i] == "diboson") or (samplesToRunOn[i] == "ttv") or (samplesToRunOn[i] == "wejets") or (samplesToRunOn[i] == "wmujets") or (samplesToRunOn[i] == "zjets")):
            filePathSegment = "2023_04_14_TTDIFFXSMASSv02_Backgrounds_-1_0_FullSim/JSF_1_00_FullSim"
        elif ((samplesToRunOn[i] == "SingleTopH7") or (samplesToRunOn[i] == "SingleTopDS")):
            filePathSegment = "2023_04_14_TTDIFFXSMASSv02_Backgrounds_0_1_FullSim/JSF_1_00_FullSim"
        elif (samplesToRunOn[i] == "SingleTop"):
            filePathSegment = "2023_05_09_TTDIFFXSMASSv02_SingleTop_AfterFedericasMR_FullSim/JSF_1_00_FullSim"

        MakeHisto_auto(filePathSegment,samplesToRunOn[i],JSFtoUse,weights[j])

print('')
print('Histograms have been saved to the output_root_files folder')
print('')