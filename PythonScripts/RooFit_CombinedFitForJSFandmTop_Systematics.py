# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np
from array import array
import math

def RooFit_Minimisation(name,meanTopJetMassValue,meanTopJetMassError,hist):

    print("\n-----------------------------------------------------------------------------------------\n")
    print("\nFile = ttallpp8, JSF = 1.00, systematics\n")
    print("\n-----------------------------------------------------------------------------------------\n")

    # Set up model for mean top jet mass, mJ
    # ---------------------------------------------------------------------------------------------------------------

    # This is one of our observables, mean(top jet mass)
    meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)

    # Nominal mean and error (for when JSF = 1 and mtop = 172.5 as inputs to creating ntuple)
    nominalMean = meanTopJetMassValue
    errorOnMean = meanTopJetMassError

    # JSF
    JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)

    # Top quark mass
    mtop = ROOT.RooRealVar("mtop","mtop",172.5,150,200)

    # Fixed parameter in the equation for mean(top jet mass)
    amJ = ROOT.RooRealVar("amJ","amJ",168.832795) # FAST SIM
    # amJ = ROOT.RooRealVar("amJ","amJ",169.246951) # FULL SIM
    amJ.setConstant(True)

    # Fixed parameter relating JSF to mean(top jet mass)
    cJSF = ROOT.RooRealVar("cJSF","cJSF",78.189705) # FAST SIM
    # cJSF = ROOT.RooRealVar("cJSF","cJSF",76.954998) # FULL SIM
    cJSF.setConstant(True)

    # Fixed parameter relating mtop to mean(top jet mass)
    bmtop = ROOT.RooRealVar("bmtop","bmtop"0.476824) # FAST SIM
    # bmtop = ROOT.RooRealVar("bmtop","bmtop",0.457656) # FULL SIM

    # code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + C*(JSF-1) + B*(mtop - 172.5)
    meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2 - 1.0) + @3*(@4-172.5)",ROOT.RooArgList(amJ,cJSF,JSF,bmtop,mtop))

    # Assume are measurements of the mean top-jet mass (we will have only 1) are distributed according to a gaussian with mean related to JSF.
    # The sigma of the gaussian is fixed to the error on the mean
    pdf_topJetMass = ROOT.RooGaussian("pdf_topJetMass","pdf_topJetMass",meanTopJetMass, meanFormula, ROOT.RooRealConstant.value(errorOnMean))

    # Create data for mJ
    # ---------------------------------------------------------------------------------------------------------------

    # Create data - just one measurement of mean(top jet mass)
    toyData = ROOT.RooDataSet("d","d",ROOT.RooArgSet(meanTopJetMass))
    meanTopJetMass.setVal(nominalMean)
    toyData.add(ROOT.RooArgSet(meanTopJetMass))
    toyData.get(0).Print("V")

    # Set up model for reconstructed W boson mass, mW
    # ---------------------------------------------------------------------------------------------------------------

    useDataLikeErrors = True # Choose whether to use data-like errors (True) or not (False)

    # Declare observable mW
    mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)

    # Delcare constants in relation mean1 = A_2 + B_2*JSF
    # FAST SIM
    # A_2 = ROOT.RooRealVar("A_2","A_2",11.6346)
    # B_2 = ROOT.RooRealVar("B_2","B_2",71.6033)
    # FULL SIM
    A_2 = ROOT.RooRealVar("A_2","A_2",13.5614)
    B_2 = ROOT.RooRealVar("B_2","B_2",70.2612)

    # Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and their parameters
    mean1 = ROOT.RooFormulaVar("mean1","mean of first gaussian","@0 + @1*@2", ROOT.RooArgList(A_2,B_2,JSF)) # Actual 83.8226
    mean2 = ROOT.RooRealVar("mean2", "mean of second gaussian", 80.7167120345, 70,90)
    sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.7785340145,0,50)
    sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.6507628753,0,50)
    
    gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
    gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
    
    # Sum the signal components into a composite p.d.f.
    gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0, 1.0)
    combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))

    if useDataLikeErrors == True: # If using data-like errors
        for bin_number in range(hist.FindBin(55)-1,hist.FindBin(110)+1):
            hist.SetBinError(bin_number,np.sqrt(hist.GetBinContent(bin_number)))

    mWDataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist, 1.0)

    # Perform negative log likelihood on both PDFs 
    # ---------------------------------------------------------------------------------------------------------------

    mW_NLL = combinedGauss.createNLL(mWDataHist)
    mJ_NLL = pdf_topJetMass.createNLL(toyData)
    Added_NLL = ROOT.RooAddition("Added_NLL","Combined NLLs", ROOT.RooArgList(mW_NLL,mJ_NLL))
    Minimise_NLL = ROOT.RooMinimizer(Added_NLL)
    Minimise_NLL.minimize("Minuit")
    fitResult = Minimise_NLL.save()
    fitResult.Print("v")
    print("\nInitial value of floating parameters")
    fitResult.floatParsInit().Print("s")
    print("\nFinal value of floating parameters")
    fitResult.floatParsFinal().Print("s")
    print("\nValue of constant parameters")
    fitResult.constPars().Print("s")

    outputfile = '../OutputTextFiles/MinimisationResults.txt'
    lines = [meanTopJetMassValue,meanTopJetMassError,JSF.getVal(),JSF.getError(),gauss1frac.getVal(),gauss1frac.getError(),sigma1.getVal(),sigma1.getError(),mean2.getVal(),mean2.getError(),sigma2.getVal(),sigma2.getError(),mtop.getVal(),mtop.getError()]
    f = open(outputfile, 'a')
    f.write(name)
    f.write('\t')
    for line in lines:
        f.write('%f' %line)
        f.write('\t')
    f.write('\n')
    f.close()

    # Save the fit result
    # ---------------------------------------------------------------------------------------------------------------

"""     mWframe = mW.frame()
    mWDataHist.plotOn(mWframe,ROOT.RooFit.MarkerSize(0.05))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("combinedGauss"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(1))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss1"),ROOT.RooFit.LineColor(4),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(2),ROOT.RooFit.Components("gauss1"))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss2"),ROOT.RooFit.LineColor(8),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(3),ROOT.RooFit.Components("gauss2"))
    mWframe.GetYaxis().SetTitle("Entries")
    mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
    canvas = ROOT.TCanvas("canvas")
    mWframe.Draw()

    legend = ROOT.TLegend(0.12,0.72,0.4,0.89)
    legend.AddEntry("gauss1", "Gaussian 1", "lp")
    legend.AddEntry("gauss2", "Gaussian 2", "lp")
    legend.AddEntry("combinedGauss", "Final fit", "lp")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    canvas.SaveAs("../histogram_images/2022-07-06-CorrectedHistogramImages/mW_twoGaussFit_SimultaneousFittingJSFmtop_" + filename + "_MC2MC.pdf") """

# Main body of code
# -------------------------------------------------------------------------------------------

names = []
mJ_values = []
mJ_errors = []
# histogram_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8.root"
histogram_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mJ/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D'):
    #if (h.ClassName() == 'TH1D') and (h.GetName() == "nominal_topjet_m"):
        names.append(h.GetName().rstrip("_topjet_m"))
        # names.append(h.GetName().rstrip("_mJ_combined"))
        hist = h
        mJ_values.append(hist.GetMean())
        mJ_errors.append(hist.GetStdDev()/math.sqrt(hist.Integral()))
histfile.Close()

histograms = []
# histogram_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8.root"
histogram_file_path = "../output_root_files/2022-11-15-NewJESFlavourUncertainties/ttallpp8/mW/JSF_1_00_ttallpp8_JERSystematicsCombinedHistograms.root"
histfile = ROOT.TFile.Open(histogram_file_path)
for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    if (h.ClassName() == 'TH1D'):
    #if (h.ClassName() == 'TH1D') and (h.GetName() == "nominal_topjet_W_invMass"):
        hist = h
        hist.SetDirectory(0)
        histograms.append(hist)
histfile.Close()

for i in range(0,len(names)):
    RooFit_Minimisation(names[i],mJ_values[i],mJ_errors[i],histograms[i])

print("\nScript has finished\n")
