import numpy as np
import sys

print("")
# total arguments
numberOfArgs = len(sys.argv)
if (numberOfArgs != 3):
    print("You have not entered two arguements! Try again with exactly two arguements")
    exit()
 
print 'You want to compare ', sys.argv[1], ' with ', sys.argv[2]
print 'This means it will be the constraints from ', sys.argv[1], ' subtracted from those in ', sys.argv[2]

fileDirectory = "/home/ppe/e/ewatton/ATLAS/TopJetMassToTopQuarkMass/ProfileLikelihoodFitting-2023-08-10/boosted-top-mass-pfit/TRExConfigs/"

try:
  file1 = open(fileDirectory + sys.argv[1],'r')
  file2 = open(fileDirectory + sys.argv[2],'r')
except:
  print("An exception occurred! Error opening input files!")
  exit()

lineNumbers_file1 = [i for i in range(1,104)]
lineNumbers_file2 = [i for i in range(1,126)]

file1_lines = file1.readlines()
file2_lines = file2.readlines()

diffDict = {}
absdiffDict = {}
npDict_file1 = {}
npDict_file2 = {}

print("")
print 'NP',sys.argv[1],sys.argv[2],'Difference'
print("")

for i in lineNumbers_file1:
    file1_line = file1_lines[i]
    file1_line.replace("\n","")
    NPconstraint_file1 = float(file1_line.split('-')[1])
    NPname_file1 = file1_line.split('  0')[0]
    npDict_file1[NPname_file1] = NPconstraint_file1

for i in lineNumbers_file2:
    file2_line = file2_lines[i]
    file2_line.replace("\n","")
    NPconstraint_file2 = float(file2_line.split('-')[1])
    NPname_file2 = file2_line.split('  0')[0]
    npDict_file2[NPname_file2] = NPconstraint_file2

for key, item in npDict_file1.items():
    try:
        diff = npDict_file2[key] - npDict_file1[key]
        diffDict[key] = diff
        absdiffDict[key] = abs(diff)
    except:
        continue

for key, item in npDict_file2.items():
    try:
        diff = npDict_file2[key] - npDict_file1[key]
        diffDict[key] = diff
        absdiffDict[key] = abs(diff)
    except:
        continue




diffDict_sorted = sorted(absdiffDict.items(), key=lambda x:x[1], reverse=True)

orderedIndices = []
for doublet in diffDict_sorted:
    orderedIndices.append(doublet[0])

for key in orderedIndices:
    print key, npDict_file1[key], npDict_file2[key], diffDict[key]

file1.close()
file2.close()
