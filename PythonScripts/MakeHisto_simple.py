# import ROOT classes
from ROOT import TFile, TH1, TH1D
import ROOT as r
import numpy as np
r.TH1.SetDefaultSumw2(True)

print('')

def FillHisto(tree_name,hist_name, varname):

    print('Filling the following histogram:')
    print(hist_name)
    print('Going to process %i events' % tree_name.GetEntries())

    # loop on events in the tree
    for i in range(0, tree_name.GetEntries()):
        tree_name.GetEntry(i)

        # access the event weight
        weight = tree_name.weight_normalise
        #weight = tree_name.mcEventNormWeight
    
        # access a variable - MUST BE CHANGED TO SPECIFIC VARIABLE WANTED
        #variable = tree_name.topjet_m
        #variable = tree_name.topjet_W_invMass
        variable = getattr(tree_name, varname)
        #variable = tree_name.topjet_Subjet_invMass_lightjets

        # fill histogram with entires with weight
        hist_name.Fill( variable / 1000.0, weight ); # have to divide by 1000 as ATLAS default units are MeV
    
    print('Finished filling histogram')
    print('')

def MakeHisto(iterator):

    #input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2022_01_24_ttbarSystematicSamplesFullSim/Systematics_JSF_1_00_mt_172_5/AllYears/ttbarBoosted/ttallpp8.root"
    #input_file_path = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2021_11_22_SecondAttemptAtFastSim/outputs_SkipTruth1_V24MM_PFlow_Nominal_SDB_JSF1_03_ttpp8AF2_attempt2_ForMap_PDF_v2_22_11_2021/outputs_SkipTruth1_V24MM_PFlow_Nominal_SDB_JSF1_00_ttpp8AF2_attempt2_ForMap_PDF_v2_22_11_2021/AllYears/ttbarBoosted/ttpp8AF2.root"
    input_file_path = "../glasgow-ana/run/output/ttbarBoosted/ttallpp8.root"
    output_file_path = "../output_root_files/test.root"
    infile = TFile.Open(input_file_path,"READ");

    # Get nominal tree
    trees = []
    for h in infile.GetListOfKeys():
        h = h.ReadObj()
        if (h.ClassName() == 'TTree' and h.GetName() == 'nominal'):
            tree = h
            trees.append(tree)

    print('')
    print(trees)
    print('')

    outfile = TFile.Open(output_file_path,"RECREATE")

    # Turn on proper stat uncertainties in Root histograms
    TH1D.SetDefaultSumw2(True)

    # Make empty mW histograms
    histograms = []
    for i in range(0,len(trees)):
        name = trees[i].GetName()
        hist = TH1D(name, "h_topjet_W_invMass; Mass [GeV/c^{2}]; Events", 200, 0.0, 200.0)
        histograms.append(hist)
    

    # Fill empty mW histograms
    for i in range(0,len(histograms)):
        FillHisto(trees[i],histograms[i])
        histograms[i].Write()
        SaveHisto(histograms[i],name)

    """ # Make empty mJ histograms
    histograms_mJ = []
    for i in range(0,len(trees)):
        name = trees[i].GetName()
        hist = TH1D(name, "h_topjet_m; Mass [GeV/c^{2}]; Events", 100, 135.0, 205.0)
        histograms_mJ.append(hist)

    # Fill empty mJ histograms
    for j in range(0,len(histograms_mJ)):
        FillHisto(trees[j],histograms_mJ[j])
        histograms_mJ[j].Write()
        SaveHisto(histograms_mJ[j],"output_mt_JSF_1_00_ttpp8AF2.pdf") """

    # SAVE HISTOGRAMS INTO OUTPUTFILE
    outfile.Close()

def SaveHisto(hist_name,hist_save_name):

    print('Saving the following histogram as a png file:')
    print(hist_name)

    canvas = r.TCanvas("canvas")
    hist_name.Draw()
    canvas.SaveAs("../histogram_images/mt_TEST/" + hist_save_name + ".pdf")

    print('')

######
###### Main body of code
######

MakeHisto(" ")

""" # Get the histograms from the file
histfile = r.TFile.Open("../output_root_files/output_systematics_mJ_JSF_1_00_mt_172_5_ttallpp8.root")

for h in histfile.GetListOfKeys():
    h = h.ReadObj()
    hist = histfile.Get(h.GetName())
    if (h.GetName() == "nominal"):
        print(hist.GetMean())
        print(hist.GetMeanError())
        print(hist.GetStdDev()/np.sqrt(hist.Integral()))
        print("\n\n\n")

histfile = r.TFile.Open("../output_root_files/output_GeneratorSamples_mJ_JSF_1_00_mt_172_5_ttph7.root")

for h in histfile.GetListOfKeys():
    print("\n\n\n")
    h = h.ReadObj()
    hist = histfile.Get(h.GetName())
    print(hist.GetMean())
    print(hist.GetMeanError())
    print(hist.GetStdDev()/np.sqrt(hist.Integral()))
    print("\n\n\n")

histfile = r.TFile.Open("../output_root_files/output_GeneratorSamples_mJ_JSF_1_00_mt_172_5_ttaMCatNLO.root")

for h in histfile.GetListOfKeys():
    print("\n\n\n")
    h = h.ReadObj()
    hist = histfile.Get(h.GetName())
    print(hist.GetMean())
    print(hist.GetMeanError())
    print(hist.GetStdDev()/np.sqrt(hist.Integral()))
    print("\n\n\n")

histfile = r.TFile.Open("../output_root_files/output_GeneratorSamples_mJ_JSF_1_00_mt_172_5_ttpp8Var.root")

for h in histfile.GetListOfKeys():
    print("\n\n\n")
    h = h.ReadObj()
    hist = histfile.Get(h.GetName())
    print(hist.GetMean())
    print(hist.GetMeanError())
    print(hist.GetStdDev()/np.sqrt(hist.Integral()))
    print("\n\n\n")

print('')
print('Histograms have been saved to the output_root_files folder, and also as images in the histogram_images folder')
print('') """
