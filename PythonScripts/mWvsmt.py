import ROOT as r
from array import array
import math

r.TH1.SetDefaultSumw2(True)

sampleDirectory = "/nfs/atlas/ttbar/ElliotWatton_ttbarBoosted/2023_07_20_TTDIFFXSMASSv03_ntuples/"
massSamples = ["171","172","ttallpp8","173","174"]
n = 5
x = array( 'f', [171.0,172.0,172.5,173.0,174.0])
xerr = array( 'f', [0.0,0.0,0.0,0.0,0.0])
y = array( 'f', [])
yerr = array( 'f', [])
useDataLikeErrors = False
makeSubPlot = False

for i in range(0,len(massSamples)):
    if i != 2:
        histfile = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/ttpp8m" + massSamples[i] + "/mW_SameBinningAsFit.root")
    else:
        histfile = r.TFile.Open("../output_root_files/2023-07-21-NewVariables/" + massSamples[i] + "/mW_SameBinningAsFit.root")
    
    for h in histfile.GetListOfKeys():
        h = h.ReadObj()
        if (h.GetName() == "nominal_topjet_W_invMass"):
            y.append(h.GetMean())
            if useDataLikeErrors == True:
                yerr.append(h.GetStdDev()/math.sqrt(h.Integral()))
            else:
                yerr.append(h.GetMeanError())
        else:
            continue
    
    histfile.Close()

if makeSubPlot:

    canvas_scatter = r.TCanvas("canvas_scatter")

    pad1 = r.TPad("pad1","pad1",0,0.33,1,1)
    pad2 = r.TPad("pad2","pad2",0,0.1,1,0.33)
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad1.SetTicks()
    pad2.SetTicks()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()

    graph = r.TGraphErrors(n, x, y, xerr, yerr)
    graph.GetXaxis().SetTitle("m_{#it{t}} [GeV]")
    graph.GetYaxis().SetTitle("#bar{m_{#it{W}}} [GeV]")
    graph.SetMarkerStyle(20)
    graph.SetMarkerSize(0.5)
    graph.GetYaxis().SetTitleOffset(1.0)
    graph.GetYaxis().SetTitleSize(0.04)
    graph.GetYaxis().SetLabelFont(62)
    graph.SetTitle("")
    graph.Draw("AP")

    xyline = r.TLine(171,y[2],174,y[2])
    xyline.SetLineColor(r.kRed)
    xyline.SetLineWidth(2)
    xyline.Draw("same")

    latex = r.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    latex.DrawLatex(0.15,0.85,"#it{ATLAS} #bf{work-in-progress}")
    latex.DrawLatex(0.15,0.80,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
    latex.DrawLatex(0.15,0.75,"#bf{t#bar{t} MC sample}")

    pad2.cd()

    r.gStyle.SetOptStat(0)

    edges = array('f',[170.25, 171.75, 172.25, 172.75, 173.25, 174.75])
    subplot2 = r.TH1D("graph","graph title",n,edges)
    subplot2.GetXaxis().SetRangeUser(170.25,174.75)
    subplot3 = r.TH1D("graph","graph title",n,edges)
    subplot3.GetXaxis().SetRangeUser(170.25,174.75)
    for i in range(1,n+1):
        subplot2.SetBinContent(i,y[i-1])
        subplot2.SetBinError(i,yerr[i-1])
    for i in range(1,n+1):
        subplot3.SetBinContent(i,y[2])
        subplot3.SetBinError(i,yerr[2])
    subplot2.Divide(subplot3)
    subplot_x_values = array('f')
    subplot_x_errors = array('f')
    subplot_y_values = array('f')
    subplot_y_errors = array('f')
    for i in range(1,n+1):
        subplot_y_values.append(subplot2.GetBinContent(i))
        subplot_y_errors.append(subplot2.GetBinError(i))
        subplot_x_values.append(x[i-1])
        subplot_x_errors.append(0)
    subplot = r.TGraphErrors(n,subplot_x_values,subplot_y_values,subplot_x_errors,subplot_y_errors)
    subplot.SetMarkerStyle(5)
    subplot.SetMarkerSize(0.5)
    subplot.Draw("AP")
    subplot.SetTitle("")
    subplot.GetXaxis().SetLabelFont(63)
    subplot.GetXaxis().SetLabelSize(10)
    subplot.GetXaxis().SetTitle("m_{#it{t}} [GeV]")
    subplot.GetXaxis().SetTitleSize(0.11)
    subplot.GetXaxis().SetTitleOffset(0.8)
    subplot.GetYaxis().SetLabelFont(63)
    subplot.GetYaxis().SetLabelSize(10)
    subplot.GetYaxis().SetTitle("Data points / Nominal value")

    subplot.GetYaxis().SetTitleOffset(0.38)
    subplot.GetYaxis().SetTitleSize(0.11)
    subplot.GetYaxis().SetNdivisions(207)
    subplot.GetYaxis().SetRangeUser(0.9,1.1)
    line = r.TLine(171,1,174,1)
    line.SetLineColor(r.kRed)
    line.SetLineWidth(2)
    line.Draw("same")
    canvas_scatter.Draw()
    if useDataLikeErrors == True:
        canvas_scatter.SaveAs("../histogram_images/2023/2023-11-13/mWvsmt_DataErrors.pdf") 
    else:
        canvas_scatter.SaveAs("../histogram_images/2023/2023-11-13/mWvsmt_MCErrors.pdf") 

else:
    #### No subplot
    canvas_scatter = r.TCanvas("canvas_scatter")
        
    graph = r.TGraphErrors(n, x, y, xerr, yerr)
    graph.GetXaxis().SetTitle("m_{#it{t}} [GeV]")
    graph.GetYaxis().SetTitle("#bar{m_{#it{W}}} [GeV]")
    graph.GetYaxis().SetTitleOffset(1.4)
    graph.SetMarkerStyle(20)
    graph.SetMarkerSize(0.5)
    # graph.GetYaxis().SetTitleOffset(1.0)
    # graph.GetYaxis().SetTitleSize(0.04)
    # graph.GetYaxis().SetLabelFont(62)
    graph.SetTitle("")
    graph.Draw("AP")

    canvas_scatter.Draw()
    if useDataLikeErrors == True:
        canvas_scatter.SaveAs("../histogram_images/2023/2023-11-13/mWvsmt_DataErrors.pdf") 
    else:
        canvas_scatter.SaveAs("../histogram_images/2023/2023-11-13/mWvsmt_MCErrors.pdf") 
