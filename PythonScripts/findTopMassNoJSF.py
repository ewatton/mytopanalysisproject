# A macro that will find the average top jet mass from previously made histograms, and then use TMinuit to find the top quark mass.
# This will use the linear relation found previously.
# First created by Elliot Watton - 18/10/2021
# Last editted by Elliot Watton - 14/12/2021

import ROOT as r
from ROOT import TMinuit , Double , Long
from array import array as arr
from array import array
import math
from optparse import OptionParser

r.TH1.SetDefaultSumw2(True)

########
######## Function definitions
########

# Chi^2 function to minimise
def CalcChi2(apar):
    chisq = 0.0
    m_t = apar[0]
    average_m_j_fit_value = Double(0)
    average_m_j_fit_value = A + (B*(m_t - 172.5))
    chisq = ((average_m_j - average_m_j_fit_value)*(average_m_j - average_m_j_fit_value)) / (average_m_j_err * average_m_j_err)
    return chisq

# The function 'fcn' is called by Minuit repeatedly with varying parameters
# Note: the function name is set via TMinuit.SetFCN()
def fcn(npar, deriv, f, apar, iflag):
    """ meaning of parameters: 
          deriv: array of derivatives df/dp_i(x), optional
          f:     value of function to be minimised (typically chi2 or negLogL)
          iflag: internal flag: 1 at first call, 3 at the last, 4 during minimisation
    """
    f[0] = CalcChi2(apar)

# This function will complete the process of minimising chi^2 and determining m_t.
def getm_t(m_j, sigma_m_j, s, c):
    
    # Define data
    global average_m_j 
    average_m_j = m_j
    global average_m_j_err 
    average_m_j_err = sigma_m_j
    global B
    B = s
    global A
    A = c
    
    npar = 1                      # Number of fit parameters
    
    # Set up Minuit
    myMinuit = TMinuit(1)         # Initialise TMinuit with maximum of parameters (1 for this case as we only have m_t)
    myMinuit.SetFCN(fcn)          # Sets the function to minimise
    arglist = arr('d', 2*[0.01])  # Sets the error definition
    ierflg = Long(0)
    arglist[0] = 1.               # 1 sigma is Delta chi^2
    myMinuit.mnexcm("SET ERR", arglist, 1, ierflg)

    # Set starting values and step size for fit parameter, m_t
    myMinuit.mnparm(0, "m_t", 170, 0.01, 0, 0, ierflg)
    arglist[0] = 6000 # Number of calls for FCN before giving up
    arglist[1] = 0.3  # Tolerance
    myMinuit.mnexcm("MIGRAD", arglist, 2, ierflg) # Executes the minimisation

    # Check TMinuit status
    amin, edm, errdef = Double(0.), Double(0.), Double(0.)
    nvpar, nparx, icstat = Long(0), Long(0), Long(0)
    myMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)

    """ meaning of parameters:
        amin:   value of fcn distance at minimum ( = chi^2) 
        edm:    estimated distance to minimum
        errdef: delta_fcn used to define 1 sigma errors
        nvpar:  total number of parameters
        icstat: status of error matrix:
                3 = accurate
                2 = forced pos. def
                1 = approximate
                0 = not calculated
    """
    myMinuit.mnprin(3,amin) # Print-out by Minuit

    # Get results from Minuit
    p, pe = Double(0), Double(0)
    myMinuit.GetParameter(0, p, pe)
    final_m_t = p
    final_m_t_err = pe

    return final_m_t, final_m_t_err


def findTopMassNoJSF(input_file_path,Aparam,Bparam): 

    # Initialise global variables 
    average_m_j = 0       # m_j
    average_m_j_err = 0   # sigma_{m_j}
    A = 0
    B = 0
    npar = 1              # Number of fit parameters

    mJ_array = []
    mJ_err_array = []
    histogram_names = []

    # Finding mJ and mJ_err 
    histogram_root_file = r.TFile.Open(input_file_path)
    for h in histogram_root_file.GetListOfKeys():
        if h.GetName() == "nominal_topjet_m":
            h = h.ReadObj()
            hist = histogram_root_file.Get(h.GetName())
            histogram_names.append(h.GetName().rstrip("_mJ_combined"))
            mJ_array.append(hist.GetMean())
            mJ_err_array.append(hist.GetStdDev()/math.sqrt(hist.Integral()))

    # Values of the constants in the equations for mJ and mW
    A = Aparam
    B = Bparam

    # Close the histogram file
    histogram_root_file.Close()

    m_t_values = []
    m_t_err_values = []

    # Now find the corresponding m_t for each and print it to the screen
    for i in range(0,len(mJ_array)):
        m_t, m_t_err = getm_t(mJ_array[i], mJ_err_array[i], B, A)
        m_t_values.append(m_t)
        m_t_err_values.append(m_t_err)

    # Write results to a text file
    outputfile = "../OutputTextFiles/MinimisationResults.txt"
    for i in range(0,len(m_t_values)):
        lines = [mJ_array[i], mJ_err_array[i], m_t_values[i], m_t_err_values[i]]
        f = open(outputfile, 'a')
        f.write(histogram_names[i])
        f.write('\t')
        for line in lines:
            f.write('\t')
            f.write('%f' %line)
        f.write('\n')
    f.close()

def main():

    parser = OptionParser()
    
    parser.add_option("-i", "--infile",
                  action="store", type="string", dest="inputFilename")
    parser.add_option("-A", "--Aparam",
                  action="store", type="float", dest="Aparam")
    parser.add_option("-B", "--Bparam",
                  action="store", type="float", dest="Bparam")
    
    (options, args) = parser.parse_args() 

    # if (isDigit(options.Aparam) == False)  or (isDigit(options.Bparam) == False):
    #     print("\nError! Please enter valid A and B parameter values!")
    #     exit()

    findTopMassNoJSF(options.inputFilename,options.Aparam,options.Bparam)

if __name__ == "__main__":
    main()
