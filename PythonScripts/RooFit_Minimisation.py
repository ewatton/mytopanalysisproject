# Imports
# ---------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import ROOT
import numpy as np
from array import array

def RooFit_Minimisation(JSF_value,filename,meanTopJetMassValue,meanTopJetMassError):

    print("\n-----------------------------------------------------------------------------------------\n")
    print("\nFile = %s, JSF = %s\n" %(filename,JSF_value))
    print("\n-----------------------------------------------------------------------------------------\n")

    # Set up model for mean top jet mass, mJ
    # ---------------------------------------------------------------------------------------------------------------

    # This is one of our observables, mean(top jet mass)
    meanTopJetMass = ROOT.RooRealVar("meanTopJetMass","meanTopJetMass",150.0,200.0)

    # Nominal mean and error (for when JSF = 1 and mtop = 172.5 as inputs to creating ntuple)
    nominalMean = meanTopJetMassValue
    errorOnMean = meanTopJetMassError

    # For fast sim samples
    # Powheg+Pythia: topjetmassmean = 168.820343104, and error = 0.0231295728549, and data-like error = 0.06116447754341293

    # For full sim samples
    # Powheg+Pythia: topjetmassmean = 169.238779044, and error = 0.0231645622299, and data-like error = 0.06148952657504641
    # Powheg+Herwig: topjetmassmean = 169.201451248, and error = 0.037004649564, and data-like error = 0.06511908013907937
    # aMC@NLO+Pythia: topjetmassmean = 169.003607345, and error = 0.0641885226976, and data-like error = 0.06285638362482038
    # Powheg+Pythia with varied hdamp parameter: topjetmassmean = 169.141936495, and error = 0.0343621419618, and data-like error = 0.060841742859861936

    # JSF
    JSF = ROOT.RooRealVar("JSF","JSF",1.0,0.5,1.5)

    # Top quark mass
    mtop = ROOT.RooRealVar("mtop","mtop",172.5,150,200)

    # Fixed parameter in the equation for mean(top jet mass)
    amJ = ROOT.RooRealVar("amJ","amJ",168.825399)
    amJ.setConstant(True)

    # Fixed parameter relating JSF to mean(top jet mass)
    cJSF = ROOT.RooRealVar("cJSF","cJSF",78.169244)
    cJSF.setConstant(True)

    # Fixed parameter relating mtop to mean(top jet mass)
    bmtop = ROOT.RooRealVar("bmtop","bmtop",0.482998)
    bmtop.setConstant(True)

    # code how the mean top jet mass depends on JSF, i.e. mtopjetmean = A + C*(JSF-1) + B*(mtop - 172.5)
    meanFormula = ROOT.RooFormulaVar("meanFormula","meanFormula", "@0 + @1*(@2 - 1.0) + @3*(@4-172.5)",ROOT.RooArgList(amJ,cJSF,JSF,bmtop,mtop))

    # Assume are measurements of the mean top-jet mass (we will have only 1) are distributed according to a gaussian with mean related to JSF.
    # The sigma of the gaussian is fixed to the error on the mean
    pdf_topJetMass = ROOT.RooGaussian("pdf_topJetMass","pdf_topJetMass",meanTopJetMass, meanFormula, ROOT.RooRealConstant.value(errorOnMean))

    # Create data for mJ
    # ---------------------------------------------------------------------------------------------------------------

    # Create data - just one measurement of mean(top jet mass)
    toyData = ROOT.RooDataSet("d","d",ROOT.RooArgSet(meanTopJetMass))
    meanTopJetMass.setVal(nominalMean)
    toyData.add(ROOT.RooArgSet(meanTopJetMass))
    toyData.get(0).Print("V")

    # Set up model for reconstructed W boson mass, mW
    # ---------------------------------------------------------------------------------------------------------------

    useDataLikeErrors = True # Choose whether to use data-like errors (True) or not (False)

    # Declare observable mW
    mW = ROOT.RooRealVar("mW", "Reconstructed W boson mass", 55, 110)

    # Delcare constants in relation mean1 = A_1 + B_1*JSF
    A_1 = ROOT.RooRealVar("A_1","A_1",11.606800)
    B_1 = ROOT.RooRealVar("B_1","B_1",71.632800)

    # A_1
    # 13.4973 for data-like errors full sim relationship
    # 13.4773 for non-data-like errors full sim relationship
    # 11.8299 for non-data-like errors fast sim relationship
    # B_1
    # 70.3441 for data-like errors full sim relationship
    # 70.3641 for non-data-like errors full sim relationship
    # 71.4273 for non-data-like errors fast sim relationship

    # Delcare constants in relation mean2 = A_2 + B_2*JSF
    A_2 = ROOT.RooRealVar("A_2","A_2",30.812600)
    B_2 = ROOT.RooRealVar("B_2","B_2",49.960500)

    # A_2
    # 30.3396 for data-like errors fast sim
    # 30.2661 for non-data-like errors full sim
    # B_2
    # 50.7292 for data-like errors fast sim
    # 50.8007 for non-data-like errors full sim

    # Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma) and their parameters
    mean1 = ROOT.RooFormulaVar("mean1","mean of first gaussian","@0 + @1*@2", ROOT.RooArgList(A_1,B_1,JSF))
    mean2 = ROOT.RooFormulaVar("mean2","mean of second gaussian","@0 + @1*@2", ROOT.RooArgList(A_2,B_2,JSF))
    sigma1 = ROOT.RooRealVar("sigma1", "width of first gaussian", 6.78358126554,0,50)
    sigma2 = ROOT.RooRealVar("sigma2", "width of second gaussian", 18.628864655,0,50)

    gauss1 = ROOT.RooGaussian("gauss1", "Gaussian 1", mW, mean1, sigma1)
    gauss2 = ROOT.RooGaussian("gauss2", "Gaussian 2", mW, mean2, sigma2)
    
    # Sum the signal components into a composite p.d.f.
    gauss1frac = ROOT.RooRealVar("gauss1frac", "fraction of gaussian 1 in combined fit", 0.712, 0, 1.0)
    combinedGauss = ROOT.RooAddPdf("total", "Combination of two Gaussians", ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(gauss1frac))
    
    # Import TH1D for mW
    # ----------------------------------------------------------------------------------------------------------------

    histogram_file_path = "../output_root_files/2022-05-16-CorrectedOutputROOTFiles/" + filename + "/mW/JSF_" + JSF_value + "_" + filename + ".root"
    histfile = ROOT.TFile.Open(histogram_file_path)
    hist = histfile.Get("nominal")
    hist.SetDirectory(0)
    histfile.Close()

    if useDataLikeErrors == True: # If using data-like errors
        for bin_number in range(hist.FindBin(55)-1,hist.FindBin(110)+1):
            hist.SetBinError(bin_number,np.sqrt(hist.GetBinContent(bin_number)))

    mWDataHist = ROOT.RooDataHist("data","mW distribution", ROOT.RooArgList(mW), hist, 1.0)

    # Perform negative log likelihood on both PDFs 
    # ---------------------------------------------------------------------------------------------------------------

    mW_NLL = combinedGauss.createNLL(mWDataHist)
    mJ_NLL = pdf_topJetMass.createNLL(toyData)
    Added_NLL = ROOT.RooAddition("Added_NLL","Combined NLLs", ROOT.RooArgList(mW_NLL,mJ_NLL))
    Minimise_NLL = ROOT.RooMinimizer(Added_NLL)
    Minimise_NLL.minimize("Minuit")
    fitResult = Minimise_NLL.save()
    fitResult.Print("v")
    print("\nInitial value of floating parameters")
    fitResult.floatParsInit().Print("s")
    print("\nFinal value of floating parameters")
    fitResult.floatParsFinal().Print("s")
    print("\nValue of constant parameters")
    fitResult.constPars().Print("s")

    # Save the fit result
    # ---------------------------------------------------------------------------------------------------------------

    mWframe = mW.frame()
    mWDataHist.plotOn(mWframe,ROOT.RooFit.MarkerSize(0.05))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("combinedGauss"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(1))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss1"),ROOT.RooFit.LineColor(4),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(2),ROOT.RooFit.Components("gauss1"))
    combinedGauss.plotOn(mWframe,ROOT.RooFit.Name("gauss2"),ROOT.RooFit.LineColor(8),ROOT.RooFit.LineWidth(2),ROOT.RooFit.LineStyle(3),ROOT.RooFit.Components("gauss2"))
    mWframe.GetYaxis().SetTitle("Entries")
    mWframe.GetXaxis().SetTitle("Reconstructed W boson mass [GeV]")
    canvas = ROOT.TCanvas("canvas")
    mWframe.Draw()

    legend = ROOT.TLegend(0.12,0.72,0.4,0.89)
    legend.AddEntry("gauss1", "Gaussian 1", "lp")
    legend.AddEntry("gauss2", "Gaussian 2", "lp")
    legend.AddEntry("combinedGauss", "Final fit", "lp")
    legend.SetLineWidth(0)
    legend.SetFillStyle(0)
    legend.Draw("same")

    canvas.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/mW_twoGaussFit_SimultaneousFittingJSFmtop_BothMeansSwappedWithJSF_" + filename + ".pdf")

# Main body of code
# -------------------------------------------------------------------------------------------

#filenames = ["ttpp8AF2","ttallpp8","ttph7","ttaMCatNLO","ttpp8Var"]
filenames = ["ttpp8MECOff"]
JSFs = ["1_00"]
meanTopJetMassValues = array('f',[168.319035])
meanTopJetMassErrors = array('f',[0.062612])

for i in range(0,len(filenames)):
    for j in range(0,len(JSFs)):
        RooFit_Minimisation(JSFs[j],filenames[i],meanTopJetMassValues[i],meanTopJetMassErrors[i])

print("\nScript has finished\n")
