# Macro that will overlay histograms and save them to PDFs
# First created by Elliot Watton - 11/10/2021
# Last editted by Elliot Watton - 11/01/2022

import ROOT as r
r.TH1.SetDefaultSumw2(True)
print('')

# Open the file containing previously made histograms and grab them
histfile = r.TFile.Open("../output_root_files/2022-05-16-CorrectedOutputROOTFiles/ttallpp8/All/JSF_1_00_ttallpp8.root")
hist_jetresponse = histfile.Get("nominal_topjet_Subjet_jetresponse")
hist_bjetresponse = histfile.Get("nominal_topjet_Subjet_bjetresponse")
hist_lightjetresponse = histfile.Get("nominal_topjet_Subjet_lightjetresponse")
hist_jetresponse.SetDirectory(0)
hist_bjetresponse.SetDirectory(0)
hist_lightjetresponse.SetDirectory(0)
histfile.Close()

hist_jetresponse.SetStats(0)
hist_bjetresponse.SetStats(0)
hist_lightjetresponse.SetStats(0)
hist_bjetresponse.SetFillColor(r.kRed-3)
hist_lightjetresponse.SetFillColor(r.kCyan-3)
hist_jetresponse.SetLineColor(r.kBlack)
hist_bjetresponse.SetLineColor(r.kRed-3)
hist_lightjetresponse.SetLineColor(r.kCyan-3)
hist_jetresponse.SetMarkerColor(r.kBlack)
hist_bjetresponse.SetMarkerColor(r.kRed-3)
hist_lightjetresponse.SetMarkerColor(r.kCyan-3)

h1 = hist_jetresponse.Clone()
h2 = hist_bjetresponse.Clone()
h3 = hist_lightjetresponse.Clone()

canvas_stacked = r.TCanvas("canvas_stacked")
canvas_stacked.SetGrid()



stack = r.THStack("stack","Stacked plot of jet repsonse contributions for Powheg+Pythia sample")

stack.Add(h2)
stack.Add(h3)

stack.Draw("hist")

h1.Draw("same")

legend = r.TLegend(0.11,0.78,0.47,0.88)
legend.AddEntry(h1, "b-tagged jet response")
legend.AddEntry(h2, "b-tagged jet response")
legend.AddEntry(h3, "Light quark jet response")
legend.SetLineWidth(0)
legend.SetFillStyle(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.02)
latex.DrawText(0.55,0.85,"Percentage of events that are light-quark jets = %.1f" %((h3.Integral()/h1.Integral())*100))
latex.DrawText(0.55,0.81,"Percentage of events that are b-tagged jets = %.1f" %((h2.Integral()/h1.Integral())*100))

stack.GetXaxis().SetTitle("Response")
stack.GetYaxis().SetTitle("Events")
""" h1.GetXaxis().SetTitleSize(0.11)
h1.GetXaxis().SetTitleOffset(0.8)
h1.GetYaxis().SetLabelFont(63)
h1.GetYaxis().SetLabelSize(10)
h1.GetYaxis().SetTitleOffset(0.38)
h1.GetYaxis().SetTitleSize(0.07) """

canvas_stacked.Draw()
canvas_stacked.SaveAs("../histogram_images/2022-05-16-CorrectedHistogramImages/Stacked_modelling_JetResponse_histograms_PP.pdf")

print('')
print('Stacked plot made!')
print('')
